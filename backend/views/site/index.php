<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use frontend\modules\cambiosdivisas\models\Estatus;
use backend\modules\cambiodivisas\models\Empresa;
use backend\modules\cambiodivisas\models\Operacionesusuarios;
$empresa=Empresa::find()->orderBy('id ASC')->one();

$session = Yii::$app->session;

$this->title = $empresa->nombre;
?>
<div class="site-indexnn">

    <div class="jumbotron">
        <h1><?= Html::encode($this->title) ?></h1>
        <p class="lead"><?= Html::decode($empresa->mensaje_back) ?></p>
    </div>

    <?php 
           // echo 'operacio'.$session['apertura']['idoperacionempresa'];

        if ($session['apertura']) {
            # code...
        //var_dump($Usuarioperfil);
       
        if ($Usuarioperfil->perfil->administrador!=0) { 
                if($dataProvider->getTotalCount()!=0){

    ?>

                    <div class=" col-md-12 col-sm-12 "  >
                        <?= Html::a('Asignar Solicitudes', ['/cambiodivisas/operacionesusuarios/create'], ['data-pjax' => 0, 'class' => 'btn btn-success pull-right', 'title' => 'Asignar Solicitudes']) ?>
                    
                    <br style="clear:both"/><br style="clear:both"/>
                    <?php

                    echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'summary'=> false,
                    'options'=>['class'=>'text-center table-cell'],
                    //'headerRowOptions'=>['class'=>'text-center table-cell'],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'idoperacion',
                        'codigo',
                        

                        //'tipo',
                        [
                            'attribute' => 'tipo',    
                            //'format' =>'amount',// ['decimal',3],
                            'contentOptions'=>['class'=>'text-center table-cell'],
                            'headerOptions'=>['class'=>'text-center table-cell'],
                            'filter'    => Select2::widget([
                                          'model' => $searchModel,
                                          'attribute' => 'tipo',
                                          'data' => ['CAMBIO'=>'CAMBIO','REMESA'=>'REMESA'],
                                          'options' => [
                                              'placeholder' => 'Opciones'
                                          ],
                                          'pluginOptions' => [
                                              'allowClear' => true
                                          ],
                                      ]),
                        ],
                        //'fecha_registro:datetime',
                        
                        [   'attribute' => 'fecha_registro',    
                            //'format' =>'datetime',// ['decimal',3], \Yii::$app->formatter->asDate(substr($detalleoper->fecha, 0, 10), 'short');
                             
                            'value' => function($data){
                        
                                return \Yii::$app->formatter->asDate(substr($data->fecha_registro, 0, 10), 'php:d-m-Y'); 
                            },
                            'contentOptions'=>['class'=>'text-center table-cell'],
                            'headerOptions'=>['class'=>'text-center table-cell'],
                            
                        ],

                        [   'attribute' => 'monto_cambio_usado',    
                            //'format' =>'amount',// ['decimal',3]
                            //'format' =>'decimal',
                            'contentOptions'=>['class'=>'text-right table-cell'],
                            'headerOptions'=>['class'=>'text-center table-cell'],
                            'value' => function($data){
                        
                            return number_format($data->monto_cambio_usado , 2, ',', '.'); 
                            },
                        ],

                        //'monto_envio',
                        [   'attribute' => 'monto_envio',    
                            //'format' =>'decimal',// ['decimal',3],
                            'contentOptions'=>['class'=>'text-right table-cell'],
                            'headerOptions'=>['class'=>'text-center table-cell'],
                            'value' => function($data){
                                    
                                    return $data->tipoMonedaEnvia->codigo.' '.number_format($data->monto_envio , 2, ',', '.'); 
                                },
                                'label'=>'Monto de Transacción',
                        ],

                        //'monto_recibe',
                        [   'attribute' => 'monto_recibe',    
                            //'format' =>'amount',// ['decimal',3],
                            'contentOptions'=>['class'=>'text-right table-cell'],
                            'headerOptions'=>['class'=>'text-center table-cell'],
                            'value' => function($data){
                                    
                                    return $data->tipoMonedaRecibe->codigo.' '.number_format($data->monto_recibe , 2, ',', '.'); 
                                },
                                'label'=>'Monto Transferido',
                        ],

                        [   'attribute' => 'agentes',   
                            'format' =>'raw', 
                            'value' => function($data, $model){
                                        
                                        $agente=Operacionesusuarios::find()->where(['id_operacion'=>$data->idoperacion, 'tipo_comision'=>'Agente'])->one(); 

                                        if ($agente) {
                                            return $agente->usuarioinfo;
                                        } else {
                                           return '-';
                                        }
                                        
                            }
                            //'format' =>'datetime',// ['decimal',3],
                            
                            
                        ],

                        //'estatus0.descripcion',
                        [
                            'attribute' => 'estatus',    
                            'format' =>'raw',// ['decimal',3],
                            'contentOptions'=>function($data){
                                    
                                    return ['class'=>'text-center table-cell', 'style'=>'background-color:'.$data->estatus0->color.'; color:#fff']; 
                                },
                            'headerOptions'=>['class'=>'text-center table-cell'],
                                
                            'value' => function($data, $model){
                                    
                                    $Operador=Operacionesusuarios::find()->where(['id_operacion'=>$data->idoperacion, 'tipo_comision'=>'Operador'])->one(); 

                                    if ($Operador) {
                                        return $data->estatus0->descripcion.
                                    '<br> <strong>Pagador:</strong> '.$Operador->usuarioinfo;
                                    } else {
                                       return $data->estatus0->descripcion;
                                    }
                                    
                                    //return $data->estatus0->descripcion.
                                    //'<br>'.$Operador->usuarioinfo;
                                },
                            'filter'    => Select2::widget([
                                          'model' => $searchModel,
                                          'attribute' => 'estatus',
                                          'data' => ArrayHelper::map(Estatus::find()->where(['tabla'=>'Operaciones'])->orderBy('descripcion ASC')->all(), 'idstatus','descripcion'),
                                          'options' => [
                                              'placeholder' => 'Opciones'
                                          ],
                                          'pluginOptions' => [
                                              'allowClear' => true
                                          ],
                                      ]),
                        ],

                        ['class' => 'yii\grid\ActionColumn',

                            //'header' => 'Acciones',
                            'headerOptions' => ['style' => 'color:#337ab7'],
                            'template' => " {verifica} {procesa} {view} ",
                            'buttons' => [
                            'verifica' => function ($url, $model) {
                                return Html::a('<span class="text-warning glyphicon glyphicon-eye-open"></span>', $url, [
                                            'title' => Yii::t('app', 'Verificar'),
                                ]);
                            },
             
                            'procesa' => function ($url, $model) {
                                return Html::a('<span class="text-success glyphicon glyphicon-ok"></span>', $url, [
                                            'title' => Yii::t('app', 'Procesar'),
                                ]);
                            },

                            'view' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-plus"></span>', $url, [
                                            'title' => Yii::t('app', 'Ver'),
                                ]);
                            },
                            
             
                            ],

                            'urlCreator' => function ($action, $model, $key, $index) {
                            /*if ($action === 'update') {
                                $url ='index.php?r=cambiosdivisas/operaciones/view&id='.$model->idoperacion;
                                return $url;
                            }*/
                            if ($action === 'verifica') {
                                $url =['/cambiodivisas/operaciones/verifica?id='.$model->idoperacion];
                                return $url;
                            }
                            if ($action === 'procesa') {
                                $url =['/cambiodivisas/operaciones/procesa?id='.$model->idoperacion];
                                return $url;
                            }
                            if ($action === 'view') {
                                $url =['/cambiodivisas/operaciones/detalle?id='.$model->idoperacion];
                                return $url;
                            }
                              
             
                            }
                        ],
                    ],
                ]);

                }
            }

            if ($Usuarioperfil->perfil->operador!=0 && $Usuarioperfil->perfil->administrador==0) {

                

                if($dataProvider->getTotalCount()!=0){

                    echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'summary'=> false,
                    'options'=>['class'=>'text-center table-cell'],
                    //'headerRowOptions'=>['class'=>'text-center table-cell'],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'idoperacion',
                        'codigo',
                        //'tipo',
                        [
                            'attribute' => 'tipo',    
                            //'format' =>'amount',// ['decimal',3],
                           
                                
                            'contentOptions'=>['class'=>'text-center table-cell'],
                            'headerOptions'=>['class'=>'text-center table-cell'],
                            'filter'    => Select2::widget([
                                          'model' => $searchModel,
                                          'attribute' => 'tipo',
                                          'data' => ['CAMBIO'=>'CAMBIO','REMESA'=>'REMESA'],
                                          'options' => [
                                              'placeholder' => 'Opciones'
                                          ],
                                          'pluginOptions' => [
                                              'allowClear' => true
                                          ],
                                      ]),
                        ],
                        //'fecha_registro:datetime',
                        
                        [   'attribute' => 'fecha_registro',    
                            //'format' =>'datetime',// ['decimal',3],
                            'value' => function($data){
                        
                                return \Yii::$app->formatter->asDate(substr($data->fecha_registro, 0, 10), 'php:d-m-Y'); 
                            },
                            'contentOptions'=>['class'=>'text-center table-cell'],
                            'headerOptions'=>['class'=>'text-center table-cell'],
                            
                        ],


                        [   'attribute' => 'monto_cambio_usado',
                            'format' =>'decimal',    
                            //'format' =>'amount',// ['decimal',3],
                            'contentOptions'=>['class'=>'text-right table-cell'],
                            'headerOptions'=>['class'=>'text-center table-cell'],
                            
                        ],


                        //'monto_envio',
                        [   'attribute' => 'monto_envio',    
                            //'format' =>'amount',// ['decimal',3],
                            'contentOptions'=>['class'=>'text-right table-cell'],
                            'headerOptions'=>['class'=>'text-center table-cell'],
                            'value' => function($data){
                                    
                                    return $data->tipoMonedaEnvia->codigo.' '.number_format($data->monto_envio , 2, ',', '.'); 
                                },
                                'label'=>'Monto de Transacción',
                            
                        ],

                        //'monto_recibe',
                        [   'attribute' => 'monto_recibe',    
                            //'format' =>'amount',// ['decimal',3],
                            'contentOptions'=>['class'=>'text-right table-cell'],
                            'headerOptions'=>['class'=>'text-center table-cell'],
                            'value' => function($data){
                                    
                                    return $data->tipoMonedaRecibe->codigo.' '.number_format($data->monto_recibe , 2, ',', '.'); 
                                },
                                'label'=>'Monto Transferido',
                            
                        ],

                        //'estatus0.descripcion',
                        [
                            'attribute' => 'estatus',    
                            //'format' =>'amount',// ['decimal',3],
                            'contentOptions'=>function($data){
                                    
                                    return ['class'=>'text-center table-cell', 'style'=>'background-color:'.$data->estatus0->color.'; color:#fff']; 
                                },
                            'headerOptions'=>['class'=>'text-center table-cell'],
                                
                            'value' => function($data){
                                    
                                    return $data->estatus0->descripcion; 

                                },
                            'filter'    => Select2::widget([
                                          'model' => $searchModel,
                                          'attribute' => 'estatus',
                                          'data' => ArrayHelper::map(Estatus::find()->where(['tabla'=>'Operaciones'])->orderBy('descripcion ASC')->all(), 'idstatus','descripcion'),
                                          'options' => [
                                              'placeholder' => 'Opciones'
                                          ],
                                          'pluginOptions' => [
                                              'allowClear' => true
                                          ],
                                      ]),
                        ],

                        ['class' => 'yii\grid\ActionColumn',

                            //'header' => 'Acciones',
                            'headerOptions' => ['style' => 'color:#337ab7'],
                            'template' => " {procesa} {view}",
                            'buttons' => [
                            'procesa' => function ($url, $model) {
                                return Html::a('<span class="text-success glyphicon glyphicon-ok"></span>', $url, [
                                            'title' => Yii::t('app', 'Procesar'),
                                ]);
                            },

                            'view' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                            'title' => Yii::t('app', 'Ver'),
                                ]);
                            },
                            
                            
             
                            ],

                            'urlCreator' => function ($action, $model, $key, $index) {
                            /*if ($action === 'update') {
                                $url ='index.php?r=cambiosdivisas/operaciones/view&id='.$model->idoperacion;
                                return $url;
                            }*/
                            if ($action === 'procesa') {
                                $url =['/cambiodivisas/operaciones/procesa?id='.$model->idoperacion];
                                return $url;
                            }
                            if ($action === 'view') {
                                $url =['/cambiodivisas/operaciones/detalle?id='.$model->idoperacion];
                                return $url;
                            }
                              
             
                            }
                        ],
                    ],
                ]);

                }
            }

            ?>

            
                 


           <?php 
        } else { ?>
            
                <div class="col-sm-6">
                  <div class="list-group list-group-item active">
                    <!-- <a href="../cambiodivisas/operacionempresa/create" class="list-group-item active"> -->
                      <h3 class="list-group-item-heading">Apertura Diaria</h3>
                      <p class="list-group-item-text">Para poder comenzar las operaciones de cambiarias es necesario que el administrador del sistema realice la apertura de operaciones.</p>
                      <br>
                      <!-- <button type="button" class="btn btn-default">Apertura de Operaciones</button> -->
                    <!-- </a> -->
                    
                  </div>
                </div>
        <?php }
        
        ?>
            </div>
    
</div>

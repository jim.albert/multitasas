<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

use kartik\sidenav\SideNav;

use yii\helpers\Url;

use backend\modules\cambiodivisas\models\Tipodecambio;
use backend\modules\cambiodivisas\models\Empresa;
use backend\modules\cambiodivisas\models\Operacionempresa;
use backend\modules\cambiodivisas\models\Operacionsaldocuentasbancarias;
use backend\modules\cambiodivisas\models\Cuentabancariasistemasaldos;

AppAsset::register($this);

$session = Yii::$app->session;


$tipocambio=Tipodecambio::find()->where(['estatus' => '9', 'operacion'=>'CAMBIO'])->orderBy('codigo ASC')->one();

$tiporemesa=Tipodecambio::find()->where(['estatus' => '9', 'operacion'=>'REMESA'])->orderBy('codigo ASC')->one();

$empresa=Empresa::find()->orderBy('id ASC')->one();

$apertura=Operacionempresa::find()->where(['estatus' => '1'])
->andFilterWhere(['like', 'fecha_apertura',date('Y-m-d') ])->one();

$session['apertura'] = $apertura; 

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    
<div class=" col-md-12 col-sm-12 " >


            <?php
                NavBar::begin([
                    'brandLabel' => Html::a(Html::img('@web/img/'.$empresa->logo, ['alt' => 'MultiTasas.com','title' => 'MultiTasas.com', 'width'=>'20%' ]),['/site/index']),
                    'brandUrl' => Yii::$app->homeUrl,
                    'options' => [
                        'class' => 'navbar-inverse navbar-fixed-top',
                    ],
                ]);
                
                if (Yii::$app->user->isGuest) {
                    // $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
                    
                    $menuItems[] = ['label' => '<span class="glyphicon glyphicon-lock"> </span> Iniciar Sesión', 'url' => ['/user/security/login']];
                } else {

                    $menuItems[] = ['label' => '<span class="glyphicon glyphicon-lock"> </span> Tickets de Soporte', 'url' => ['/support/ticket/manage']];


                    $menuItems[] = ['label' => '<span class="glyphicon glyphicon-user"> </span> Perfil de Usuario', 'url' => ['#'],
                        'items' => [
                            
                            
                            ['label' => 'Perfil', 'url' => ['/cambiodivisas/usuario/perfil']],
                            ['label' => 'Cuenta', 'url' => ['/user/settings/account']],
                            

                        ],

                            //['label' => '<span class="glyphicon glyphicon-user"> </span> Perfil de Usuario', 'url' => ['/cambiodivisas/usuario/perfil']


                ];

                
                   
                    

                    $menuItems[] = '<li>'
                        . Html::beginForm(['/user/security/logout'], 'post')
                        . Html::submitButton(
                            '<span class="glyphicon glyphicon-lock"> </span> Cerrar Sesión (' . Yii::$app->user->identity->username . ')',
                            ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm()
                        . '</li>';
                }
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-right'],
                    'encodeLabels' => false,
                    'items' => $menuItems,
                ]);
                NavBar::end();

            echo "</br></br>";
                
                ?>


     <!--   </div>
     </div> -->
</div>

<?php if (!Yii::$app->user->isGuest) { ?>


<div class="left side-menu col-md-3 col-sm-3" > 

    <div class="container">

            <br style="clear:both"/>
        <?php
        echo "</br></br>";
            echo SideNav::widget([
            'type' => SideNav::TYPE_DEFAULT,
            'heading' => 'Menú Princial',
            'items' => [
                [
                    'url' => ['/site/index'],
                    'label' => 'Inicio',
                    'icon' => 'home'
                ],
                [
                    'label' => 'Empresa',
                    'icon' => 'record',
                    'items' => [
                        ['label' => 'Configuración', 'icon'=>'', 'url'=>['/cambiodivisas/empresa/create']],
                        ['label' => 'Cuentas Bancarias del Sistema', 'icon'=>'', 'url'=>['/cambiodivisas/cuentasbancariadesistema']],
                        ['label' => 'Perfiles de Acceso', 'icon'=>'', 'url'=>['/cambiodivisas/perfil']],

                        //['label' => 'Contacto', 'icon'=>'phone', 'url'=>'#'],
                    ],
                ],
                
                [

                    'label' => 'Configuración',
                    'icon' => 'cog',
                    'items' => [
                        ['label' => 'Banco', 'icon'=>'', 'url'=>['/cambiodivisas/bancos']],
                        
                        ['label' => 'Estatus', 'icon'=>'', 'url'=>['/cambiodivisas/estatus']],
                        ['label' => 'País', 'icon'=>'', 'url'=>['/cambiodivisas/paises']],
                        ['label' => 'Tipo de Cambio', 'icon'=>'', 'url'=>['/cambiodivisas/tipodecambio']],
                        ['label' => 'Tipo de Cuenta', 'icon'=>'', 'url'=>['/cambiodivisas/tipodecuenta']],
                        ['label' => 'Tipo de Documentos', 'icon'=>'', 'url'=>['/cambiodivisas/tipodedocumento']],
                        ['label' => 'Tipo de Monedas', 'icon'=>'', 'url'=>['/cambiodivisas/tipodemoneda']],
                        ['label' => 'Validaciones', 'icon'=>'', 'url'=>['/cambiodivisas/validaciones']],
                        
                    ],
                ],
                [
                    'label' => 'Operaciones',
                    'icon' => 'tasks',
                    'items' => [
                        ['label' => 'Cambio de divisas', 'icon'=>'', 'url'=>['/cambiodivisas/operaciones']],
                        //['label' => 'Contacto', 'icon'=>'phone', 'url'=>'#'],
                    ],
                ],
                [

                    'label' => 'Usuarios',
                    'icon' => 'user',
                    'items' => [
                        ['label' => 'Cuentas Bancarias', 'icon'=>'', 'url'=>'#'],
                        ['label' => 'Mantenimiento de Usuarios', 'icon'=>'', 'url'=>['/cambiodivisas/usuario/']],
                        ['label' => 'Asiganción de Perfil a Usuarios', 'icon'=>'', 'url'=>['/cambiodivisas/usuarioperfil/']],

                    ],
                ],
                [

                    'label' => 'Ayuda',
                    'icon' => 'question-sign',
                    'items' => [
                        ['label' => 'Manual de usuario', 'icon'=>'info-sign', 'url'=>'#'],
                        
                    ],
                ],
            ],
        ])
            ?>

            


<h3 class="bg bg-information text-center" style="border-radius: .2rem; margin-bottom: 0px; padding-bottom: 5px; padding-top: 5px; font-weight: 900"> <i class="glyphicon glyphicon-calendar"></i> Apertura Diarias</h3>
         
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class=" panel-title " style="border-radius: .2rem; margin-bottom: 0px; padding-bottom: 5px; padding-top: 5px; "><b> Fecha:</b> <?= date("d/m/Y")  ?> 


                <?php if (!$apertura) {
                        
                        echo Html::a('<i class="glyphicon glyphicon-plus pull-right"></i>', ['/cambiodivisas/operacionempresa/create'], [ 'class' => ' btn-default', 'title' => 'Aperturar Operaciones']);
                        echo '</h4> </div> ';
                }else{

                        echo Html::a('<i class="glyphicon glyphicon-minus pull-right"></i>', ['/cambiodivisas/operacionempresa/cierre?id='.$apertura->idoperacionempresa], [ 'class' => ' btn-default', 'title' => 'Cerrar Operaciones']);

                        echo '</h4> </div> ';

                        $saldoapert=Operacionsaldocuentasbancarias::find()->where(['id_operacion_empresa' => $apertura->idoperacionempresa])->orderBy('id_cuentabancaria_sistema ASC')->all();
                        

                        echo '<div > <div class=" col-md-6 col-sm-6 bg bg-info text-info  ">';

                        echo '<center><b>Apertura </b></center>';

                        foreach($saldoapert as $key => $value) {


                            echo '<br><span>'.$value->cuentabancariasistema->alias.': </span> <span>'.  number_format($value->saldo_apertura , 2, ',', '.').'</span>';
                        }
                        echo '</div></div>';

                        echo '<div > <div class=" col-md-6 col-sm-6 bg bg-primary text-primary  ">';

                        echo '<center><b>Disponible </b></center>';



                        foreach($saldoapert as $key => $value) {

                            $acumulado=Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['like', 'fecha',date('Y-m-d') ])->sum('monto_operacion');
                            
                                $saldo=$value->saldo_apertura-$acumulado;
                                if ($saldo<=0) {
                                    echo ' <br><span >'.$value->cuentabancariasistema->alias.': </span> <span class="bg bg-danger text-danger  ">'. number_format($saldo , 2, ',', '.')  .'</span>';
                                } else {
                                    echo ' <br><span>'.$value->cuentabancariasistema->alias.': </span> <span>'. number_format($saldo , 2, ',', '.')  .'</span>';
                                }
                                

                            
                        }
                        echo '</div></div>';


                } ?>
                
           
        </div>
</div>
        <br style="clear:both"/><br style="clear:both"/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class=" panel-title " style="border-radius: .2rem; margin-bottom: 0px; padding-bottom: 5px; padding-top: 5px; ">Tipo de Cambio <?=Html::a('<i class="glyphicon glyphicon-plus pull-right"></i>', ['/cambiodivisas/tipodecambio/create'], [ 'class' => ' btn-default', 'title' => 'Nuevo Tipo de Cambio']) //. ' '.
                    
            //Html::a('<i class="glyphicon glyphicon-edit"></i>', ['/cambiodivisas/tipodecambio/update?id='.$tipocambio->idcambio], ['data-pjax' => 0, 'class' => 'btn-default', 'title' => 'Actualizar Tipo de Cambio'])?>
                </h4>
            </div>

            <div class=" text-center ">
                <div class=" col-md-6 col-sm-6 bg bg-info text-info text-center ">
                    <h4>Compra</h4>
                    <p><?php if ($tipocambio->monto) {  echo number_format($tipocambio->monto , 3, ',', '.') }?></p>
                </div>
                
            
                <div class="col-md-6 col-sm-6 bg bg-primary text-primary text-center ">
                    <h4>Venta</h4>
                    <p><?= number_format($tipocambio->venta , 3, ',', '.') ?></p>
                </div>
            
             </div>
        </div>

        <br style="clear:both"/><br style="clear:both"/>


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class=" panel-title " style="border-radius: .2rem; margin-bottom: 0px; padding-bottom: 5px; padding-top: 5px; ">Tipo de Remesa <?=Html::a('<i class="glyphicon glyphicon-plus pull-right"></i>', ['/cambiodivisas/tipodecambio/createtasa'], [ 'class' => ' btn-default', 'title' => 'Nuevo Tipo de Remesa']) //. ' '.
                    
            //Html::a('<i class="glyphicon glyphicon-edit"></i>', ['/cambiodivisas/tipodecambio/updatetasa?id='.$tiporemesa->idcambio], ['data-pjax' => 0, 'class' => 'btn-default', 'title' => 'Actualizar Tipo de Cambio'])?>
                </h3>
            </div>

            <div class=" text-center ">
                <div class=" col-md-6 col-sm-6 bg bg-primary text-primary text-center ">
                    <h4>Soles -S/</h4>
                    <p><?= number_format($tiporemesa->venta , 3, ',', '.') ?></p>
                </div>
                
            
                <div class="col-md-6 col-sm-6 bg bg-info text-info text-center ">
                    <h4>Dólares -$</h4>
                    <p><?= number_format($tiporemesa->monto , 3, ',', '.') ?></p>
                </div>
            
            </div>
        </div>

<br style="clear:both"/><br style="clear:both"/>
        </div>
    </div>



    <div class="col-md-9 col-sm-9" style="padding-left: 0px; padding-right: 0px;" >

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div> 

<?php }else{

    Yii::$app->urlManager->createUrl(['/user/security/login']);

    ?>

    <div class="wrap col-md-12 col-sm-12"  >
         <br style="clear:both"/><br style="clear:both"/>
        <div class="container">
            
            
            <?= $content ?>
        </div>
    </div>
    
<?php } ?>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">Copyright &copy; <?= date('Y') ?>. All Rights Reserved.  </p>

        <p class="pull-right">Powered by <?= Html::a(Html::decode(Yii::$app->name, ['alt' => 'MultiTasas.com','title' => 'MultiTasas.com', 'width'=>'50%' ]),'https://multitasas.com/') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

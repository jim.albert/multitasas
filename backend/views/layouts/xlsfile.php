<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

use kartik\sidenav\SideNav;

use yii\helpers\Url;

use backend\modules\cambiodivisas\models\Tipodecambio;
use backend\modules\cambiodivisas\models\Empresa;
use backend\modules\cambiodivisas\models\Operacionempresa;
use backend\modules\cambiodivisas\models\Operacionsaldocuentasbancarias;
use backend\modules\cambiodivisas\models\Cuentabancariasistemasaldos;
use backend\modules\cambiodivisas\models\Usuarioperfil;

//date_default_timezone_set('America/Lima');

AppAsset::register($this);

$session = Yii::$app->session;


$tipocambio=Tipodecambio::find()->where(['estatus' => '9', 'operacion'=>'CAMBIO'])->orderBy('codigo ASC')->one();

$tiporemesa=Tipodecambio::find()->where(['estatus' => '9', 'operacion'=>'REMESA'])->orderBy('codigo ASC')->one();

$empresa=Empresa::find()->orderBy('id ASC')->one();

$apertura=Operacionempresa::find()->where(['estatus' => '1'])
//->andFilterWhere(['like', 'fecha_apertura',date('Y-m-d') ])
->one();

$session['apertura'] = $apertura; 

if (!Yii::$app->user->isGuest) { 

$Usuarioperfil=Usuarioperfil::find()->where(['id_usuario'=>Yii::$app->user->identity->getId()])->one();
}
// header('Content-type: application/vnd.ms-excel');
// //header('Content-Language: en');
// header('Content-Disposition: attachment; filename=file.xls');
//header("Pragma: no-cache");
//header("Expires: 0");
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    
       <div class="container">
             <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>


</div>

<footer class="footer">
    <div class="container">
        
        <p class="pull-right">Powered by <?= Html::a(Html::decode(Yii::$app->name, ['alt' => 'MultiTasas.com','title' => 'MultiTasas.com', 'width'=>'50%' ]),'https://multitasas.com/') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

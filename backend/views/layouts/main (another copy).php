<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

use kartik\sidenav\SideNav;

use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    
<div class=" col-md-12 col-sm-12 header" >

    <!-- <div class="left side-menu col-md-3 col-sm-4" >
    
       <div align="center" >

            <?php //echo Html::img('@web/img/logo_color.png', ['alt' => 'MultiTasas.com','title' => 'MultiTasas.com', 'width'=>'40%' ])?>
           

            <?php //echo Html::a(Html::img('@web/img/logo.png', ['alt' => 'MultiTasas.com','title' => 'MultiTasas.com', 'width'=>'30%' ]),['/site/index']) ?>

        </div>
    </div>

    <div class="col-md-9 col-sm-8" > 
    
       <div align="right txt-wite" >-->

            <?php
                NavBar::begin([
                    'brandLabel' => Html::a(Html::img('@web/img/logo.png', ['alt' => 'MultiTasas.com','title' => 'MultiTasas.com', 'width'=>'30%' ]),['/site/index']),
                    'brandUrl' => Yii::$app->homeUrl,
                    'options' => [
                        'class' => 'navbar-inverse navbar-fixed-top',
                    ],
                ]);
                /*$menuItems = [
                    ['label' => 'Home', 'url' => ['/site/index']],
                    
                    
                ];*/
                if (Yii::$app->user->isGuest) {
                    // $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
                    $menuItems[] = ['label' => '', 'url' => '#'];
                } else {
                    $menuItems[] = ['label' => '<span class="glyphicon glyphicon-user"> </span> Perfil de Usuario', 'url' => '#', 'icon'=>'user'];

                    /*$menuItems = [
                    ['label' => 'Operaciones', 'url' => ['/site/index']],
                    ['label' => 'Configuración', 'url' => ['/site/index']],
                    ['label' => 'Usuarios', 'url' => ['/site/index']],
                    
                    
                ];*/

                    

                    $menuItems[] = '<li>'
                        . Html::beginForm(['/user/security/logout'], 'post')
                        . Html::submitButton(
                            'Cerrar Sesión (' . Yii::$app->user->identity->username . ')',
                            ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm()
                        . '</li>';
                }
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-right'],
                    'encodeLabels' => false,
                    'items' => $menuItems,
                ]);
                NavBar::end();

            echo "</br></br>";
                
                ?>


     <!--   </div>
     </div> -->
</div>

<?php if (!Yii::$app->user->isGuest) { ?>

<div class="left side-menu col-md-3 col-sm-4" >

    

    <br style="clear:both"/>
<?php
echo "</br></br>";
    echo SideNav::widget([
    'type' => SideNav::TYPE_DEFAULT,
    'heading' => 'Menú Princial',
    'items' => [
        [
            'url' => '#',
            'label' => 'Inicio',
            'icon' => 'home'
        ],
        [
            'label' => 'Operaciones',
            'icon' => 'tasks',
            'items' => [
                ['label' => 'Cambio de divisas', 'icon'=>'', 'url'=>'#'],
                //['label' => 'Contacto', 'icon'=>'phone', 'url'=>'#'],
            ],
        ],
        [

            'label' => 'Configuración',
            'icon' => 'cog',
            'items' => [
                ['label' => 'Banco', 'icon'=>'', 'url'=>['/cambiodivisas/bancos']],
                ['label' => 'Cuentas Bancarias del Sistema', 'icon'=>'', 'url'=>['/cambiodivisas/cuentasbancariadesistema']],
                ['label' => 'Estatus', 'icon'=>'', 'url'=>['/cambiodivisas/estatus']],
                ['label' => 'Tipo de Cambio', 'icon'=>'', 'url'=>['/cambiodivisas/tipodecambio']],
                ['label' => 'Tipo de Cuenta', 'icon'=>'', 'url'=>['/cambiodivisas/tipodecuenta']],
                ['label' => 'Tipo de Documentos', 'icon'=>'', 'url'=>['/cambiodivisas/tipodedocumento']],
                ['label' => 'Tipo de Monedas', 'icon'=>'', 'url'=>['/cambiodivisas/tipodemoneda']],
                ['label' => 'Validaciones', 'icon'=>'', 'url'=>['/cambiodivisas/validaciones']],
                
            ],
        ],
        [

            'label' => 'Usuarios',
            'icon' => 'user',
            'items' => [
                ['label' => 'Cuentas Bancarias', 'icon'=>'', 'url'=>'#'],
                ['label' => 'Mantenimiento de Usuarios', 'icon'=>'', 'url'=>'#'],
            ],
        ],
        [

            'label' => 'Ayuda',
            'icon' => 'question-sign',
            'items' => [
                ['label' => 'Manual de usuario', 'icon'=>'info-sign', 'url'=>'#'],
                
            ],
        ],
    ],
])
    ?>
</div>



    <div class="col-md-9 col-sm-8" style="padding-left: 0px; padding-right: 0px;" >

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>
</div>
<?php }else{

    Yii::$app->urlManager->createUrl(['/user/security/login']);

    ?>

    <div class="wrap col-md-12 col-sm-12"  >
         <br style="clear:both"/><br style="clear:both"/>
        <div class="container">
            
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>
    <br style="clear:both"/><br style="clear:both"/><br style="clear:both"/>
<?php } ?>

<br style="clear:both"/>
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right">Una plataforma MultiExchange.</p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

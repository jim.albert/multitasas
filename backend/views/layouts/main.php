<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

use kartik\sidenav\SideNav;

use yii\helpers\Url;

use backend\modules\cambiodivisas\models\Tipodecambio;
use backend\modules\cambiodivisas\models\Empresa;
use backend\modules\cambiodivisas\models\Operaciones;
use backend\modules\cambiodivisas\models\Operacionempresa;
use backend\modules\cambiodivisas\models\Operacionsaldocuentasbancarias;
use backend\modules\cambiodivisas\models\Cuentabancariasistemasaldos;
use backend\modules\cambiodivisas\models\Usuarioperfil;
use akiraz2\support\models\Ticket;

use kartik\widgets\SwitchInput;


//date_default_timezone_set('America/Lima');

AppAsset::register($this);

$session = Yii::$app->session;


$tipocambio=Tipodecambio::find()->where(['estatus' => '9', 'operacion'=>'CAMBIO'])->orderBy('codigo ASC')->one();

//$tiporemesa=Tipodecambio::find()->where(['estatus' => '9', 'operacion'=>'REMESA'])->orderBy('codigo ASC')->one();

$tiporemesa=Tipodecambio::find()->joinWith('monedacompra')->where(['tipo_cambio.estatus' => '9', 'operacion'=>'REMESA'])->andwhere(['tipo_moneda.remesas' => '1'])->all();

$empresa=Empresa::find()->orderBy('id ASC')->one();

$apertura=Operacionempresa::find()->where(['estatus' => '1'])
//->andFilterWhere(['like', 'fecha_apertura',date('Y-m-d') ])
->one();

$session['apertura'] = $apertura; 

if (!Yii::$app->user->isGuest) { 

$Usuarioperfil=Usuarioperfil::find()->where(['id_usuario'=>Yii::$app->user->identity->getId()])->one();
}
$totalopentickets= Ticket::find()->where(['status'=>0])->count();

if ($totalopentickets>=1) {
   $notificacion_tic='<span style="position: absolute;margin-top:-10px" class="label label-danger" id="suportnotifi">'.$totalopentickets.'</span>';
} else {
    $notificacion_tic='';
}

// $jsc = <<< JS


//     function nrotickets() {
        
//         $.post("index.php?r=/support/ticket/count", 

//             function(data){
//                 console.log(data);
               
                
//             });        
//     }
// nrotickets();
// JS;

// $this->registerJs($jsc, $this::POS_END);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    
<div class=" col-md-12 col-sm-12 " >


            <?php
                NavBar::begin([
                    'brandLabel' => Html::a(Html::img('@web/img/'.$empresa->logo, ['alt' => 'MultiTasas.com','title' => 'MultiTasas.com', 'width'=>'70px' ]),['/site/index']),
                    'brandUrl' => Yii::$app->homeUrl,
                    'options' => [
                        'class' => 'navbar-inverse navbar-fixed-top',
                    ],
                ]);
                
                
                

                if (Yii::$app->user->isGuest) {
                    // $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];


                    
                    $menuItems[] = ['label' => '<span class="glyphicon glyphicon-lock"> </span> Iniciar Sesión', 'url' => ['/user/security/login']];
                } else {

                    if ($Usuarioperfil->perfil->administrador!=0) {

                        $menuItems[] = ['label' => '<span class="glyphicon glyphicon-record"> </span> Empresa', 'url' => ['#'],
                            'items' => [
                                
                                ['label' => 'Configuración', 'icon'=>'', 'url'=>['/cambiodivisas/empresa/create']],
                                ['label' => 'Cuentas Bancarias del Sistema', 'icon'=>'', 'url'=>['/cambiodivisas/cuentasbancariadesistema']],
                                ['label' => 'Saldos Cuentas Bancarias del Sistema', 'icon'=>'', 'url'=>['/cambiodivisas/operacionsaldocuentasbancarias']],
                                ['label' => 'Perfiles de Acceso', 'icon'=>'', 'url'=>['/cambiodivisas/perfil']],

                            ],
                        ];


                        $menuItems[] = ['label' => '<span class="glyphicon glyphicon-cog"> </span> Configuración', 'url' => ['#'],
                            'items' => [
                                
                                ['label' => 'Banco', 'icon'=>'', 'url'=>['/cambiodivisas/bancos']],
                                ['label' => 'Estatus', 'icon'=>'', 'url'=>['/cambiodivisas/estatus']],
                                ['label' => 'País', 'icon'=>'', 'url'=>['/cambiodivisas/paises']],
                                ['label' => 'Tipo de Cambio', 'icon'=>'', 'url'=>['/cambiodivisas/tipodecambio']],
                                ['label' => 'Tipo de Cuenta', 'icon'=>'', 'url'=>['/cambiodivisas/tipodecuenta']],
                                ['label' => 'Tipo de Documentos', 'icon'=>'', 'url'=>['/cambiodivisas/tipodedocumento']],
                                ['label' => 'Tipo de Monedas', 'icon'=>'', 'url'=>['/cambiodivisas/tipodemoneda']],
                                ['label' => 'Validaciones', 'icon'=>'', 'url'=>['/cambiodivisas/validaciones']],
                                

                            ],
                        ];

                        $menuItems[] = ['label' => '<span class="glyphicon glyphicon-tasks"> </span> Operaciones', 'url' => ['#'],
                            'items' => [
                                
                                ['label' => 'Envío de Dinero', 'icon'=>'', 'url'=>['/cambiodivisas/operaciones']]
                                

                            ],
                        ];

                        $menuItems[] = ['label' => '<span class="glyphicon glyphicon-stats"> </span> Reportes', 'url' => ['#'],
                            'items' => [
                                
                                ['label' => 'Resumen de Operaciones Diarias', 'icon'=>'', 'url'=>['/cambiodivisas/operacionempresa/index']],
                                ['label' => 'Envío de Dinero', 'icon'=>'', 'url'=>['/cambiodivisas/operacionempresa/printreportgeneral']],
                                ['label' => 'Comisiones', 'icon'=>'', 'url'=>'#']
                                

                            ],
                        ];

                        $menuItems[] = ['label' => '<span class="glyphicon glyphicon-user"> </span> Usuarios', 'url' => ['#'],
                            'items' => [
                                
                                ['label' => 'Cuentas Bancarias', 'icon'=>'', 'url'=>'#'],
                                ['label' => 'Usuarios Registrados', 'icon'=>'', 'url'=>['/cambiodivisas/usuario/']],
                                ['label' => 'Asignación de Perfil a Usuarios', 'icon'=>'', 'url'=>['/cambiodivisas/usuarioperfil/']],

                            ],
                        ];
                    }
                    $menuItems[] = ['label' => '<span class="glyphicon glyphicon-lock"> </span> Tickets de Soporte '. $notificacion_tic, 'url' => ['/support/ticket/manage']];


                    $menuItems[] = ['label' => '<span class="glyphicon glyphicon-log-out"> </span> Perfil de Usuario', 'url' => ['#'],
                        'items' => [
                            
                            
                            ['label' => 'Perfil', 'url' => ['/cambiodivisas/usuario/perfil']],
                            ['label' => 'Cuenta', 'url' => ['/user/settings/account']],
                            ['label'=> '<li>'
                        . Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                            '<span class="glyphicon glyphicon-lock"> </span> Cerrar Sesión (' . Yii::$app->user->identity->username . ')',
                            ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm()
                        . '</li>',]

                        ],
                    ];

         
                }
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-right'],
                    'encodeLabels' => false,
                    'items' => $menuItems,
                ]);
                NavBar::end();

            echo "</br></br>";
                
                ?>


     <!--   </div>
     </div> -->
</div>

<?php if (!Yii::$app->user->isGuest) {     ?>


<div class="columnmovil left side-menu col-md-3 col-sm-3 " > 

    <div class="container">

            <br style="clear:both"/>


<?php
            if ($Usuarioperfil->perfil->operador!=0 && $Usuarioperfil->perfil->administrador==0) {
                echo "</br></br>";

                    echo SideNav::widget([
                    'type' => SideNav::TYPE_DEFAULT,
                    'heading' => 'Menú Princial',
                    'items' => [
                        [
                            'url' => ['/site/index'],
                            'label' => 'Inicio',
                            'icon' => 'home'
                        ],
                        
                        
                        [
                            'label' => 'Operaciones',
                            'icon' => 'tasks',
                            'items' => [
                                ['label' => 'Envío de Dinero', 'icon'=>'', 'url'=>['/cambiodivisas/operaciones']],
                                //['label' => 'Contacto', 'icon'=>'phone', 'url'=>'#'],
                            ],
                        ],
                        [
                            'label' => 'Reportes',
                            'icon' => 'stats',
                            'items' => [
                                //['label' => 'Contacto', 'icon'=>'phone', 'url'=>'#'],
                                ['label' => 'Envío de Dinero', 'icon'=>'', 'url'=>['#']],
                                ['label' => 'Comisiones', 'icon'=>'', 'url'=>'#']
                            ],
                        ],
                        
                    ],
                ]);
            }
            ?>

            

<br style="clear:both"/>
<h3 class="list-group-item success text-center" style="border-radius: .2rem; margin-bottom: 0px; padding-bottom: 5px; padding-top: 5px; font-weight: 900"> <i class="glyphicon glyphicon-calendar"></i> Apertura Diaria</h3>
         
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class=" panel-title " style="border-radius: .2rem; margin-bottom: 0px; padding-bottom: 5px; padding-top: 5px; "><b> Fecha:</b> <?= date("d/m/Y")  ?> &nbsp;&nbsp;&nbsp;


                <?php if (!$apertura) {

                    if ($Usuarioperfil->perfil->administrador!=0) {
                        
                        echo Html::a('<i class="glyphicon glyphicon-plus pull-right"></i>', ['/cambiodivisas/operacionempresa/create'], [ 'class' => ' btn-default', 'title' => 'Aperturar Operaciones']);
                        echo '</h4> </div> ';
                    }
                }else{

                    if ($Usuarioperfil->perfil->administrador!=0) {
                        echo Html::a('<i class="text-success glyphicon glyphicon-plus "></i>', ['/cambiodivisas/operacionsaldocuentasbancarias/create'], [ 'class' => ' btn-default', 'title' => 'Saldo de Cuentas Bancarias']);

                        echo Html::a('<i class="glyphicon glyphicon-minus pull-right"></i>', ['/cambiodivisas/operacionempresa/cierre?id='.$apertura->idoperacionempresa], [ 'class' => ' btn-default', 'title' => 'Cerrar Operaciones']);
                    }
                        echo '</h4> </div> ';


                        $saldoapert=Operacionsaldocuentasbancarias::find()->where(['id_operacion_empresa' => $apertura->idoperacionempresa])->groupBy('id_cuentabancaria_sistema')->all();
                        
                        ;
                        echo '<div > <div class="col-xs-6 col-md-6 col-sm-6 bg bg-info text-info  ">';

                        echo '<center><b>Apertura </b></center>';

                        foreach($saldoapert as $key => $value) {

                            $saldo_apertura=Operacionsaldocuentasbancarias::find()->where(['id_operacion_empresa' => $apertura->idoperacionempresa])->andWhere(['id_cuentabancaria_sistema'=>$value->id_cuentabancaria_sistema])->sum('saldo_apertura');

                            if ($saldo_apertura>0) {
                                 echo '<br><span>'.$value->cuentabancariasistema->alias.': </span> <span class="operation-details-item">'.  number_format($saldo_apertura , 2, ',', '.').'</span>';
                            }
                           
                        }
                        echo '</div></div>';

                        echo '<div > <div class="col-xs-6 col-md-6 col-sm-6 bg bg-primary text-primary  ">';

                        echo '<center><b>Disponible </b></center>';

                        $disponible1=0;

                        foreach($saldoapert as $key => $value) {

                            //$acumulado=Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['like', 'fecha',date('Y-m-d') ])->sum('monto_operacion');
                            $acumulado=Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['=', 'id_operacion_empresa',$apertura->idoperacionempresa ])->sum('monto_operacion');

                            $saldo_apertura=Operacionsaldocuentasbancarias::find()->where(['id_operacion_empresa' => $apertura->idoperacionempresa])->andWhere(['id_cuentabancaria_sistema'=>$value->id_cuentabancaria_sistema])->sum('saldo_apertura');
                            
                                $saldo=$saldo_apertura-$acumulado;
                                if ($saldo>0) {
                                     echo ' <br><span>'.$value->cuentabancariasistema->alias.': </span> <span class="operation-details-item">'. number_format($saldo , 2, ',', '.')  .'</span>';
                                     //$disponible=$acumulado+$acumulado;
                                     
                                
                                    $disponible1=$disponible1+$saldo;

                                
                                }
                                // } else {
                                //     echo ' <br><span>'.$value->cuentabancariasistema->alias.': </span> <span class="operation-details-item">'. number_format($saldo , 2, ',', '.')  .'</span>';
                                // }
                                

                            
                        }
/*
$alerta='bg bg-success text-success text-center';
$alerta2='bg bg-success text-success text-center';
$solesdiponibles=0;
 $solescomprometidos=0;
 $solesrestantes=0;*/
                        if ($tiporemesa) {
                            
                        foreach ($tiporemesa as $key => $tiporemesas) {
                        
                            $solesdiponibles=$disponible1/$tiporemesas->venta; 
                            $solescomprometidos=Operaciones::find()->where(['estatus' => 23])->andWhere(['id_tipo_moneda_envia'=>$tiporemesas->id_moneda_compra])->sum('monto_envio');

                            $solesrestantes=$solesdiponibles-$solescomprometidos;

                            $alerta='bg bg-success text-success text-center';

                            $alerta2='bg bg-success text-success text-center';

                            if ($solesdiponibles<=200) {
                                $alerta='bg bg-warning text-warning text-center';
                            }
                            if ($solesdiponibles<=100) {
                                $alerta='bg bg-danger text-danger text-center';
                            } 

                            if ($solesrestantes<=200) {
                                $alerta2='bg bg-warning text-warning text-center';
                            }
                            if ($solesrestantes<=100) {
                                $alerta2='bg bg-danger text-danger text-center';
                            } 

                            echo '</div> ';



                            
                            echo '<div class="col-xs-12 col-md-12 col-sm-12 '.$alerta.' text-left">';

                                echo '<h5> '.Html::img(Yii::$app->params['imgUrl'].'flags/png/'.$tiporemesas->monedacompra->idbtipomoneda.'.png', ['alt' => $tiporemesas->monedacompra->descripcion ,'title' => $tiporemesas->monedacompra->descripcion, 'width'=>'20px' ]).' '.$tiporemesas->monedacompra->descripcion.' - '.$tiporemesas->monedacompra->codigo . ' :  <b>'. number_format($solesdiponibles , 2, ',', '.').'</b></h5>';
                                
                            echo '</div>';

                             echo '<div class="col-xs-6 col-md-6 col-sm-6   text-center" style="background-color:#f5f5f5;">';

                                echo '<h5> Comprometido </h5>';
                                
                                echo '<h5><strong>'.number_format($solescomprometidos , 2, ',', '.').'</strong></h5>';

                            echo '</div>';

                           
                             echo '<div class="col-xs-6 col-md-6 col-sm-6 '.$alerta2.'">';

                                echo '<h5> Restantes </h5>';
                                echo '<h5><strong>'.number_format($solesrestantes , 2, ',', '.').'</strong></h5>';

                            echo '</div>';

                        }
                        } 
                        

                        if ($Usuarioperfil->perfil->administrador!=0) {    

                        //if ($Usuarioperfil->perfil->administrador!=0) {

                            echo ' <div class="col-xs-12 col-md-12 col-sm-12 list-group-item success text-center"> ';

                                echo "<BR>CIERRE DE OPERACIONES";
                                // Change the widget size and set colors for on and off statuses
                                echo SwitchInput::widget([

                                    'name' => 'permite_operaciones',
                                    'value'=>$apertura->permite_operaciones,

                                    'pluginOptions' => [
                                        'size' => 'mini',
                                        'onColor' => 'success',
                                        'offColor' => 'danger',
                                        'onText' => 'No',
                                        'offText' => 'Sí',
                                    ],
                                    // 'pluginEvents' => [
                                    //         "switchChange.bootstrapSwitch" => "function(item) { 
                                    //             console.log(item); 

                                    //         }"
                                    // ]
                                    'pluginEvents' => [
                                        "switchChange.bootstrapSwitch" => 'function() { 
                                            $.ajax({
                                              method: "POST",
                                              url: "'.Url::to(['/cambiodivisas/operacionempresa/cierreoperaciones?id='.$apertura->idoperacionempresa]).'",
                                              data: { permite_operaciones: this.value}
                                          })
                                        }',
                                    ]
                                ]);
                            echo '</div>';
                        }

                } ?>
                
           
        
<?php if ($Usuarioperfil->perfil->administrador!=0) { ?>

        <br style="clear:both"/><br style="clear:both"/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class=" panel-title " style="border-radius: .2rem; margin-bottom: 0px; padding-bottom: 5px; padding-top: 5px; ">Tipo de Cambio <?=Html::a('<i class="glyphicon glyphicon-plus pull-right"></i>', ['/cambiodivisas/tipodecambio/create'], [ 'class' => ' btn-default', 'title' => 'Nuevo Tipo de Cambio']) //. ' '.
                    
            //Html::a('<i class="glyphicon glyphicon-edit"></i>', ['/cambiodivisas/tipodecambio/update?id='.$tipocambio->idcambio], ['data-pjax' => 0, 'class' => 'btn-default', 'title' => 'Actualizar Tipo de Cambio'])?>
                </h4>
            </div>

            <div class=" text-center ">
                <div class=" col-xs-6 col-md-6 col-sm-6 bg bg-info text-info text-center ">
                    <h4>Compra</h4>
                    <p><?php if ($tipocambio) {  echo number_format($tipocambio->monto , 3, ',', '.'); }?></p>
                </div>
                
            
                <div class=" col-xs-6 col-md-6 col-sm-6 bg bg-primary text-primary text-center ">
                    <h4>Venta</h4>
                    <p><?php if ($tipocambio) {  echo number_format($tipocambio->venta , 3, ',', '.'); }?></p>
                </div>
            
             </div>
        </div>

        <br style="clear:both"/><br style="clear:both"/>


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class=" panel-title " style="border-radius: .2rem; margin-bottom: 0px; padding-bottom: 5px; padding-top: 5px; ">Tasa del Día Envío de Dinero <?=Html::a('<i class="glyphicon glyphicon-plus pull-right"></i>', ['/cambiodivisas/tipodecambio/createtasa'], [ 'class' => ' btn-default', 'title' => 'Nuevo Tipo de Remesa']) //. ' '.
                    
            //Html::a('<i class="glyphicon glyphicon-edit"></i>', ['/cambiodivisas/tipodecambio/updatetasa?id='.$tiporemesa->idcambio], ['data-pjax' => 0, 'class' => 'btn-default', 'title' => 'Actualizar Tipo de Cambio'])?>
                </h3>
            </div>

           
                <div class="col-xs-12 col-md-12 col-sm-12 bg bg-info text-info  ">
                    
                    <?php if ($tiporemesa) {  
                    
                        foreach ($tiporemesa as $key => $value) {
                            echo '<h5> '.Html::img(Yii::$app->params['imgUrl'].'flags/png/'.$value->monedacompra->idbtipomoneda.'.png', ['alt' => $value->monedacompra->descripcion ,'title' => $value->monedacompra->descripcion, 'width'=>'20px' ]).' '.$value->monedacompra->descripcion.' - '.$value->monedacompra->codigo . ' :  <b>'. number_format($value->venta , 0, ',', '.').'</b></h5>'; 
                            
                        }
                    }
                ?>
                </div>
                
            
            
        </div>
<?php } ?>
<br style="clear:both"/><br style="clear:both"/>
        </div>
    </div>



    <div class="columnmovil col-xs-9 col-md-9 col-sm-9 " style="padding-left: 0px; padding-right: 0px;" >
<br style="clear:both"/><br style="clear:both"/>
        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div> 

<?php }else{

    Yii::$app->urlManager->createUrl(['/user/security/login']);

    ?>

    <div class="wrap col-md-12 col-sm-12"  >
         <br style="clear:both"/><br style="clear:both"/>
        <div class="container">
            
            
            <?= $content ?>
        </div>
    </div>
    
<?php } ?>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">Copyright &copy; <?= date('Y') ?>. All Rights Reserved.  </p>

        <p class="pull-right">Powered by <?= Html::a(Html::decode(Yii::$app->name, ['alt' => 'MultiTasas.com','title' => 'MultiTasas.com', 'width'=>'50%' ]),'https://multitasas.com/') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

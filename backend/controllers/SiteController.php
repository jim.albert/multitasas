<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

use backend\modules\cambiodivisas\models\Operaciones;
use backend\modules\cambiodivisas\models\Usuarioperfil;
use backend\modules\cambiodivisas\models\Perfil;
use backend\modules\cambiodivisas\models\OperacionesinicioSearch;
use backend\modules\cambiodivisas\models\OperacionesiniciooperadorSearch;
use akiraz2\support\models\Ticket;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'captcha'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'countsupport'],
                        'allow' => true, 
                        'roles' => ['@'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'offset'=> 5,
                
            ], 
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {


            $Usuarioperfil=Usuarioperfil::find()->where(['id_usuario'=>Yii::$app->user->identity->getId()])->one();


            if ($Usuarioperfil->perfil->administrador!=0) {
            
                $searchModel = new OperacionesinicioSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            }else{
                $searchModel = new OperacionesiniciooperadorSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            }
            //$searchModel->estatus=22;

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'Usuarioperfil'=>$Usuarioperfil,
            ]);

            //return $this->render('index', );
        
        }
        //return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Count open tickets.
     * @return mixed
     */
    public function actionCountsupport()
    {
        $totalopentickets= Ticket::find()->where(['status'=>0])->count();

        return $totalopentickets;
    }
}

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Operacionesusuarios */

$this->title = Yii::t('app', 'Asignar Operaciones a Usuarios');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asignar Operaciones a Usuarios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operacionesusuarios-create">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,'model2' => $model2,
    ]) ?>

</div>

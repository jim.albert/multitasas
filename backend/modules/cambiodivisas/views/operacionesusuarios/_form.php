<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\cambiodivisas\models\Usuarioperfil; 
use backend\modules\cambiodivisas\models\Operaciones; 
use kartik\widgets\SwitchInput;
/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Operacionesusuarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="operacionesusuarios-form">

    <?php $form = ActiveForm::begin(); 

// foreach ($model as $index => $model) {
//     echo $form->field($model, "[$index]id_usuario")->label($model->id_usuario);
//     echo $form->field($model, "[$index]id_operacion")->label($model->id_operacion);
// }
    ?>

    <div class="col-md-12 col-sm-12">

    	
    	 <?= $form->field($model, 'id_usuario')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Usuarioperfil::find()->orderBy('id ASC')->all(), 'id_usuario','usuariodatos'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?> 
    </div>

     <div class="col-md-12 col-sm-12">
     	<div class="table-responsive">
            <table class="table table-striped table-sm">
           
                <thead class="bg bg-info text info">
                <tr>
                    <td style="border: 0px"><span class="operation-details-item table-cell">Seleccione</span></td>
                    <td style="border: 0px"><span class="operation-details-item table-cell">Código</span></td>
                    <td style="border: 0px"><span class="operation-details-item table-cell">Tipo</span></td>
                    <td style="border: 0px"><span class="operation-details-item table-cell">Fecha de Registro</span></td>
                    <td style="border: 0px"><span class="operation-details-item table-cell">Monto de Transacción</span></td>
                    <td style="border: 0px"><span class="operation-details-item table-cell">Monto Transferido</span></td>
                    
                </tr>
                </thead>
                 <tbody>
     	 <?php 
                 $Operaciones=Operaciones::find()->where(['estatus'=>22])->all();
                 $count=0;
                 foreach ($Operaciones as $i => $Operaciones) {

      //           	//$modelcuentasbanco[$i]->saldo_anetrior=$opercierre->saldo_cierre;
                 	$nombrechechk="operacioncheck_".$i;
                 ?>
                 <tr>
                    
                    <td>
                    

                     <?= $form->field($model2[0], 'operacioncheck['.$i.']', ['inputOptions'=>['class'=>'form-control input-lg ']])->checkbox( [ 'value'=>1, 'uncheckValue'=>0 ] )->label(false) ?>

                    <?= $form->field($model2[0], "[{$i}]id_operacion")->hiddenInput(['value' => $Operaciones->idoperacion])->label(false) ?></td>
                    
                    <td><?= $Operaciones->codigo ?></td>
                    <td><?= $Operaciones->tipo ?></td>
                    <td><?= $Operaciones->fecha_registro ?></td>
                    <td><?= $Operaciones->tipoMonedaEnvia->codigo.' '.$Operaciones->monto_envio; ?></td>
                    <td><?= $Operaciones->tipoMonedaRecibe->codigo.' '.$Operaciones->monto_recibe ?></td>
                </tr>

	            </div>
                
		   			
		<?php   $count=$i;  } 		?> 
			</tbody>
            </table>
        </div>
        <?= $form->field($model, 'count')->hiddenInput(['value'=>$count])->label(false) ?>
    </div>

    

     <div class="form-group col-md-12 col-sm-12">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancelar', ['/'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

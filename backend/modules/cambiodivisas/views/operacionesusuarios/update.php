<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Operacionesusuarios */

$this->title = Yii::t('app', 'Update Operacionesusuarios: {name}', [
    'name' => $model->idoperusu,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Operacionesusuarios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idoperusu, 'url' => ['view', 'id' => $model->idoperusu]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="operacionesusuarios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

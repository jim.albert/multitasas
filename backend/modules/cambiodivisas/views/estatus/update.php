<?php

use yii\helpers\Html;

/* @var $this yii\web\View */ 
/* @var $model backend\modules\cambiodivisas\models\Estatus */

$this->title = 'Estatus: ' .$model->codigo.' - '.$model->descripcion;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Estatus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo, 'url' => ['view', 'id' => $model->idstatus]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="estatus-update">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

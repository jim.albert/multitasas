<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Estatus */ 

$this->title = 'Estatus: ' .$model->codigo.' - '.$model->descripcion;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Estatuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="estatus-view">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->idstatus], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idstatus], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) 
        ?>
        <?= Html::a('Cancelar', ['index'], ['class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idstatus',
            'codigo',
            'descripcion',
            'tabla',
            'fecha',
        ],
    ]) ?>

</div>

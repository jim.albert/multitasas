<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Estatus */

$this->title = Yii::t('app', 'Registrar Estatus'); 
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Estatus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estatus-create">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

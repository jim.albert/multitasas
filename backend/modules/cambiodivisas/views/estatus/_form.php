<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2; 
use kartik\color\ColorInput;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Estatus */
/* @var $form yii\widgets\ActiveForm */
$data_tables=[

    'Bancos'=>'Bancos',
    'Cuentas Bancarias del Sistema'=>'Cuentas Bancarias del Sistema',
    'Cuentas Bancarias de Usuarios'=>'Cuentas Bancarias de Usuarios',
    'Empresa'=>'Empresa',
    'Operaciones'=>'Operaciones',
    'Pais'=>'País',
    'Perfiles de Accesos'=>'Perfiles de Accesos',
    'Tipo de Cambio'=>'Tipo de Cambio',
    'Tipo de Cuenta'=>'Tipo de Cuenta',
    'Tipo de Documento'=>'Tipo de Documento',
    'Tipo de Monedas'=>'Tipo de Monedas',
    'Usuarios'=>'Usuarios',
    'Validaciones'=>'Validaciones',
    
];
 


?>


<div class="estatus-form">

    <?php $form = ActiveForm::begin(); ?>

   

    <div class="col-md-6 col-sm-6">

        <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-6 col-sm-6">

        <?= $form->field($model, 'tabla')->widget(Select2::classname(), [
            'data' => $data_tables,
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
     <div class="col-md-6 col-sm-6">

        <?= $form->field($model, 'color')->widget(ColorInput::classname(), [
            'useNative' => true,
            'options' => ['placeholder' => 'Selecciona un color ...']]) ?>
    </div>
    <div class="col-md-6 col-sm-6">

        <?= $form->field($model, 'estatus')->widget(Select2::classname(), [
            'data' => ['1'=>'Activo', '0'=>'Inactivo'],
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
   

    <div class="form-group col-md-12 col-sm-12">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancelar', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

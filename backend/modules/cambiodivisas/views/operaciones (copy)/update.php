<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Operaciones */

$this->title = Yii::t('app', 'Update Operaciones: {name}', [
    'name' => $model->idoperacion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Operaciones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idoperacion, 'url' => ['view', 'id' => $model->idoperacion]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="operaciones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

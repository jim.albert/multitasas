<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2; 
use yii\helpers\ArrayHelper;

use frontend\modules\cambiosdivisas\models\Estatus;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\cambiosdivisas\models\OperacionesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', 'Operaciones');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operaciones-index">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

 
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary'=> false,
        'options'=>['class'=>'text-center table-cell'],
        //'headerRowOptions'=>['class'=>'text-center table-cell'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idoperacion',
            'codigo',
            //'tipo',
            [
                'attribute' => 'tipo',    
                //'format' =>'amount',// ['decimal',3],
               
                    
                'contentOptions'=>['class'=>'text-center table-cell'],
                'headerOptions'=>['class'=>'text-center table-cell'],
                'filter'    => Select2::widget([
                              'model' => $searchModel,
                              'attribute' => 'tipo',
                              'data' => ['CAMBIO'=>'CAMBIO','REMESA'=>'REMESA'],
                              'options' => [
                                  'placeholder' => 'Opciones'
                              ],
                              'pluginOptions' => [
                                  'allowClear' => true
                              ],
                          ]),
            ],
            //'fecha_registro:datetime',
            
            [   'attribute' => 'fecha_registro',    
                'format' =>'datetime',// ['decimal',3],
                'contentOptions'=>['class'=>'text-center table-cell'],
                'headerOptions'=>['class'=>'text-center table-cell'],
                
            ],


            [   'attribute' => 'monto_cambio_usado',    
                //'format' =>'amount',// ['decimal',3],
                'contentOptions'=>['class'=>'text-right table-cell'],
                'headerOptions'=>['class'=>'text-center table-cell'],
                
            ],


            //'monto_envio',
            [   'attribute' => 'monto_envio',    
                //'format' =>'amount',// ['decimal',3],
                'contentOptions'=>['class'=>'text-right table-cell'],
                'headerOptions'=>['class'=>'text-center table-cell'],
                'value' => function($data){
                        
                        return $data->tipoMonedaEnvia->codigo.' '.$data->monto_envio; 
                    },
                    'label'=>'Monto de Transacción',
                
            ],

            //'monto_recibe',
            [   'attribute' => 'monto_recibe',    
                //'format' =>'amount',// ['decimal',3],
                'contentOptions'=>['class'=>'text-right table-cell'],
                'headerOptions'=>['class'=>'text-center table-cell'],
                'value' => function($data){
                        
                        return $data->tipoMonedaRecibe->codigo.' '.$data->monto_recibe; 
                    },
                    'label'=>'Monto Transferido',
                
            ],

            //'estatus0.descripcion',
            [
                'attribute' => 'estatus',    
                //'format' =>'amount',// ['decimal',3],
                'contentOptions'=>function($data){
                        
                        return ['class'=>'text-center table-cell', 'style'=>'background-color:'.$data->estatus0->color.'; color:#fff']; 
                    },
                'headerOptions'=>['class'=>'text-center table-cell'],
                    
                'value' => function($data){
                        
                        return $data->estatus0->descripcion; 
                    },
                'filter'    => Select2::widget([
                              'model' => $searchModel,
                              'attribute' => 'estatus',
                              'data' => ArrayHelper::map(Estatus::find()->where(['tabla'=>'Operaciones'])->orderBy('descripcion ASC')->all(), 'idstatus','descripcion'),
                              'options' => [
                                  'placeholder' => 'Opciones'
                              ],
                              'pluginOptions' => [
                                  'allowClear' => true
                              ],
                          ]),
            ],
            
            

            //'id_banco_envia',
            //'id_cuenta_bancaria_usuario',
            //'id_tipo_moneda_envia',
            //,
            //'id_tipo_cambio',
            //'id_tipo_moneda_recibe',
            //
            //
            //'id_cuenta_bancaria_sistema',
            //'cod_referencia',
            //,
            //'fecha_pago_sistema',
            //'fecha_proceso',
            //

            ['class' => 'yii\grid\ActionColumn',

                //'header' => 'Acciones',
                'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => " {view} {update} {delete}",
                'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'Ver Detalle'),
                    ]);
                },
 
                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('app', 'update'),
                    ]);
                },

                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('app', 'delete'),
                    ]);
                },
                
 
                ],

                'urlCreator' => function ($action, $model, $key, $index) {
                /*if ($action === 'update') {
                    $url ='index.php?r=cambiosdivisas/operaciones/view&id='.$model->idoperacion;
                    return $url;
                }*/
                if ($action === 'view') {
                    $url =['/cambiodivisas/operaciones/detalle?id='.$model->idoperacion];
                    return $url;
                }
                if ($action === 'update') {
                    $url =['/cambiodivisas/operaciones/update?id='.$model->idoperacion];
                    return $url;
                }
                if ($action === 'delete') {
                    $url =['/cambiodivisas/operaciones/delete?id='.$model->idoperacion];
                    return $url;
                }
                  
 
                }
            ],
        ],
    ]); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Bancos */

$this->title = Yii::t('app', 'Registrar Bancos');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bancos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bancos-create">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
 
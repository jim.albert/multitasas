<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2; 
use yii\helpers\ArrayHelper;

use frontend\modules\cambiosdivisas\models\Estatus;
use backend\modules\cambiodivisas\models\Operacionesusuarios;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\cambiosdivisas\models\OperacionesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */ 


$this->title = Yii::t('app', 'Operaciones');
$this->params['breadcrumbs'][] = $this->title;

if ($Usuarioperfil->perfil->administrador!=0) {
    $template = " {view} {update} {delete}";
} else {
    $template =  " {view} ";
}
?>
<div class="operaciones-index">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

 
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary'=> false,
        'options'=>['class'=>'text-center table-cell'],
        //'headerRowOptions'=>['class'=>'text-center table-cell'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idoperacion',
            'codigo',
            //'tipo',
            [
                'attribute' => 'tipo',    
                //'format' =>'amount',// ['decimal',3],
               
                    
                'contentOptions'=>['class'=>'text-center table-cell'],
                'headerOptions'=>['class'=>'text-center table-cell'],
                'filter'    => Select2::widget([
                              'model' => $searchModel,
                              'attribute' => 'tipo',
                              'data' => ['CAMBIO'=>'CAMBIO','REMESA'=>'REMESA'],
                              'options' => [
                                  'placeholder' => 'Opciones'
                              ],
                              'pluginOptions' => [
                                  'allowClear' => true
                              ],
                          ]),
            ],
            //'fecha_registro:datetime',
            
            [   'attribute' => 'fecha_registro',    
                //'format' =>'datetime',// ['decimal',3],
                'value' => function($data){
                        
                                return \Yii::$app->formatter->asDate(substr($data->fecha_registro, 0, 10), 'php:d-m-Y'); 
                            },
                'contentOptions'=>['class'=>'text-center table-cell'],
                'headerOptions'=>['class'=>'text-center table-cell'],
                
            ],


            [   'attribute' => 'monto_cambio_usado',    
                //'format' =>'amount',// ['decimal',3],
                'contentOptions'=>['class'=>'text-right table-cell'],
                'headerOptions'=>['class'=>'text-center table-cell'],
                'value' => function($data){
                        
                        return number_format($data->monto_cambio_usado , 2, ',', '.'); 
                    },
                
            ],


            //'monto_envio',
            [   'attribute' => 'monto_envio',    
                //'format' =>'amount',// ['decimal',3],
                'contentOptions'=>['class'=>'text-right table-cell'],
                'headerOptions'=>['class'=>'text-center table-cell'],
                'value' => function($data){
                        
                        return $data->tipoMonedaEnvia->codigo.' '.number_format($data->monto_envio , 2, ',', '.'); 
                    },
                    'label'=>'Monto de Transacción',
                
            ],

            //'monto_recibe',
            [   'attribute' => 'monto_recibe',    
                //'format' =>'amount',// ['decimal',3],
                'contentOptions'=>['class'=>'text-right table-cell'],
                'headerOptions'=>['class'=>'text-center table-cell'],
                'value' => function($data){
                        
                        return $data->tipoMonedaRecibe->codigo.' '.number_format($data->monto_recibe , 2, ',', '.'); 
                    },
                    'label'=>'Monto Transferido', 
                
            ],

            [   'attribute' => 'agentes',   
                            'format' =>'raw', 
                            'value' => function($data, $model){
                                        
                                        $agente=Operacionesusuarios::find()->where(['id_operacion'=>$data->idoperacion, 'tipo_comision'=>'Agente'])->one(); 

                                        if ($agente) {
                                            return $agente->usuarioinfo;
                                        } else {
                                           return '-';
                                        }
                                        
                            }
                            //'format' =>'datetime',// ['decimal',3],
                            
                            
                        ],

            //'estatus0.descripcion',
            [
                            'attribute' => 'estatus',    
                            'format' =>'raw',// ['decimal',3],
                            'contentOptions'=>function($data){
                                    
                                    return ['class'=>'text-center table-cell', 'style'=>'background-color:'.$data->estatus0->color.'; color:#fff']; 
                                },
                            'headerOptions'=>['class'=>'text-center table-cell'],
                                
                            'value' => function($data, $model){
                                    
                                    $Operador=Operacionesusuarios::find()->where(['id_operacion'=>$data->idoperacion, 'tipo_comision'=>'Operador'])->one(); 

                                    if ($Operador) {
                                        return $data->estatus0->descripcion.
                                    '<br> <strong>Pagador:</strong> '.$Operador->usuarioinfo;
                                    } else {
                                       return $data->estatus0->descripcion;
                                    }
                                    
                                    //return $data->estatus0->descripcion.
                                    //'<br>'.$Operador->usuarioinfo;
                                },
                            'filter'    => Select2::widget([
                                          'model' => $searchModel,
                                          'attribute' => 'estatus',
                                          'data' => ArrayHelper::map(Estatus::find()->where(['tabla'=>'Operaciones'])->orderBy('descripcion ASC')->all(), 'idstatus','descripcion'),
                                          'options' => [
                                              'placeholder' => 'Opciones'
                                          ],
                                          'pluginOptions' => [
                                              'allowClear' => true
                                          ],
                                      ]),
                        ],
            
            

            //'id_banco_envia',
            //'id_cuenta_bancaria_usuario',
            //'id_tipo_moneda_envia',
            //,
            //'id_tipo_cambio',
            //'id_tipo_moneda_recibe',
            //
            //
            //'id_cuenta_bancaria_sistema',
            //'cod_referencia',
            //,
            //'fecha_pago_sistema',
            //'fecha_proceso',
            //

            ['class' => 'yii\grid\ActionColumn',

                //'header' => 'Acciones',
                'headerOptions' => ['style' => 'color:#337ab7'],
                
                
                'template' => $template,
                'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'Ver Detalle'),
                    ]);
                },
 
                'update' => function ($url, $model) {
                    if ($model->estatus==22) {
                        # code...
                    
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'update'),
                        ]);
                    }
                },

                'delete' => function ($url, $model) {
                    // return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                    //             'title' => Yii::t('app', 'update'),
                    // ]);
                    if ($model->estatus!=24) {

                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'class' => 'text-danger',
                            'data' => [
                                'confirm' => '¿Seguro que desea eliminar Ésta Operación?',
                                'method' => 'post',
                            ],
                        ]);
                    }

                    // return Html::beginForm(['/cambiodivisas/operaciones/delete?id='.$model->idoperacion], 'post')
                    //     . Html::submitButton(

                    //         '<span class="glyphicon glyphicon-trash"> </span>  ',
                    //         ['class' => 'btn btn-link logout']
                    //     )
                    //     . Html::endForm();
                },
                
 
                ],

                'urlCreator' => function ($action, $model, $key, $index) {
                /*if ($action === 'update') {
                    $url ='index.php?r=cambiosdivisas/operaciones/view&id='.$model->idoperacion;
                    return $url;
                }*/
                if ($action === 'view') {
                    $url =['/cambiodivisas/operaciones/detalle?id='.$model->idoperacion];
                    return $url;
                }
                if ($action === 'update') {
                    $url =['/cambiodivisas/operaciones/update?id='.$model->idoperacion];
                    return $url;
                }
                if ($action === 'delete') {
                    $url =['/cambiodivisas/operaciones/delete?id='.$model->idoperacion, 'post'];
                    
                    return $url;
                }
                  
 
                }
            ],
        ],
    ]); ?>

</div>

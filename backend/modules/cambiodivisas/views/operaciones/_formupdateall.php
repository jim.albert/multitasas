<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use yii\helpers\Url;
use kartik\widgets\SwitchInput;
 
use backend\modules\cambiodivisas\models\Tipodemoneda;
//use frontend\modules\cambiosdivisas\models\Tipodecambio;
use backend\modules\cambiodivisas\models\Bancos;
use backend\modules\cambiodivisas\models\Cuentabancariadeusuarios;
use backend\modules\cambiodivisas\models\Cuentasbancariadesistema;
use backend\modules\cambiodivisas\models\Operacionsaldocuentasbancarias;
use backend\modules\cambiodivisas\models\Operacioncomprobantes;
use backend\modules\cambiodivisas\models\Usuarioperfil; 

use kartik\number\NumberControl;
/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Operaciones */
/* @var $form yii\widgets\ActiveForm */ 

$jsc = <<< JS


        

       

    function montorecibe(){


        var monto_cambio =$('#operaciones-monto_cambio_usado').val();
        var monto_envio=$('#operaciones-monto_envio').val();
       
        var monto_operacion=parseFloat(monto_envio)*parseFloat(monto_cambio);

        $('#operaciones-monto_recibe').val(monto_operacion.toFixed(3));
        $('#operaciones-monto_recibe-disp').val(monto_operacion.toFixed(3));
        
        
    }

       


JS;

$this->registerJs($jsc, $this::POS_END);
?>

<div class="operaciones-form">
    <div class=" col-md-12 col-sm-12 " style="background-color: #f5f5f5;padding-top: 10px;">
         <h3 class="bg bg-primary text-primary text-center " style="padding: 5px">Detalle de Operacion</h3>

        <?php $form = ActiveForm::begin(); ?>
        <div class=" col-md-6 col-sm-6 " >
                <?= $form->field($model, 'cod_referencia')->textInput(['maxlength' => true]) ?>
        </div>
        <div class=" col-md-6 col-sm-6 " >
                <?= $form->field($model, 'id_banco_envia')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Bancos::find()->where([])->orderBy('codigo ASC')->all(), 'idbancos','codigo', 'codigopais'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...',
                          // 'onchange'  => '
                          //   $.post("index.php?r=/cambiosdivisas/cuentabancariadeusuarios/listcuentas&id=' . '" + $(this).val(), function(data){
                          //       $("select#operaciones-id_cuenta_bancaria_usuario").html(data);
                          //   })',
                    ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        </div>
        <div class=" col-md-12 col-sm-12 " >
             <?= $form->field($model, 'id_cuenta_bancaria_usuario')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Cuentabancariadeusuarios::find()->where([])->orderBy('nombre_beneficiario ASC')->all(), 'idcuenta_bancaria','cuentanro','cuentanrodetalleupdate'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...',
                          // 'onchange'  => '
                          //   $.post("index.php?r=/cambiosdivisas/operaciones/listmonedas_dt&id=' . '" + $(this).val(), function(data){
                          //       $("#operaciones-id_tipo_moneda_envia").val(data);
                          //   }); ',
                    ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        </div>
        

        <div class=" col-md-3 col-sm-3 " >
            <?= $form->field($model, 'monto_envio')->widget(NumberControl::classname(),
                [
                    'options' => ['onchange' => 'montorecibe()',],
                    'maskedInputOptions' => [
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'digits' => 4,
                ],

                ])->label('Usuario Envía:')     ?>
        </div>

        <div class=" col-md-3 col-sm-3 " >
                 <?= $form->field($model, 'id_tipo_moneda_envia')->widget(Select2::classname(), [
            'data' => ['2'=>'Soles - S/'],
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...', 
                          'onchange' => 'montorecibe()',
                         ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        </div>
        
        <div class=" col-md-3 col-sm-3 " >
                
                <?= $form->field($model, 'monto_cambio_usado')->widget(NumberControl::classname(),
                [
                    'options' => ['onchange' => 'montorecibe()',],
                    'maskedInputOptions' => [
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'digits' => 4,
                ],

                ])->label('Tasa de Envío:')     ?>
        </div>
        

 
        <div class=" col-md-3 col-sm-3 " >
               <?= $form->field($model, 'monto_recibe')->widget(NumberControl::classname(),
                [
                    //'options' => ['onchange' => 'montorecibe()',],
                    'readonly'=>true,
                    'maskedInputOptions' => [
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'digits' => 4,

                ],

                ])->label('Monto a Recibir:')     ?>

        </div>
        <div class="col-md-12 col-sm-12">
            <h3 class="bg bg-information text-information text-center " style="padding: 5px">Detalle de Pago</h3>
            <h4 class="bg bg-info text-info text-center " style="padding: 5px">Usuario Pagador</h4>
        </div>
         <div class="col-md-12 col-sm-12">

        
         <?= $form->field($Operacionesusuarios, 'id_usuario')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Usuarioperfil::find()->orderBy('id ASC')->all(), 'id_usuario','usuariodatos','perfildatos'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?> 
        </div>
        <div class="col-md-12 col-sm-12">
            <h4 class="bg bg-info text-info text-center " style="padding: 5px">Detalle de Pago</h4>
        
        </div>
         <div class=" col-md-6 col-sm-6 " >

            <?= $form->field($modelsaldo, 'id_cta_bancaria')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Operacionsaldocuentasbancarias::find()->where(['id_operacion_empresa' => $apertura['idoperacionempresa'] ])->andWhere(['!=' ,'saldo_apertura', '0'])->orderBy('id_cuentabancaria_sistema ASC')->all(), 'id_cuentabancaria_sistema','cuentabancsistemoperativa','paiscuentabanc'),
                'language' => 'es',
                'options' => ['placeholder' => 'Seleccionar una Opción ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); 

            ?>
                
        </div>
        <div class=" col-md-4 col-sm-4" >

            <?= $form->field($modelsaldo, 'referancia_pago')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-2 col-sm-2"style="padding-right: 2px">
            <?= $form->field($modelsaldo, 'pmovil')->widget(SwitchInput::classname(),[
                            'pluginOptions' => [
                                'onColor' => 'success',
                                'offColor' => 'danger',
                                'onText'=>'Si',
                                'offText'=>'No',
                            ],]) ?>
            
        </div>    

        <div class="col-md-12 col-sm-12">
    
            
              
                    <?= $form->field($modelcomprobantes, 'imagen')->widget(FileInput::classname(), [
                  'options' => ['accept' => 'image/*'],
                   'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png', 'jpeg'],
                   

                   'showPreview' => false,
                    'showCaption' => true,
                    'showRemove' => true,
                    'showUpload' => false,
                    'maxFileSize'=>1024,

               ],
              ]);   ?>
              <span class="bg bg-warning text-warning">Las imagenes a procesar deben pesar menos de 1024 Kb (1 Mb).</span>
               
        </div>


<!--
        <div class=" col-md-4 col-sm-4 " >   
                <?= $form->field($model, 'estatus')->textInput() ?>
        </div>

<div class=" col-md-4 col-sm-4 " >
        <?= $form->field($model, 'id_cuenta_bancaria_sistema')->textInput() ?>
        </div>

<div class=" col-md-4 col-sm-4 " >
        <?= $form->field($model, 'fecha_registro')->textInput() ?>
        </div>

      -->
      <div class=" col-md-12 col-sm-12" style="margin-top: 20px;" >
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            <?= Html::a('Regresar', ['/'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Regresar']) ?>
            <?= Html::a('Rechazar', ['rechaza','id'=>$model->idoperacion], ['data-pjax' => 0, 'class' => 'btn btn-danger', 'title' => 'Regresar']) ?>
        </div>

        <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

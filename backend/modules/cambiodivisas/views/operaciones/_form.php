<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use yii\helpers\Url;
use kartik\widgets\SwitchInput;
 
use backend\modules\cambiodivisas\models\Tipodemoneda;
//use frontend\modules\cambiosdivisas\models\Tipodecambio;
use backend\modules\cambiodivisas\models\Bancos;
use backend\modules\cambiodivisas\models\Cuentabancariadeusuarios;
use backend\modules\cambiodivisas\models\Cuentasbancariadesistema;
use backend\modules\cambiodivisas\models\Operacionsaldocuentasbancarias;
use backend\modules\cambiodivisas\models\Operacioncomprobantes;
use backend\modules\cambiodivisas\models\Usuarioperfil; 

use kartik\number\NumberControl;
/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Operaciones */
/* @var $form yii\widgets\ActiveForm */ 

$jsc = <<< JS


        

       

    function montorecibe(){


        var monto_cambio =$('#operaciones-monto_cambio_usado').val();
        var monto_envio=$('#operaciones-monto_envio').val();
       
        var monto_operacion=parseFloat(monto_envio)*parseFloat(monto_cambio);

        $('#operaciones-monto_recibe').val(monto_operacion.toFixed(3));
        $('#operaciones-monto_recibe-disp').val(monto_operacion.toFixed(3));
        
        
    }

       


JS;

$this->registerJs($jsc, $this::POS_END);
?>

<div class="operaciones-form">
    <div class=" col-md-12 col-sm-12 " style="background-color: #f5f5f5;padding-top: 10px;">
         <h3 class="bg bg-primary text-primary text-center " style="padding: 5px">Detalle de Operacion</h3>

        <?php $form = ActiveForm::begin(); ?>
        <div class=" col-md-6 col-sm-6 " >
                <?= $form->field($model, 'cod_referencia')->textInput(['maxlength' => true]) ?>
        </div>
        <div class=" col-md-6 col-sm-6 " >
                <?= $form->field($model, 'id_banco_envia')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Bancos::find()->where([])->orderBy('codigo ASC')->all(), 'idbancos','codigo', 'codigopais'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...',
                          // 'onchange'  => '
                          //   $.post("index.php?r=/cambiosdivisas/cuentabancariadeusuarios/listcuentas&id=' . '" + $(this).val(), function(data){
                          //       $("select#operaciones-id_cuenta_bancaria_usuario").html(data);
                          //   })',
                    ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        </div>
        <div class=" col-md-12 col-sm-12 " >
             <?= $form->field($model, 'id_cuenta_bancaria_usuario')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Cuentabancariadeusuarios::find()->where([])->orderBy('nombre_beneficiario ASC')->all(), 'idcuenta_bancaria','cuentanro','cuentanrodetalleupdate'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...',
                          // 'onchange'  => '
                          //   $.post("index.php?r=/cambiosdivisas/operaciones/listmonedas_dt&id=' . '" + $(this).val(), function(data){
                          //       $("#operaciones-id_tipo_moneda_envia").val(data);
                          //   }); ',
                    ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        </div>
        

        <div class=" col-md-3 col-sm-3 " >
            <?= $form->field($model, 'monto_envio')->widget(NumberControl::classname(),
                [
                    'options' => ['onchange' => 'montorecibe()',],
                    'maskedInputOptions' => [
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'digits' => 4,
                ],

                ])->label('Usuario Envía:')     ?>
        </div>

        <div class=" col-md-3 col-sm-3 " >
                 <?= $form->field($model, 'id_tipo_moneda_envia')->widget(Select2::classname(), [
            'data' => ['2'=>'Soles - S/'],
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...', 
                          'onchange' => 'montorecibe()',
                         ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        </div>
        
        <div class=" col-md-3 col-sm-3 " >
                
                <?= $form->field($model, 'monto_cambio_usado')->widget(NumberControl::classname(),
                [
                    'options' => ['onchange' => 'montorecibe()',],
                    'maskedInputOptions' => [
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'digits' => 4,
                ],

                ])->label('Tasa de Envío:')     ?>
        </div>
        

 
        <div class=" col-md-3 col-sm-3 " >
               <?= $form->field($model, 'monto_recibe')->widget(NumberControl::classname(),
                [
                    //'options' => ['onchange' => 'montorecibe()',],
                    'readonly'=>true,
                    'maskedInputOptions' => [
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'digits' => 4,

                ],

                ])->label('Monto a Recibir:')     ?>

        </div>
        
      <div class=" col-md-12 col-sm-12" style="margin-top: 20px;" >
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            <?= Html::a('Regresar', ['/'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Regresar']) ?>
            <?= Html::a('Rechazar', ['rechaza','id'=>$model->idoperacion], ['data-pjax' => 0, 'class' => 'btn btn-danger', 'title' => 'Regresar']) ?>
        </div>

        <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

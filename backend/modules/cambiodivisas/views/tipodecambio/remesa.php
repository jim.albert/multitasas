<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Tipodecambio */

$this->title = Yii::t('app', 'Registrar Tipo de Tasa');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipos de Cambios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipodecambio-create">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formremesa', [
        'model' => $model,
    ]) ?>

</div>

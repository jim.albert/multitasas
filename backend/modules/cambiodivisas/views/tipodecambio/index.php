<?php

//use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\cambiodivisas\models\Estatus;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\cambiodivisas\models\EstatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tipos de Cambios');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipodecambio-index">

    

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php 
    

    $gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],

    
            'codigo',
            //'descripcion',
            //'operacion',
            [
                //'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'operacion', 
                //'value' => 'estatus0.descripcion', 
                //'label'=>'Estatus',
                'vAlign' => 'middle',
                
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ['CAMBIO'=>'CAMBIO', 'REMESA'=>'REMESA'],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Opciones'],
                'format' => 'raw'
                
            ],
            'monto',
            'venta',
            'fecha',
            
            [
                //'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'estatus', 
                'value' => 'estatus0.descripcion', 
                //'label'=>'Estatus',
                'vAlign' => 'middle',
                
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Estatus::find()->where(['tabla'=>'Tipo de Cambio'])->orderBy('descripcion ASC')->all(), 'idstatus','descripcion'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Opciones'],
                'format' => 'raw',
                'contentOptions'=>function($data){
                        
                        return ['class'=>'text-center table-cell', 'style'=>'background-color:'.$data->estatus0->color.'; color:#fff']; 
                    },
                
            ],
            
    ['class' => 'kartik\grid\ActionColumn', ]
            //['class' => 'kartik\grid\CheckboxColumn']
    ];


    echo GridView::widget([
        'id'=> 'tipo_cambio',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,

        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        
        'resizableColumns'=>true,
        
        'toolbar' =>  [
            ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i> Cambio de Divisas', ['create'], ['class' => 'btn btn-success', 'title' => 'Registrar Tipo de Cambios']) . ' '.

                Html::a('<i class="glyphicon glyphicon-plus"></i> Envío de Dinero', ['createtasa'], ['class' => 'btn btn-primary', 'title' => 'Registrar Tipo de Tasa']) . ' '.
                    
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Refrescar Consulta'])
            ],
            '{export}',
            '{toggleData}'
        ],

        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => false,
        //'toggleDataOptions' => ['maxCount' => 5],
        //'showPageSummary' => true,
        'panel' => [
            'heading'=>'<h1 class="h1-head">
                        <i class="glyphicon glyphicon-cog"> </i> '.Html::encode($this->title).'</h1>',
            'type' => GridView::TYPE_PRIMARY,
            'footer'=>true
        ],
        
        /*'pager' => [ 
            //'firstPageLabel' => '<span class="glyphicon glyphicon-fast-backward"></span>',
            //'lastPageLabel' => '<span class="glyphicon glyphicon-fast-forward"></span>',
            //'nextPageLabel' => '<span class="glyphicon glyphicon-step-forward"></span>',
            //'prevPageLabel' => '<span class="glyphicon glyphicon-step-backward"></span>',
            'maxButtonCount' => 3,
            'hideOnSinglePage' => false, 
            'linkOptions' => ['class' => 'mylinkclass'],
        ],*/
    
    ]);

    ?>

    
</div>


<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Tipodecambio */

$this->title = 'Tipo de Cambio: ' .$model->codigo.' - '.$model->descripcion; 
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipos de cambios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo, 'url' => ['view', 'id' => $model->idcambio]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tipodecambio-update">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

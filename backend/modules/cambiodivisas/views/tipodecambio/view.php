<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Tipodecambio */

$this->title = 'Tipo de Cambios: ' .$model->codigo.' - '.$model->descripcion; 
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipos de Cambios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tipodecambio-view">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->idcambio], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idcambio], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Cancelar', ['index'], ['class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idcambio',
            'codigo',
            'descripcion',
            'monto',
            'fecha',
            [
                'attribute' => 'estatus0.descripcion',
                //'label'=>'Estatus',
                'label' => ' Estatus ',                

            ]
        ],
    ]) ?>

</div>

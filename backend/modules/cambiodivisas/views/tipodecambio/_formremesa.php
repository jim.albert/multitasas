<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use backend\modules\cambiodivisas\models\Estatus;
use kartik\number\NumberControl;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Tipodecambio */
/* @var $form yii\widgets\ActiveForm */ 

$jsc = <<< JS


        // function valor(item){


        //     var id = $('#defunidadcurricular-regimen_id').val();
        //     //alert(id);
        //     $.get('index.php?r=mallacurricular/defregimen/get-valor', {id : id}, function(data){
        //         $('#defunidadcurricular-valor').val(data);

        //     });
        // }

        function valores_ganancia(item){

            
            var compra_reserva = $('#tipodecambio-compra_reserva').val();
            var venta_reserva = $('#tipodecambio-venta_reserva').val();
            var ganacia=item.val();

            console.log(compra_reserva);
            console.log(venta_reserva);
            console.log(ganacia);
        
            var porcentaje_compra=parseFloat(compra_reserva*ganacia/100);
            var porcentaje_venta=parseFloat(venta_reserva*ganacia/100);

            console.log("porcentajes");
            console.log(porcentaje_compra);
            console.log(porcentaje_venta);

            var compra=parseFloat(compra_reserva)+parseFloat(porcentaje_compra);
            var venta=parseFloat(venta_reserva)+parseFloat(porcentaje_venta);

            console.log("result");
            console.log(compra);
            console.log(venta);

            $('#tipodecambio-monto').val(compra.toFixed(3));
            $('#tipodecambio-venta').val(venta.toFixed(3));
            
        }


JS;

$this->registerJs($jsc, $this::POS_END);
?>

<div class="tipodecambio-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'codigo')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'operacion')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
    </div>

    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'compra_reserva')->hiddenInput()->label(false) ?>
    </div>
    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'venta_reserva')->hiddenInput()->label(false) ?>
    </div>
    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'porcentaje_ganancia')->hiddenInput()->label(false) ?>
    </div>

   
    <div class="col-md-4 col-sm-4">
        <!-- <?= $form->field($model, 'venta')->textInput()->label('Soles') ?> -->
         <?= $form->field($model, 'venta')->widget(NumberControl::classname(),
                [
                    
                    'maskedInputOptions' => [
                        'groupSeparator' => '.',
                        'radixPoint' => ','
                    ],

                ])->label('Soles')    ?>
        <?= $form->field($model, 'monto')->hiddenInput(['value'=>0])->label(false) ?>
    </div>

    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'fecha')->widget(DatePicker::classname(),[
            'name' => 'fecha', 
            'value' => date('Y-M-D'),
            'options' => ['placeholder' => 'Seleccionar Fecha ...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
            ]
        ]); ?>

    </div>
    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'estatus')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Estatus::find()->where(['tabla'=>'Tipo de Cambio'])->orderBy('descripcion ASC')->all(), 'idstatus','descripcion'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>

    <div class="form-group col-md-12 col-sm-12">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancelar', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

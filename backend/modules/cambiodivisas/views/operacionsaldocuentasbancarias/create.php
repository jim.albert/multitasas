<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Operacionsaldocuentasbancarias */

$this->title = Yii::t('app', 'Registro de  Saldo de Cuentasbancarias');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Saldo de Cuentasbancarias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operacionsaldocuentasbancarias-create">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

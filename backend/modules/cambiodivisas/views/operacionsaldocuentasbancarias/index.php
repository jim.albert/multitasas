<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\cambiodivisas\models\Operacionsaldocuentasbancarias;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\cambiodivisas\models\OperacionsaldocuentasbancariasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Saldo de cuentas bancarias');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operacionsaldocuentasbancarias-index"> 

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>
 
    <p class="pull-right">
        <?= Html::a(Yii::t('app', 'Registrar Saldo de cuentas bancarias'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?> 

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idopersaldocuentas',
            //'id_operacion_empresa',
            [   'attribute' => 'id_operacion_empresa',    
                //'format' =>'amount',// ['decimal',3],
                // 'contentOptions'=>['class'=>'text-right table-cell'],
                // 'headerOptions'=>['class'=>'text-center table-cell'],
                'format' => ['date', 'php: d M Y'],
                'value' => function($data){
                        
                        return $data->operacionEmpresa->fecha_apertura; 
                    },
                    'label'=>'Fecha Operaciones',
                
            ],
            [   'attribute' => 'id_cuentabancaria_sistema',    
                //'format' =>'amount',// ['decimal',3],
                // 'contentOptions'=>['class'=>'text-right table-cell'],
                // 'headerOptions'=>['class'=>'text-center table-cell'],
                'format' => ['raw'],
                'value' => function($data){
                        
                        return $data->cuentabancariasistema->nroctassaldos; 
                    },
                    'label'=>'Cuentas Bancarias',
                
            ],
            //'id_cuentabancaria_sistema',
            //'comprado',
            //'saldo_anetrior',
            
            [   'attribute' => 'saldo_apertura',    
                //'format' =>'amount',// ['decimal',3],
                 'contentOptions'=>['class'=>'text-right table-cell'],
                // 'headerOptions'=>['class'=>'text-center table-cell'],
                'format' => ['raw'],
                'value' => function($data){
                        $saldoapertura = Operacionsaldocuentasbancarias::find()->where(['id_operacion_empresa'=>$data->id_operacion_empresa])
                        ->andWhere(['id_cuentabancaria_sistema'=>$data->id_cuentabancaria_sistema])
                        ->andWhere(['apertura'=>1])
                        ->one();
                        if ($saldoapertura) {
                            return number_format($saldoapertura->saldo_apertura , 2, ',', '.');
                        }
                        
                         
                    },
                    
                
            ],
            //'saldo_cierre',

            ['class' => 'yii\grid\ActionColumn',

                //'header' => 'Acciones',
                'headerOptions' => ['style' => 'color:#337ab7'],
                
                
                'template' => ' {view}  ',
                'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'Ver Detalle'),
                    ]);
                },
 
                // 'update' => function ($url, $model) {
                //     return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                //                 'title' => Yii::t('app', 'update'),
                //     ]);
                // },

                
                
 
                ],

                'urlCreator' => function ($action, $model, $key, $index) {
                /*if ($action === 'update') {
                    $url ='index.php?r=cambiosdivisas/operaciones/view&id='.$model->idoperacion;
                    return $url;
                }*/
                if ($action === 'view') {
                    $url =['/cambiodivisas/operacionsaldocuentasbancarias/detalle?id='.$model->id_operacion_empresa.'&id_cuentabancaria_sistema='.$model->id_cuentabancaria_sistema];
                    return $url;
                }
                // if ($action === 'update') {
                //     $url =['/cambiodivisas/operaciones/update?id='.$model->id_operacion_empresa];
                //     return $url;
                // }
                
                  
 
                }
            ],
        ],
    ]); ?>


</div>

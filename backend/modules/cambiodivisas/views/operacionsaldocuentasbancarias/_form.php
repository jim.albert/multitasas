<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\number\NumberControl;
use backend\modules\cambiodivisas\models\Cuentasbancariadesistema;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Operacionsaldocuentasbancarias */
/* @var $form yii\widgets\ActiveForm */
$jsc = <<< JS

        function saldo_cuenta(){

             var valor_anterior = $('#operacionsaldocuentasbancarias-saldo_anetrior').val();

             var comprado = $('#operacionsaldocuentasbancarias-comprado').val();

            var moto_operacion=parseFloat(valor_anterior)+parseFloat(comprado);
           
            $('#operacionsaldocuentasbancarias-saldo_apertura').val(moto_operacion.toFixed(2));
            $('#operacionsaldocuentasbancarias-saldo_apertura-disp').val(moto_operacion.toFixed(2));
            
        }



JS;

$this->registerJs($jsc, $this::POS_END);
?>

<div class="operacionsaldocuentasbancarias-form">

    <?php $form = ActiveForm::begin(); ?>

    

    <div class="col-md-12 col-sm-12">
        
        <?= $form->field($model, 'id_cuentabancaria_sistema')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Cuentasbancariadesistema::find()->joinWith('operacionSaldocuentasBancarias')->where(['saldos'=>1])->andWhere(['=' ,'saldo_apertura', '0'])->orderBy('alias ASC')->all(), 'idcuenta_bancaria','nroctasselect','paiscuenta'),
                    'language' => 'es',
                    'options' => ['placeholder' => 'Seleccionar una Opción ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); 

                ?>

    </div>

    <div class="col-md-4 col-sm-4">

        
        <?= $form->field($model, "comprado")->widget(NumberControl::classname(),
                [
                    'options' => ['onchange' => 'saldo_cuenta()',],
                    'maskedInputOptions' => [
                        'groupSeparator' => '.',
                        'radixPoint' => ','
                    ], 

                ])    ?>
    </div>


    <div class="col-md-4 col-sm-4">
        
        <?= $form->field($model, "saldo_anetrior")->widget(NumberControl::classname(),
                [
                    'options' => ['onchange' => 'saldo_cuenta()',],
                    'maskedInputOptions' => [
                        'groupSeparator' => '.',
                        'radixPoint' => ','
                    ],

                ])    ?>

    </div>
    <div class="col-md-4 col-sm-4">
        <!-- <?= $form->field($model, 'saldo_apertura')->textInput() ?> -->
        <?= $form->field($model, "saldo_apertura")->widget(NumberControl::classname(),
                [
                    'readonly'=>true, 
                    'maskedInputOptions' => [
                        'groupSeparator' => '.',
                        'radixPoint' => ','
                    ],

                ])->label('Saldo Actual')    ?>
    </div>
    

    <div class="form-group col-md-12 col-sm-12">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancelar', ['/'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

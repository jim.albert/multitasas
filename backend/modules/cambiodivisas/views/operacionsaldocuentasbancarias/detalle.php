<?php

use yii\helpers\Html;
use yii\widgets\DetailView; 
use backend\modules\cambiodivisas\models\Operacionsaldocuentasbancarias;

$model = Operacionsaldocuentasbancarias::find()->where(['id_operacion_empresa'=>$id_operacion_empresa])
        ->andWhere(['id_cuentabancaria_sistema'=>$id_cuentabancaria_sistema])
        ->andWhere(['apertura'=>1])
        ->one();
/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Operacionsaldocuentasbancarias */

$this->title = $model->cuentabancariasistema->nroctasselect;


$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Saldo de cuentas bancarias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="operacionsaldocuentasbancarias-view">


    <h1  class=" text-center bg bg-primary"><?= Html::decode($model->cuentabancariasistema->nroctassaldos) ?></h1>
    <div class="pull-right">
        <span class="operation-details-item">Fecha:</span> <span ><?= \Yii::$app->formatter->asDate(substr($model->operacionEmpresa->fecha_apertura, 0, 10), 'php:d-m-Y')?></span>
    </div>
    <br>
<?php 
$operaciones = Operacionsaldocuentasbancarias::find()->where(['id_operacion_empresa'=>$id_operacion_empresa])
        ->andWhere(['id_cuentabancaria_sistema'=>$id_cuentabancaria_sistema])
        ->orderBy('apertura DESC')
        ->all(); ?>

    
    <table class="table table-striped">
        <thead class="text-center">
            
            <tr class="bg-primary">
                <th scope="col" >Depósito</th>
                <th scope="col">Saldo Anterior</th>
                <th scope="col">Saldo Apertura</th>
                <th scope="col">Saldo Cierre</th>
                <th scope="col">&nbsp;</th>
                
                
            </tr>

            <?php foreach($operaciones as $key => $operacion) { 

                if ($operacion->saldo_apertura<0) {
                                 
                     $class='bg bg-danger text text-danger ';
                } else {
                    
                     $class='bg bg-success text text-success ';
                }
                if ($operacion->apertura==1) {
                    $class='bg bg-information ';
                }
                            ?>

                <tr class="<?=$class?>">
                    <td class="text-right"><?= number_format($operacion->comprado , 2, ',', '.')  ?></td>
                    <td class="text-right"><?= number_format($operacion->saldo_anetrior , 2, ',', '.')  ?></td>
                    <td class="text-right"><?= number_format($operacion->saldo_apertura , 2, ',', '.')  ?></td>
                    <td class="text-right"><?= number_format($operacion->saldo_cierre , 2, ',', '.')  ?></td>
                    
                    <td ><?=Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id'=>$operacion->idopersaldocuentas], ['data-pjax' => 0, 'class' => 'btn btn-danger', 'title' => 'Eliminar',
                            'data' => [
                                'confirm' => '¿Seguro que desea eliminar Ésta Operación?',
                                'method' => 'post',
                            ],]) ?></td>
                    
                    
                </tr>
            <?php } ?>
             
        </thead>
        <tbody>
        </tbody>
    </table>
    <br>
    <div class="col-md-12 col-sm-12">

        <div class="form-group">
           
            <?= Html::a('Cancelar', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
        </div>
    </div>
</div>

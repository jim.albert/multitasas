<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Operacionsaldocuentasbancarias */

$this->title = Yii::t('app', 'Update Operacionsaldocuentasbancarias: {name}', [
    'name' => $model->idopersaldocuentas,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Operacionsaldocuentasbancarias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idopersaldocuentas, 'url' => ['view', 'id' => $model->idopersaldocuentas]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="operacionsaldocuentasbancarias-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

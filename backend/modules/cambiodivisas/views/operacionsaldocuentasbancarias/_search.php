<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\OperacionsaldocuentasbancariasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="operacionsaldocuentasbancarias-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idopersaldocuentas') ?>

    <?= $form->field($model, 'id_operacion_empresa') ?>

    <?= $form->field($model, 'id_cuentabancaria_sistema') ?>

    <?= $form->field($model, 'comprado') ?>

    <?= $form->field($model, 'saldo_anetrior') ?>

    <?php // echo $form->field($model, 'saldo_apertura') ?>

    <?php // echo $form->field($model, 'saldo_cierre') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

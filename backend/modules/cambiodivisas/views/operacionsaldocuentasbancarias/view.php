<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Operacionsaldocuentasbancarias */

$this->title = $model->idopersaldocuentas;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Operacionsaldocuentasbancarias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="operacionsaldocuentasbancarias-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->idopersaldocuentas], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idopersaldocuentas], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idopersaldocuentas',
            'id_operacion_empresa',
            'id_cuentabancaria_sistema',
            'comprado',
            'saldo_anetrior',
            'saldo_apertura',
            'saldo_cierre',
        ],
    ]) ?>

</div>

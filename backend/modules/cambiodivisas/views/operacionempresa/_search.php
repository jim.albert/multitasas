<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\OperacionempresaSearch */
/* @var $form yii\widgets\ActiveForm */
$jsc = <<< JS



function pdf(){

    var desde = $('#operacionempresasearch-desde').val();
    var hasta = $('#operacionempresasearch-hasta').val();


    
    if(desde==""){
        
        alert("Debe Selecionar una fecha de inicio para generar el reporte");
        exit;
    }
    if(hasta==""){
        
        alert("Debe Selecionar una fecha final para generar el reporte");
        exit;
    }
    if(desde>hasta){
        
        alert("La fecha de inicio no puede ser mayor que la fecha final");
        exit;
    }

         window.open("printreportgeneralpdf?desde="+desde+"&hasta="+hasta);

}
function xls(){

    var desde = $('#operacionempresasearch-desde').val();
    var hasta = $('#operacionempresasearch-hasta').val();


    
    if(desde==""){
        
        alert("Debe Selecionar una fecha de inicio para generar el reporte");
        exit;
    }
    if(hasta==""){
        
        alert("Debe Selecionar una fecha final para generar el reporte");
        exit;
    }
    if(desde>hasta){
        
        alert("La fecha de inicio no puede ser mayor que la fecha final");
        exit;
    }

    
         window.open("printreportgeneralxls?desde="+desde+"&hasta="+hasta);

}

JS;

$this->registerJs($jsc, $this::POS_END);
?>

<div class="operacionempresa-search">

    <?php $form = ActiveForm::begin([
        //'action' => ['/cambiodivisas/operacionempresa/reporteGeneral'],
        //'method' => 'get',
    ]); ?>

    <div class="col-md-12 col-sm-12" style="background-color: #eeeeee;">
        <h4 ><strong>Fecha de apertura</strong></h4>
    </div>

    <div class="col-md-6 col-sm-6">

        
         <?= $form->field($model, 'desde')->widget(DatePicker::classname(),[
            'name' => 'fecha', 
            'value' => date('Y-M-D'),
            'options' => ['placeholder' => 'Seleccionar Fecha ...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
            ]
        ]); ?>
    </div>
    <div class="col-md-6 col-sm-6">

        
         <?= $form->field($model, 'hasta')->widget(DatePicker::classname(),[
            'name' => 'fecha', 
            'value' => date('Y-M-D'),
            'options' => ['placeholder' => 'Seleccionar Fecha ...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
            ]
        ]); ?>
    </div>
   

    <?php // echo $form->field($model, 'porcentaje_pagador') ?>

    <?php // echo $form->field($model, 'porcentaje_pmovil') ?>

    <?php // echo $form->field($model, 'porcentaje_interbancario') ?>

    <?php // echo $form->field($model, 'saldo_anterior') ?>

    <?php // echo $form->field($model, 'estatus') ?>

    <div class="form-group col-md-12 col-sm-12 text-center">
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span>',null, ['target'=>'_blank','class' => 'btn btn-danger','onClick'  =>'pdf();', 'title'=>'Ver en Pdf']) ?>

        <?= Html::a('<span class="glyphicon glyphicon-export"></span>',null, ['target'=>'_blank','class' => 'btn btn-success','onClick'  =>'xls();','title'=>'Ver en Excel']) ?>
        
        <?= Html::resetButton('<span class="glyphicon glyphicon-repeat"></span>', ['class' => 'btn btn-outline-secondary ', 'title'=>'Limpiar']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\cambiodivisas\models\Tipodecambio;
use backend\modules\cambiodivisas\models\Operacionsaldocuentasbancarias;
use backend\modules\cambiodivisas\models\Cuentabancariasistemasaldos;
use backend\modules\cambiodivisas\models\Operacionempresa;
use backend\modules\cambiodivisas\models\Operacionesusuarios;
/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Operacionempresa */
/* @var $form yii\widgets\ActiveForm */

$model=Operacionempresa::find()->where(['estatus' => '0'])
        ->andFilterWhere(['between', 'fecha_apertura', $desde, $hasta ])->all();



 //$estimado_sol=$apertura['porcentaje_agentes']
?>
<div class="operacionempresacierre-form">
    
    <div class="col-md-12 col-sm-12  ">
        <?php 
            foreach($model as $key => $model) { 

                //$tipocambio=Tipodecambio::find()->where(['estatus' => '9', 'operacion'=>'CAMBIO'])
                // ->andFilterWhere(['between', 'fecha', $desde, $hasta ])
                // ->orderBy('codigo ASC')->one();

                // $tiporemesa=Tipodecambio::find()->where(['estatus' => '9', 'operacion'=>'REMESA'])->orderBy('codigo ASC')->one();
         ?>
        <div class="bg bg-information">
           
            <table>
            <tr bgcolor="#343a40">
                <td >
                    
           
                        <span class="operation-details-item" ><strong>Fecha de Apertura:</span> <span ><?= $model->fecha_apertura ?></strong></span>
                         
                   
                </td>  
                <td >
                    
           
                         <div class="pull-right">
                            <span class="operation-details-item"><strong>Fecha de Cierre:</span>
                        
                             <span ><?= $model->fecha_cierre ?></strong></span> 

                        </div>
                    
                </td>    
            </tr>
        </table>
        </div>
       

 </div>

    <div class="col-md-12 col-sm-12 " style="margin-top: 10px;">

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

        <?php

            $saldoapert=Operacionsaldocuentasbancarias::find()
                ->where(['id_operacion_empresa' => $model->idoperacionempresa])
                ->andFilterWhere(['>', 'saldo_apertura','0' ])->groupBy('id_cuentabancaria_sistema')
                ->orderBy('id_cuentabancaria_sistema ASC')->all();

            foreach($saldoapert as $key => $value) { ?>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id=<?= "heading".$value->idopersaldocuentas ?>>
                      <div class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href=<?= "#collapse".$value->idopersaldocuentas ?> aria-expanded="false" aria-controls=<?= "collapse".$value->idopersaldocuentas ?>>
                            <b><?= $value->cuentabancariasistema->alias ?> > <?= $value->cuentabancariasistema->moneda->monedas ?></b>
                            
                        </a>
                          <br>
                           <?php   
                            $saldo_apertura=Operacionsaldocuentasbancarias::find()
                                    ->where(['id_operacion_empresa' => $model->idoperacionempresa])
                                    ->andWhere(['id_cuentabancaria_sistema'=>$value->id_cuentabancaria_sistema])
                                    ->andWhere(['apertura'=>1])
                                    ->sum('saldo_apertura');

                            $saldoacumulado=Operacionsaldocuentasbancarias::find()
                                    ->where(['id_operacion_empresa' => $model->idoperacionempresa])
                                    ->andWhere(['id_cuentabancaria_sistema'=>$value->id_cuentabancaria_sistema])
                                    ->andWhere(['apertura'=>0])
                                    ->sum('saldo_apertura');
                           
                            $acumulado=Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['=', 'id_operacion_empresa',$model->idoperacionempresa ])->sum('monto_operacion');
                
                              $saldo=($saldo_apertura+$saldoacumulado)-$acumulado;

                            if ($saldo<=0) {
                                 //echo ' <span class="bg bg-danger text-danger pull-right operation-details-item">Disponible:  <span>'. number_format($saldo , 2, ',', '.')  .'</span></span>';
                                 $class='#f2dede';
                            } else {
                                // echo ' <span class="bg bg-success text-success pull-right operation-details-item">Disponible:  <span>'. number_format($saldo , 2, ',', '.')  .'</span></span>';
                                 $class='#dff0d8';

                            }
                            $nroopercaiones=Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['=', 'id_operacion_empresa',$model->idoperacionempresa ])->count();
                    
                            
                        ?>
                        <table class="table table-striped">
                            <thead class="text-center">
                                
                                <tr >
                                    <th scope="col" class="bg-info text-info" bgcolor="#d9edf7">Apertura: <?= number_format($saldo_apertura , 2, ',', '.') ?></th>
                                    <th scope="col" class="bg-primary text-primary" bgcolor="#337ab7">Saldo Actualizado: <?= number_format($saldoacumulado , 2, ',', '.') ?></th>
                                    <th scope="col" class="bg-warning text-warning" bgcolor="#fcf8e3">Total Pagado: <?= number_format($acumulado , 2, ',', '.') ?></th>
                                    <th scope="col" bgcolor="<?= $class ?>">Disponible: <?= number_format($saldo , 2, ',', '.') ?></th>
                                </tr>
                                 
                            </thead>
                        </table>
                        
                        
                      </div>
                    </div>
                    <div id=<?= "collapse".$value->idopersaldocuentas ?> class="panel-collapse collapse" role="tabpanel" aria-labelledby=<?= "heading".$value->idopersaldocuentas ?>>
                      <div class="panel-body">
<?php if ($nroopercaiones) { ?>
                        <table class="table table-striped">
                        <thead class="text-center">
                            
                            <tr class="bg-primary" bgcolor="#337ab7" >
                                <th scope="col" ><font color="#ffffff">Beneficiario</font></th>

                                <th scope="col" ><font color="#ffffff">Banco</font></th>
                                <th scope="col" ><font color="#ffffff">Nro de Cuenta</font></th>
                                
                                <!-- <th scope="col">Monto Envía</th>
                                <th scope="col">Moneda</span></th>
    -->
                                <th scope="col"><font color="#ffffff">Monto Envía</font></th>
                                <th scope="col"><font color="#ffffff">Moneda</font></th>
                                <th scope="col"><font color="#ffffff">Tasa</font></th>
                                <th scope="col"><font color="#ffffff">Monto Recibe</font></th>
                                
                                <th scope="col"><font color="#ffffff">Usuario Pagador</font></th>

                                <th scope="col"><font color="#ffffff">Com. Pagador</font></th>
                                <th scope="col"><font color="#ffffff">Com. Interbancario</font></th>
                                <th scope="col"><font color="#ffffff">Com. Pago Móvil</font></th>

                                <th scope="col"><font color="#ffffff">Agente</font></th>
                                <th scope="col"><font color="#ffffff">Com. Agentes</font></th>

                                <th scope="col"><font color="#ffffff">Sub-total</font></th>
                                
                            </tr>
                             
                        </thead>
                        <tbody>


                        <?php
                            $detalleoper=Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['=', 'id_operacion_empresa',$model->idoperacionempresa ])->all();
                            $totalMontoEnvio=0;
                            $totalMontoRecibe=0;

                            foreach($detalleoper as $key => $detalleoper) {

                                $Operador=Operacionesusuarios::find()->where(['id_operacion'=>$detalleoper->id_operacion, 'tipo_comision'=>'Operador'])->one(); 

                                    if ($Operador) {
                                        $pagador= $Operador->usuarioinfo;
                                    } else {
                                       $pagador= '-';
                                    }

                                $agente=Operacionesusuarios::find()->where(['id_operacion'=>$detalleoper->id_operacion, 'tipo_comision'=>'Agente'])->one(); 

                                        if ($agente) {
                                            $usuagente=$agente->usuarioinfo;
                                        } else {
                                           $usuagente='-';
                                        }

                                $totalMontoEnvio=$totalMontoEnvio+$detalleoper->operacion->monto_envio;
                                $totalMontoRecibe=$totalMontoRecibe+$detalleoper->operacion->monto_recibe;

                            ?>
                            
                                <tr>
                                    <td ><?= $detalleoper->operacion->cuentabancariabsuario->cuentanrodetallecierre  ?>
                                        
                                    </td>

                                     <td ><?= $detalleoper->operacion->cuentabancariabsuario->banco->descripcion  ?>
                                        
                                    </td>
                                     <td ><?= '<span class="operation-details-bank">"'.$detalleoper->operacion->cuentabancariabsuario->nro_cuenta.'"</span>'  ?>
                                        
                                    </td>
                                    <!-- <td ><?= number_format($detalleoper->operacion->monto_envio , 2, ',', '.')  ?></td>
                                    <td ><?= $detalleoper->operacion->tipoMonedaEnvia->monedas  ?></td> -->
                                    <td style="text-align: right;"><?= number_format($detalleoper->operacion->monto_envio , 2, ',', '.')  ?></td>
                                    <td ><?= $detalleoper->operacion->tipoMonedaEnvia->monedas  ?></td>
                                    <td style="text-align: right;"><?= number_format($detalleoper->operacion->monto_cambio_usado , 2, ',', '.')  ?></td>
                                    <td style="text-align: right;"><?= number_format($detalleoper->operacion->monto_recibe , 2, ',', '.')  ?></td>

                                    <td ><?=  $pagador  ?> </td>
                                    
                                    <td style="text-align: right;"><?= number_format($detalleoper->comision_pagador , 2, ',', '.')  ?></td>
                                    <td style="text-align: right;"><?= number_format($detalleoper->comision_interbancario , 2, ',', '.')  ?></td>
                                    <td style="text-align: right;"><?= number_format($detalleoper->comision_pmovil , 2, ',', '.')  ?></td>

                                    <td ><?=  $usuagente  ?> </td>

                                    <td style="text-align: right;"><?= number_format($detalleoper->comision_agentes , 2, ',', '.')  ?></td>
                                    <td style="text-align: right;"><?= number_format($detalleoper->monto_operacion , 2, ',', '.')  ?></td>
                                    
                                </tr>
                          <?php  } ?>

                        </tbody>
                        </table>
                        <table class="table ">
                        <thead class="text-center">
                            
                            <tr bgcolor="#dddddd">
                                <th scope="col" >&nbsp;</th>

                                <th scope="col" >&nbsp;</th>
                                <th scope="col" >&nbsp;</th>
                                <th scope="col"  bgcolor="#dddddd" style="text-align: right;"><?= number_format($totalMontoEnvio, 2, ',', '.') ?></th>
                                <th scope="col" >&nbsp;</th>
                                <th scope="col" >&nbsp;</th>
                                <th scope="col" ><?= number_format($totalMontoRecibe, 2, ',', '.') ?></th>
                                <th scope="col" >&nbsp;</th>
                                

                                <th scope="col" bgcolor="#dddddd" style="text-align: right;"><?= number_format(Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['=', 'id_operacion_empresa',$model->idoperacionempresa ])->sum('comision_pagador'), 2, ',', '.') ?></span></th>
                                <th scope="col" bgcolor="#dddddd" style="text-align: right;"><?= number_format(Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['=', 'id_operacion_empresa',$model->idoperacionempresa ])->sum('comision_interbancario'), 2, ',', '.') ?></span></th>
                                <th scope="col" bgcolor="#dddddd" style="text-align: right;"><?= number_format(Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['=', 'id_operacion_empresa',$model->idoperacionempresa ])->sum('comision_pmovil'), 2, ',', '.') ?></span></th>
                                <th scope="col" >&nbsp;</th>
                                <th scope="col" bgcolor="#dddddd" style="text-align: right;"><?= number_format(Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['=', 'id_operacion_empresa',$model->idoperacionempresa ])->sum('comision_agentes'), 2, ',', '.') ?></span></th>
                                <th scope="col" bgcolor="#dddddd" style="text-align: right;"><?= number_format(Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['=', 'id_operacion_empresa',$model->idoperacionempresa ])->sum('monto_operacion'), 2, ',', '.') ?></span></th>
                                
                            </tr>
                             
                        </thead>
                        
                        </table>

                      </div>
                    </div>
                  </div>
                
                 
    <?php } ?>   
                    
       <?php     }  ?>
                       <!-- </div>
                       </div>
                    </div>
                  </div> 
            </div>-->
        </div>
        <?php     }  ?>
    </div>

    
</div>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\cambiodivisas\models\Tipodecambio;
use backend\modules\cambiodivisas\models\Operacionsaldocuentasbancarias;
use backend\modules\cambiodivisas\models\Cuentabancariasistemasaldos;
/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Operacionempresa */
/* @var $form yii\widgets\ActiveForm */
 $session = Yii::$app->session;
 $apertura=$session['apertura'];

$tipocambio=Tipodecambio::find()->where(['estatus' => '9', 'operacion'=>'CAMBIO'])->orderBy('codigo ASC')->one();

$tiporemesa=Tipodecambio::find()->where(['estatus' => '9', 'operacion'=>'REMESA'])->orderBy('codigo ASC')->one();


 $estimado_sol=$apertura['porcentaje_agentes']
?>
<div class="operacionempresacierre-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-12 col-sm-12 ">
        <div>
           
            <span class="operation-details-item">Fecha de Apertura:</span> <span ><?= $model->fecha_apertura ?></span>
             <div class="pull-right">
                <span class="operation-details-item">Fecha de Cierre:</span>
            
                 <span ><?= $model->fecha_cierre ?></span> 
            </div>
        </div>
       
        
        
            <div class="col-md-4 col-sm-4 text-center list-group list-group-item bg bg-success">
                
                <span class="h4 operation-details-item ">Tasa al Mayor (Remesas)</span> 
                <br>
                    <span class="operation-details-item">S/-</span> <?= number_format($model->tasa_mayor_soles , 2, ',', '.') ?>
                <br>
                
                    <span class="operation-details-item">$-</span> <?= number_format($model->tasa_mayor_dolar , 2, ',', '.') ?>
                
            </div>
            
            <div class="col-md-4 col-sm-4 text-center list-group list-group-item">
                
                <span class="h4 operation-details-item ">Tipo de Cambio (Remesas)</span> 
                <br>
                    <span class="operation-details-item">S/-</span> <?= number_format($tiporemesa->venta , 2, ',', '.') ?>
                
                <br>
                    <span class="operation-details-item">$-</span> <?= number_format($tiporemesa->monto , 2, ',', '.') ?>
                
            </div>
            

            <div class="col-md-4 col-sm-4 text-center list-group list-group-item">
                
                <span class="h4 operation-details-item ">Tipo de Cambio</span> 
               <br>
                    <span class="operation-details-item">Compra: </span> <?= number_format($tipocambio->monto , 3, ',', '.') ?>
                <br>
                    <span class="operation-details-item">Venta: </span> <?= number_format($tipocambio->venta , 3, ',', '.') ?>
                
            </div>

 </div>

    <div class="col-md-12 col-sm-12 " style="margin-top: 10px;">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                    <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingpanel">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsepanel" aria-expanded="false" aria-controls="collapsepanel">
                           Detalle operaciones <i class="glyphicon glyphicon-expand pull-right text-success"></i>
                        </a>
                      </h4>
                    </div>
                    <div id="collapsepanel" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingpanel">
                      <div class="panel-body">

                         <div class="panel-group" id="accordionosaldos" role="tablist" aria-multiselectable="true">

                            <?php

                                $saldoapert=Operacionsaldocuentasbancarias::find()
                                    ->where(['id_operacion_empresa' => $apertura->idoperacionempresa])
                                    ->andFilterWhere(['>', 'saldo_apertura','0' ])
                                    ->orderBy('id_cuentabancaria_sistema ASC')->all();

                                foreach($saldoapert as $key => $value) { 

                                    echo '<div > <div class=" col-md-12 col-sm-12 bg bg-info text-info  ">';
                                    echo '<center><b>'.$value->cuentabancariasistema->alias.' > '.$value->cuentabancariasistema->moneda->monedas.' </b></center>';

                                    echo '<span class="operation-details-item">Apertura:  <span>'.  number_format($value->saldo_apertura , 2, ',', '.').'</span></span>';

                                    $acumulado=Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['like', 'fecha',date('Y-m-d') ])->sum('monto_operacion');
                                    
                                        $saldo=$value->saldo_apertura-$acumulado;

                                        if ($saldo<=0) {
                                             echo ' <span class="bg bg-danger text-danger pull-right operation-details-item">Disponible:  <span>'. number_format($saldo , 2, ',', '.')  .'</span></span>';
                                        } else {
                                             echo ' <span class="bg bg-success text-success pull-right operation-details-item">Disponible:  <span>'. number_format($saldo , 2, ',', '.')  .'</span></span>';
                                        }
                                        
                                     echo '</div></div>';

                                    ?>

                                     
                                      <table class="table table-striped">
                                        <thead class="text-center">
                                            
                                            <tr class="bg-primary">
                                                <th scope="col" >Beneficiario</th>
                                                
                                                <!-- <th scope="col">Monto Envía</th>
                                                <th scope="col">Moneda</span></th>
     -->
                                                <th scope="col">Tasa</span></th>
                                                <th scope="col">Monto Recibe</span></th>
                                                
                                                <th scope="col">Com. Pagador</span></th>
                                                <th scope="col">Com. Interbancario</span></th>
                                                <th scope="col">Com. Pago Móvil</span></th>
                                                <th scope="col">Com. Agentes</span></th>

                                                <th scope="col">Sub-total</span></th>
                                                
                                            </tr>
                                             
                                        </thead>
                                        <tbody>

                                        <?php
                                            $detalleoper=Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['like', 'fecha',date('Y-m-d') ])->all();

                                            foreach($detalleoper as $key => $detalleoper) {

                                            ?>
                                            
                                                <tr>
                                                    <td ><?= $detalleoper->operacion->cuentabancariabsuario->cuentanrodetallecierre  ?>
                                                        <br>
                                                        <span class="operation-details-bank">Monto Envía: </span><?= number_format($detalleoper->operacion->monto_envio , 2, ',', '.')  ?>
                                                        <br>
                                                        <span class="operation-details-bank">Moneda: </span><?= $detalleoper->operacion->tipoMonedaEnvia->monedas  ?>
                                                    </td>
                                                    <!-- <td ><?= number_format($detalleoper->operacion->monto_envio , 2, ',', '.')  ?></td>
                                                    <td ><?= $detalleoper->operacion->tipoMonedaEnvia->monedas  ?></td> -->
                                                    <td ><?= number_format($detalleoper->operacion->monto_cambio_usado , 2, ',', '.')  ?></td>
                                                    <td ><?= number_format($detalleoper->operacion->monto_recibe , 2, ',', '.')  ?></td>
                                                    

                                                    <td ><?= number_format($detalleoper->comision_pagador , 2, ',', '.')  ?></td>
                                                    <td ><?= number_format($detalleoper->comision_interbancario , 2, ',', '.')  ?></td>
                                                    <td ><?= number_format($detalleoper->comision_pmovil , 2, ',', '.')  ?></td>
                                                    <td ><?= number_format($detalleoper->comision_agentes , 2, ',', '.')  ?></td>
                                                    <td ><?= number_format($detalleoper->monto_operacion , 2, ',', '.')  ?></td>
                                                    
                                                </tr>
                                          <?php  } ?>
                                        </tbody>
                                        </table>
                           <?php     }  ?>
                        </div>
                      </div>
                    </div>
                  </div>
    
            </div>
    </div>


        
        <div class="col-md-12 col-sm-12 ">
            

             <h2 class="h3 text-center bg bg-information" style="margin-bottom: 0">Cierre</h2>
        </div>
   
    <div class="col-md-12 col-sm-12">

       

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Procesar'), ['class' => 'btn btn-success']) ?>
            <?= Html::a('Cancelar', ['/site/index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    
</div>
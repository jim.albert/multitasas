<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\cambiodivisas\models\Cuentasbancariadesistema;
use backend\modules\cambiodivisas\models\Operacionsaldocuentasbancarias;
use kartik\number\NumberControl;
/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Operacionempresa */
/* @var $form yii\widgets\ActiveForm */

$jsc = <<< JS


        // function valor(item){


        //     var id = $('#defunidadcurricular-regimen_id').val();
        //     //alert(id);
        //     $.get('index.php?r=mallacurricular/defregimen/get-valor', {id : id}, function(data){
        //         $('#defunidadcurricular-valor').val(data);

        //     });
        // }

        function valores_ganancia(item){

            
            var compra_reserva = $('#tipodecambio-compra_reserva').val();
            var venta_reserva = $('#tipodecambio-venta_reserva').val();
            var ganacia=item.val();

            console.log(compra_reserva);
            console.log(venta_reserva);
            console.log(ganacia);
        
            var porcentaje_compra=parseFloat(compra_reserva*ganacia/100);
            var porcentaje_venta=parseFloat(venta_reserva*ganacia/100);

            console.log("porcentajes");
            console.log(porcentaje_compra);
            console.log(porcentaje_venta);

            var compra=parseFloat(compra_reserva)+parseFloat(porcentaje_compra);
            var venta=parseFloat(venta_reserva)+parseFloat(porcentaje_venta);

            console.log("result");
            console.log(compra);
            console.log(venta);

            $('#tipodecambio-monto').val(compra.toFixed(3));
            $('#tipocambiomodel-monto').val(compra.toFixed(3));

            $('#tipodecambio-monto-disp').val(compra.toFixed(3));
            
            
            $('#tipodecambio-venta').val(venta.toFixed(2));
            $('#tipocambiomodel-venta').val(venta.toFixed(2));

            $('#tipodecambio-venta-disp').val(venta.toFixed(2));
            
        }

        function ganancia_tasa_dolar(item){

            
            var tasa_mayor = $('#operacionempresa-tasa_mayor_dolar').val();
            var ganacia=item.val();
                // console.log(tasa_mayor);
                // console.log(ganacia);
        
            var porcentaje_tasa=parseFloat(tasa_mayor*ganacia/100);

                // console.log("porcentajes");
                // console.log(porcentaje_tasa);
           
            var tasa=parseFloat(tasa_mayor)+parseFloat(porcentaje_tasa);
            
                // console.log("result");
                // console.log(tasa);
            
             $('#tiporemesamodel-monto').val(tasa.toFixed(3));
            
        }

        function ganancia_tasa_soles(item){

            
            var tasa_mayor = $('#operacionempresa-tasa_mayor_soles').val();
            var ganacia=item.val();
                // console.log(tasa_mayor);
                // console.log(ganacia);
        
            var porcentaje_tasa=parseFloat(tasa_mayor*ganacia/100);

                // console.log("porcentajes");
                // console.log(porcentaje_tasa);
           
            var tasa=parseFloat(tasa_mayor)+parseFloat(porcentaje_tasa);
            
                // console.log("result");
                // console.log(tasa);
            
             $('#tiporemesamodel-venta').val(tasa.toFixed(2));
            
        }

        function saldo_cuenta(item,id){

             var valor_anterior = $('#operacionsaldocuentasbancarias-'+id+'-saldo_anetrior').val();

             var comprado = $('#operacionsaldocuentasbancarias-'+id+'-comprado').val();

             //var comprado=item.val();

            var moto_operacion=parseFloat(valor_anterior)+parseFloat(comprado);
           
                 console.log(item.val());
                 console.log(id);
                 console.log(moto_operacion);
        
            $('#operacionsaldocuentasbancarias-'+id+'-saldo_apertura').val(moto_operacion.toFixed(2));
            $('#operacionsaldocuentasbancarias-'+id+'-saldo_apertura-disp').val(moto_operacion.toFixed(2));
            
        }



JS;

$this->registerJs($jsc, $this::POS_END);
?>

<div class="operacionempresa-form">

    <?php $form = ActiveForm::begin(); ?>
    <h2 class="h3 text-center bg bg-information" style="margin-bottom: 0">Porcentajes</h2>
    <div class="col-md-12 col-sm-12 panel panel-default panel-heading">
        
        <div class="col-md-3 col-sm-3">

            <?= $form->field($model, 'porcentaje_pagador')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3 col-sm-3">
            <?= $form->field($model, 'porcentaje_pmovil')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3 col-sm-3">
            <?= $form->field($model, 'porcentaje_interbancario')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3 col-sm-3">
            <?= $form->field($model, 'porcentaje_agentes')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    
    <div class="col-md-12 col-sm-12 ">
        
        <h2 class="h3 text-center bg bg-information" style="margin-bottom: 0">Tasas (Envío de Dinero)</h2>
        <div class="col-md-12 col-sm-12 panel panel-default panel-heading">
            <div class="col-md-3 col-sm-3">

                <?= $form->field($model, 'tasa_mayor_dolar')->hiddenInput()->label(false)?>
                

            </div>
            <div class="col-md-3 col-sm-3">
                <?= $form->field($model, 'porcentaje_dolar')->hiddenInput()->label(false) ?>
            </div>
            <div class="col-md-3 col-sm-3">

                <?= $form->field($model, 'tasa_mayor_soles')->hiddenInput()->label(false) ?>
                

            </div>
            <div class="col-md-3 col-sm-3">
                <?= $form->field($model, 'porcentaje_soles')->hiddenInput()->label(false) ?>
                <?= $form->field($tiporemesamodel, 'monto')->hiddenInput(['value'=>0])->label(false) ?>
            </div>

            <!-- <div class="col-md-6 col-sm-6"> -->
                <!-- <?= $form->field($tiporemesamodel, 'monto')->textInput(['id'=>'tiporemesamodel-monto', 'name'=>'tiporemesamodel-monto'])->label('Tasa del Día Dolares') ?> -->

                <!-- <?= $form->field($tiporemesamodel, 'monto')->widget(NumberControl::classname(),
                [
                    'options' => [
                        
                        'id'=>'tiporemesamodel-monto', 
                        'name'=>'tiporemesamodel-monto',
                    ],
                    
                    'maskedInputOptions' => [
                    'groupSeparator' => '.',
                    'radixPoint' => ','
                ],

                ])->label('Tasa del Día Dolares')     ?> -->
            <!-- </div> -->
            <div class="col-md-6 col-sm-6">
                <!-- <?= $form->field($tiporemesamodel, 'venta')->textInput(['id'=>'tiporemesamodel-venta', 'name'=>'tiporemesamodel-venta', ])->label('Tasa del Día Soles') ?> -->
                <?= $form->field($tiporemesamodel, 'venta')->widget(NumberControl::classname(),
                [
                    'options' => [
                        
                        'id'=>'tiporemesamodel-venta', 
                        'name'=>'tiporemesamodel-venta',
                    ],
                    'maskedInputOptions' => [
                        'groupSeparator' => '.',
                        'radixPoint' => ','
                    ],

                ])->label('Tasa del Día Soles')     ?>

            </div>
        </div>
    </div>

   
    <div class="col-md-12 col-sm-12 ">
        <h2 class="h3 text-center bg bg-information" style="margin-bottom: 0">Tipo de Cambio</h2>
        <div class="col-md-12 col-sm-12 panel panel-default panel-heading">
            <div class="col-md-6 col-sm-6">
                 <?= $form->field($tipocambiomodel, 'compra_reserva')->textInput() ?> 
               <!--  <?= $form->field($tipocambiomodel, 'compra_reserva')->widget(NumberControl::classname(),
                [
                    
                    'maskedInputOptions' => [
                        'groupSeparator' => '.',
                        'radixPoint' => ','
                    ],

                ])    ?> -->
                
            </div>
            <div class="col-md-6 col-sm-6">
                 <?= $form->field($tipocambiomodel, 'venta_reserva')->textInput() ?> 
                 <?= $form->field($tipocambiomodel, 'porcentaje_ganancia')->hiddenInput()->label(false) ?> 
                <!-- <?= $form->field($tipocambiomodel, 'venta_reserva')->widget(NumberControl::classname(),
                [
                    
                    'maskedInputOptions' => [
                        'groupSeparator' => '.',
                        'radixPoint' => ','
                    ],

                ])    ?> -->
                

            </div>
           <!--  <div class="col-md-4 col-sm-4"> -->
                 
                <!-- <?= $form->field($tipocambiomodel, 'porcentaje_ganancia')->widget(NumberControl::classname(),
                [

                    'options' => [
                        'onchange' => 'valores_ganancia($(this))',
                        
                        'name'=>'tipocambiomodel-porcentaje_ganancia',
                    ],
                    'maskedInputOptions' => [
                        'groupSeparator' => '.',
                        'radixPoint' => ','
                    ],

                ])    ?> -->
            <!-- </div> -->

            <div class="col-md-6 col-sm-6">
                <!-- <?= $form->field($tipocambiomodel, 'monto')->textInput(['readonly'=>true ]) ?> -->
                <?= $form->field($tipocambiomodel, 'monto')->widget(NumberControl::classname(),
                [
                    
                    'options' => [
                        
                        
                        'name'=>'tipocambiomodel-monto',
                    ],
                    
                    'maskedInputOptions' => [
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'digits' => 3,
                    ],

                ])    ?>

                
            </div>
            <div class="col-md-6 col-sm-6">
                <!-- <?= $form->field($tipocambiomodel, 'venta')->textInput(['readonly'=>true]) ?> -->
                <?= $form->field($tipocambiomodel, 'venta')->widget(NumberControl::classname(),
                [
                    
                    'options' => [
                        
                        
                        'name'=>'tipocambiomodel-venta',
                    ],
                    
                    'maskedInputOptions' => [
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'digits' => 3,
                    ],

                ])    ?>

            </div>
    </div>

    <div class="col-md-12 col-sm-12 ">
        <h2 class="h3 text-center bg bg-information " style="margin-bottom: 0">Cuentas Bancarias</h2>

        <?php 
                $cuentaBancaria=Cuentasbancariadesistema::find()->where(['saldos'=>1])->all(); 

                foreach ($cuentaBancaria as $i => $cuentaBancaria) {

                $opercierre=Operacionsaldocuentasbancarias::find()->where(['id_cuentabancaria_sistema'=>$cuentaBancaria->idcuenta_bancaria])->andFilterWhere(['cierre'=>1])->orderBy('idopersaldocuentas DESC')->one();

                //echo $opercierre->saldo_cierre;
                if ($opercierre) {
                    $saldo_anetrior=$opercierre->saldo_cierre;
                } else {
                    $saldo_anetrior=0;
                }
                

                //$modelcuentasbanco[$i]->saldo_anetrior=$opercierre->saldo_cierre;

         ?>

        <div class="col-md-12 col-sm-12 center-block panel panel-default panel-heading" style="margin-bottom: 5px">

            <div class="col-md-3 col-sm-3  " >
                <?= $cuentaBancaria->nroctas ?>
                <?= $form->field($modelcuentasbanco[0], "[{$i}]id_cuentabancaria_sistema")->hiddenInput(['value' => $cuentaBancaria->idcuenta_bancaria])->label(false) ?>
                
            </div>
            <!-- <div class="col-md-4 col-sm-4 " >
                <?php // $form->field($modelcuentasbanco[0], "[{$i}]saldo_cierre")->textInput([]) ?>
            </div> -->
            <div class="col-md-3 col-sm-3 ">
                <!-- <?= $form->field($modelcuentasbanco[0], "[{$i}]comprado")->textInput(['onchange' => 'saldo_cuenta($(this),'.$i.')']) ?> -->
                <?= $form->field($modelcuentasbanco[0], "[{$i}]comprado")->widget(NumberControl::classname(),
                [
                    'options' => ['onchange' => 'saldo_cuenta($(this),'.$i.')',],
                    'maskedInputOptions' => [
                        'groupSeparator' => '.',
                        'radixPoint' => ','
                    ],

                ])    ?>
            </div>
            <div class="col-md-3 col-sm-3 ">
                <!-- <?= $form->field($modelcuentasbanco[0], "[{$i}]saldo_anetrior")->textInput(['readonly'=>true, 'value'=>$saldo_anetrior]) ?> -->
                <?= $form->field($modelcuentasbanco[0], "[{$i}]saldo_anetrior")->widget(NumberControl::classname(),
                [
                    'options' => [
                        'onchange' => 'saldo_cuenta($(this),'.$i.')',
                        'value' => $saldo_anetrior
                    ],
                    
                    //'readonly'=>true, 
                    'maskedInputOptions' => [
                        'groupSeparator' => '.',
                        'radixPoint' => ','
                    ],

                ])    ?>
            </div>
            <div class="col-md-3 col-sm-3 ">
                <!-- <?= $form->field($modelcuentasbanco[0], "[{$i}]saldo_apertura")->textInput(['readonly'=>true]) ?> -->
                <?= $form->field($modelcuentasbanco[0], "[{$i}]saldo_apertura")->widget(NumberControl::classname(),
                [
                    'readonly'=>true, 
                    'maskedInputOptions' => [
                        'groupSeparator' => '.',
                        'radixPoint' => ','
                    ],

                ])    ?>
            </div>
        </div>
        <?php } ?>
        
    </div>

    <br>
    <div class="col-md-12 col-sm-12">

        <?= $form->field($model, 'count_cuentabancarias')->hiddenInput(['value'=>$i])->label(false) ?>

        <?= $form->field($model, 'saldo_cierre')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'fecha_apertura')->hiddenInput()->label(false)  ?>

        <?= $form->field($model, 'fecha_cierre')->hiddenInput()->label(false)  ?>

        <?= $form->field($model, 'estatus')->hiddenInput()->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            <?= Html::a('Cancelar', ['/site/index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>

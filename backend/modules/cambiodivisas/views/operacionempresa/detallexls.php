<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Operacionempresa */

$this->title = Yii::t('app', 'Resumen de Operaciones al: '.$model->fecha_apertura);


header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename='.$this->title.'.xls');

?>
<div class="operacionempresa-create">

    <!-- +<h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_detallexls', [
        'model' => $model,
        
    ]) ?>

</div>

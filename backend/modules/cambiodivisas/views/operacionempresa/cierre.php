<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Operacionempresa */

$this->title = Yii::t('app', 'Cierre de Operaciones');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Operacionempresas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operacionempresa-create">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formcierre', [
        'model' => $model,
        
    ]) ?>

</div>

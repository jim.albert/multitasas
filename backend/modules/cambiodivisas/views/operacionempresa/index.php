<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\cambiodivisas\models\OperacionesempresaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Operaciones Diarias');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operacionempresa-index">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idoperacionempresa',
            // 'fecha_apertura',
            // 'fecha_cierre',
            [
                //'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'fecha_apertura',    
                'format' => ['date', 'php: d M Y'],

                'filter' => DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'fecha_apertura', 
                //'value' => date('Y-M-D'),
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => ['placeholder' => 'Seleccionar Fecha ...'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                    ]
                
                ]),
                
            ],
            [
                //'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'fecha_cierre',    
                'format' => ['date', 'php: d M Y'],
                'filter' => DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'fecha_cierre', 
                //'value' => date('Y-M-D'),
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => ['placeholder' => 'Seleccionar Fecha ...'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                    ]
                
                ]),
                
            ],
            //'porcentaje_pagador',
            //'porcentaje_interbancario',
            //'porcentaje_pmovil',
            //'porcentaje_agentes',
            //'tasa_mayor_dolar',
            //'tasa_mayor_soles',
            //'porcentaje_dolar',
            //'porcentaje_soles',
            //'saldo_apertura',
            //'saldo_cierre',
            //'saldo_anterior',
            //'estatus',

            ['class' => 'yii\grid\ActionColumn',

                'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => " {print} {view} ",
                'buttons' => [
                /*'verifica' => function ($url, $model) {
                    return Html::a('<span class="text-warning glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'Verificar'),
                    ]);
                },*/
 
                'print' => function ($url, $model) {
                    return Html::a('<span class="text-success glyphicon glyphicon-download-alt"></span>', $url, [
                                'title' => Yii::t('app', 'Descargar'),
                    ]);
                },

                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'Ver'),
                    ]);
                },
                
 
                ],

                'urlCreator' => function ($action, $model, $key, $index) {
                /*if ($action === 'update') {
                    $url ='index.php?r=cambiosdivisas/operaciones/view&id='.$model->idoperacion;
                    return $url;
                }*
                if ($action === 'verifica') {
                    $url =['/cambiodivisas/operaciones/verifica?id='.$model->idoperacionempresa];
                    return $url;
                }*/
                if ($action === 'print') {
                    $url =['/cambiodivisas/operacionempresa/print?id='.$model->idoperacionempresa];
                    return $url;
                }
                if ($action === 'view') {
                    $url =['/cambiodivisas/operacionempresa/detalle?id='.$model->idoperacionempresa];
                    return $url;
                }
                  
 
                }
            ],
        ],
    ]); ?>


</div>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\cambiodivisas\models\Tipodecambio;
use backend\modules\cambiodivisas\models\Operacionsaldocuentasbancarias;
use backend\modules\cambiodivisas\models\Cuentabancariasistemasaldos;
use backend\modules\cambiodivisas\models\Operacionesusuarios;
/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Operacionempresa */
/* @var $form yii\widgets\ActiveForm */
 $session = Yii::$app->session;
 $apertura=$session['apertura'];
 
$tipocambio=Tipodecambio::find()->where(['estatus' => '9', 'operacion'=>'CAMBIO'])->orderBy('codigo ASC')->one();

$tiporemesa=Tipodecambio::find()->where(['estatus' => '9', 'operacion'=>'REMESA'])->orderBy('codigo ASC')->one();


 //$estimado_sol=$apertura['porcentaje_agentes']
?>
<div class="operacionempresacierre-form">
    
    <div class="col-md-12 col-sm-12 ">
        <table>
            <tr>
                <td bgcolor="#f5f5f5">
                    <div class="col-md-6 col-sm-6" >
           
                        <span class="operation-details-item" ><strong>Fecha de Apertura:</span> <span ><?= $model->fecha_apertura ?></strong></span>
                         
                    </div>
                </td>  
                <td bgcolor="#f5f5f5">
                    <div class="col-md-6 col-sm-6" >
           
                         <div class="pull-right">
                            <span class="operation-details-item"><strong>Fecha de Cierre:</span>
                        
                             <span ><?= $model->fecha_cierre ?></strong></span> 

                        </div>
                    </div>
                </td>    
            </tr>
        </table>
        
        <table>
            <tr>
                <td bgcolor="#d9edf7">
                    <div class="col-md-6 col-sm-6 text-center list-group list-group-item">
                
                <span class="h4 operation-details-item ">Tasa del Día (Envío de Dinero)</span> 
                <br>
                    <span class="operation-details-item">S/-</span> <?= number_format($tiporemesa->venta , 2, ',', '.') ?>
                <br>
                    <span class="operation-details-item">$-</span> <?= number_format($tiporemesa->monto , 2, ',', '.') ?>
                
            </div>
                </td>  
                <td bgcolor="#d9edf7">
                    <div class="col-md-6 col-sm-6 text-center list-group list-group-item">
                
                        <span class="h4 operation-details-item ">Tipo de Cambio</span> 
                       <br>
                            <span class="operation-details-item">Compra: </span> <?= number_format($tipocambio->monto , 3, ',', '.') ?>
                        <br>
                            <span class="operation-details-item">Venta: </span> <?= number_format($tipocambio->venta , 3, ',', '.') ?>
                        
                    </div>
                </td>    
            </tr>
        </table>
        
       
        
        
            <!-- <div class="col-md-4 col-sm-4 text-center list-group list-group-item bg bg-success">
                
                <span class="h4 operation-details-item ">Tasa al Mayor (Remesas)</span> 
                <br>
                    <span class="operation-details-item">S/-</span> <?= number_format($model->tasa_mayor_soles , 2, ',', '.') ?>
                <br>
                
                    <span class="operation-details-item">$-</span> <?= number_format($model->tasa_mayor_dolar , 2, ',', '.') ?>
                
            </div>
            
            <div class="col-md-4 col-sm-4 text-center list-group list-group-item">
                
                <span class="h4 operation-details-item ">Tipo de Cambio (Remesas)</span> 
                <br>
                    <span class="operation-details-item">S/-</span> <?= number_format($tiporemesa->venta , 2, ',', '.') ?>
                
                <br>
                    <span class="operation-details-item">$-</span> <?= number_format($tiporemesa->monto , 2, ',', '.') ?>
                
            </div>
            

            <div class="col-md-4 col-sm-4 text-center list-group list-group-item">
                
                <span class="h4 operation-details-item ">Tipo de Cambio</span> 
               <br>
                    <span class="operation-details-item">Compra: </span> <?= number_format($tipocambio->monto , 3, ',', '.') ?>
                <br>
                    <span class="operation-details-item">Venta: </span> <?= number_format($tipocambio->venta , 3, ',', '.') ?>
                
            </div> -->

 </div>

    <div class="col-md-12 col-sm-12 " style="margin-top: 10px;">
           
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

        <?php

            $saldoapert=Operacionsaldocuentasbancarias::find()
                ->where(['id_operacion_empresa' => $model->idoperacionempresa])
                ->andFilterWhere(['>', 'saldo_apertura','0' ])->groupBy('id_cuentabancaria_sistema')
                ->orderBy('id_cuentabancaria_sistema ASC')->all();

            foreach($saldoapert as $key => $value) { ?>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id=<?= "heading".$value->idopersaldocuentas ?>>
                      <div class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href=<?= "#collapse".$value->idopersaldocuentas ?> aria-expanded="false" aria-controls=<?= "collapse".$value->idopersaldocuentas ?>>
                            <b><?= $value->cuentabancariasistema->alias ?> > <?= $value->cuentabancariasistema->moneda->monedas ?></b>
                            <span class="pull-right">Detalle <i class="glyphicon glyphicon-chevron-down"></i></span>
                        </a>
                          <br>
                           <?php   

                           $saldo_apertura=Operacionsaldocuentasbancarias::find()
                                    ->where(['id_operacion_empresa' => $model->idoperacionempresa])
                                    ->andWhere(['id_cuentabancaria_sistema'=>$value->id_cuentabancaria_sistema])
                                    ->andWhere(['apertura'=>1])
                                    ->sum('saldo_apertura');

                            $saldoacumulado=Operacionsaldocuentasbancarias::find()
                                    ->where(['id_operacion_empresa' => $model->idoperacionempresa])
                                    ->andWhere(['id_cuentabancaria_sistema'=>$value->id_cuentabancaria_sistema])
                                    ->andWhere(['apertura'=>0])
                                    ->sum('saldo_apertura');
                           
                           
                           //$saldo_apertura=Operacionsaldocuentasbancarias::find()->where(['id_operacion_empresa' => $model->idoperacionempresa])->andWhere(['id_cuentabancaria_sistema'=>$value->id_cuentabancaria_sistema])->sum('saldo_apertura');
                           

                            $acumulado=Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['=', 'id_operacion_empresa',$model->idoperacionempresa ])->sum('monto_operacion');
                
                             //$saldo=$saldo_apertura-$acumulado;
                            $saldo=($saldo_apertura+$saldoacumulado)-$acumulado;

                            if ($saldo<=0) {
                                 //echo ' <span class="bg bg-danger text-danger pull-right operation-details-item">Disponible:  <span>'. number_format($saldo , 2, ',', '.')  .'</span></span>';
                                 $class='#f2dede';
                            } else {
                                // echo ' <span class="bg bg-success text-success pull-right operation-details-item">Disponible:  <span>'. number_format($saldo , 2, ',', '.')  .'</span></span>';
                                 $class='#dff0d8';

                            }
                        $nroopercaiones=Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['=', 'id_operacion_empresa',$model->idoperacionempresa ])->count();
                            
                        ?>
                        <table class="table table-striped">
                            <thead class="text-center">
                                
                                <tr >
                                    <th scope="col" class="bg-info text-info" bgcolor="#d9edf7">Apertura: <?= number_format($saldo_apertura , 2, ',', '.') ?></th>
                                    <th scope="col" class="bg-primary text-primary" bgcolor="#337ab7">Saldo Actualizado: <?= number_format($saldoacumulado , 2, ',', '.') ?></th>
                                    <th scope="col" class="bg-warning text-warning" bgcolor="#fcf8e3">Total Pagado: <?= number_format($acumulado , 2, ',', '.') ?></th>
                                    <th scope="col" bgcolor="<?= $class ?>">Disponible: <?= number_format($saldo , 2, ',', '.') ?></th>
                                </tr>
                                 
                            </thead>
                        </table>
                        
                        
                      </div>
                    </div>
                    <div id=<?= "collapse".$value->idopersaldocuentas ?> class="panel-collapse collapse" role="tabpanel" aria-labelledby=<?= "heading".$value->idopersaldocuentas ?>>
                      <div class="panel-body">
<?php if ($nroopercaiones) { ?>
                        <table class="table table-striped">
                        <thead class="text-center">
                            
                            <tr class="bg-primary" bgcolor="#337ab7" >
                                <th scope="col" ><font color="#ffffff">Beneficiario</font></th>

                                <th scope="col" ><font color="#ffffff">Banco</font></th>
                                <th scope="col" ><font color="#ffffff">Nro de Cuenta</font></th>

                                
                                
                                <!-- <th scope="col">Monto Envía</th>
                                <th scope="col">Moneda</span></th>
    -->
                                <th scope="col"><font color="#ffffff">Monto Envía</font></th>
                                <th scope="col"><font color="#ffffff">Moneda</font></th>
                                <th scope="col"><font color="#ffffff">Tasa</font></th>
                                <th scope="col"><font color="#ffffff">Monto Recibe</font></th>
                                
                                <th scope="col"><font color="#ffffff">Usuario Pagador</font></th>

                                <th scope="col"><font color="#ffffff">Com. Pagador</font></th>
                                <th scope="col"><font color="#ffffff">Com. Interbancario</font></th>
                                <th scope="col"><font color="#ffffff">Com. Pago Móvil</font></th>

                                <th scope="col"><font color="#ffffff">Agente</font></th>
                                <th scope="col"><font color="#ffffff">Com. Agentes</font></th>

                                <th scope="col"><font color="#ffffff">Sub-total</font></th>
                                
                            </tr>
                             
                        </thead>
                        <tbody>


                        <?php
                            $detalleoper=Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['=', 'id_operacion_empresa',$model->idoperacionempresa ])->all();
                            $totalMontoEnvio=0;
                            $totalMontoRecibe=0;

                            foreach($detalleoper as $key => $detalleoper) {

                                $Operador=Operacionesusuarios::find()->where(['id_operacion'=>$detalleoper->id_operacion, 'tipo_comision'=>'Operador'])->one(); 

                                    if ($Operador) {
                                        $pagador= $Operador->usuarioinfo;
                                    } else {
                                       $pagador= '-';
                                    }

                                $agente=Operacionesusuarios::find()->where(['id_operacion'=>$detalleoper->id_operacion, 'tipo_comision'=>'Agente'])->one(); 

                                        if ($agente) {
                                            $usuagente=$agente->usuarioinfo;
                                        } else {
                                           $usuagente='-';
                                        }

                                $totalMontoEnvio=$totalMontoEnvio+$detalleoper->operacion->monto_envio;
                                $totalMontoRecibe=$totalMontoRecibe+$detalleoper->operacion->monto_recibe;

                            ?>
                            
                                <tr>
                                    <td ><?= $detalleoper->operacion->cuentabancariabsuario->cuentanrodetallecierre  ?>
                                        
                                    </td>

                                     <td ><?= $detalleoper->operacion->cuentabancariabsuario->banco->descripcion  ?>
                                        
                                    </td>
                                     <td ><?= '<span class="operation-details-bank">"'.$detalleoper->operacion->cuentabancariabsuario->nro_cuenta.'"</span>'  ?>
                                        
                                    </td>

                                   

                                    <!-- <td ><?= number_format($detalleoper->operacion->monto_envio , 2, ',', '.')  ?></td>
                                    <td ><?= $detalleoper->operacion->tipoMonedaEnvia->monedas  ?></td> -->
                                    <td style="text-align: right;"><?= number_format($detalleoper->operacion->monto_envio , 2, '.', ',')  ?></td>
                                    <td ><?= $detalleoper->operacion->tipoMonedaEnvia->monedas  ?></td>
                                    <td style="text-align: right;"><?= number_format($detalleoper->operacion->monto_cambio_usado , 2, ',', '.')  ?></td>
                                    <td style="text-align: right;"><?= number_format($detalleoper->operacion->monto_recibe , 2, '.', ',')  ?></td>

                                    <td ><?=  $pagador  ?> </td>
                                    
                                    <td style="text-align: right;"><?= number_format($detalleoper->comision_pagador , 2, '.', ',')  ?></td>
                                    <td style="text-align: right;"><?= number_format($detalleoper->comision_interbancario , 2, '.', ',')  ?></td>
                                    <td style="text-align: right;"><?= number_format($detalleoper->comision_pmovil , 2, '.', ',')  ?></td>

                                    <td ><?=  $usuagente  ?> </td>

                                    <td style="text-align: right;"><?= number_format($detalleoper->comision_agentes , 2, '.', ',')  ?></td>
                                    <td style="text-align: right;"><?= number_format($detalleoper->monto_operacion , 2, '.', ',')  ?></td>
                                    
                                </tr>
                          <?php  } ?>

                        </tbody>
                        </table>
                        <table class="table ">
                        <thead class="text-center">
                            
                            <tr bgcolor="#dddddd">
                                <th scope="col" >&nbsp;</th>

                                <th scope="col" >&nbsp;</th>
                                <th scope="col" >&nbsp;</th>
                                <th scope="col"  bgcolor="#dddddd" style="text-align: right;"><?= number_format($totalMontoEnvio, 2, ',', '.') ?></th>
                                <th scope="col" >&nbsp;</th>
                                <th scope="col" >&nbsp;</th>
                                <th scope="col" ><?= number_format($totalMontoRecibe, 2, ',', '.') ?></th>
                                <th scope="col" >&nbsp;</th>
                                

                                <th scope="col" bgcolor="#dddddd" style="text-align: right;"><?= number_format(Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['=', 'id_operacion_empresa',$model->idoperacionempresa ])->sum('comision_pagador'), 2, ',', '.') ?></span></th>
                                <th scope="col" bgcolor="#dddddd" style="text-align: right;"><?= number_format(Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['=', 'id_operacion_empresa',$model->idoperacionempresa ])->sum('comision_interbancario'), 2, ',', '.') ?></span></th>
                                <th scope="col" bgcolor="#dddddd" style="text-align: right;"><?= number_format(Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['=', 'id_operacion_empresa',$model->idoperacionempresa ])->sum('comision_pmovil'), 2, ',', '.') ?></span></th>
                                <th scope="col" >&nbsp;</th>
                                <th scope="col" bgcolor="#dddddd" style="text-align: right;"><?= number_format(Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['=', 'id_operacion_empresa',$model->idoperacionempresa ])->sum('comision_agentes'), 2, ',', '.') ?></span></th>
                                <th scope="col" bgcolor="#dddddd" style="text-align: right;"><?= number_format(Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['=', 'id_operacion_empresa',$model->idoperacionempresa ])->sum('monto_operacion'), 2, ',', '.') ?></span></th>
                                
                            </tr>
                             
                        </thead>
                        
                        </table>
        <?php } ?>                        

                      </div>
                    </div>
                  </div>
                
                 
                  
                    
 <?php     }  ?>

 <table class="table table-striped">
                        <thead class="text-center">
                            
                            <tr class="bg bg-information text-center" bgcolor="#343a40" >
                                <th scope="col" colspan="2" class="text-center"><font color="#ffffff" class="text-center">Comisiones de Operadores</font></th>

                            </tr>
                            
                             
                        </thead>
                        <thead class="text-center">
                            
                            <tr class="bg-primary text-center" bgcolor="#337ab7" >
                                <th scope="col" ><font color="#ffffff">Operador</font></th>

                                <th scope="col" ><font color="#ffffff">Comisión</font></th>
                               
                                
                            </tr>
                             
                        </thead>


                        <?php $Operadores=Operacionesusuarios::find()
                                ->where(['tipo_comision'=>'Operador'])
                                ->andFilterWhere(['!=', 'id_usuario', '29'])
                                ->groupBy('id_usuario')
                                ->orderBy('id_usuario ASC')
                                ->all();  

                               
                        foreach($Operadores as $key => $Operadores) {

                                $operacionComisiones=Operacionesusuarios::find()
                                ->where(['id_usuario'=>$Operadores->id_usuario])
                                ->all();  
                                $Totalcomision=0;
                                foreach($operacionComisiones as $key => $operacionComisiones) {

                                    $comision=Cuentabancariasistemasaldos::find()->Where(['=', 'id_operacion_empresa',$model->idoperacionempresa ])
                                    ->andFilterWhere(['id_operacion'=>$operacionComisiones->id_operacion])->one();

                                    if ($comision) {
                                       $Totalcomision=$Totalcomision+$comision->comision_pagador;
                                    }
                                
                                }
                            ?>
                            <tr  >

                                <td scope="col" ><?=  $Operadores->usuarioinfo;  ?></td>
                                <td scope="col" ><?= number_format($Totalcomision, 2, ',', '.') ?></td>
                                 
                            </tr>

                        <?php } ?>

            </table>

         <table class="table table-striped">
                <thead class="text-center">
                    
                    <tr class="bg bg-information text-center" bgcolor="#343a40" >
                        <th scope="col" colspan="2" class="text-center"><font color="#ffffff" class="text-center">Comisiones de Agentes</font></th>

                    </tr>
                    
                </thead>
                <thead class="text-center">
                    
                    <tr class="bg-primary text-center" bgcolor="#337ab7" >
                        <th scope="col" ><font color="#ffffff">Agente</font></th>
                        <th scope="col" ><font color="#ffffff">Comisión</font></th>
                        
                    </tr>
                     
                </thead>
                <?php  

                    $Agentes=Operacionesusuarios::find()
                    ->where(['tipo_comision'=>'Agente'])
                    ->groupBy('id_usuario')
                    ->orderBy('id_usuario ASC')
                    ->all();  

                foreach($Agentes as $key => $Agentes) {
                    $operacionComisionesagente=Operacionesusuarios::find()
                    ->where(['id_usuario'=>$Agentes->id_usuario])
                    ->all();  
                    $Totalcomisionagente=0;
                    foreach($operacionComisionesagente as $key => $operacionComisionesagente) {

                        $comisionagente=Cuentabancariasistemasaldos::find()->Where(['=', 'id_operacion_empresa',$model->idoperacionempresa ])
                        ->andFilterWhere(['id_operacion'=>$operacionComisionesagente->id_operacion])->one();

                        if ($comisionagente) {
                           $Totalcomisionagente=$Totalcomisionagente+$comisionagente->comision_agentes;
                        }
                        
                    
                    }

            ?>
                <tr  >
                    
                    <td scope="col" ><?=  $Agentes->usuarioinfo;  ?></td>
                    <td scope="col" ><?= number_format($Totalcomisionagente, 2, ',', '.') ?></td>
                                    
                </tr>

            <?php } ?>

        </table>
                       <!-- </div>
                       </div>
                    </div>
                  </div> 
            </div>-->
        </div>
    </div>


    
    
</div>
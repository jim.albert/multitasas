<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\cambiodivisas\models\Tipodecambio;
use backend\modules\cambiodivisas\models\Operacionsaldocuentasbancarias;
use backend\modules\cambiodivisas\models\Cuentabancariasistemasaldos;
use backend\modules\cambiodivisas\models\Operacionempresa;
use backend\modules\cambiodivisas\models\Operacionesusuarios;
/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Operacionempresa */
/* @var $form yii\widgets\ActiveForm */

//$model=Operacionempresa::find()->where(['estatus' => '0'])
  //      ->andFilterWhere(['between', 'fecha_apertura', $desde, $hasta ])->all();



 //$estimado_sol=$apertura['porcentaje_agentes']
$desde=$desde.' 00:00:00';
$hasta=$hasta.' 23:59:59';
?>
<div class="operacionempresacierre-form">
    
   
    <div class="col-md-12 col-sm-12 " style="margin-top: 10px;">

  
            <table class="table table-striped">
                        <thead class="text-center">
                            
                            <tr class="bg-primary" bgcolor="#337ab7" >
                                <!-- <th scope="col" ><font color="#ffffff">Id</font></th> -->
                                <th scope="col" ><font color="#ffffff">Beneficiario</font></th>

                                <th scope="col" ><font color="#ffffff">Fecha</font></th>
                                

                                <th scope="col" ><font color="#ffffff">Banco</font></th>
                                <th scope="col" ><font color="#ffffff">Nro de Cuenta</font></th>
                                
                                <!-- <th scope="col">Monto Envía</th>
                                <th scope="col">Moneda</span></th>
    -->
                                <th scope="col"><font color="#ffffff">Monto Envía</font></th>
                                <th scope="col"><font color="#ffffff">Moneda</font></th>
                                <th scope="col"><font color="#ffffff">Tasa</font></th>
                                <th scope="col"><font color="#ffffff">Monto Recibe</font></th>
                                
                                <th scope="col"><font color="#ffffff">Usuario Pagador</font></th>

                                <th scope="col"><font color="#ffffff">Com. Pagador</font></th>
                                <th scope="col"><font color="#ffffff">Com. Interbancario</font></th>
                                <th scope="col"><font color="#ffffff">Com. Pago Móvil</font></th>

                                <th scope="col"><font color="#ffffff">Agente</font></th>
                                <th scope="col"><font color="#ffffff">Com. Agentes</font></th>

                                <th scope="col"><font color="#ffffff">Sub-total</font></th>
                                
                            </tr>
                             
                        </thead> 
                        

        <?php

            //foreach($model as $key => $model) { 

            
       

                //$saldoapert=Operacionsaldocuentasbancarias::find()
                //->where(['id_operacion_empresa' => $model->idoperacionempresa])
                //->andFilterWhere(['>', 'saldo_apertura','0' ])
                //->andFilterWhere(['between', 'fecha', $desde, $hasta ])
                //->groupBy('id_cuentabancaria_sistema')
                //->orderBy('id_cuentabancaria_sistema ASC')->all();

                //foreach($saldoapert as $key => $value) { ?>


                        <?php
                            $detalleoper=Cuentabancariasistemasaldos::find()->joinWith('operacion')
                            ->where(['operaciones.estatus'=>24 ])
                            //->where(['between', 'fecha', $desde, $hasta])
                            ->andFilterWhere(['>=', 'fecha', $desde ])
                            ->andFilterWhere(['<=', 'fecha', $hasta ])
                            ->orderBy('id_operacion ASC')
                            ->all();
                            $totalMontoEnvio=0;
                            $totalMontoRecibe=0;

                            $totalComisionOperador=0;

                            foreach($detalleoper as $key => $detalleoper) {

                                $Operador=Operacionesusuarios::find()->where(['id_operacion'=>$detalleoper->id_operacion, 'tipo_comision'=>'Operador'])->one(); 

                                    //$newoper[]=$Operador->id_usuario;

                                    if ($Operador) {

                                        $pagador= $Operador->usuarioinfo;
                                        
                                        

                                    } else {
                                       $pagador= '-';
                                    }

                                $agente=Operacionesusuarios::find()->where(['id_operacion'=>$detalleoper->id_operacion, 'tipo_comision'=>'Agente'])->one(); 

                                        if ($agente) {
                                            $usuagente=$agente->usuarioinfo;
                                        } else {
                                           $usuagente='-';
                                        }

                                

                                $totalMontoEnvio=$totalMontoEnvio+$detalleoper->operacion->monto_envio;
                                $totalMontoRecibe=$totalMontoRecibe+$detalleoper->operacion->monto_recibe;

                                //$fechaoperacion=;

                            ?>
                           
                            
                                <tr>
                                    <!-- <td ><?= $detalleoper->id_operacion  ?>
                                        
                                    </td> -->
                                     <td ><?= $detalleoper->operacion->cuentabancariabsuario->cuentanrodetallecierre  ?>
                                        
                                    </td>

                                     <td ><?=  \Yii::$app->formatter->asDate(substr($detalleoper->fecha, 0, 10), 'short');  ?>
                                        
                                    </td>
                                     
                                        
                                    </td>
                                     <td ><?= $detalleoper->operacion->cuentabancariabsuario->banco->descripcion  ?>
                                        
                                    </td>
                                     <td ><?= '<span class="operation-details-bank">"'.$detalleoper->operacion->cuentabancariabsuario->nro_cuenta.'"</span>'  ?>
                                        
                                    </td>
                                    <!-- <td ><?= number_format($detalleoper->operacion->monto_envio , 2, ',', '.')  ?></td>
                                    <td ><?= $detalleoper->operacion->tipoMonedaEnvia->monedas  ?></td> -->
                                    <td style="text-align: right;"><?= number_format($detalleoper->operacion->monto_envio , 2, '.', ',')  ?></td>
                                    <td ><?= $detalleoper->operacion->tipoMonedaEnvia->monedas  ?></td>
                                    <td style="text-align: right;"><?= number_format($detalleoper->operacion->monto_cambio_usado , 2, '.', ',')  ?></td>
                                    <td style="text-align: right;"><?= number_format($detalleoper->operacion->monto_recibe , 2, '.', ',')  ?></td>

                                    <td ><?=  $pagador  ?> </td>
                                    
                                    <td style="text-align: right;"><?= number_format($detalleoper->comision_pagador , 2, '.', ',')  ?></td>
                                    <td style="text-align: right;"><?= number_format($detalleoper->comision_interbancario , 2, '.', ',')  ?></td>
                                    <td style="text-align: right;"><?= number_format($detalleoper->comision_pmovil , 2, '.', ',')  ?></td>

                                    <td ><?=  $usuagente  ?> </td>

                                    <td style="text-align: right;"><?= number_format($detalleoper->comision_agentes , 2, '.', ',')  ?></td>
                                    <td style="text-align: right;"><?= number_format($detalleoper->monto_operacion , 2, '.', ',')  ?></td>
                                    
                                </tr>
                          <?php  } ?>

                        
                        
           
                    
       <?php    // }  ?>
                       <!-- </div>
                       </div>
                    </div>
                  </div> 
            </div>-->
           
                        
     
        <?php    // }  ?>
         </table>
        <table class="table ">
                            <thead class="text-center">
                                
                                <tr bgcolor="#dddddd">
                                    <th scope="col" >&nbsp;</th>

                                    <th scope="col" >&nbsp;</th>
                                    <th scope="col" >&nbsp;</th>
                                    <th scope="col" >&nbsp;</th>
                                    
                                    <th scope="col"  bgcolor="#dddddd" style="text-align: right;"><?= number_format($totalMontoEnvio, 2, ',', '.') ?></th>
                                    <th scope="col" >&nbsp;</th>
                                    <th scope="col" >&nbsp;</th>
                                    <th scope="col" ><?= number_format($totalMontoRecibe, 2, ',', '.') ?></th>
                                    <th scope="col" >&nbsp;</th>
                                    

                                    <th scope="col" bgcolor="#dddddd" style="text-align: right;"><?= number_format(Cuentabancariasistemasaldos::find()->where(['between', 'fecha', $desde, $hasta ])->sum('comision_pagador'), 2, ',', '.') ?></span></th>
                                    <th scope="col" bgcolor="#dddddd" style="text-align: right;"><?= number_format(Cuentabancariasistemasaldos::find()->where(['between', 'fecha', $desde, $hasta ])->sum('comision_interbancario'), 2, ',', '.') ?></span></th>
                                    <th scope="col" bgcolor="#dddddd" style="text-align: right;"><?= number_format(Cuentabancariasistemasaldos::find()->where(['between', 'fecha', $desde, $hasta ])->sum('comision_pmovil'), 2, ',', '.') ?></span></th>
                                    <th scope="col" >&nbsp;</th>
                                    <th scope="col" bgcolor="#dddddd" style="text-align: right;"><?= number_format(Cuentabancariasistemasaldos::find()->where(['between', 'fecha', $desde, $hasta ])->sum('comision_agentes'), 2, ',', '.') ?></span></th>
                                    <th scope="col" bgcolor="#dddddd" style="text-align: right;"><?= number_format(Cuentabancariasistemasaldos::find()->where(['between', 'fecha', $desde, $hasta])->sum('monto_operacion'), 2, ',', '.') ?></span></th>
                                    
                                </tr>
                                 
                            </thead>
                        
                        </table>

             <table class="table table-striped">
                        <thead class="text-center">
                            
                            <tr class="bg bg-information text-center" bgcolor="#343a40" >
                                <th scope="col" colspan="2" class="text-center"><font color="#ffffff" class="text-center">Comisiones de Operadores</font></th>

                            </tr>
                            
                             
                        </thead>
                        <thead class="text-center">
                            
                            <tr class="bg-primary text-center" bgcolor="#337ab7" >
                                <th scope="col" ><font color="#ffffff">Operador</font></th>

                                <th scope="col" ><font color="#ffffff">Comisión</font></th>
                               
                                
                            </tr>
                             
                        </thead>


                        <?php $Operadores=Operacionesusuarios::find()
                                ->where(['tipo_comision'=>'Operador'])
                                ->andFilterWhere(['!=', 'id_usuario', '29'])
                                ->groupBy('id_usuario')
                                ->orderBy('id_usuario ASC')
                                ->all();  

                               
                        foreach($Operadores as $key => $Operadores) {

                                $operacionComisiones=Operacionesusuarios::find()
                                ->where(['id_usuario'=>$Operadores->id_usuario])
                                ->all();  
                                $Totalcomision=0;
                                foreach($operacionComisiones as $key => $operacionComisiones) {

                                    /*$comision=Cuentabancariasistemasaldos::find()->where(['between', 'fecha', $desde, $hasta])
                                    ->andFilterWhere(['id_operacion'=>$operacionComisiones->id_operacion])->one();*/

                                    $comision=Cuentabancariasistemasaldos::find()
                                    ->where(['id_operacion'=>$operacionComisiones->id_operacion ])
                                    //->where(['between', 'fecha', $desde, $hasta])
                                    ->andFilterWhere(['>=', 'fecha', $desde ])
                                    ->andFilterWhere(['<=', 'fecha', $hasta ])
                                    //->orderBy('id_operacion ASC')
                                    ->one();                    

                                    if ($comision) {
                                       $Totalcomision=$Totalcomision+$comision->comision_pagador;
                                    }
                                
                                }
                            ?>
                            <tr  >

                                <td scope="col" ><?=  $Operadores->usuarioinfo;  ?></td>
                                <td scope="col" ><?= number_format($Totalcomision, 2, ',', '.') ?></td>
                                 
                            </tr>

                        <?php } ?>

            </table>

         <table class="table table-striped">
                <thead class="text-center">
                    
                    <tr class="bg bg-information text-center" bgcolor="#343a40" >
                        <th scope="col" colspan="2" class="text-center"><font color="#ffffff" class="text-center">Comisiones de Agentes</font></th>

                    </tr>
                    
                </thead>
                <thead class="text-center">
                    
                    <tr class="bg-primary text-center" bgcolor="#337ab7" >
                        <th scope="col" ><font color="#ffffff">Agente</font></th>
                        <th scope="col" ><font color="#ffffff">Comisión</font></th>
                        
                    </tr>
                     
                </thead>
                <?php  

                    $Agentes=Operacionesusuarios::find()
                    ->where(['tipo_comision'=>'Agente'])
                    ->groupBy('id_usuario')
                    ->orderBy('id_usuario ASC')
                    ->all();  

                foreach($Agentes as $key => $Agentes) {
                    $operacionComisionesagente=Operacionesusuarios::find()
                    ->where(['id_usuario'=>$Agentes->id_usuario])
                    ->all();  
                    $Totalcomisionagente=0;
                    foreach($operacionComisionesagente as $key => $operacionComisionesagente) {

                        $comisionagente=Cuentabancariasistemasaldos::find()->where(['between', 'fecha', $desde, $hasta])
                        ->andFilterWhere(['id_operacion'=>$operacionComisionesagente->id_operacion])->one();

                        if ($comisionagente) {
                           $Totalcomisionagente=$Totalcomisionagente+$comisionagente->comision_agentes;
                        }
                        
                    
                    }

            ?>
                <tr  >
                    
                    <td scope="col" ><?=  $Agentes->usuarioinfo;  ?></td>
                    <td scope="col" ><?= number_format($Totalcomisionagente, 2, ',', '.') ?></td>
                                    
                </tr>

            <?php } ?>

        </table>

    </div>

    
</div>
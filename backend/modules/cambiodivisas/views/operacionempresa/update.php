<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Operacionempresa */

$this->title = Yii::t('app', 'Update Operacionempresa: {name}', [
    'name' => $model->idoperacionempresa,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Operacionempresas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idoperacionempresa, 'url' => ['view', 'id' => $model->idoperacionempresa]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="operacionempresa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Operacionempresa */

$this->title = Yii::t('app', 'Operaciones Desde:'.$desde.' Hasta:'.$hasta);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Operaciones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operacionempresa-create">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_generalpdf', [
        'desde' => $desde, 'hasta' => $hasta,
        
    ]) ?>

</div>

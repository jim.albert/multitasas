<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\cambiodivisas\models\OperacionesempresaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Operaciones Diarias');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operacionempresa-index">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>


    <?php  echo $this->render('_search', ['model' => $model]); ?>

    


</div>
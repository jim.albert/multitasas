<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\widgets\SwitchInput;
use yii\helpers\ArrayHelper;
use backend\modules\cambiodivisas\models\Usuario; 
use backend\modules\cambiodivisas\models\Perfil; 
/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Usuarioperfil */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuarioperfil-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <div class="col-md-6 col-sm-6">
        <?= $form->field($model, 'id_usuario')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Usuario::find()->orderBy('id ASC')->all(), 'id','datosusuarios'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-md-6 col-sm-6">
        <?= $form->field($model, 'id_perfil')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Perfil::find()->where(['estatus'=>'28'])->orderBy('id ASC')->all(), 'id','descripcion'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-md-12 col-sm-12">
        <h3 class="text-center bg bg-info text text-info">Notificaciones</h3>
        <div class="col-md-4 col-sm-4">
            <?= $form->field($model, 'email_notification')->widget(SwitchInput::classname(), []) ?>
        </div>
        <div class="col-md-4 col-sm-4">
            <?= $form->field($model, 'telefono_notification')->widget(SwitchInput::classname(), []) ?>
        </div>
        <div class="col-md-4 col-sm-4">
            
            <?= $form->field($model, 'telefono_numero')->textInput([]) ?>
        </div>
    </div>

    

   <div class="form-group col-md-12 col-sm-12">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancelar', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

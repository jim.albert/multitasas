<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Usuarioperfil */

$this->title = Yii::t('app', 'Asignar Perfil');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Perfiles de usuarios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuarioperfil-create">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>


<?php

//use kartik\export\ExportMenu; 
use kartik\grid\GridView;
use kartik\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\cambiodivisas\models\Perfil;
use backend\modules\cambiodivisas\models\Usuario;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\cambiodivisas\models\EstatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Asignación de Perfil a Usuarios');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estatus-index">

    

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php 
    

    $gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],

    
            
            // 'id',
            // 'id_usuario',
            // 'id_perfil',
            //'color',
            //'fecha',
            [
                //'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'id_usuario', 
                'value' => 'usuario.datosusuarios', 
                //'label'=>'Estatus',
                'vAlign' => 'middle',
                
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Usuario::find()->orderBy('id ASC')->all(), 'id','datosusuarios'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Opciones'],
                'format' => 'raw'
                
            ],
                        
            [
                //'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'id_perfil', 
                'value' => 'perfil.descripcion', 
                //'label'=>'Estatus',
                'vAlign' => 'middle',
                
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Perfil::find()->orderBy('descripcion ASC')->all(), 'id','descripcion'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Opciones'],
                'format' => 'raw'
                
            ],
            
            [
                'attribute' => 'email_notification',
                'format'=>'raw',
                'label' => 'Email ',
                'value'=> function ($model){

                    if ($model->email_notification==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            [
                'attribute' => 'telefono_notification',
                'format'=>'raw',
                'label' => 'Teléfono ',
                'value'=> function ($model){

                    if ($model->telefono_notification==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
                'telefono_numero',

    ['class' => 'kartik\grid\ActionColumn', ]
            //['class' => 'kartik\grid\CheckboxColumn']
    ];


    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,

        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        
        'resizableColumns'=>true,
        
        'toolbar' =>  [
            ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success', 'title' => 'Registrar Estatus']) . ' '.
                    
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Refrescar Consulta'])
            ],
            '{export}',
            '{toggleData}'
        ],

        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => false, 
        
        'panel' => [
            'heading'=>'<h1 class="h1-head"><i class="glyphicon glyphicon-cog"> </i> '.Html::encode($this->title).'</h1>',
            'type' => GridView::TYPE_PRIMARY,
            'footer'=>true
        ],
    
    ]);

    ?>

    
</div>


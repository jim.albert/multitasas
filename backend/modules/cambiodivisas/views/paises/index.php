
<?php

//use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\cambiodivisas\models\Estatus;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\cambiodivisas\models\EstatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Países');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paises-index">

    

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php 
    

    $gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],

    
            //'idpaises',
            'codigo',
            'descripcion',
            [
                'attribute' => 'remesas',
                'format'=>'raw',
                'label' => ' Remesas ',
                'value'=> function ($model){

                    if ($model->remesas==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            [
                'attribute' => 'cambios',
                'format'=>'raw',
                'label' => ' Cambios ',
                'value'=> function ($model){

                    if ($model->cambios==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            [
                'attribute' => 'criptomonedas',
                'format'=>'raw',
                'label' => ' Criptomonedas ',
                'value'=> function ($model){

                    if ($model->criptomonedas==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            [
                //'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'estatus', 
                'value' => 'estatus0.descripcion', 
                //'label'=>'Estatus',
                'vAlign' => 'middle',
                
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Estatus::find()->where(['tabla'=>'Pais'])->orderBy('descripcion ASC')->all(), 'idstatus','descripcion'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Opciones'],
                'format' => 'raw'
                
            ],
            
    ['class' => 'kartik\grid\ActionColumn', ]
            //['class' => 'kartik\grid\CheckboxColumn']
    ];


    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,

        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        
        'resizableColumns'=>true,
        
        'toolbar' =>  [
            ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success', 'title' => 'Registrar País']) . ' '.
                    
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Refrescar Consulta'])
            ],
            '{export}',
            '{toggleData}'
        ],

        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => false,
        
        'panel' => [
            'heading'=>'<h1 class="h1-head"><i class="glyphicon glyphicon-cog"> </i> '.Html::encode($this->title).'</h1>',
            'type' => GridView::TYPE_PRIMARY,
            'footer'=>false
        ],
    
    ]);

    ?>

    
</div>

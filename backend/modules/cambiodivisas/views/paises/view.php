<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Paises */

$this->title = 'País: ' .$model->codigo.' - '.$model->descripcion;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Países'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="paises-view">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->idpaises], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idpaises], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Cancelar', ['index'], ['class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idpaises',
            'codigo',
            'descripcion',
           // 'estatus',
            //'remesas',
            [
                'attribute' => 'remesas',
                'format'=>'raw',
                'label' => ' Remesas ',
                'value'=> function ($model){

                    if ($model->remesas==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            [
                'attribute' => 'cambios',
                'format'=>'raw',
                'label' => ' Cambios ',
                'value'=> function ($model){

                    if ($model->cambios==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            [
                'attribute' => 'criptomonedas',
                'format'=>'raw',
                'label' => ' Criptomonedas ',
                'value'=> function ($model){

                    if ($model->criptomonedas==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            
            [
                'attribute' => 'estatus0.descripcion',
                //'label'=>'Estatus',
                'label' => ' Estatus ',
            ]
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Paises */

$this->title = Yii::t('app', 'Update Paises: {name}', [
    'name' => $model->idpaises,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Paises'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idpaises, 'url' => ['view', 'id' => $model->idpaises]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="paises-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

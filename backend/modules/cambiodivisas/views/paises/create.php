<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Paises */

$this->title = Yii::t('app', 'Registrar País');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Países'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paises-create">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

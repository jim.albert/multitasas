<?php

use yii\helpers\Html;
 
/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Tipodedocumento */

$this->title = 'Tipo de Documento: ' .$model->codigo.' - '.$model->descripcion;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipodedocumentos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo, 'url' => ['view', 'id' => $model->idbtipodocumento]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tipodedocumento-update">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Usuario */

$this->title = Yii::t('app', 'Actualizar Usuario: {name}', [
    'name' => $model->nombres.' '.$model->apellidos,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Usuarios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="usuario-update">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

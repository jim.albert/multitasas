<?php

use yii\helpers\Html;

/* @var $this yii\web\View */ 
/* @var $model backend\modules\cambiodivisas\models\Tipodemoneda */

$this->title = Yii::t('app', 'Registrar Tipo de moneda');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipo de Monedas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipodemoneda-create">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

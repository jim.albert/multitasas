<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Cuentasbancariadesistema */

$this->title = Yii::t('app', 'Registrar Cuentas Bancarias de Sistema');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cuentasbancariadesistemas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cuentasbancariadesistema-create">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\widgets\SwitchInput;
use backend\modules\cambiodivisas\models\Estatus;
use backend\modules\cambiodivisas\models\Tipodemoneda;
use backend\modules\cambiodivisas\models\Bancos;
use backend\modules\cambiodivisas\models\Tipodecuenta;
/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Cuentasbancariadesistema */
/* @var $form yii\widgets\ActiveForm */ 
?>

<div class="cuentasbancariadesistema-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-4 col-sm-4">
        
        <?= $form->field($model, 'id_tipo_cuenta')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Tipodecuenta::find()->orderBy('codigo ASC')->all(), 'idtipocuenta','codigo'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-md-4 col-sm-4">
       
        <?= $form->field($model, 'id_banco')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Bancos::find()->orderBy('codigo ASC')->all(), 'idbancos','codigo','codigopais'), 
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-md-4 col-sm-4">
        

        <?= $form->field($model, 'id_moneda')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Tipodemoneda::find()->orderBy('descripcion ASC')->all(), 'idbtipomoneda','monedas'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'nro_cuenta')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'nro_interbancario')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4 col-sm-4">
        
        <?= $form->field($model, 'estatus')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Estatus::find()->where(['tabla'=>'Cuentas Bancarias del Sistema'])->orderBy('descripcion ASC')->all(), 'idstatus','descripcion'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>

    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'saldos')->widget(SwitchInput::classname(), []) ?>
    </div>
    
     <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'visible')->widget(SwitchInput::classname(), []) ?>
    </div>

    <div class="form-group col-md-12 col-sm-12">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancelar', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

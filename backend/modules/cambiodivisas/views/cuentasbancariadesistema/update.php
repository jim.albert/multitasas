<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Cuentasbancariadesistema */

$this->title = 'Cuenta Bancaria: ' .$model->alias.' - '.$model->moneda->monedas;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cuentas bancarias del sistemas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->alias.' - '.$model->moneda->monedas, 'url' => ['view', 'id' => $model->idcuenta_bancaria]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="cuentasbancariadesistema-update">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1> 

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

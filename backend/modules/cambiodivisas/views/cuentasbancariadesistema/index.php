<?php

//use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\cambiodivisas\models\Estatus;
use backend\modules\cambiodivisas\models\Tipodecuenta;
use backend\modules\cambiodivisas\models\Bancos;
use backend\modules\cambiodivisas\models\Tipodemoneda;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\cambiodivisas\models\EstatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Cuentas Bancarias del Sistemas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cuentasbancariadesistemas-index">

    

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php 
    

    $gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],

    
            //'id_tipo_cuenta',
            [
                //'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'id_tipo_cuenta', 
                'value' => 'tipoCuenta.codigo', 
                //'label'=>'Estatus',
                'vAlign' => 'middle',
                
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Tipodecuenta::find()->orderBy('codigo ASC')->all(), 'idtipocuenta','codigo'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Opciones'],
                'format' => 'raw'
                
            ],
            //'id_banco',
            [
                //'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'id_banco', 
                'value' => 'banco.codigo', 
                //'label'=>'Estatus',
                'vAlign' => 'middle',
                
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Bancos::find()->orderBy('codigo ASC')->all(), 'idbancos','codigo'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Opciones'],
                'format' => 'raw'
                
            ],
            //'id_moneda',
            [
                //'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'id_moneda', 
                'value' => 'moneda.monedas', 
                //'label'=>'Estatus',
                'vAlign' => 'middle',
                
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Tipodemoneda::find()->orderBy('descripcion ASC')->all(), 'idbtipomoneda','monedas'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Opciones'],
                'format' => 'raw'
                
            ],
            
            [
                'attribute' => 'nro_cuenta',
                'vAlign' => 'middle',
            ],
             [
                'attribute' => 'nro_interbancario',
                'vAlign' => 'middle',
            ],
             [
                'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'saldos',
                'vAlign' => 'middle',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ['1'=>'SI', '0'=>'NO'],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Opciones'],
                'format' => 'raw'
            ],
            [
                'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'visible',
                'vAlign' => 'middle',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ['1'=>'SI', '0'=>'NO'],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Opciones'],
                'format' => 'raw'
            ],
            
            
            [
                //'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'estatus', 
                'value' => 'estatus0.descripcion', 
                //'label'=>'Estatus',
                'vAlign' => 'middle',
                
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Estatus::find()->where(['tabla'=>'Cuentas Bancarias del Sistema'])->orderBy('descripcion ASC')->all(), 'idstatus','descripcion'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Opciones'],
                'format' => 'raw'
                
            ],
            
    ['class' => 'kartik\grid\ActionColumn', ]
            //['class' => 'kartik\grid\CheckboxColumn']
    ];


    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'options'=>['class'=>'text-center table-cell'],

        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        
        'resizableColumns'=>true,
        
        'toolbar' =>  [
            ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success', 'title' => 'Registrar Cuenta Bancaria']) . ' '.
                    
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Refrescar Consulta'])
            ],
            '{export}',
            '{toggleData}'
        ],

        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => false,
        
        'panel' => [
            'heading'=>'<h1 class="h1-head"><i class="glyphicon glyphicon-cog"> </i> '.Html::encode($this->title).'</h1>',
            'type' => GridView::TYPE_PRIMARY,
            'footer'=>true
        ],
    
    ]);

    ?>

    
</div>


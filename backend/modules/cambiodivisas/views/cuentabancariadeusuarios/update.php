<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Cuentabancariadeusuarios */

$this->title = Yii::t('app', 'Update Cuentabancariadeusuarios: {name}', [
    'name' => $model->idcuenta_bancaria,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cuentabancariadeusuarios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idcuenta_bancaria, 'url' => ['view', 'id' => $model->idcuenta_bancaria]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="cuentabancariadeusuarios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

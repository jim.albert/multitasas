<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Cuentabancariadeusuarios */

$this->title = $model->idcuenta_bancaria;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cuentabancariadeusuarios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cuentabancariadeusuarios-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->idcuenta_bancaria], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idcuenta_bancaria], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idcuenta_bancaria',
            'id_tipo_cuenta',
            'id_banco',
            'id_moneda',
            'nro_cuenta',
            'nro_interbancario',
            'alias',
            'id_usuario',
            'estatus',
            'fecha_registro',
        ],
    ]) ?>

</div>

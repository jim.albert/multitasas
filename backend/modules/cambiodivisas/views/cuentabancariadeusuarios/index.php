<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\cambiodivisas\models\CuentabancariadeusuariosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Cuentabancariadeusuarios');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cuentabancariadeusuarios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Cuentabancariadeusuarios'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idcuenta_bancaria',
            'id_tipo_cuenta',
            'id_banco',
            'id_moneda',
            'nro_cuenta',
            //'nro_interbancario',
            //'alias',
            //'id_usuario',
            //'estatus',
            //'fecha_registro',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

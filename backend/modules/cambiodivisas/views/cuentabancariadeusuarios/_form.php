<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Cuentabancariadeusuarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cuentabancariadeusuarios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_tipo_cuenta')->textInput() ?>

    <?= $form->field($model, 'id_banco')->textInput() ?>

    <?= $form->field($model, 'id_moneda')->textInput() ?>

    <?= $form->field($model, 'nro_cuenta')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nro_interbancario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_usuario')->textInput() ?>

    <?= $form->field($model, 'estatus')->textInput() ?>

    <?= $form->field($model, 'fecha_registro')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

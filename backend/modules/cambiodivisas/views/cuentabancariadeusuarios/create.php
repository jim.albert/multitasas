<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Cuentabancariadeusuarios */

$this->title = Yii::t('app', 'Create Cuentabancariadeusuarios');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cuentabancariadeusuarios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cuentabancariadeusuarios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Tipodecuenta */

$this->title = Yii::t('app', 'Registrar Tipo de cuenta');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipo de cuentas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipodecuenta-create">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1> 

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

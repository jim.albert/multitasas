<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\cambiodivisas\models\Estatus;
/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Tipodecuenta */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tipodecuenta-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="col-md-12 col-sm-12">

	    <?= $form->field($model, 'codigo')->textInput(['maxlength' => true]) ?>
	</div>
	<div class="col-md-12 col-sm-12">
	    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>
	</div>
	<div class="col-md-12 col-sm-12">
	    <?= $form->field($model, 'estatus')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Estatus::find()->where(['tabla'=>'Tipo de Cuenta'])->orderBy('descripcion ASC')->all(), 'idstatus','descripcion'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
	</div>

    <div class="form-group col-md-12 col-sm-12">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancelar', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

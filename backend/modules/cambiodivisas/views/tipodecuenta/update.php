<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Tipodecuenta */

$this->title = 'Tipo de cuenta: ' .$model->codigo.' - '.$model->descripcion; 
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipo de cuentas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo, 'url' => ['view', 'id' => $model->idtipocuenta]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tipodecuenta-update">

     <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\modules\cambiodivisas\models\Estatus;
/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Empresa */
/* @var $form yii\widgets\ActiveForm */ 
?>
 
<div class="empresa-form">

    <?php $form = ActiveForm::begin(); ?>

    

    <div class="col-md-6 col-sm-6">
        
        <table class="table">
        <tr>
            <td>
                <?= $form->field($model, 'image')->widget(FileInput::classname(), [
              'options' => ['accept' => 'image/*'],
               'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png'],'showUpload' => false,],
          ]);   ?>
            </td>
            <td>
                <?php if ($model->logo!=''){

                          echo '<img src="'.Url::base(). '/img/'.$model->logo.'" width="150px" height="auto">';
                     }   ?>
            </td>
        </tr>
    </table>
    </div>

    <div class="col-md-6 col-sm-6">
        <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-6 col-sm-6">
        <?= $form->field($model, 'mensaje_front')->textarea(['rows' => 4]) ?>
    </div>
    <div class="col-md-6 col-sm-6">
        <?= $form->field($model, 'mensaje_back')->textarea(['rows' => 4]) ?>
    </div>

    <div class="col-md-6 col-sm-6">
        <?= $form->field($model, 'horario')->textarea(['rows' => 4]) ?>

    </div>
    
     

   
    <div class="col-md-3 col-sm-3">
        <?= $form->field($model, 'porcentaje_pagador')->textInput() ?>
    </div>
    <div class="col-md-3 col-sm-3">
        <?= $form->field($model, 'porcentaje_pmovil')->textInput() ?>
    </div>
    <div class="col-md-3 col-sm-3">
        <?= $form->field($model, 'porcentaje_interbancario')->textInput() ?>
    </div>
    <div class="col-md-3 col-sm-3">
        <?= $form->field($model, 'porcentaje_agentes')->textInput() ?>
    </div>
    <div class="col-md-9 col-sm-9">
        <?= $form->field($model, 'mensaje_apertura')->textarea(['rows' => 4]) ?>

    </div>
    <div class="col-md-3 col-sm-3">
        <?= $form->field($model, 'estatus')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Estatus::find()->where(['tabla'=>'Empresa'])->orderBy('descripcion ASC')->all(), 'idstatus','descripcion'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    

     <div class="form-group col-md-12 col-sm-12">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancelar', ['/'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>

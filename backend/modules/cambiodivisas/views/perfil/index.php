
<?php

//use kartik\export\ExportMenu; 
use kartik\grid\GridView;
use kartik\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\cambiodivisas\models\Estatus;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\cambiodivisas\models\EstatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Perfiles de Acceso');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estatus-index">

    

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php 
    

    $gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],

    
            //'id',
            'descripcion',
            [
                'attribute' => 'remesas',
                'format'=>'raw',
                'label' => ' Envío de Dinero ',
                'value'=> function ($model){

                    if ($model->remesas==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            [
                'attribute' => 'cambios',
                'format'=>'raw',
                'label' => ' Cambios ',
                'value'=> function ($model){

                    if ($model->cambios==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            // [
            //     'attribute' => 'criptomoneda',
            //     'format'=>'raw',
            //     'label' => ' Criptomonedas ',
            //     'value'=> function ($model){

            //         if ($model->criptomoneda==0) {
            //             return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
            //         }else{

            //             return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
            //         }
            //     }
            // ],
            
            [
                'attribute' => 'pagador',
                'format'=>'raw',
                'label' => 'Pagador ',
                'value'=> function ($model){

                    if ($model->pagador==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            [
                'attribute' => 'operador',
                'format'=>'raw',
                'label' => 'Operador ',
                'value'=> function ($model){

                    if ($model->operador==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            
            [
                'attribute' => 'agentes',
                'format'=>'raw',
                'label' => 'Agentes ',
                'value'=> function ($model){

                    if ($model->agentes==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            [
                'attribute' => 'administrador',
                'format'=>'raw',
                'label' => 'Administrador ',
                'value'=> function ($model){

                    if ($model->administrador==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            
            //'estatus',
            [
                //'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'estatus', 
                'value' => 'estatus0.descripcion', 
                //'label'=>'Estatus',
                'vAlign' => 'middle',
                
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Estatus::find()->where(['tabla'=>'Pais'])->orderBy('descripcion ASC')->all(), 'idstatus','descripcion'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Opciones'],
                'format' => 'raw'
                
            ],
    ['class' => 'kartik\grid\ActionColumn', ]
            //['class' => 'kartik\grid\CheckboxColumn']
    ];


    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,

        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        
        'resizableColumns'=>true,
        
        'toolbar' =>  [
            ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success', 'title' => 'Registrar Estatus']) . ' '.
                    
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Refrescar Consulta'])
            ],
            '{export}',
            '{toggleData}'
        ],

        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => false, 
        
        'panel' => [
            'heading'=>'<h1 class="h1-head"><i class="glyphicon glyphicon-cog"> </i> '.Html::encode($this->title).'</h1>',
            'type' => GridView::TYPE_PRIMARY,
            'footer'=>true
        ],
    
    ]);

    ?>

    
</div>


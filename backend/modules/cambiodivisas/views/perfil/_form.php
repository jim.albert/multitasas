<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\widgets\SwitchInput;
use yii\helpers\ArrayHelper;
use backend\modules\cambiodivisas\models\Estatus;
/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Perfil */
/* @var $form yii\widgets\ActiveForm */  
?>

<div class="perfil-form">

    <?php $form = ActiveForm::begin(); ?>
        <div class="col-md-6 col-sm-6">
            <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6 col-sm-6">
            <?= $form->field($model, 'estatus')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Estatus::find()->where(['tabla'=>'Perfiles de Accesos'])->orderBy('descripcion ASC')->all(), 'idstatus','descripcion'),
                'language' => 'es',
                'options' => ['placeholder' => 'Seleccionar una Opción ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>


        <div class="col-md-12 col-sm-12">
            <h3 class="text-center bg bg-info text text-info">Módulo</h3>
        </div>
        
        <div class="col-md-4 col-sm-4">
            <?= $form->field($model, 'remesas')->widget(SwitchInput::classname(), []) ?>
        </div>
        <div class="col-md-4 col-sm-4">
            <?= $form->field($model, 'cambios')->widget(SwitchInput::classname(), []) ?>
        </div>
        <div class="col-md-4 col-sm-4">
            <?= $form->field($model, 'criptomoneda')->widget(SwitchInput::classname(), []) ?>
        </div>

        <div class="col-md-12 col-sm-12">
            <h3 class="text-center bg bg-info text text-info">Acceso</h3>
        </div>

        <div class="col-md-3 col-sm-3">
            <?= $form->field($model, 'pagador')->widget(SwitchInput::classname(), []) ?>
        </div>
        <div class="col-md-3 col-sm-3">
            <?= $form->field($model, 'operador')->widget(SwitchInput::classname(), []) ?>
        </div>
        <div class="col-md-3 col-sm-3">
            <?= $form->field($model, 'agentes')->widget(SwitchInput::classname(), []) ?>
        </div>
        <div class="col-md-3 col-sm-3">
            <?= $form->field($model, 'administrador')->widget(SwitchInput::classname(), []) ?>
        </div>
        

        <div class="form-group col-md-12 col-sm-12">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            <?= Html::a('Cancelar', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Perfil */

$this->title = Yii::t('app', 'Actualizar Perfil: {name}', [
    'name' => $model->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Perfils'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="perfil-update">

     <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Validaciones */

$this->title = 'Validaciones: ' .$model->codigo.' - '.$model->descripcion;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Validaciones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo, 'url' => ['view', 'id' => $model->idvalidacion]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="validaciones-update">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use backend\modules\cambiodivisas\models\Estatus;
use backend\modules\cambiodivisas\models\Tipodemoneda;
/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Validaciones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="validaciones-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'codigo')->textInput(['maxlength' => true]) ?>

    </div>
    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-4 col-sm-4">
        
        <?= $form->field($model, 'tipo_transferencia')->widget(Select2::classname(), [
            'data' => ['Mismo Banco'=>'Mismo Banco', 'Interbancario'=>'Interbancario', 'Internacional'=>'Internacional'],
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>

    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'desde')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'hasta')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4 col-sm-4">
        
        <?= $form->field($model, 'tipo_moneda')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Tipodemoneda::find()->orderBy('descripcion ASC')->all(), 'idbtipomoneda','monedas'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>

    
    <div class="col-md-6 col-sm-6">
        
        <?= $form->field($model, 'fecha_registro')->widget(DatePicker::classname(),[
            'name' => 'fecha', 
            'value' => date('Y-M-D'),
            'options' => ['placeholder' => 'Seleccionar Fecha ...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
            ]
        ]); ?>
    </div>
    <div class="col-md-6 col-sm-6">
        <?= $form->field($model, 'estatus')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Estatus::find()->where(['tabla'=>'Validaciones'])->orderBy('descripcion ASC')->all(), 'idstatus','descripcion'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>



    <div class="form-group col-md-12 col-sm-12">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancelar', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

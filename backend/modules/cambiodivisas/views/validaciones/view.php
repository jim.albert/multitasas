<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Validaciones */

$this->title = 'Validaciones: ' .$model->codigo.' - '.$model->descripcion;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Validaciones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="validaciones-view">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->idvalidacion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idvalidacion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Cancelar', ['index'], ['class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idvalidacion',
            'codigo',
            'descripcion',
            'tipo_transferencia',
            //'tipo_moneda',
            [
                'attribute' => 'tipoMoneda.monedas',
                //'label'=>'Estatus',
                'label' => ' Tipo de Moneda ',                

            ],
            'desde',
            'hasta',
            'fecha_registro',
            
            [
                'attribute' => 'estatus0.descripcion',
                //'label'=>'Estatus',
                'label' => ' Estatus ',                

            ],
        ],
    ]) ?>

</div>

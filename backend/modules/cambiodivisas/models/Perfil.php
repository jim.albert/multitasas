<?php

namespace backend\modules\cambiodivisas\models;

use Yii;

/**
 * This is the model class for table "perfil".
 *
 * @property int $id
 * @property string $descripcion
 * @property int $remesas
 * @property int $cambios
 * @property int $criptomoneda
 * @property int $pagador
 * @property int $operador
 * @property int $administrador
 * @property int $agentes
 * @property int $estatus
 *
 * @property Estatus $estatus0
 */
class Perfil extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'perfil';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'remesas', 'cambios', 'criptomoneda', 'pagador', 'operador', 'administrador','agentes', 'estatus'], 'required'],
            [['remesas', 'cambios', 'criptomoneda', 'pagador', 'operador', 'administrador', 'agentes', 'estatus'], 'integer'],
            [['descripcion'], 'string', 'max' => 45],
            [['estatus'], 'exist', 'skipOnError' => true, 'targetClass' => Estatus::className(), 'targetAttribute' => ['estatus' => 'idstatus']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'descripcion' => Yii::t('app', 'Descripción'),
            'remesas' => Yii::t('app', 'Envío de Dinero'),
            'cambios' => Yii::t('app', 'Cambios'),
            'criptomoneda' => Yii::t('app', 'Criptomoneda'),
            'pagador' => Yii::t('app', 'Pagador'),
            'operador' => Yii::t('app', 'Operador'),
            'administrador'=> Yii::t('app', 'Administrador'),
            'agentes'=> Yii::t('app', 'Agentes'),
            'estatus' => Yii::t('app', 'Estatus'),
        ];
    }

    /**
     * Gets query for [[Estatus0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstatus0()
    {
        return $this->hasOne(Estatus::className(), ['idstatus' => 'estatus']);
    }
}

<?php

namespace backend\modules\cambiodivisas\models;

use Yii;

/**
 * This is the model class for table "paises".
 *
 * @property int $idpaises
 * @property string $codigo
 * @property string $descripcion
 * @property int $estatus
 * @property int $remesas
 * @property int $cambios
 * @property int $criptomonedas
 *
 * @property Bancos[] $bancos
 * @property Estatus $estatus0
 * @property Usuario[] $usuarios
 */
class Paises extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paises';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'descripcion', 'estatus', 'remesas', 'cambios', 'criptomonedas'], 'required'],
            [['estatus', 'remesas', 'cambios', 'criptomonedas'], 'integer'],
            [['codigo', 'descripcion'], 'string', 'max' => 45],
            [['estatus'], 'exist', 'skipOnError' => true, 'targetClass' => Estatus::className(), 'targetAttribute' => ['estatus' => 'idstatus']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idpaises' => Yii::t('app', 'Idpaises'),
            'codigo' => Yii::t('app', 'Codigo'),
            'descripcion' => Yii::t('app', 'Descripcion'),
            'estatus' => Yii::t('app', 'Estatus'),
            'remesas' => Yii::t('app', 'Remesas'),
            'cambios' => Yii::t('app', 'Cambios de Divisas'),
            'criptomonedas' => Yii::t('app', 'Criptomonedas'),
        ];
    }

    /**
     * Gets query for [[Bancos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBancos()
    {
        return $this->hasMany(Bancos::className(), ['id_pais' => 'idpaises']);
    }

    /**
     * Gets query for [[Estatus0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstatus0()
    {
        return $this->hasOne(Estatus::className(), ['idstatus' => 'estatus']);
    }

    /**
     * Gets query for [[Usuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['id_pais' => 'idpaises']);
    }
}

<?php

namespace backend\modules\cambiodivisas\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\cambiodivisas\models\Operacionesusuarios;

/**
 * OperacionesusuariosSearch represents the model behind the search form of `backend\modules\cambiodivisas\models\Operacionesusuarios`.
 */
class OperacionesusuariosSearch extends Operacionesusuarios
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idoperusu', 'id_usuario', 'id_operacion'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Operacionesusuarios::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idoperusu' => $this->idoperusu,
            'id_usuario' => $this->id_usuario,
            'id_operacion' => $this->id_operacion,
        ]);

        return $dataProvider;
    }
}

<?php

namespace backend\modules\cambiodivisas\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\cambiodivisas\models\Tipodemoneda;

/**
 * TipodemonedaSearch represents the model behind the search form of `backend\modules\cambiodivisas\models\Tipodemoneda`.
 */
class TipodemonedaSearch extends Tipodemoneda
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idbtipomoneda', 'estatus'], 'integer'],
            [['codigo', 'descripcion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tipodemoneda::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idbtipomoneda' => $this->idbtipomoneda,
            'estatus' => $this->estatus,
        ]);

        $query->andFilterWhere(['like', 'codigo', $this->codigo])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}

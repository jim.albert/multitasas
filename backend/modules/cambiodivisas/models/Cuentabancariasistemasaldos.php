<?php

namespace backend\modules\cambiodivisas\models;

use Yii;

/**
 * This is the model class for table "cuenta_bancaria_sistema_Saldos".
 *
 * @property int $id
 * @property int $id_cta_bancaria
 * @property int $id_operacion
 * @property string $referancia_pago
 * @property float $monto_operacion
 * @property string $fecha
 * @property float $comision_pagador
 * @property float $comision_interbancario
 * @property float $comision_pmovil
 * @property float $comision_agentes
 * @property int $pmovil
 * @property int $id_operacion_empresa 
 *
 * @property CuentaBancariaSistema $ctaBancaria
 * @property Operaciones $operacion
 * @property OperacionEmpresa $operacionEmpresa
 */
class Cuentabancariasistemasaldos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cuenta_bancaria_sistema_Saldos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_cta_bancaria', 'id_operacion', 'referancia_pago', 'monto_operacion', 'fecha', 'comision_pagador', 'comision_interbancario', 'comision_pmovil', 'comision_agentes', 'pmovil', 'id_operacion_empresa'], 'required'],
            [['id_cta_bancaria', 'id_operacion', 'id_operacion_empresa'], 'integer'],
            [['monto_operacion', 'comision_pagador', 'comision_interbancario', 'comision_pmovil', 'comision_agentes'], 'number'],
            [['fecha'], 'safe'],
            [['referancia_pago'], 'string', 'max' => 30],
            [['id_cta_bancaria'], 'exist', 'skipOnError' => true, 'targetClass' => Cuentasbancariadesistema::className(), 'targetAttribute' => ['id_cta_bancaria' => 'idcuenta_bancaria']],
            [['id_operacion'], 'exist', 'skipOnError' => true, 'targetClass' => Operaciones::className(), 'targetAttribute' => ['id_operacion' => 'idoperacion']],
            [['id_operacion_empresa'], 'exist', 'skipOnError' => true, 'targetClass' => Operacionempresa::className(), 'targetAttribute' => ['id_operacion_empresa' => 'idoperacionempresa']], 
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_cta_bancaria' => Yii::t('app', 'Cuenta Bancaria'),
            'id_operacion' => Yii::t('app', 'Id Operacion'),
            'referancia_pago' => Yii::t('app', 'Referancia de Pago'),
            'monto_operacion' => Yii::t('app', 'Monto Operación'),
            'fecha' => Yii::t('app', 'Fecha'),
            'comision_pagador' => Yii::t('app', 'Comisión Pagador'),
            'comision_interbancario' => Yii::t('app', 'Comisión Interbancario'),
            'comision_pmovil' => Yii::t('app', 'Comisión Pago móvil'),
            'comision_agentes' => Yii::t('app', 'Comisión Agentes'),
            'pmovil' => Yii::t('app', 'Pago Móvil'),
            'id_operacion_empresa' => Yii::t('app', 'Apertura Empresa'),
        ];
    }
 
    /**
     * Gets query for [[CtaBancaria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCtabancaria()
    {
        return $this->hasOne(Cuentasbancariadesistema::className(), ['idcuenta_bancaria' => 'id_cta_bancaria']);
    }

    /**
     * Gets query for [[Operacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOperacion()
    {
        return $this->hasOne(Operaciones::className(), ['idoperacion' => 'id_operacion']);
    }

    /** 
    * Gets query for [[OperacionEmpresa]]. 
    * 
    * @return \yii\db\ActiveQuery 
    */ 
   public function getOperacionempresa() 
   { 
       return $this->hasOne(Operacionempresa::className(), ['idoperacionempresa' => 'id_operacion_empresa']); 
   } 
}

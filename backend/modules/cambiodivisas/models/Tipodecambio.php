<?php

namespace backend\modules\cambiodivisas\models;

use Yii;

/**
 * This is the model class for table "tipo_cambio".
 *
 * @property int $idcambio
 * @property string $codigo
 * @property string $descripcion
 * @property string $operacion
 * @property float $monto
 * @property string $fecha
 * @property int $estatus
 * @property float $venta 
 * @property float $porcentaje_ganancia 
 * @property float $compra_reserva 
 * @property float $venta_reserva 
 * @property int $id_moneda_compra
* @property int $id_moneda_vende
 *
 * @property Operaciones[] $operaciones
 * @property Estatus $estatus0
* @property TipoMoneda $monedacompra 
* @property TipoMoneda $monedaVende 
 */
class Tipodecambio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo_cambio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
            [['codigo', 'descripcion', 'operacion', 'monto', 'fecha', 'estatus', 'venta', 'porcentaje_ganancia', 'compra_reserva', 'venta_reserva', 'id_moneda_compra', 'id_moneda_vende'], 'required'],
            [['monto', 'venta', 'porcentaje_ganancia', 'compra_reserva', 'venta_reserva'], 'number'],
            [['fecha'], 'safe'],
            [['estatus', 'id_moneda_compra', 'id_moneda_vende'], 'integer'],
            [['codigo', 'descripcion'], 'string', 'max' => 45],
            [['operacion'], 'string', 'max' => 15],
            [['estatus'], 'exist', 'skipOnError' => true, 'targetClass' => Estatus::className(), 'targetAttribute' => ['estatus' => 'idstatus']],
            [['id_moneda_compra'], 'exist', 'skipOnError' => true, 'targetClass' => Tipodemoneda::className(), 'targetAttribute' => ['id_moneda_compra' => 'idbtipomoneda']], 
           [['id_moneda_vende'], 'exist', 'skipOnError' => true, 'targetClass' => Tipodemoneda::className(), 'targetAttribute' => ['id_moneda_vende' => 'idbtipomoneda']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcambio' => Yii::t('app', 'Id Tipo de Cambio'),
            'codigo' => Yii::t('app', 'Código'),
            'descripcion' => Yii::t('app', 'Descripción'),
            'operacion' => Yii::t('app', 'Operación'),
            'monto' => Yii::t('app', 'Compra'),
            'venta' => Yii::t('app', 'Venta'), 
            'fecha' => Yii::t('app', 'Fecha'),
            'estatus' => Yii::t('app', 'Estatus'),
            'porcentaje_ganancia' => Yii::t('app', 'Porcentaje de Ganancia'), 
            'compra_reserva' => Yii::t('app', 'Compra BCRP'), 
            'venta_reserva' => Yii::t('app', 'Venta BCRP'),
        ];
    }

    /**
     * Gets query for [[Operaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOperaciones()
    {
        return $this->hasMany(Operaciones::className(), ['id_tipo_cambio' => 'idcambio']);
    }

    /**
     * Gets query for [[Estatus0]].
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getEstatus0()
    {
        return $this->hasOne(Estatus::className(), ['idstatus' => 'estatus']);
    }
    /** 
    * Gets query for [[MonedaCompra]]. 
    * 
    * @return \yii\db\ActiveQuery 
    */ 
   public function getMonedacompra() 
   { 
       return $this->hasOne(Tipodemoneda::className(), ['idbtipomoneda' => 'id_moneda_compra']); 
   } 
 
   /** 
    * Gets query for [[MonedaVende]]. 
    * 
    * @return \yii\db\ActiveQuery 
    */ 
   public function getMonedavende() 
   { 
       return $this->hasOne(Tipodemoneda::className(), ['idbtipomoneda' => 'id_moneda_vende']); 
   } 
}

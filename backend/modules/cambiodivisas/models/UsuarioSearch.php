<?php

namespace backend\modules\cambiodivisas\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\cambiodivisas\models\Usuario;

/**
 * UsuarioSearch represents the model behind the search form of `backend\modules\cambiodivisas\models\Usuario`.
 */
class UsuarioSearch extends Usuario
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_tipo_documento', 'pep', 'estatus'], 'integer'],
            [['nombres', 'apellidos', 'nro_documento', 'fecha_nacimiento', 'email', 'nro_operacion', 'monto_operacion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Usuario::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_tipo_documento' => $this->id_tipo_documento,
            'fecha_nacimiento' => $this->fecha_nacimiento,
            'pep' => $this->pep,
            'estatus' => $this->estatus,
        ]);

        $query->andFilterWhere(['like', 'nombres', $this->nombres])
            ->andFilterWhere(['like', 'apellidos', $this->apellidos])
            ->andFilterWhere(['like', 'nro_documento', $this->nro_documento])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'nro_operacion', $this->nro_operacion])
            ->andFilterWhere(['like', 'monto_operacion', $this->monto_operacion]);

        return $dataProvider;
    }
}

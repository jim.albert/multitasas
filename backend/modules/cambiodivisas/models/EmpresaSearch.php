<?php

namespace backend\modules\cambiodivisas\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\cambiodivisas\models\Empresa;

/**
 * EmpresaSearch represents the model behind the search form of `backend\modules\cambiodivisas\models\Empresa`.
 */
class EmpresaSearch extends Empresa
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'estatus'], 'integer'],
            [['nombre', 'mensaje_front', 'mensaje_back', 'logo', ], 'safe'],
            [['porcentaje_pagador', 'porcentaje_pmovil', 'porcentaje_interbancario'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Empresa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'estatus' => $this->estatus,
            'porcentaje_pagador' => $this->porcentaje_pagador,
            'porcentaje_pmovil' => $this->porcentaje_pmovil,
            'porcentaje_interbancario' => $this->porcentaje_interbancario,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'mensaje_front', $this->mensaje_front])
            ->andFilterWhere(['like', 'mensaje_back', $this->mensaje_back])
            ->andFilterWhere(['like', 'logo', $this->logo]);

        return $dataProvider;
    }
}

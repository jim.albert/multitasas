<?php

namespace backend\modules\cambiodivisas\models;

use Yii;

/**
 * This is the model class for table "empresa".
 *
 * @property int $id
 * @property string $nombre
 * @property string $mensaje_front
 * @property string $mensaje_back
 * @property string $horario
 * @property string $logo
 * @property string $mensaje_apertura
 * @property int $estatus
 * @property float $porcentaje_pagador
 * @property float $porcentaje_pmovil
 * @property float $porcentaje_interbancario
 * @property float $porcentaje_agentes
 *
 * @property Estatus $estatus0
 */
class Empresa extends \yii\db\ActiveRecord
{
    public $image;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empresa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'mensaje_front', 'mensaje_back', 'logo', 'estatus',  'porcentaje_pagador', 'porcentaje_pmovil', 'porcentaje_interbancario','porcentaje_agentes','horario','mensaje_apertura'], 'required'],
            [['mensaje_front', 'mensaje_back', 'logo', 'horario','mensaje_apertura'], 'string'],
            [['estatus'], 'integer'],
            [['porcentaje_pagador', 'porcentaje_pmovil', 'porcentaje_interbancario','porcentaje_agentes'], 'number'],
            [['nombre'], 'string', 'max' => 30],
            [['estatus'], 'exist', 'skipOnError' => true, 'targetClass' => Estatus::className(), 'targetAttribute' => ['estatus' => 'idstatus']],
            [['image'], 'safe'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png'],
            [['image'], 'file', 'maxSize'=>2048*1024*1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'mensaje_front' => Yii::t('app', 'Mensaje Clientes'),
            'mensaje_back' => Yii::t('app', 'Mensaje Operadores'),
            'logo' => Yii::t('app', 'Logo'),
            'estatus' => Yii::t('app', 'Estatus'),
            'porcentaje_pagador' => Yii::t('app', 'Porcentaje Pagador'),
            'porcentaje_pmovil' => Yii::t('app', 'Porcentaje Pago Movil'),
            'porcentaje_interbancario' => Yii::t('app', 'Porcentaje Interbancario'),
            'image'=>'Logo',
            'horario'=>'Horario de Atención',
            'mensaje_apertura'=>'Mensaje operaciones cerradas',
            'porcentaje_agentes'=>'Porcentaje Agentes'
        ];
    }

    /**
     * Gets query for [[Estatus0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstatus0()
    {
        return $this->hasOne(Estatus::className(), ['idstatus' => 'estatus']);
    }
}

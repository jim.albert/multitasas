<?php

namespace backend\modules\cambiodivisas\models;

use Yii;

/**
 * This is the model class for table "cuenta_bancaria_sistema".
 *
 * @property int $idcuenta_bancaria
 * @property int $id_tipo_cuenta
 * @property int $id_banco
 * @property int $id_moneda
 * @property string $nro_cuenta
 * @property string $nro_interbancario
 * @property string $alias
 * @property int $estatus
 * @property string $fecha_registro
 * @property int $saldos
 * @property int $visible
 *
 * @property Bancos $banco
 * @property Estatus $estatus0
 * @property TipoCuenta $tipoCuenta
 * @property TipoMoneda $moneda
 * @property Operaciones[] $operaciones
 * @property CuentaBancariaSistemaSaldos[] $cuentaBancariaSistemaSaldos
 * @property OperacionSaldocuentasBancarias[] $operacionSaldocuentasBancarias 
 */
class Cuentasbancariadesistema extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cuenta_bancaria_sistema';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tipo_cuenta', 'id_banco', 'id_moneda', 'nro_cuenta', 'nro_interbancario', 'alias', 'estatus',], 'required'],
            [['id_tipo_cuenta', 'id_banco', 'id_moneda', 'estatus','saldos','visible'], 'integer'],

           // [['nro_cuenta',  ], 'string', 'length' => 14, ],

          //  [['nro_interbancario',  ], 'string', 'length' => 20,],

            [['nro_cuenta', 'nro_interbancario',], 'number' ],

            [['fecha_registro'], 'safe'],
            [[ 'alias'], 'string', 'max' => 45],

            [['id_banco'], 'exist', 'skipOnError' => true, 'targetClass' => Bancos::className(), 'targetAttribute' => ['id_banco' => 'idbancos']],
            [['estatus'], 'exist', 'skipOnError' => true, 'targetClass' => Estatus::className(), 'targetAttribute' => ['estatus' => 'idstatus']],
            [['id_tipo_cuenta'], 'exist', 'skipOnError' => true, 'targetClass' => Tipodecuenta::className(), 'targetAttribute' => ['id_tipo_cuenta' => 'idtipocuenta']],
            [['id_moneda'], 'exist', 'skipOnError' => true, 'targetClass' => Tipodemoneda::className(), 'targetAttribute' => ['id_moneda' => 'idbtipomoneda']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcuenta_bancaria' => Yii::t('app', 'Id Cuenta Bancaria'),
            'id_tipo_cuenta' => Yii::t('app', 'Tipo Cuenta'),
            'id_banco' => Yii::t('app', 'Banco'),
            'id_moneda' => Yii::t('app', 'Tipo de Moneda'),
            'nro_cuenta' => Yii::t('app', 'Nro. de Cuenta'),
            'nro_interbancario' => Yii::t('app', 'Nro. Cta. Interbancario'),
            'alias' => Yii::t('app', 'Alias'),
            'estatus' => Yii::t('app', 'Estatus'),
            'fecha_registro' => Yii::t('app', 'Fecha de Registro'),
            'saldos' => Yii::t('app', 'Afectación de Saldos'),
            'visible' => Yii::t('app', 'Visible para los Usuarios'),
        ];
    }

    /**
     * Gets query for [[Banco]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBanco()
    {
        return $this->hasOne(Bancos::className(), ['idbancos' => 'id_banco']);
    }

    /**
     * Gets query for [[Estatus0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstatus0()
    {
        return $this->hasOne(Estatus::className(), ['idstatus' => 'estatus']);
    }

    /**
     * Gets query for [[TipoCuenta]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoCuenta()
    {
        return $this->hasOne(Tipodecuenta::className(), ['idtipocuenta' => 'id_tipo_cuenta']);
    }

    /**
     * Gets query for [[Moneda]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMoneda()
    {
        return $this->hasOne(Tipodemoneda::className(), ['idbtipomoneda' => 'id_moneda']);
    }

    /**
     * Gets query for [[Operaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOperaciones()
    {
        return $this->hasMany(Operaciones::className(), ['id_cuenta_bancaria_sistema' => 'idcuenta_bancaria']);
    }

    /**
     * Gets query for [[CuentaBancariaSistemaSaldos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuentaBancariaSistemaSaldos()
    {
        return $this->hasMany(Cuentabancariasistemasaldos::className(), ['id_cta_bancaria' => 'idcuenta_bancaria']);
    }

    /**
     * Gets query for [[OperacionSaldocuentasBancarias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOperacionSaldocuentasBancarias()
    {
        return $this->hasMany(Operacionsaldocuentasbancarias::className(), ['id_cuentabancaria_sistema' => 'idcuenta_bancaria']);
    }


    public function getNroctas()
    {
        return '<b>'.$this->alias.' <br> Moneda:</b> '.$this->moneda->monedas.'</b><br><b>Nro: </b>'.$this->nro_cuenta.'<br> <b>CCI:</b> '.$this->nro_interbancario;
    }

    public function getNroctassaldos()
    {
        return '<b>'.$this->alias.' <br> Moneda:</b> '.$this->moneda->monedas.'</b><br><b>Nro: </b>'.$this->nro_cuenta.'<br>';
    }

    public function getNroctasselect()
    {
        return $this->alias.' Nro: '.$this->nro_cuenta;
    }

    public function getPaiscuenta()
    {
        return $this->banco->pais->descripcion; 
    }
}

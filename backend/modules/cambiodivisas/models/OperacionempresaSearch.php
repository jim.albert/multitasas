<?php

namespace backend\modules\cambiodivisas\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\cambiodivisas\models\Operacionempresa;

/**
 * OperacionempresaSearch represents the model behind the search form of `backend\modules\cambiodivisas\models\Operacionempresa`.
 */
class OperacionempresaSearch extends Operacionempresa
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idoperacionempresa', 'estatus'], 'integer'],
            [['fecha_apertura', 'fecha_cierre'], 'safe'],
            [['saldo_apertura', 'saldo_cierre', 'porcentaje_pagador', 'porcentaje_pmovil', 'porcentaje_interbancario', 'saldo_anterior'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */ 
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Operacionempresa::find();



        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->defaultOrder = ['idoperacionempresa' => SORT_DESC];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idoperacionempresa' => $this->idoperacionempresa,
            //'fecha_apertura' => $this->fecha_apertura,
            //'fecha_cierre' => $this->fecha_cierre,
            'saldo_apertura' => $this->saldo_apertura,
            'saldo_cierre' => $this->saldo_cierre,
            'porcentaje_pagador' => $this->porcentaje_pagador,
            'porcentaje_pmovil' => $this->porcentaje_pmovil,
            'porcentaje_interbancario' => $this->porcentaje_interbancario,
            'saldo_anterior' => $this->saldo_anterior,
            'estatus' => $this->estatus,
        ]);

        $query->andFilterWhere([
            
            //'fecha_apertura' => $this->fecha_apertura,
            //'fecha_cierre' => $this->fecha_cierre,
            'like', 'fecha_apertura',$this->fecha_apertura,
        ]);
        $query->andFilterWhere([
            
            //'fecha_apertura' => $this->fecha_apertura,
            //'fecha_cierre' => $this->fecha_cierre,
            'like', 'fecha_cierre',$this->fecha_cierre,
        ]);

        return $dataProvider;
    }
}

<?php

namespace backend\modules\cambiodivisas\models;

use Yii;

/**
 * This is the model class for table "usuario_perfil".
 *
 * @property int $id
 * @property int $id_usuario
 * @property int $id_perfil
 * @property int $email_notification 
 * @property int $telefono_notification 
 * @property string $telefono_numero 
 *
 * @property Perfil $perfil
 * @property Usuario $usuario
 */
class Usuarioperfil extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario_perfil';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuario', 'id_perfil', 'email_notification', 'telefono_notification', 'telefono_numero'], 'required'],
            [['id_usuario', 'id_perfil', 'email_notification', 'telefono_notification', 'telefono_numero'], 'integer'],
            [['id_perfil'], 'exist', 'skipOnError' => true, 'targetClass' => Perfil::className(), 'targetAttribute' => ['id_perfil' => 'id']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['id_usuario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_usuario' => Yii::t('app', 'Usuario'),
            'id_perfil' => Yii::t('app', 'Perfil'),
            'email_notification' => Yii::t('app', 'Notificación por Email'),
           'telefono_notification' => Yii::t('app', 'Notificación por Teléfono'),
           'telefono_numero' => Yii::t('app', 'Número de Teléfono'),
        ];
    }

    /**
     * Gets query for [[Perfil]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPerfil()
    {
        return $this->hasOne(Perfil::className(), ['id' => 'id_perfil']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'id_usuario']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuariodatos()
    {
        return $this->usuario->datosusuarios;
    }
    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPerfildatos()
    {
        return $this->perfil->descripcion;
    }
}

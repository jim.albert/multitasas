<?php

namespace backend\modules\cambiodivisas\models;

use Yii;

/**
 * This is the model class for table "operacion_comprobantes".
 *
 * @property int $idcomprobante
 * @property string $imagen
 * @property int $id_operacion
 * @property int $orden
 *
 * @property Operaciones $operacion
 */
class Operacioncomprobantes extends \yii\db\ActiveRecord
{
    const SCENARIOEDITA = 'edita'; 
    const SCENARIOPAGA = 'paga';
    // scenarios encapsulated
        public function getCustomScenarios()
        {
          
          return [
              self::SCENARIOEDITA      =>  ['id_operacion'],
              self::SCENARIOPAGA      =>  [ 'imagen','id_operacion'],
          ];
        }
        // get scenarios
        public function scenarios()
        {
            $scenarios = $this->getCustomScenarios();
            return $scenarios;
        }

        // modify itens required for rules
        // public function ModifyRequired()
        // {

        //   $allscenarios = $this->getCustomScenarios();
        //   // published not required
        //   $allscenarios[self::SCENARIOEDITA] = array_diff($allscenarios[self::SCENARIOPAGA],['referancia_proceso']);
        //   return $allscenarios;

        // }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operacion_comprobantes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $allscenarios = $this->getCustomScenarios();
        return [
            // [['imagen', 'id_operacion'], 'required'],
             [$allscenarios[self::SCENARIOEDITA], 'required', 'on' => self::SCENARIOPROCESO],
             [$allscenarios[self::SCENARIOPAGA], 'required', 'on' => self::SCENARIOPAGA],

            [['imagen'], 'string'],
            [['id_operacion', 'orden'], 'integer'],
            [['id_operacion'], 'exist', 'skipOnError' => true, 'targetClass' => Operaciones::className(), 'targetAttribute' => ['id_operacion' => 'idoperacion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcomprobante' => Yii::t('app', 'Idcomprobante'),
            'imagen' => Yii::t('app', 'Comprobante Bancario'),
            'id_operacion' => Yii::t('app', 'Operacion'),
        ];
    }

    /**
     * Gets query for [[Operacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOperacion()
    {
        return $this->hasOne(Operaciones::className(), ['idoperacion' => 'id_operacion']);
    }
}

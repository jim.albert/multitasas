<?php

namespace backend\modules\cambiodivisas\models;

use Yii;

/**
 * This is the model class for table "cuenta_bancaria_usuarios".
 *
 * @property int $idcuenta_bancaria
 * @property int $id_tipo_cuenta
 * @property int $id_banco
 * @property int $id_moneda
 * @property string|null $tipo 
 * @property string|null $tipo_documento 
 * @property string|null $doc_beneficiario 
 * @property string|null $nombre_beneficiario 
 * @property string $nro_cuenta
 * @property string $nro_interbancario
 * @property string $alias
 * @property int $id_usuario
 * @property int $estatus
 * @property string|null $fecha_registro
 * @property int $pago_movil 
 * @property string $nro_telefono
 *
 * @property Bancos $banco
 * @property Estatus $estatus0
 * @property TipoCuenta $tipoCuenta
 * @property TipoMoneda $moneda
 * @property Usuario $usuario
 * @property Operaciones[] $operaciones
 */
class Cuentabancariadeusuarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cuenta_bancaria_usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tipo_cuenta', 'id_banco', 'id_moneda', 'nro_cuenta', 'nro_interbancario', 'alias', 'id_usuario', 'estatus','nombre_beneficiario','tipo_documento','doc_beneficiario','pago_movil'], 'required'],
            
            [['nro_telefono'] , 'required' , 'when' => function($model) {
                     return ($model->nro_telefono!==null);
                 } , 'whenClient' => 'function(attribute,value){return ($("#cuentabancariadeusuarios-pago_movil").bootstrapSwitch("state")==true)}' ] ,

            [['id_tipo_cuenta', 'id_banco', 'id_moneda', 'id_usuario', 'estatus', 'pago_movil'], 'integer'],
            [['fecha_registro'], 'safe'],
            [[  'alias', 'nombre_beneficiario','tipo_documento',], 'string', 'max' => 45],
            [['tipo',  ], 'string', 'max' => 15],

            [['nro_cuenta',  ], 'string', 'length' => 17, ],

            [['nro_interbancario',  ], 'string', 'length' => 20,],


            [['nro_cuenta','doc_beneficiario', 'nro_telefono' ], 'number'],
            [['id_banco'], 'exist', 'skipOnError' => true, 'targetClass' => Bancos::className(), 'targetAttribute' => ['id_banco' => 'idbancos']],
            [['estatus'], 'exist', 'skipOnError' => true, 'targetClass' => Estatus::className(), 'targetAttribute' => ['estatus' => 'idstatus']],
            [['id_tipo_cuenta'], 'exist', 'skipOnError' => true, 'targetClass' => Tipodecuenta::className(), 'targetAttribute' => ['id_tipo_cuenta' => 'idtipocuenta']],
            [['id_moneda'], 'exist', 'skipOnError' => true, 'targetClass' => Tipodemoneda::className(), 'targetAttribute' => ['id_moneda' => 'idbtipomoneda']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['id_usuario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcuenta_bancaria' => Yii::t('app', 'Id'),
            'id_tipo_cuenta' => Yii::t('app', 'Tipo Cuenta'),
            'id_banco' => Yii::t('app', 'Banco'),
            'id_moneda' => Yii::t('app', 'Moneda'),
            'nro_cuenta' => Yii::t('app', 'Nro. de Cuenta'),
            'nro_interbancario' => Yii::t('app', 'Nro. Interbancario'),
            'alias' => Yii::t('app', 'Alias'),
            'id_usuario' => Yii::t('app', 'Usuario'),
            'estatus' => Yii::t('app', 'Estatus'),
            'fecha_registro' => Yii::t('app', 'Fecha de Registro'),
            'tipo' => Yii::t('app', 'Tipo'),
            'tipo_documento' => Yii::t('app', 'Tipo de Documento'),
            'doc_beneficiario' => Yii::t('app', 'Documento del Beneficiario'),
            'nombre_beneficiario' => Yii::t('app', 'Nombre del Beneficiario'),
            'pago_movil' => Yii::t('app', '¿Pago Movil?'),
            'nro_telefono' => Yii::t('app', 'Nro Telefónico'),
        ];
    }

    /**
     * Gets query for [[Banco]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBanco()
    {
        return $this->hasOne(Bancos::className(), ['idbancos' => 'id_banco']);
    }

    /**
     * Gets query for [[Estatus0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstatus0()
    {
        return $this->hasOne(Estatus::className(), ['idstatus' => 'estatus']);
    }

    /**
     * Gets query for [[TipoCuenta]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoCuenta()
    {
        return $this->hasOne(Tipodecuenta::className(), ['idtipocuenta' => 'id_tipo_cuenta']);
    }

    /**
     * Gets query for [[Moneda]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMoneda()
    {
        return $this->hasOne(Tipodemoneda::className(), ['idbtipomoneda' => 'id_moneda']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario() 
    {
        return $this->hasOne(Usuario::className(), ['id' => 'id_usuario']);
    }

    /**
     * Gets query for [[Operaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOperaciones()
    {
        return $this->hasMany(Operaciones::className(), ['id_cuenta_bancaria_usuario' => 'idcuenta_bancaria']);
    }

    public function getCuentanro()
    {
        return $this->alias.' - '.$this->moneda->monedas.' - '.$this->nro_cuenta;

    }
     public function getCuentanrodetalle()
    {
        if ($this->tipo=="Propia") {
           $nombreben=$this->usuario->datosusuariosproceso;
           $tipodocben=$this->usuario->tipoDocumento->descripcion;
           $nrodocben=$this->usuario->nro_documento;

        }else{
            
            $nombreben=$this->nombre_beneficiario;
            $tipodocben=$this->tipo_documento;
            $nrodocben=$this->doc_beneficiario;
        }

        return '<span class="operation-details-bank">Nombre: </span><br>'.$nombreben.
        '<br><span class="operation-details-bank">Documento: </span><br>'.$tipodocben.
        ' <br> <span class="operation-details-bank">Documento Nro.:</span> <a style="cursor: pointer;" title="Copiar" onclick=copyToClipboard("'.$nrodocben.'")>'.$nrodocben.'</a>'.
        '<br><span class="operation-details-bank">Banco: </span><br>'.$this->banco->descripcion.
        ' <br> <span class="operation-details-bank">Nro. Cta:</span> <a style="cursor: pointer;" title="Copiar" onclick=copyToClipboard("'.$this->nro_cuenta.'")>'.$this->nro_cuenta.'</a>';

    }

    public function getCuentanrodetallecierre()
    {
        if ($this->tipo=="Propia") {
           $informacionusuario=$this->usuario->datosusuarios;
        }else{
            $informacionusuario=$this->nombre_beneficiario.' - '.$this->doc_beneficiario;
        }
        return '<strong>'.$informacionusuario.'</strong>';
        //'<br><span class="operation-details-bank">Banco: </span>'.$this->banco->descripcion.
        //' <br> <span class="operation-details-bank">Nro. Cta:</span> '.$this->nro_cuenta;

    }

    public function getCuentanrodetalleupdate()
    {

           $informacionusuario=$this->usuario->datosusuarios;
        
        return $informacionusuario;
        //'<br><span class="operation-details-bank">Banco: </span>'.$this->banco->descripcion.
        //' <br> <span class="operation-details-bank">Nro. Cta:</span> '.$this->nro_cuenta;

    }

}


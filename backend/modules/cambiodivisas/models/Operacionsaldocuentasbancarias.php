<?php

namespace backend\modules\cambiodivisas\models;

use Yii;

/**
 * This is the model class for table "operacion_saldocuentas_bancarias".
 *
 * @property int $idopersaldocuentas
 * @property int $id_operacion_empresa
 * @property int $id_cuentabancaria_sistema
 * @property float $saldo_apertura
 * @property float $saldo_cierre
 * @property float $comprado 
 * @property float $saldo_anetrior 
 * @property int $apertura
 * @property CuentaBancariaSistema $cuentabancariaSistema
 * @property OperacionEmpresa $operacionEmpresa
 */
class Operacionsaldocuentasbancarias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operacion_saldocuentas_bancarias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_operacion_empresa', 'id_cuentabancaria_sistema', 'saldo_apertura', 'saldo_cierre','comprado', 'saldo_anetrior','apertura'], 'required'],
            [['id_operacion_empresa', 'id_cuentabancaria_sistema','apertura'], 'integer'],
            [['saldo_apertura', 'saldo_cierre','comprado', 'saldo_anetrior'], 'number'],
            [['id_cuentabancaria_sistema'], 'exist', 'skipOnError' => true, 'targetClass' => Cuentasbancariadesistema::className(), 'targetAttribute' => ['id_cuentabancaria_sistema' => 'idcuenta_bancaria']],
            [['id_operacion_empresa'], 'exist', 'skipOnError' => true, 'targetClass' => Operacionempresa::className(), 'targetAttribute' => ['id_operacion_empresa' => 'idoperacionempresa']],
        ];
    }

    /** 
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idopersaldocuentas' => Yii::t('app', 'Idopersaldocuentas'),
            'id_operacion_empresa' => Yii::t('app', 'Id Operación Empresa'),
            'id_cuentabancaria_sistema' => Yii::t('app', 'Cuenta Bancaria de Sistema'),
            'saldo_apertura' => Yii::t('app', 'Saldo de Apertura'),
            'saldo_cierre' => Yii::t('app', 'Saldo de Cierre'),
            'comprado' => Yii::t('app', 'Depósito'),
           'saldo_anetrior' => Yii::t('app', 'Saldo Anterior'),
           'apertura'=> Yii::t('app', 'Apertura'),
        ];
    }

    /**
     * Gets query for [[CuentabancariaSistema]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuentabancariasistema()
    {
        return $this->hasOne(Cuentasbancariadesistema::className(), ['idcuenta_bancaria' => 'id_cuentabancaria_sistema']); 
    }
 
    /**
     * Gets query for [[OperacionEmpresa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOperacionEmpresa()
    {
        return $this->hasOne(Operacionempresa::className(), ['idoperacionempresa' => 'id_operacion_empresa']);
    }

    public function getPaiscuentabanc()
    {
        return $this->cuentabancariasistema->paiscuenta; 
    }

    public function getCuentabancsistemoperativa()
    {
        return $this->cuentabancariasistema->nroctasselect; 
    }

}

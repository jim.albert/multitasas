<?php

namespace backend\modules\cambiodivisas\models;

use Yii;

/**
 * This is the model class for table "usuario".
 *
 * @property int $id
 * @property string $nombres
 * @property string $apellidos
 * @property int $id_tipo_documento
 * @property string $nro_documento
 * @property string $fecha_nacimiento
 * @property string $email
 * @property int $pep
 * @property string|null $nro_operacion
 * @property string|null $monto_operacion
 * @property int $estatus
 * @property int $id_pais 
 *
 * @property CuentaBancariaUsuarios[] $cuentaBancariaUsuarios
 * @property TipoDocumento $tipoDocumento
 * @property Estatus $estatus0
 * @property Paises $pais 
 * @property UsuarioPerfil[] $usuarioPerfils
 * @property OperacionesUsuarios[] $operacionesUsuarios
 */
class Usuario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'nombres', 'apellidos', 'id_tipo_documento', 'nro_documento', 'fecha_nacimiento', 'email', 'pep', 'estatus', 'id_pais'], 'required'],
            [['id', 'id_tipo_documento', 'pep', 'estatus', 'id_pais'], 'integer'],
            [['fecha_nacimiento'], 'safe'],
            [['nombres'], 'string', 'max' => 55],
            [['apellidos', 'nro_documento', 'nro_operacion', 'monto_operacion'], 'string', 'max' => 45],
            [['email'], 'string', 'max' => 255],
            [['email'], 'unique'],
            [['nro_documento'], 'unique'],
            [['id'], 'unique'],
            [['id_tipo_documento'], 'exist', 'skipOnError' => true, 'targetClass' => Tipodedocumento::className(), 'targetAttribute' => ['id_tipo_documento' => 'idbtipodocumento']],
            [['estatus'], 'exist', 'skipOnError' => true, 'targetClass' => Estatus::className(), 'targetAttribute' => ['estatus' => 'idstatus']],
            [['id_pais'], 'exist', 'skipOnError' => true, 'targetClass' => Paises::className(), 'targetAttribute' => ['id_pais' => 'idpaises']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombres' => Yii::t('app', 'Nombres'),
            'apellidos' => Yii::t('app', 'Apellidos'),
            'id_tipo_documento' => Yii::t('app', 'Tipo Documento'),
            'nro_documento' => Yii::t('app', 'Nro. Documento'),
            'fecha_nacimiento' => Yii::t('app', 'Fecha de Nacimiento'),
            'email' => Yii::t('app', 'Email'),
            'pep' => Yii::t('app', '¿Pep?'),
            'nro_operacion' => Yii::t('app', 'Nro de Operación'),
            'monto_operacion' => Yii::t('app', 'Monto de Operación'),
            'estatus' => Yii::t('app', 'Estatus'),
            'id_pais' => Yii::t('app', 'País'), 
        ];
    }

    /**
     * Gets query for [[CuentaBancariaUsuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuentaBancariaUsuarios()
    {
        return $this->hasMany(Cuentabancariadeusuarios::className(), ['id_usuario' => 'id']);
    }

    /**
     * Gets query for [[TipoDocumento]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoDocumento()
    {
        return $this->hasOne(Tipodedocumento::className(), ['idbtipodocumento' => 'id_tipo_documento']);
    }

    /**
     * Gets query for [[Estatus0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstatus0()
    {
        return $this->hasOne(Estatus::className(), ['idstatus' => 'estatus']);
    }

     /** 
    * Gets query for [[Pais]]. 
    * 
    * @return \yii\db\ActiveQuery 
    */ 
   public function getPais() 
   { 
       return $this->hasOne(Paises::className(), ['idpaises' => 'id_pais']); 
   } 

    /** 
    * Gets query for [[Pais]]. 
    * 
    * @return \yii\db\ActiveQuery 
    */ 
   public function getDatosusuarios() 
   { 
       return $this->nombres.' - '.$this->apellidos .' - '.$this->nro_documento;
   } 
   /** 
    * Gets query for [[Pais]]. 
    * 
    * @return \yii\db\ActiveQuery 
    */ 
   public function getDatosusuariosproceso() 
   { 
       return $this->nombres.'  '.$this->apellidos;
       //return '<span class="operation-details-bank">Nombres: </span>'.$this->nombres.'  '.$this->apellidos.' <br> <span class="operation-details-bank">Tipo de Documento:</span> '.$this->tipoDocumento->codigo.' <br> <span class="operation-details-bank">Nro. Doc.:</span> <a style="cursor: pointer;" title="Copiar" onclick=copyToClipboard('.$this->nro_documento.')>'.$this->nro_documento.'</a>';
   } 

   /**
     * Gets query for [[OperacionesUsuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOperacionesusuarios()
    {
        return $this->hasMany(Operacionesusuarios::className(), ['id_usuario' => 'id']);
    }
    /**
     * Gets query for [[UsuarioPerfils]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioperfils()
    {
        return $this->hasMany(Usuarioperfil::className(), ['id_usuario' => 'id']);
    }
}

 
<?php

namespace backend\modules\cambiodivisas\models;

use Yii;

/**
 * This is the model class for table "operaciones_usuarios".
 *
 * @property int $idoperusu
 * @property int $id_usuario
 * @property int $id_operacion
 * @property string $tipo_comision
 *
 * @property Operaciones $operacion
 * @property Usuario $usuario
 */
class Operacionesusuarios extends \yii\db\ActiveRecord
{
    public $count;
    public $operacioncheck;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operaciones_usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuario',], 'required'],
            [['id_usuario', 'id_operacion'], 'integer'],
            [['tipo_comision'], 'string', 'max' => 10],
            [['id_operacion'], 'exist', 'skipOnError' => true, 'targetClass' => Operaciones::className(), 'targetAttribute' => ['id_operacion' => 'idoperacion']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['id_usuario' => 'id']],
        ];
    }

    /** 
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idoperusu' => Yii::t('app', 'Idoperusu'), 
            'id_usuario' => Yii::t('app', 'Usuario'),
            'id_operacion' => Yii::t('app', 'Operación'),
            'tipo_comision'=> Yii::t('app', 'Tipo de Comisión'),
        ];
    }

    /**
     * Gets query for [[Operacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    // public function getOperacion()
    // {
    //     //return $this->hasOne(Operaciones::className(), ['idoperacion' => 'id_operacion']); 
    //     return $this->hasMany(Operaciones::className(), ['idoperacion' => 'id_operacion']);
    // }
    public function getOperacion()
    {
        return $this->hasOne(Operaciones::className(), ['idoperacion' => 'id_operacion']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'id_usuario']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioinfo()
    {
        return $this->usuario->datosusuariosproceso;
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuariooperacion()
    {
        return 'jjjj';
    }

}

<?php

namespace backend\modules\cambiodivisas\models;

use Yii;

/**
 * This is the model class for table "operaciones".
 *
 * @property int $idoperacion
 * @property string|null $codigo
 * @property int|null $id_banco_envia
 * @property int|null $id_cuenta_bancaria_usuario
 * @property int|null $id_tipo_moneda_envia
 * @property float|null $monto_envio
 * @property int|null $id_tipo_cambio
 * @property int|null $id_tipo_moneda_recibe
 * @property float|null $monto_recibe
 * @property float|null $monto_cambio_usado
 * @property int|null $id_cuenta_bancaria_sistema
 * @property string|null $cod_referencia
 * @property string|null $fecha_registro
 * @property string|null $fecha_pago_sistema
 * @property string|null $referancia_proceso 
 * @property string|null $fecha_proceso
 * @property int|null $estatus
 * @property int $com_agentes
 *
 * @property Bancos $bancoEnvia
 * @property CuentaBancariaSistema $cuentaBancariaSistema
 * @property CuentaBancariaUsuarios $cuentaBancariaUsuario
 * @property Estatus $estatus0
 * @property TipoCambio $tipoCambio
 * @property TipoMoneda $tipoMonedaEnvia
 * @property TipoMoneda $tipoMonedaRecibe
 * @property OperacionComprobantes[] $operacionComprobantes
* @property OperacionesUsuarios[] $operacionesUsuarios
* @property CuentaBancariaSistemaSaldos[] $cuentaBancariaSistemaSaldos
 */
class Operaciones extends \yii\db\ActiveRecord
{
    // const SCENARIO_PROCESO = 'proceso';
    // const SCENARIO_PAGA = 'paga';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operaciones';
    }

    // public function scenarios()
    // {
    //     $scenarios = parent::scenarios();
    //     $scenarios[self::SCENARIO_PROCESO] = [ 'estatus','fecha_proceso'];
    //     $scenarios[self::SCENARIO_PAGA] = ['referancia_proceso', 'estatus','fecha_pago_sistema'];

    //     return $scenarios;
    // }

      const SCENARIOPROCESO = 'proceso'; 
      const SCENARIOPAGA = 'paga';
      const SCENARIOUPDATE = 'update';
      
      public $agentes;

        // scenarios encapsulated
        public function getCustomScenarios()
        {
          
          return [
              self::SCENARIOPROCESO   =>  ['estatus','fecha_proceso'],
              self::SCENARIOPAGA      =>  ['estatus','fecha_pago_sistema'],
              self::SCENARIOUPDATE    =>  ['cod_referencia', 'id_banco_envia','id_cuenta_bancaria_usuario','monto_envio','id_tipo_moneda_envia','monto_cambio_usado','monto_recibe'],
          ];
        }
        // get scenarios
        public function scenarios()
        {
            $scenarios = $this->getCustomScenarios();
            return $scenarios;
        }

        // modify itens required for rules
        public function ModifyRequired()
        {

          $allscenarios = $this->getCustomScenarios();
          // published not required
          $allscenarios[self::SCENARIOPROCESO] = array_diff($allscenarios[self::SCENARIOPAGA],$allscenarios[self::SCENARIOUPDATE],['referancia_proceso']);
          return $allscenarios;

        }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $allscenarios = $this->ModifyRequired();
        return [
           // [['id_banco_envia', 'id_cuenta_bancaria_usuario', 'id_tipo_moneda_envia', 'monto_envio',
           //   'id_tipo_cambio', 'id_tipo_moneda_recibe', 'monto_recibe', 'monto_cambio_usado', 'fecha_registro', 'estatus','id_cuenta_bancaria_sistema','cod_referencia','tipo'], 'required', 'except' =>self::SCENARIO_PROCESO],
            [$allscenarios[self::SCENARIOPROCESO], 'required', 'on' => self::SCENARIOPROCESO],
            [$allscenarios[self::SCENARIOPAGA], 'required', 'on' => self::SCENARIOPAGA],
            [$allscenarios[self::SCENARIOUPDATE], 'required', 'on' => self::SCENARIOUPDATE],

            // [['referancia_proceso', 'estatus',], 'required', 'on' => self::SCENARIO_PROCESO],
            
            // [['id_cuenta_bancaria_sistema','cod_referencia'] , 'required' , 'when' => function($model) {
            //         return ($model->cod_referencia!==null);
            //     } , 'whenClient' => 'function(attribute,value){return ($("#Cod_refencia").bootstrapSwitch("state")==true)}' ] ,
            [['referancia_proceso'], 'string', 'max' => 30],

            [['id_banco_envia', 'id_cuenta_bancaria_usuario', 'id_tipo_moneda_envia', 'id_tipo_cambio', 'id_cuenta_bancaria_sistema', 'estatus', 'com_agentes'], 'integer'],
            [['monto_envio', 'monto_recibe', 'monto_cambio_usado'], 'number'],
            [['fecha_registro', 'fecha_pago_sistema', 'fecha_proceso', 'id_tipo_moneda_recibe'], 'safe'],
            [['codigo', 'cod_referencia'], 'string', 'max' => 45],
            [['codigo'], 'unique'],
            [['id_banco_envia'], 'exist', 'skipOnError' => true, 'targetClass' => Bancos::className(), 'targetAttribute' => ['id_banco_envia' => 'idbancos']],
            [['id_cuenta_bancaria_sistema'], 'exist', 'skipOnError' => true, 'targetClass' => Cuentasbancariadesistema::className(), 'targetAttribute' => ['id_cuenta_bancaria_sistema' => 'idcuenta_bancaria']],
            [['id_cuenta_bancaria_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Cuentabancariadeusuarios::className(), 'targetAttribute' => ['id_cuenta_bancaria_usuario' => 'idcuenta_bancaria']],
            [['estatus'], 'exist', 'skipOnError' => true, 'targetClass' => Estatus::className(), 'targetAttribute' => ['estatus' => 'idstatus']],
            [['id_tipo_cambio'], 'exist', 'skipOnError' => true, 'targetClass' => Tipodecambio::className(), 'targetAttribute' => ['id_tipo_cambio' => 'idcambio']],
            [['id_tipo_moneda_envia'], 'exist', 'skipOnError' => true, 'targetClass' => Tipodemoneda::className(), 'targetAttribute' => ['id_tipo_moneda_envia' => 'idbtipomoneda']],
            [['id_tipo_moneda_recibe'], 'exist', 'skipOnError' => true, 'targetClass' => Tipodemoneda::className(), 'targetAttribute' => ['id_tipo_moneda_recibe' => 'idbtipomoneda']], 
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idoperacion' => Yii::t('app', 'Idoperacion'),
            'codigo' => Yii::t('app', 'Código'),
            'id_banco_envia' => Yii::t('app', 'Entidad Bancaria desde la que nos enviarás el dinero.'),
            'id_cuenta_bancaria_usuario' => Yii::t('app', '¿A qué cuenta deseas que depositemos el dinero?'),
            'id_tipo_moneda_envia' => Yii::t('app', 'Moneda de Envío'),
            'monto_envio' => Yii::t('app', 'Quiero comprar:'),
            'id_tipo_cambio' => Yii::t('app', 'Tipo de Cambio'),
            'id_tipo_moneda_recibe' => Yii::t('app', 'Moneda que Recibe'),
            'monto_recibe' => Yii::t('app', 'Monto a Trasferir'),
            'monto_cambio_usado' => Yii::t('app', 'Tipo de Cambio'),
            'id_cuenta_bancaria_sistema' => Yii::t('app', 'Cuenta Bancaria En la que nos Transferiste/Depositaste el Dinero'),
            'cod_referencia' => Yii::t('app', 'Código de Referencia / Número de Operación'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_pago_sistema' => Yii::t('app', 'Fecha Pago Sistema'),
            'referancia_proceso' => Yii::t('app', 'Referencia de Transferencia'),
            'fecha_proceso' => Yii::t('app', 'Fecha Proceso'),
            'estatus' => Yii::t('app', 'Estatus'),
            'tipo' => Yii::t('app', 'Operación'),
            'com_agentes'=> Yii::t('app', 'Com. Agente'),
            
        ];
    }

     
     /**
     * Gets query for [[CuentaBancariaSistemaSaldos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuentabancariasistemasaldos()
    {
        return $this->hasMany(Cuentasancariasistemasaldos::className(), ['id_operacion' => 'idoperacion']);
    }
     /**
     * Gets query for [[BancoEnvia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBancoEnvia()
    {
        return $this->hasOne(Bancos::className(), ['idbancos' => 'id_banco_envia']);
    }

    /**
     * Gets query for [[CuentaBancariaSistema]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuentaBancariaSistema()
    {
        return $this->hasOne(Cuentasbancariadesistema::className(), ['idcuenta_bancaria' => 'id_cuenta_bancaria_sistema']);
    }

    /**
     * Gets query for [[CuentaBancariaUsuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuentabancariabsuario()
    {
        return $this->hasOne(Cuentabancariadeusuarios::className(), ['idcuenta_bancaria' => 'id_cuenta_bancaria_usuario']);
    }

    /**
     * Gets query for [[Estatus0]]. 
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstatus0()
    {
        return $this->hasOne(Estatus::className(), ['idstatus' => 'estatus']);
    }

    /**
     * Gets query for [[TipoCambio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoCambio()
    {
        return $this->hasOne(Tipodecambio::className(), ['idcambio' => 'id_tipo_cambio']);
    }

    /**
     * Gets query for [[TipoMonedaEnvia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoMonedaEnvia()
    {
        return $this->hasOne(Tipodemoneda::className(), ['idbtipomoneda' => 'id_tipo_moneda_envia']);
    }

    /**
     * Gets query for [[TipoMonedaRecibe]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoMonedaRecibe()
    {
        return $this->hasOne(Tipodemoneda::className(), ['idbtipomoneda' => 'id_tipo_moneda_recibe']);
    }

    /**
     * Gets query for [[TipoMonedaRecibe]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSoporteoperacion()
    {
        return 'Código: '.$this->codigo.' - Cuenta Bancaria:'.$this->cuentabancariabsuario->alias;
        /*tipo de oiperacion */
    }
    /**
     * Gets query for [[OperacionesUsuarios]].
     *
     * @return \yii\db\ActiveQuery 
     */
    // public function getOperacionesusuarios()
    // {
    //     //return $this->hasMany(Operacionesusuarios::className(), ['id_operacion' => 'idoperacion']);
    //     return $this->hasOne(Operacionesusuarios::className(), ['id_operacion' => 'idoperacion']);
    // }

    public function getOperacionesusuarios()
    {
        return $this->hasMany(Operacionesusuarios::className(), ['id_operacion' => 'idoperacion']);
    }
    /**
     * Gets query for [[OperacionComprobantes]]. 
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOperacioncomprobantes()
    {
        return $this->hasMany(Operacioncomprobantes::className(), ['id_operacion' => 'idoperacion']);
    }

    public function getUseropera()
    {
        //return $this->operacionesusuarios->id_usuario;
        return $this->operacion->codigo;
    }


}
 
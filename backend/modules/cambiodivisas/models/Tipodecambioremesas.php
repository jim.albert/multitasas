<?php

namespace backend\modules\cambiodivisas\models;

use Yii;

/**
 * This is the model class for table "tipo_cambio".
 *
 * @property int $idcambio
 * @property string $codigo
 * @property string $descripcion
 * @property string $operacion
 * @property float $monto
 * @property string $fecha
 * @property int $estatus
 * @property float $venta 
 * @property float $porcentaje_ganancia 
 * @property float $compra_reserva 
 * @property float $venta_reserva 
 *
 * @property Operaciones[] $operaciones
 * @property Estatus $estatus0
 */
class Tipodecambioremesas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo_cambio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['codigo', 'descripcion', 'monto', 'fecha', 'estatus'], 'required'],
            //[['monto'], 'number'],
            [['codigo', 'descripcion', 'operacion', 'fecha', 'estatus', 'venta', 'porcentaje_ganancia', 'compra_reserva', 'venta_reserva'], 'required'],
            [['monto', 'venta', 'porcentaje_ganancia', 'compra_reserva', 'venta_reserva'], 'number'],
            [['fecha'], 'safe'],
            [['estatus'], 'integer'],
            [['codigo', 'descripcion'], 'string', 'max' => 45],
            [['operacion'], 'string', 'max' => 15],
            [['estatus'], 'exist', 'skipOnError' => true, 'targetClass' => Estatus::className(), 'targetAttribute' => ['estatus' => 'idstatus']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcambio' => Yii::t('app', 'Id Tipo de Cambio'),
            'codigo' => Yii::t('app', 'Código'),
            'descripcion' => Yii::t('app', 'Descripción'),
            'operacion' => Yii::t('app', 'Operación'),
            'monto' => Yii::t('app', 'Tasa del Día Dolares'),
            'venta' => Yii::t('app', 'Tasa del Día Soles'), 
            'fecha' => Yii::t('app', 'Fecha'),
            'estatus' => Yii::t('app', 'Estatus'),
            'porcentaje_ganancia' => Yii::t('app', 'Porcentaje de Ganancia'), 
            'compra_reserva' => Yii::t('app', 'Compra BCRP'), 
            'venta_reserva' => Yii::t('app', 'Venta BCRP'),
        ];
    }

    /**
     * Gets query for [[Operaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOperaciones()
    {
        return $this->hasMany(Operaciones::className(), ['id_tipo_cambio' => 'idcambio']);
    }

    /**
     * Gets query for [[Estatus0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstatus0()
    {
        return $this->hasOne(Estatus::className(), ['idstatus' => 'estatus']);
    }
}

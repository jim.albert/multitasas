<?php

namespace backend\modules\cambiodivisas\models;

use Yii;

/**
 * This is the model class for table "operacion_empresa".
 *
 * @property int $idoperacionempresa
 * @property string $fecha_apertura
 * @property string|null $fecha_cierre
 * @property float $porcentaje_pagador
 * @property float $porcentaje_interbancario
 * @property float $porcentaje_pmovil
 * @property float $porcentaje_agentes
 * @property float $tasa_mayor_dolar
 * @property float $tasa_mayor_soles
 * @property float $porcentaje_dolar
 * @property float $porcentaje_soles
 * @property float $saldo_apertura
 * @property float $saldo_cierre
 * @property float $saldo_anterior
 * @property int $estatus
 * @property int $permite_operaciones
 * @property int $count_cuentabancarias
 */
class Operacionempresa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $count_cuentabancarias;

    public $desde;
    public $hasta;

    public static function tableName()
    {
        return 'operacion_empresa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_apertura', 'porcentaje_pagador', 'porcentaje_interbancario', 'porcentaje_pmovil', 'porcentaje_agentes', 'tasa_mayor_dolar', 'tasa_mayor_soles', 'porcentaje_dolar', 'porcentaje_soles', 'saldo_apertura', 'saldo_cierre', 'saldo_anterior', 'estatus', 'permite_operaciones'], 'required'],
            [['fecha_apertura', 'fecha_cierre', 'desde', 'hasta'], 'safe'],
            [['porcentaje_pagador', 'porcentaje_interbancario', 'porcentaje_pmovil', 'porcentaje_agentes', 'tasa_mayor_dolar', 'tasa_mayor_soles', 'porcentaje_dolar', 'porcentaje_soles', 'saldo_apertura', 'saldo_cierre', 'saldo_anterior'], 'number'],
            [['estatus','permite_operaciones'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            // 'idoperacionempresa' => Yii::t('app', 'Idoperacionempresa'),
            // 'fecha_apertura' => Yii::t('app', 'Fecha de Apertura'),
            // 'fecha_cierre' => Yii::t('app', 'Fecha de Cierre'),
            // 'saldo_apertura' => Yii::t('app', 'Tasa Al Mayor (Soles)'),
            // 'saldo_cierre' => Yii::t('app', 'Saldo de Cierre'),
            // 'porcentaje_pagador' => Yii::t('app', 'Porcentaje Comisión Usuario'),
            // 'porcentaje_pmovil' => Yii::t('app', 'Porcentaje Pago movil'),
            // 'porcentaje_interbancario' => Yii::t('app', 'Porcentaje Interbancario'),
            // 'saldo_anterior' => Yii::t('app', 'Tasa Al Mayor (Dólares)'),
            // 'estatus' => Yii::t('app', 'Estatus'), 
            'idoperacionempresa' => Yii::t('app', 'Idoperacionempresa'),
            'fecha_apertura' => Yii::t('app', 'Fecha Apertura'),
            'fecha_cierre' => Yii::t('app', 'Fecha Cierre'),
            'porcentaje_pagador' => Yii::t('app', 'Porcentaje Pagador'),
            'porcentaje_interbancario' => Yii::t('app', 'Porcentaje Interbancario'),
            'porcentaje_pmovil' => Yii::t('app', 'Porcentaje Pmovil'),
            'porcentaje_agentes' => Yii::t('app', 'Porcentaje Agentes'),
            'tasa_mayor_dolar' => Yii::t('app', 'Tasa al Mayor -$'),
            'tasa_mayor_soles' => Yii::t('app', 'Tasa al Mayor -S/'),
            'porcentaje_dolar' => Yii::t('app', 'Porcentaje Ganacia -$'),
            'porcentaje_soles' => Yii::t('app', 'Porcentaje Ganacia -S/'),
            'saldo_apertura' => Yii::t('app', 'Saldo Apertura'),
            'saldo_cierre' => Yii::t('app', 'Saldo Cierre'),
            'saldo_anterior' => Yii::t('app', 'Saldo Anterior'),
            'estatus' => Yii::t('app', 'Estatus'),
            'permite_operaciones' => Yii::t('app', 'Registro de Operaciones'),

            'desde' => Yii::t('app', 'Desde'),
            'hasta' => Yii::t('app', 'Hasta'),
            
        ];
    }

    /**
     * Gets query for [[OperacionSaldocuentasBancarias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOperacionSaldocuentasBancarias()
    {
        return $this->hasMany(Operacionsaldocuentasbancarias::className(), ['id_operacion_empresa' => 'idoperacionempresa']);
    }
}

<?php

namespace backend\modules\cambiodivisas\controllers;

use Yii;
use backend\modules\cambiodivisas\models\Operacionsaldocuentasbancarias;
use backend\modules\cambiodivisas\models\OperacionsaldocuentasbancariasSearch;
use backend\modules\cambiodivisas\models\OperacionsaldocuentasbancariasgroupSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OperacionsaldocuentasbancariasController implements the CRUD actions for Operacionsaldocuentasbancarias model.
 */
class OperacionsaldocuentasbancariasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Operacionsaldocuentasbancarias models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $searchModel = new OperacionsaldocuentasbancariasgroupSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Operacionsaldocuentasbancarias model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            return $this->render('view', [
            'model' => $this->findModel($id), 
            ]);
        }
    }

    /**
     * Displays a single Operacionsaldocuentasbancarias model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDetalle($id, $id_cuentabancaria_sistema)
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {

            echo $id_cuentabancaria_sistema;

            return $this->render('detalle', [
            'id_operacion_empresa' => $id, 
            'id_cuentabancaria_sistema' => $id_cuentabancaria_sistema, 
            ]);
        }
    }

    /**
     * Creates a new Operacionsaldocuentasbancarias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $session = Yii::$app->session;
            $apertura=$session['apertura'];
            $model = new Operacionsaldocuentasbancarias();
            $model->saldo_cierre=0;
            $model->saldo_anetrior=0; 
            $model->id_operacion_empresa=$apertura['idoperacionempresa'];
            $model->apertura=0;

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['/']);
            }

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Operacionsaldocuentasbancarias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {

            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->idopersaldocuentas]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Operacionsaldocuentasbancarias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Operacionsaldocuentasbancarias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Operacionsaldocuentasbancarias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Operacionsaldocuentasbancarias::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

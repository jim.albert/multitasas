<?php

namespace backend\modules\cambiodivisas\controllers;

use Yii;
use backend\modules\cambiodivisas\models\Cuentasbancariadesistema;
use backend\modules\cambiodivisas\models\CuentasbancariadesistemaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CuentasbancariadesistemaController implements the CRUD actions for Cuentasbancariadesistema model.
 */
class CuentasbancariadesistemaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cuentasbancariadesistema models.
     * @return mixed
     */
    public function actionIndex()
    {

        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {

            $searchModel = new CuentasbancariadesistemaSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Cuentasbancariadesistema model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Cuentasbancariadesistema model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {

            $model = new Cuentasbancariadesistema();

            if ($model->load(Yii::$app->request->post())) {


                if ( $model->save() ) {
                        Yii::$app->session->setFlash('success','El Registro de la Cuenta Bancaria '.$model->alias.' - '.$model->moneda->monedas.' se realizó correctamente');
                        return $this->redirect(['index']);
                    } else {

                        $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                        Yii::$app->session->setFlash('error',$errores);
                    }

                
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            } 
        }
    }

    /**
     * Updates an existing Cuentasbancariadesistema model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {

            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post())) {


                if ( $model->save() ) {
                        Yii::$app->session->setFlash('success','Actualización de la Cuenta Bancaria '.$model->alias.' - '.$model->moneda->monedas.' se realizó correctamente');
                        return $this->redirect(['index']);
                    } else {

                        $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                        Yii::$app->session->setFlash('error',$errores);
                    }

                
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            } 
        }
    }

    /**
     * Deletes an existing Cuentasbancariadesistema model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {

            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Cuentasbancariadesistema model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cuentasbancariadesistema the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cuentasbancariadesistema::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

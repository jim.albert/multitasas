<?php

namespace backend\modules\cambiodivisas\controllers;

use yii\web\Controller;
use Yii;
/**
 * Default controller for the `cambiodivisas` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
        	return $this->render('index');
        }
    }
}

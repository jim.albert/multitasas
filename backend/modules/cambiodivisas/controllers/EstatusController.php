<?php

namespace backend\modules\cambiodivisas\controllers;

use Yii;
use backend\modules\cambiodivisas\models\Estatus;
use backend\modules\cambiodivisas\models\EstatusSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;



/**
 * EstatusController implements the CRUD actions for Estatus model.
 */
class EstatusController extends Controller
{




    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],


        ];

    }

    /**
     * Lists all Estatus models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $searchModel = new EstatusSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->setPagination(['pageSize' => 10]);
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Estatus model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            return $this->render('view', [
            'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Estatus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
       
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $model = new Estatus();
            $data_cod=[

                'Bancos'=>'BNC-',
                'Cuentas Bancarias del Sistema'=>'CBS-',
                'Cuentas Bancarias de Usuarios'=>'CBU-',
                'Empresa'=>'EMP-',
                'Operaciones'=>'OPR-',
                'Pais'=>'PAS-',
                'Perfiles de Accesos'=>'PFA-',
                'Tipo de Cambio'=>'TCA-',
                'Tipo de Cuenta'=>'TCU-',
                'Tipo de Documento'=>'TDO-',
                'Tipo de Monedas'=>'TMO-',
                'Usuarios'=>'USR-',
                'Validaciones'=>'VAL-',
        
            ];
            
            if ($model->load(Yii::$app->request->post())) {

                $model->codigo=$data_cod[$model->tabla].strtoupper($model->descripcion);

                if ( $model->save() ) {
                        Yii::$app->session->setFlash('success','El Registro del Estatus '.$model->codigo.' se realizó correctamente');
                        return $this->redirect(['index']);
                    } else {

                        $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                        Yii::$app->session->setFlash('error',$errores);
                    }

                
                } else {
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
        }
    }

    /**
     * Updates an existing Estatus model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $model = $this->findModel($id);
            $data_cod=[

                'Bancos'=>'BNC-',
                'Cuentas Bancarias del Sistema'=>'CBS-',
                'Cuentas Bancarias de Usuarios'=>'CBU-',
                'Empresa'=>'EMP-',
                'Operaciones'=>'OPR-',
                'Pais'=>'PAS-',
                'Perfiles de Accesos'=>'PFA-',
                'Tipo de Cambio'=>'TCA-',
                'Tipo de Cuenta'=>'TCU-',
                'Tipo de Documento'=>'TDO-',
                'Tipo de Monedas'=>'TMO-',
                'Usuarios'=>'USR-',
                'Validaciones'=>'VAL-',
        
            ];
            $model->fecha= date('Y-m-d');

            // if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //     return $this->redirect(['view', 'id' => $model->idstatus]);
            // }

            // return $this->render('update', [
            //     'model' => $model,
            // ]);
            if ($model->load(Yii::$app->request->post())) {

                $model->codigo=$data_cod[$model->tabla].strtoupper($model->descripcion);

                if ( $model->save() ) {
                        Yii::$app->session->setFlash('success','Actualización del Estatus '.$model->codigo.' se realizó correctamente');
                        return $this->redirect(['index']);
                    } else {

                        $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                        Yii::$app->session->setFlash('error',$errores);
                    }

                
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Deletes an existing Estatus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    // public function actionDelete($id)
    // {
    //     $this->findModel($id)->delete();

    //     return $this->redirect(['index']);
    // }

    /**
     * Finds the Estatus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Estatus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Estatus::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

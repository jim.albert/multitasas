<?php

namespace backend\modules\cambiodivisas\controllers;

use Yii;
use yii\base\Model;
use backend\modules\cambiodivisas\models\Operacionesusuarios;
use backend\modules\cambiodivisas\models\OperacionesusuariosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OperacionesusuariosController implements the CRUD actions for Operacionesusuarios model.
 */
class OperacionesusuariosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Operacionesusuarios models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OperacionesusuariosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Operacionesusuarios model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Operacionesusuarios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {

            // $count = count(Yii::$app->request->post('Operacionesusuarios', []));

             $model = new Operacionesusuarios();

            // for($i = 1; $i < $count; $i++) {
            //     $model[] = new Operacionesusuarios();
            // }

            // if (Model::loadMultiple($model, Yii::$app->request->post()) && Model::validateMultiple($model)) {

            //     foreach ($model as $model) {

            //         //Try to save the models. Validation is not needed as it's already been done.
            //         $model->save(false);

            //     }
            //     return $this->redirect('view');
            // }

             $model2 =[new Operacionesusuarios()];

            // $model->id_operacion=0;

            if ($model->load(Yii::$app->request->post()) && $model2[0]->load(Yii::$app->request->post())) {

                $total=$_POST['Operacionesusuarios']['count'];
                $useroper=$model->id_usuario;
                $successinsert=false;


                for ($i = 0; $i <= $total; $i++) {
                    

                        $checkoperacion=$_POST['Operacionesusuarios']['operacioncheck'][$i];

                        if ($checkoperacion==1) {
                            $idoperacion=$_POST['Operacionesusuarios'][$i]['id_operacion'];
                            $Operacionesusuarios= new Operacionesusuarios();
                            $Operacionesusuarios->id_operacion=$idoperacion;
                            $Operacionesusuarios->id_usuario=$useroper;
                            $Operacionesusuarios->tipo_comision='Operador';
                            $Operacionesusuarios->save();
                            $successinsert=true;

                            $command =  Yii::$app->db->createCommand('UPDATE operaciones SET estatus=23, fecha_proceso="'.date('Y-m-d h:m:s').'" WHERE idoperacion='.$idoperacion.'');
                            $command->execute();
                            
                        }
                }


                if ( $successinsert==true ) {
                        Yii::$app->session->setFlash('success','Operaciones Asignadas al Usuarios: <strong>'.$model->usuario->datosusuariosproceso.'</strong>');
                        return $this->redirect(['/']);
                    } else {

                        $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                        Yii::$app->session->setFlash('error',$errores);
                        return $this->render('create', [
                'model' => $model,'model2' => $model2,
            ]);
                    }


                
                
            }

            return $this->render('create', [
                'model' => $model,'model2' => $model2,
            ]);
        }
    }

    /**
     * Updates an existing Operacionesusuarios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idoperusu]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Operacionesusuarios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Operacionesusuarios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Operacionesusuarios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Operacionesusuarios::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

<?php

namespace backend\modules\cambiodivisas\controllers;

use Yii;
use backend\modules\cambiodivisas\models\Tipodecambio;
use backend\modules\cambiodivisas\models\TipodecambioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * TipodecambioController implements the CRUD actions for Tipodecambio model.
 */
class TipodecambioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tipodecambio models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $searchModel = new TipodecambioSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->setPagination(['pageSize' => 10]);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Tipodecambio model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            return $this->render('view', [
            'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Tipodecambio model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $model = new Tipodecambio();
            $model->codigo = "CD-".date("Y-m-d");
            $model->fecha = date("Y-m-d h:m:s");
            $model->operacion="CAMBIO";

            $data_api = json_decode( file_get_contents('https://deperu.com/api/rest/cotizaciondolar.json'), true );
            $model->compra_reserva=$data_api['Cotizacion'][0]['Compra'];
            $model->venta_reserva=$data_api['Cotizacion'][0]['Venta'];
            
            //print_r($data);

            if ($model->load(Yii::$app->request->post())) {

                Tipodecambio::updateAll(['estatus' => '10'],'operacion="CAMBIO"');

                if ( $model->save() ) {
                        Yii::$app->session->setFlash('success','El Registro del Tipo de Cambio '.$model->codigo.' se realizó correctamente');
                        return $this->redirect(['index']);
                    } else {

                        $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                        Yii::$app->session->setFlash('error',$errores);
                    }

                
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
                
            }
        }
    }

    public function actionCreatetasa()
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $model = new Tipodecambio(); 
            $model->codigo = "ED-".date("Y-m-d");
            $model->fecha = date("Y-m-d h:m:s");
            $model->operacion="REMESA";

            // $data_api = json_decode( file_get_contents('https://deperu.com/api/rest/cotizaciondolar.json'), true );
             $model->compra_reserva=0;
             $model->venta_reserva=0;
             $model->porcentaje_ganancia=0;
             $model->monto=0;
            //print_r($data);

            if ($model->load(Yii::$app->request->post())) {

                Tipodecambio::updateAll(['estatus' => '10'],'operacion="REMESA"');

                if ( $model->save() ) {
                        Yii::$app->session->setFlash('success','El Registro del Tipo de Tasa '.$model->codigo.' se realizó correctamente');
                        return $this->redirect(['index']);
                    } else {

                        $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                        Yii::$app->session->setFlash('error',$errores);
                    }

                
            } else {
                return $this->render('remesa', [
                    'model' => $model,
                ]);
                
            }
        }
    }

    /**
     * Updates an existing Tipodecambio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post())) {


                if ( $model->save() ) {
                        Yii::$app->session->setFlash('success','El Registro del Tipo de Cambio '.$model->codigo.' se realizó correctamente');
                        return $this->redirect(['index']);
                    } else {

                        $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                        Yii::$app->session->setFlash('error',$errores);
                    }

                
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing Tipodecambio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdatetasa($id)
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post())) {


                if ( $model->save() ) {
                        Yii::$app->session->setFlash('success','El Registro del Tipo de Tasa '.$model->codigo.' se realizó correctamente');
                        return $this->redirect(['index']);
                    } else {

                        $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                        Yii::$app->session->setFlash('error',$errores);
                    }

                
            } else {
                return $this->render('updatetasa', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Deletes an existing Tipodecambio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tipodecambio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tipodecambio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tipodecambio::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

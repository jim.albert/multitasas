<?php

namespace backend\modules\cambiodivisas\controllers;

use Yii;
//use yii\base\Model;
use backend\modules\cambiodivisas\models\Model;
use backend\modules\cambiodivisas\models\Operacionempresa;
use backend\modules\cambiodivisas\models\Operacionsaldocuentasbancarias;
use backend\modules\cambiodivisas\models\OperacionempresaSearch;
use backend\modules\cambiodivisas\models\Empresa;
use backend\modules\cambiodivisas\models\Tipodecambio;
use backend\modules\cambiodivisas\models\Tipodecambioremesas;
use backend\modules\cambiodivisas\models\Cuentabancariasistemasaldos;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OperacionempresaController implements the CRUD actions for Operacionempresa model.
 */
class OperacionempresaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Operacionempresa models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $searchModel = new OperacionempresaSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Operacionempresa model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }*/

    /**
     * Creates a new Operacionempresa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $tipocambio=Tipodecambio::find()->where(['estatus' => '9', 'operacion'=>'CAMBIO'])->orderBy('codigo ASC')->one();

            
            $empresa=Empresa::find()->orderBy('id ASC')->one();

            $model = new Operacionempresa();
            $modelcuentasbanco= [new Operacionsaldocuentasbancarias()];
            $tipocambiomodel= new Tipodecambio();
            $tiporemesamodel= new Tipodecambioremesas();

            $model->fecha_apertura=date('Y-m-d H:i:s');
            $model->saldo_apertura=0;
            $model->saldo_cierre=0;
            $model->saldo_anterior=0;
            $model->estatus=1;
            $model->porcentaje_soles=0;
            $model->porcentaje_dolar=0;
            $model->porcentaje_pagador=$empresa->porcentaje_pagador;
            $model->porcentaje_pmovil=$empresa->porcentaje_pmovil;
            $model->porcentaje_interbancario=$empresa->porcentaje_interbancario;
            $model->porcentaje_agentes=$empresa->porcentaje_agentes;
            $model->tasa_mayor_dolar=0;
            $model->tasa_mayor_soles=0;
            $model->permite_operaciones=1;

            /*Campos para tipo de cambio Remesas*/
            $tiporemesamodel->codigo='Apr-'.date('Y-m-d H:i:s');
            $tiporemesamodel->descripcion='Apertura Diaria del sistema';
            $tiporemesamodel->operacion='REMESA';
            $tiporemesamodel->fecha = date("Y-m-d h:m:s");
            $tiporemesamodel->compra_reserva=0;
            $tiporemesamodel->venta_reserva=0;
            $tiporemesamodel->porcentaje_ganancia=0;
            $tiporemesamodel->estatus=9;
            $tiporemesamodel->monto=0;
            
            /*Campos para tipo de cambios cambios de dinero*/
            $tipocambiomodel->codigo = "Apr-".date("Y-m-d");
            $tipocambiomodel->descripcion='Apertura Diaria del sistema';
            $tipocambiomodel->fecha = date("Y-m-d h:m:s");
            $tipocambiomodel->operacion="CAMBIO";

            $contenidoapi=@file_get_contents('https://deperu.com/api/rest/cotizaciondolar.json');

            if ($contenidoapi) {
               $data_api = json_decode($contenidoapi , true );
               $tipocambiomodel->compra_reserva=$data_api['Cotizacion'][0]['Compra'];
                $tipocambiomodel->venta_reserva=$data_api['Cotizacion'][0]['Venta'];
            } else {
                $tipocambiomodel->compra_reserva=$tipocambio->compra_reserva;
                $tipocambiomodel->venta_reserva=$tipocambio->venta_reserva;
            }
            
            //$data_api = json_decode($contenidoapi , true );
            //$tipocambiomodel->compra_reserva=$data_api['Cotizacion'][0]['Compra'];
            //$tipocambiomodel->venta_reserva=$data_api['Cotizacion'][0]['Venta'];
            $tipocambiomodel->estatus=9;
             $tipocambiomodel->porcentaje_ganancia=0;
            

            //$modelcuentasbanco[0]->saldo_anetrior=20;
             $modelcuentasbanco[0]->saldo_cierre=0;
             $modelcuentasbanco[0]->comprado="";
             
            

            if ($model->load(Yii::$app->request->post()) ) {

               // && $tipocambiomodel->load(Yii::$app->request->post()) && $modelcuentasbanco[0]->load(Yii::$app->request->post()))

                    $tiporemesamodel->monto=0;//$_POST['tiporemesamodel-monto'];
                    $tiporemesamodel->venta=$_POST['tiporemesamodel-venta'];

                    $tipocambiomodel->monto=$_POST['tipocambiomodel-monto'];
                    $tipocambiomodel->venta=$_POST['tipocambiomodel-venta'];
                    // $tipocambiomodel->porcentaje_ganancia=$_POST['tipocambiomodel-porcentaje_ganancia'];
                    $tipocambiomodel->porcentaje_ganancia=0;
                    
                    $total=$_POST['Operacionempresa']['count_cuentabancarias'];

               
                if ( $model->save() ) {
                 

                        /*Guardo tipo de Remesa*/
                        Tipodecambioremesas::updateAll(['estatus' => '10'],'operacion="REMESA"');

                        if ( $tiporemesamodel->save() ) {
                                Yii::$app->session->setFlash('success','El Registro del Tipo de Tasa '.$tiporemesamodel->codigo.' se realizó correctamente');
                                //return $this->redirect(['index']);
                            } else {

                                $Tipodecambioremesaserror = "";

                                foreach ( $tiporemesamodel->getErrors() as $key => $value ) {
                                    foreach ( $value as $row => $field ) {
                                        $Tipodecambioremesaserror .= $field . "<br>";
                                    }
                                }

                                Yii::$app->session->setFlash('error',$Tipodecambioremesaserror);
                            }

                        /*Guardo tipo de Cambio*/

                        Tipodecambio::updateAll(['estatus' => '10'],'operacion="CAMBIO"');

                        if ( $tipocambiomodel->save() ) {
                                Yii::$app->session->setFlash('success','El Registro del Tipo de Tasa '.$tipocambiomodel->codigo.' se realizó correctamente');
                                //return $this->redirect(['index']);
                            } else {

                                $Tipodecambioerror = "";

                                foreach ( $tipocambiomodel->getErrors() as $key => $value ) {
                                    foreach ( $value as $row => $field ) {
                                        $Tipodecambioerror .= $field . "<br>";
                                    }
                                }

                                Yii::$app->session->setFlash('error',$Tipodecambioerror);
                            }

                            //echo '<br>ver2'.$count_cuentabancarias;
                             for ($i = 0; $i <= $total; $i++) {

                                $modelcuentasapr= new Operacionsaldocuentasbancarias();
                                
                                 $modelcuentasapr->comprado=$_POST['Operacionsaldocuentasbancarias'][$i]['comprado'];

                                 $modelcuentasapr->saldo_cierre=0;
                                 $modelcuentasapr->saldo_anetrior=$_POST['Operacionsaldocuentasbancarias'][$i]['saldo_anetrior'];
                                 $modelcuentasapr->id_cuentabancaria_sistema=$_POST['Operacionsaldocuentasbancarias'][$i]['id_cuentabancaria_sistema'];
                                 $modelcuentasapr->saldo_apertura=$_POST['Operacionsaldocuentasbancarias'][$i]['saldo_apertura'];

                                $modelcuentasapr->id_operacion_empresa=$model->idoperacionempresa;
                                $modelcuentasapr->apertura=1;

                                $modelcuentasapr->save();
                                
                              //echo $i;

                            }
                        
 

                        Yii::$app->session->setFlash('success','La apertura se realizó correctamente');
                        return $this->redirect(['index']);
                } else {
                 

                        $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                        Yii::$app->session->setFlash('error',$errores);
                }

                
            } else {

                return $this->render('create', [
                    'model' => $model,
                    'modelcuentasbanco' => $modelcuentasbanco,
                    'tipocambiomodel' => $tipocambiomodel,
                    'tiporemesamodel' => $tiporemesamodel,
                ]);
            }
        }
    }


    /**
     * Updates an existing Operacionempresa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCierre($id)
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $model = $this->findModel($id);
            $model->fecha_cierre=date('Y-m-d H:i:s');
            $session = Yii::$app->session;
            $apertura=$session['apertura'];

             if ($model->load(Yii::$app->request->post())){

                $saldoapert=Operacionsaldocuentasbancarias::find()
                ->where(['id_operacion_empresa' => $apertura->idoperacionempresa])
                ->andFilterWhere(['>', 'saldo_apertura','0' ])
                ->orderBy('id_cuentabancaria_sistema ASC')->all();


                foreach($saldoapert as $key => $value) { 

                    $saldo_apertura=Operacionsaldocuentasbancarias::find()->where(['id_operacion_empresa' => $apertura->idoperacionempresa])->andWhere(['id_cuentabancaria_sistema'=>$value->id_cuentabancaria_sistema])->sum('saldo_apertura');

                    $acumulado=Cuentabancariasistemasaldos::find()->where(['id_cta_bancaria' => $value->id_cuentabancaria_sistema])->andFilterWhere(['=', 'id_operacion_empresa',$apertura->idoperacionempresa ])->sum('monto_operacion');
                
                    $saldo=$saldo_apertura-$acumulado;
                    $value->saldo_cierre=$saldo;

                    $value->cierre=1;

                    $value->save();

                }

                $model->estatus=0;
                $model->permite_operaciones=0;


                if ($model->save()) {
                    Yii::$app->session->setFlash('success','Cierre de Operaciones realizado con éxito.');
                    return $this->redirect(['/cambiodivisas/operacionempresa']);
                }else {

                        $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                    
                        Yii::$app->session->setFlash('error',$errores);

                        return $this->render('cierre', [
                            'model' => $model,
                        ]);
                       
                    }
                
             }

            return $this->render('cierre', [
                'model' => $model,
            ]);
        }
    }

    /**
     * If search is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDetalle($id)
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $model = $this->findModel($id);
            //$model->fecha_cierre=date('Y-m-d H:i:s');
            $session = Yii::$app->session;
            $apertura=$session['apertura'];


            return $this->render('detalle', [
                'model' => $model,
            ]);
        }
    }


    /**
     * If search is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCierreoperaciones($id)
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $model = $this->findModel($id);
            //$model->fecha_cierre=date('Y-m-d H:i:s');

            if ($model->permite_operaciones==1) {
                $model->permite_operaciones=0;
                Yii::$app->session->setFlash('warning','El cierre de operaciones se realizó correctamente');
            } else {
                $model->permite_operaciones=1;
                Yii::$app->session->setFlash('success','Activación de operaciones se realizó correctamente');
            }
            
            
            $model->save();

            
            return $this->redirect(['/']);
        }
    }


    /**
     * If search is successful, the browser will be redirected to the 'csv' file.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPrint($id)
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $model = $this->findModel($id);
            //$model->fecha_cierre=date('Y-m-d H:i:s');
            $filename=$model->fecha_apertura.'.csv';
            $session = Yii::$app->session;
            //$apertura=$session['apertura'];
             // $outputBuffer = fopen("php://detalle", 'w');

              //$this->appendToFile($outputBuffer);

             // fclose($outputBuffer);
            $this->layout = '@backend/views/layouts/xlsfile';
             return $this->render('detallexls', [
                 'model' => $model,
             ]);
            
            // return $this->render('detalle', [
            //     'model' => $model,
            // ]);
        }
    }

    /**
     * If search is successful, the browser will be redirected to the 'csv' file.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPrintreportgeneral()
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            //$model = $this->findModel($id);
            $model = new OperacionempresaSearch();
                
            //$filename=$model->fecha_apertura.'.csv';
            //$session = Yii::$app->session;
                
                return $this->render('reporteGeneral', [
                    'model' => $model,
                    
                ]);
         
        }
    }
    /** 
     * If search is successful, the browser will be redirected to the 'csv' file.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPrintreportgeneralpdf($desde, $hasta)
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            //$model = $this->findModel($id);
            //$model = new OperacionempresaSearch();
                
            //$filename=$model->fecha_apertura.'.csv'; 
            //$session = Yii::$app->session;
               
                return $this->render('generalpdf', [
                    'desde' => $desde, 'hasta' => $hasta,
                    
                ]);
        
        }
    }
    /**
     * If search is successful, the browser will be redirected to the 'csv' file.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPrintreportgeneralxls($desde, $hasta)
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            //$model = $this->findModel($id);
            //$model = new OperacionempresaSearch();
                
            //$filename=$model->fecha_apertura.'.csv';
            //$session = Yii::$app->session;
            $this->layout = '@backend/views/layouts/xlsfile';
               
                return $this->render('generalxls', [
                    'desde' => $desde, 'hasta' => $hasta,
                    
                ]);
        
        }
    }

/**
     * Updates an existing Operacionempresa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idoperacionempresa]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Operacionempresa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Operacionempresa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Operacionempresa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Operacionempresa::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

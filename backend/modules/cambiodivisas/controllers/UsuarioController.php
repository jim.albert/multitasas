<?php

namespace backend\modules\cambiodivisas\controllers;

use Yii;
use backend\modules\cambiodivisas\models\Usuario;
use backend\modules\cambiodivisas\models\UsuarioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\cambiodivisas\models\Estatus;
/**
 * UsuarioController implements the CRUD actions for Usuario model.
 */
class UsuarioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Usuario models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $searchModel = new UsuarioSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Usuario model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            return $this->render('view', [
            'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Usuario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $model = new Usuario();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Usuario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionPerfil()
    {

        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $modeluser = Usuario::findOne(Yii::$app->user->identity->getId());

            if ($modeluser!== null) {

                // if ($modeluser->load(Yii::$app->request->post()) && $modeluser->save()) {
                //     return $this->redirect(['view', 'id' => $modeluser->id]);
                // }

                //     return $this->render('update', [
                //         'model' => $modeluser,
                // ]);

                if ($modeluser->load(Yii::$app->request->post())) {
                    
                    if ( $modeluser->save() ) {
                            Yii::$app->session->setFlash('success','Actualización del Perfil '.$modeluser->nombres.' - '.$modeluser->apellidos.' se realizó correctamente');
                            return $this->redirect(['perfil']);
                        } else {

                            $errores = "";

                            foreach ( $modeluser->getErrors() as $key => $value ) {
                                foreach ( $value as $row => $field ) {
                                    $errores .= $field . "<br>";
                                }
                            }

                            Yii::$app->session->setFlash('error',$errores);
                        }

                    
                } else {
                    return $this->render('perfil', [
                        'model' => $modeluser,
                    ]);
                } 

            }else{

                $model = new Usuario();

                $model->email=Yii::$app->user->identity->email;
                $model->id=Yii::$app->user->identity->getId();
                //$modelstatus=Estatus::findOne('18');
                //$model->estatus=$modelstatus->descripcion;
                $model->estatus='18';

                if ($model->load(Yii::$app->request->post())) {
                    $model->estatus='18';

                    if ( $model->save() ) {
                            Yii::$app->session->setFlash('success','El Registro del Perfil '.$model->nombres.' - '.$model->apellidos.' se realizó correctamente');
                            return $this->redirect(['perfil']);
                        } else {

                            $errores = "";

                            foreach ( $model->getErrors() as $key => $value ) {
                                foreach ( $value as $row => $field ) {
                                    $errores .= $field . "<br>";
                                }
                            }

                            Yii::$app->session->setFlash('error',$errores);
                        }

                    
                } else {
                    return $this->render('perfil', [
                        'model' => $model,
                    ]);
                } 

                /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }

                return $this->render('perfil', [
                    'model' => $model,
                ]);*/
            }
        }
    }

    /**
     * Updates an existing Usuario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Usuario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed 
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Usuario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usuario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Usuario::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

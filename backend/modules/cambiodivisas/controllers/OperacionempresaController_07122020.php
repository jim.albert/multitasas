<?php

namespace backend\modules\cambiodivisas\controllers;

use Yii;
//use yii\base\Model;
use backend\modules\cambiodivisas\models\Model;
use backend\modules\cambiodivisas\models\Operacionempresa;
use backend\modules\cambiodivisas\models\Operacionsaldocuentasbancarias;
use backend\modules\cambiodivisas\models\OperacionempresaSearch;
use backend\modules\cambiodivisas\models\Empresa;
use backend\modules\cambiodivisas\models\Tipodecambio;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OperacionempresaController implements the CRUD actions for Operacionempresa model.
 */
class OperacionempresaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Operacionempresa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OperacionempresaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Operacionempresa model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Operacionempresa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $empresa=Empresa::find()->orderBy('id ASC')->one();

            $model = new Operacionempresa();
            $modelcuentasbanco= [new Operacionsaldocuentasbancarias()];
            $tipocambiomodel= new Tipodecambio();
            $tiporemesamodel= new Tipodecambio();

            $model->fecha_apertura=date('Y-m-d H:i:s');
            $model->saldo_apertura=0;
            $model->saldo_cierre=0;
            $model->saldo_anterior=0;
            $model->estatus=1;
            $model->porcentaje_pagador=$empresa->porcentaje_pagador;
            $model->porcentaje_pmovil=$empresa->porcentaje_pmovil;
            $model->porcentaje_interbancario=$empresa->porcentaje_interbancario;

            /*Campos para tipo de cambio Remesas*/
            $tiporemesamodel->codigo='Apr-'.date('Y-m-d H:i:s');
            $tiporemesamodel->descripcion='Apertura Diaria del sistema';
            $tiporemesamodel->operacion='REMESA';
            $tiporemesamodel->fecha = date("Y-m-d h:m:s");
            $tiporemesamodel->compra_reserva=0;
            $tiporemesamodel->venta_reserva=0;
            $tiporemesamodel->porcentaje_ganancia=0;
            $tiporemesamodel->estatus=9;
            
            
            /*Campos para tipo de cambios cambios de dinero*/
            $tipocambiomodel->codigo = "Apr-".date("Y-m-d");
            $tipocambiomodel->descripcion='Apertura Diaria del sistema';
            $tipocambiomodel->fecha = date("Y-m-d h:m:s");
            $tipocambiomodel->operacion="CAMBIO";

            $data_api = json_decode( file_get_contents('https://deperu.com/api/rest/cotizaciondolar.json'), true );
            $tipocambiomodel->compra_reserva=$data_api['Cotizacion'][0]['Compra'];
            $tipocambiomodel->venta_reserva=$data_api['Cotizacion'][0]['Venta'];
            $tipocambiomodel->estatus=9;
            

            //$modelcuentasbanco[0]->saldo_anetrior=20;
            $modelcuentasbanco[0]->saldo_cierre=0;
             $modelcuentasbanco[0]->comprado="";
             

            if ($model->load(Yii::$app->request->post()) && $tiporemesamodel->load(Yii::$app->request->post()) && $tipocambiomodel->load(Yii::$app->request->post()) && $modelcuentasbanco[0]->load(Yii::$app->request->post())) {

                    $tiporemesamodel->monto=$_POST['tiporemesamodel-monto'];
                    $tiporemesamodel->venta=$_POST['tiporemesamodel-venta'];
                    
                    $total=$_POST['Operacionempresa']['count_cuentabancarias'];

               
                if ( $model->save() ) {

                        /*Guardo tipo de Remesa*/
                        Tipodecambio::updateAll(['estatus' => '10'],'operacion="REMESA"');

                        if ( $tiporemesamodel->save() ) {
                                Yii::$app->session->setFlash('success','El Registro del Tipo de Tasa '.$tiporemesamodel->codigo.' se realizó correctamente');
                                //return $this->redirect(['index']);
                            } else {

                                $errores = "";

                                foreach ( $tiporemesamodel->getErrors() as $key => $value ) {
                                    foreach ( $value as $row => $field ) {
                                        $errores .= $field . "<br>";
                                    }
                                }

                                Yii::$app->session->setFlash('error',$errores);
                            }

                        /*Guardo tipo de Cambio*/

                        Tipodecambio::updateAll(['estatus' => '10'],'operacion="CAMBIO"');

                        if ( $tipocambiomodel->save() ) {
                                Yii::$app->session->setFlash('success','El Registro del Tipo de Tasa '.$tipocambiomodel->codigo.' se realizó correctamente');
                                //return $this->redirect(['index']);
                            } else {

                                $errores = "";

                                foreach ( $tipocambiomodel->getErrors() as $key => $value ) {
                                    foreach ( $value as $row => $field ) {
                                        $errores .= $field . "<br>";
                                    }
                                }

                                Yii::$app->session->setFlash('error',$errores);
                            }



                            //echo '<br>ver2'.$count_cuentabancarias;
                             for ($i = 0; $i <= $total; $i++) {

                                $modelcuentasapr= new Operacionsaldocuentasbancarias();
                                
                                 $modelcuentasapr->comprado=$_POST['Operacionsaldocuentasbancarias'][$i]['comprado'];

                                 $modelcuentasapr->saldo_cierre=0;
                                 $modelcuentasapr->saldo_anetrior=$_POST['Operacionsaldocuentasbancarias'][$i]['saldo_anetrior'];
                                 $modelcuentasapr->id_cuentabancaria_sistema=$_POST['Operacionsaldocuentasbancarias'][$i]['id_cuentabancaria_sistema'];
                                 $modelcuentasapr->saldo_apertura=$_POST['Operacionsaldocuentasbancarias'][$i]['saldo_apertura'];

                                $modelcuentasapr->id_operacion_empresa=$model->idoperacionempresa;

                                $modelcuentasapr->save();
                                

                              //echo $i;

                            }
                        
 

                        Yii::$app->session->setFlash('success','La apertura se realizó correctamente');
                        return $this->redirect(['index']);
                } else {

                        $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                        Yii::$app->session->setFlash('error',$errores);
                }

                
            } else {

                return $this->render('create', [
                    'model' => $model,
                    'modelcuentasbanco' => $modelcuentasbanco,
                    'tipocambiomodel' => $tipocambiomodel,
                    'tiporemesamodel' => $tiporemesamodel,
                ]);
            }
        }
    }


// public function actionCreate()
//     {
//         if (Yii::$app->user->isGuest) {            
//             return $this->redirect(['/user/security/login']);
//         }else {
//             $empresa=Empresa::find()->orderBy('id ASC')->one();

//             $model = new Operacionempresa();
//             $modelcuentasbanco[]= new Operacionsaldocuentasbancarias();
//             $tipocambiomodel= new Tipodecambio();
//             $tiporemesamodel= new Tipodecambio();

//             $model->fecha_apertura=date('Y-m-d H:i:s');
//             $model->saldo_apertura=0;
//             $model->saldo_cierre=0;
//             $model->saldo_anterior=0;
//             $model->estatus=1;
//             $model->porcentaje_pagador=$empresa->porcentaje_pagador;
//             $model->porcentaje_pmovil=$empresa->porcentaje_pmovil;
//             $model->porcentaje_interbancario=$empresa->porcentaje_interbancario;

//             /*Campos para tipo de cambio Remesas*/
//             $tiporemesamodel->codigo='Apr-'.date('Y-m-d H:i:s');
//             $tiporemesamodel->descripcion='Apertura Diaria del sistema';
//             $tiporemesamodel->operacion='REMESA';
//             $tiporemesamodel->fecha = date("Y-m-d h:m:s");
//             $tiporemesamodel->compra_reserva=0;
//             $tiporemesamodel->venta_reserva=0;
//             $tiporemesamodel->porcentaje_ganancia=0;
//             //$tiporemesamodel->monto=
//             //$tiporemesamodel->venta=
//             //$model->saldo_anterior=0;
//             //$model->fecha_cierre=date('00-00-00 00:00:00');
            
//             /*Campos para tipo de cambios cambios de dinero*/
//             $tipocambiomodel->codigo = "Apr-".date("Y-m-d");
//             $tipocambiomodel->descripcion='Apertura Diaria del sistema';
//             $tipocambiomodel->fecha = date("Y-m-d h:m:s");
//             $tipocambiomodel->operacion="CAMBIO";

//             $data_api = json_decode( file_get_contents('https://deperu.com/api/rest/cotizaciondolar.json'), true );
//             $tipocambiomodel->compra_reserva=$data_api['Cotizacion'][0]['Compra'];
//             $tipocambiomodel->venta_reserva=$data_api['Cotizacion'][0]['Venta'];
            

//             $modelcuentasbanco[0]->saldo_anetrior=20;
//             $modelcuentasbanco[0]->saldo_cierre=0;
//              $modelcuentasbanco[0]->comprado="";
//              //$modelcuentasbanco[0]->id_cuentabancaria_sistema="";
//          //   $modelcuentasbanco[0]->id_operacion_empresa=Operacionempresa::;
//             //$modelcuentasbanco[1]->id_operacion_empresa=$model->idoperacionempresa;

//                 if ($model->load(Yii::$app->request->post())) {

//  Yii::$app->session->setFlash('success','entro');
//                     $modelcuentasbanco = Model::createMultiple(Operacionsaldocuentasbancarias::classname());

//                     Model::loadMultiple($modelcuentasbanco, Yii::$app->request->post());


//                     // validate all models

//                     $valid = $model->validate();

//                     $valid = Model::validateMultiple($modelcuentasbanco) && $valid;

// Yii::$app->session->setFlash('info',$valid);
//                     if ($valid) {
//                          Yii::$app->session->setFlash('success','valid');

//                         $transaction = \Yii::$app->db->beginTransaction();


//                         try {

//                             if ($flag = $model->save(false)) {
//                                  Yii::$app->session->setFlash('success','save');

//                                 foreach ($modelcuentasbanco as $modelcuentasbanco) {

//                                     $modelcuentasbanco->id = $model->idoperacionempresa;

//                                     if (! ($flag = $modelcuentasbanco->save(false))) {

//                                         $errores = "";

//                                         foreach ( $modelcuentasbanco->getErrors() as $key => $value ) {
//                                             foreach ( $value as $row => $field ) {
//                                                 $errores .= $field . "<br>";
//                                             }
//                                         }

//                                         Yii::$app->session->setFlash('error',$errores);

//                                         $transaction->rollBack();

//                                         break;

//                                     }

//                                 }

//                             }


//                             if ($flag) {

//                                 $transaction->commit();

//                                 return $this->redirect(['view', 'id' => $model->id]);

//                             }

//                         } catch (Exception $e) {

//                             $errores = "";

//                                         foreach ( $modelcuentasbanco->getErrors() as $key => $value ) {
//                                             foreach ( $value as $row => $field ) {
//                                                 $errores .= $field . "<br>";
//                                             }
//                                         }

//                                         Yii::$app->session->setFlash('error',$errores);

//                                         $transaction->rollBack();

//                             $transaction->rollBack();

//                         }

// $errores = "";

//                                         foreach ( $modelcuentasbanco->getErrors() as $key => $value ) {
//                                             foreach ( $value as $row => $field ) {
//                                                 $errores .= $field . "<br>";
//                                             }
//                                         }

//                                         Yii::$app->session->setFlash('error',$errores);

//                                         $transaction->rollBack();
//                     }

//                 }
            

//                 return $this->render('create', [
//                     'model' => $model,
//                     'modelcuentasbanco' => $modelcuentasbanco,
//                     'tipocambiomodel' => $tipocambiomodel,
//                     'tiporemesamodel' => $tiporemesamodel,
//                 ]);
            
//         }
//     }
    /**
     * Updates an existing Operacionempresa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCierre($id)
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            $model = $this->findModel($id);

            $model->fecha_cierre=date('Y-m-d H:i:s');
                

            return $this->render('cierre', [
                'model' => $model,
            ]);
        }
    }

/**
     * Updates an existing Operacionempresa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idoperacionempresa]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Operacionempresa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Operacionempresa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Operacionempresa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Operacionempresa::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

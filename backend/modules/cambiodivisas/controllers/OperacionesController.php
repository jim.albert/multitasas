<?php

namespace backend\modules\cambiodivisas\controllers;

use Yii;
use backend\modules\cambiodivisas\models\Operaciones;
use backend\modules\cambiodivisas\models\OperacionesSearch;
use backend\modules\cambiodivisas\models\Cuentabancariasistemasaldos;
use frontend\modules\cambiosdivisas\models\Operacioncomprobantes;
use backend\modules\cambiodivisas\models\Usuarioperfil;
use backend\modules\cambiodivisas\models\Operacionesusuarios;
use backend\modules\cambiodivisas\models\OperacionesinicioSearch;
use backend\modules\cambiodivisas\models\OperacionesoperadorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper; 

//date_default_timezone_set('America/Lima');
/**
 * OperacionesController implements the CRUD actions for Operaciones model.
 */
class OperacionesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    } 

    /**
     * Lists all Operaciones models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {

            $Usuarioperfil=Usuarioperfil::find()->where(['id_usuario'=>Yii::$app->user->identity->getId()])->one();


            if ($Usuarioperfil->perfil->administrador!=0) {
            
                $searchModel = new OperacionesSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            }else{
                $searchModel = new OperacionesoperadorSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            }

            
            $dataProvider->setPagination(['pageSize' => 15]);
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'Usuarioperfil'=>$Usuarioperfil,
            ]);
        }
    }

    

    /**
     * Updates an existing Operaciones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {
          $session = Yii::$app->session;
          $model = $this->findModel($id);
          $modelsaldo= new Cuentabancariasistemasaldos();
          $Operacionesusuarios = new Operacionesusuarios();
           if ($model->estatus==23 or $model->estatus==24) {
             $modelsaldo= Cuentabancariasistemasaldos::find()->where(['id_operacion'=> $id])->one();
              $Operacionesusuarios = Operacionesusuarios::find()
                                    ->where(['id_operacion'=> $id])
                                    ->andFilterWhere(['tipo_comision'=>'Operador'])
                                    ->one();
           }
          
          
          $modelcomprobantes = new Operacioncomprobantes();
          $modelcomprobantes->scenario ='edita';

          $Usuarioperfil=Usuarioperfil::find()->where(['id_usuario'=>Yii::$app->user->identity->getId()])->one();
          $apertura=$session['apertura'];
          $model->scenario ='update';

          $modelsaldo->id_operacion=$model->idoperacion;
          $modelsaldo->id_operacion_empresa=$apertura['idoperacionempresa'];
          $modelsaldo->fecha=date('Y-m-d h:m:s');

          if ($model->load(Yii::$app->request->post()) ) {

              if ($model->save()) {
                Yii::$app->session->setFlash('success','La Operación con el Código <strong>'.$model->codigo.'</strong> se ha Actualizado correctamente ');
                        return $this->render('update', [
                            'model' => $model,
                            'modelsaldo'=>$modelsaldo, 
                            'modelcomprobantes'=> $modelcomprobantes, 
                            'apertura'=>$apertura,
                            'Operacionesusuarios'=>$Operacionesusuarios
                        ]);
              } else {
               $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                        Yii::$app->session->setFlash('error',$errores);
                        return $this->render('update', [
                            'model' => $model,
                            'modelsaldo'=>$modelsaldo, 
                            'modelcomprobantes'=> $modelcomprobantes, 
                            'apertura'=>$apertura,
                            'Operacionesusuarios'=>$Operacionesusuarios
                        ]);
              }
              
              return $this->redirect(['view', 'id' => $model->idoperacion]);
          }

          return $this->render('update', [
              'model' => $model,
              'modelsaldo'=>$modelsaldo, 
              'modelcomprobantes'=> $modelcomprobantes, 
              'apertura'=>$apertura,
              'Operacionesusuarios'=>$Operacionesusuarios
          ]);
        }
    }

    /**
     * Deletes an existing Operaciones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionRechaza($id)
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {
            //$this->layout = '@frontend/views/layouts/informativo';

            $model = $this->findModel($id);
            $model->scenario ='proceso';
            $model->fecha_proceso=date('Y-m-d h:m:s');
            $model->estatus=25;
            $model->save();

            /*Notificacion usuario de solicitud*/
            $this->mailsend($id, $model->cuentabancariabsuario->usuario->email, 'Operación Rechazada - La Solicitud de Envío de Dinero de Código: '.$model->codigo.'no cumple los requerimientos mínimos para procesada', '@common/mail/operaciondetalle' );

             
            return $this->redirect(['/']);
        }
    }


    /**
     * Displays a single Operaciones model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDetalle($id)
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {
            //$this->layout = '@frontend/views/layouts/informativo';

            $model = $this->findModel($id);

            
             $modelsaldo= Cuentabancariasistemasaldos::find()->where(['id_operacion'=>$id])->one();

               
            return $this->render('detalle', [
            'model' => $this->findModel($id), 'modelsaldo'=>$modelsaldo,
            ]);

        }


    }

    /**
     * Displays a single Operaciones model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionVerifica($id)
    {
        if (Yii::$app->user->isGuest) {
            
            return $this->redirect(['/user/security/login']);
        }else {
            //$this->layout = '@frontend/views/layouts/informativo';

            $model = $this->findModel($id);            
            $modelsaldo= Cuentabancariasistemasaldos::find()->where(['id_operacion'=>$id])->one();
            $Operacionesusuarios = new Operacionesusuarios();

             if ($Operacionesusuarios->load(Yii::$app->request->post())) {

                $Operacionesusuarios->id_operacion=$id;
                $Operacionesusuarios->tipo_comision='Operador';
                //$Operacionesusuarios->id_usuario=$useroper;
                            
                if ( $Operacionesusuarios->save() ) {

                         $model->scenario ='proceso';
                        //if ($model->estatus=22) {
                            $model->estatus=23;
                            $model->fecha_proceso=date('Y-m-d h:m:s');
                            $model->save();
                        //}

                        $Usuarionotificacion=Usuarioperfil::find()
                        ->where(['id_usuario'=>$Operacionesusuarios->id_usuario])
                        //->andFilterwhere(['email_notification'=>1])
                        ->one();

                        //foreach ($Usuarionotificaciones as $key => $Usuarionotificacion) {
                            if ($Usuarionotificacion->email_notification==1) {
                              $this->mailsend($id, $Usuarionotificacion->usuario->email, 'Asignación de Operación de Envío de Dinero - Código: '.$model->codigo, '@common/mail/operaciondetalle_operadores' );
                              
                            }
                        //}

                        Yii::$app->session->setFlash('success','La Operación con el Código <strong>'.$model->codigo.'</strong> se ha asignado correctamente ');
                        return $this->redirect(['/']);
                    } else {

                        $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                        Yii::$app->session->setFlash('error',$errores);
                        return $this->render('verifica', [
                        'model' => $this->findModel($id), 'modelsaldo'=>$modelsaldo, 'Operacionesusuarios'=>$Operacionesusuarios
                        ]);
                    }

            }
               
            return $this->render('verifica', [
            'model' => $this->findModel($id), 'modelsaldo'=>$modelsaldo, 'Operacionesusuarios'=>$Operacionesusuarios
            ]);

        }


    }

    
    

    /**
     * Displays a single Operaciones model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionProcesa($id)
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {
            //$this->layout = '@frontend/views/layouts/informativo';
            $session = Yii::$app->session;
            $model = $this->findModel($id);
            $modelsaldo= new Cuentabancariasistemasaldos();
            $modelcomprobantes = new Operacioncomprobantes();
            $Usuarioperfil=Usuarioperfil::find()->where(['id_usuario'=>Yii::$app->user->identity->getId()])->one();
            $apertura=$session['apertura'];
            $model->scenario ='paga';

             $modelsaldo->id_operacion=$model->idoperacion;
              $modelsaldo->id_operacion_empresa=$apertura['idoperacionempresa'];
             //$modelsaldo->monto_operacion=$model->monto_recibe;
             $modelsaldo->fecha=date('Y-m-d h:m:s');


             if ($modelsaldo->load(Yii::$app->request->post())) {
                

                $model->estatus=24;
                $model->fecha_pago_sistema=date('Y-m-d h:m:s');
                if ( $model->save()  ) {

                        $modelsaldo->id_operacion=$model->idoperacion;
                        $modelsaldo->monto_operacion=$model->monto_recibe;
                        $modelsaldo->fecha=date('Y-m-d h:m:s');
                        $modelsaldo->id_operacion_empresa=$apertura['idoperacionempresa'];
                        //$model->cuentabancariabsuario->id_banco
                        //$modelsaldo->Ctabancaria->id_banco

                        //$comision_agentes=($model->monto_recibe*$apertura['porcentaje_agentes']/100);
                        //$comision_agentes=0;

                        /**comision pago movil*/
                        if ($modelsaldo->pmovil==1) {
                            $comision_pmovil=($model->monto_recibe*$apertura['porcentaje_pmovil']/100);
                            $comision_interbancario=0;
                        } else {
                            $comision_pmovil=0;
                            /**comision inter bancario*/
                            if ($model->cuentabancariabsuario->id_banco!=$modelsaldo->ctabancaria->id_banco) {
                                $comision_interbancario=($model->monto_recibe*$apertura['porcentaje_interbancario']/100);
                            } else {
                                $comision_interbancario=0;
                            }
                        }
                        
                        /**comision de usuario pagador*/
                        if ($Usuarioperfil->perfil->pagador==1) {
                            $comision_pagador=($model->monto_recibe*$apertura['porcentaje_pagador']/100);
                        } else {
                            $comision_pagador=0;
                        }

                        /**comision inter bancario*/
                        // if ($model->cuentabancariabsuario->id_banco!=$modelsaldo->ctabancaria->id_banco) {
                        //     $comision_interbancario=($model->monto_recibe*$apertura['porcentaje_interbancario']/100);
                        // } else {
                        //     $comision_interbancario=0;
                        // }

                        /**comision de agentes*/
                        if ($model->com_agentes==1) {
                            $comision_agentes=($model->monto_envio*$apertura['porcentaje_agentes']/100);
                        } else {
                            $comision_agentes=0;
                        }
                        
                        //$modelsaldo->comision_pagador=$apertura['porcentaje_pagador'];
                        $modelsaldo->comision_pagador=$comision_pagador;
                        $modelsaldo->comision_interbancario=$comision_interbancario;
                        $modelsaldo->comision_pmovil=$comision_pmovil;
                        $modelsaldo->comision_agentes=$comision_agentes;

                        $modelsaldo->monto_operacion=($model->monto_recibe+$comision_pagador+$comision_interbancario+$comision_pmovil);//+$comision_agentes);
                        

                        if ( $modelsaldo->save()  ) {

                            $modelcomprobantes->id_operacion = $model->idoperacion;
                            $modelcomprobantes->orden=2;

                            $image = UploadedFile::getInstance($modelcomprobantes, 'imagen');
                
                           if (!is_null($image)) {
                             
                                    
                                 $modelcomprobantes->imagen = $image->name;
                                 $basenameAndExtension = explode('.', $image->name);
                                 $ext = end($basenameAndExtension);
                                 //$model->logo = $model->image;//$image->name;
                                  // generate a unique file name to prevent duplicate filenames
                                 // $model->documentopath = Yii::$app->security->generateRandomString().".{$ext}";
                                  // the path to save file, you can set an uploadPath
                                  // in Yii::$app->params (as used in example below)                  
                                 // Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/img/';

                                  Yii::$app->params['uploadPath'] = Yii::getAlias('@common') . '/uploads/img/';


                                  $path = Yii::$app->params['uploadPath'].'comprobantes/'.$model->idoperacion.'/';// . $estudiante->documento_identidad.'/';
                                  $path2 = $path .$modelcomprobantes->imagen;

                                  //$modelcomprobantes->imagen = Yii::getAlias('@frontend'). '/web/img/comprobantes/'.$model->idoperacion.'/'.$modelcomprobantes->imagen;
                                   
                                  if (FileHelper::createDirectory($path,$mode=0775,$recursive=true)){
                                    $image->saveAs($path2);
                                  }     
                                  $modelcomprobantes->save();
                            }else{
                                $modelcomprobantes->imagen="logo.png";
                            }

                            Yii::$app->session->setFlash('success','La Operación con el Código <strong>'.$model->codigo.'</strong> se ha procesado correctamente ');
                        }else{

                             $errores2 = "";
                        foreach ( $modelsaldo->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores2 .= $field . "<br>";
                            }
                        }

                            Yii::$app->session->setFlash('error',$errores2);

                        }

                        /*Notificacion usuario de solicitud*/

                        $this->mailsend($id, $model->cuentabancariabsuario->usuario->email, 'Su Operación de Envío de Dinero - Código: '.$model->codigo.' fué Procesada', '@common/mail/operaciondetalle' );

                        /*notificacion administracion*/
                       
                        $Usuarionotificaciones=Usuarioperfil::find()->where(['email_notification'=>1])->all();
                        foreach ($Usuarionotificaciones as $key => $Usuarionotificacion) {
                            if ($Usuarionotificacion->perfil->administrador==1) {
                              $this->mailsend($id, $Usuarionotificacion->usuario->email, 'Operación de Envío de Dinero - Código: '.$model->codigo.' fué Procesada', '@common/mail/operaciondetalle_operadores' );
                              
                            }
                        }


                        Yii::$app->session->setFlash('success','La Operación con el Código <strong>'.$model->codigo.'</strong> se ha procesado correctamente ');
                       // $this->layout = '@frontend/views/layouts/main';
                        return $this->redirect(['/']);
                    } else {

                        $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                    
                        Yii::$app->session->setFlash('error',$errores);

                        return $this->render('detalle', [
                        'model' => $this->findModel($id), 'modelsaldo'=>$modelsaldo, 'modelcomprobantes'=> $modelcomprobantes, 'apertura'=>$apertura,
                        ]);
                       
                    }

            }else{
                // $model->scenario ='proceso';
                // if ($model->estatus=22) {
                //     $model->estatus=23;
                //     $model->fecha_proceso=date('Y-m-d h:m:s');
                //     $model->save();
                // }
                
                return $this->render('proceso', [
            'model' => $this->findModel($id), 'modelsaldo'=>$modelsaldo, 'modelcomprobantes'=> $modelcomprobantes, 'apertura'=>$apertura,
            ]);

            }


            
        }
    }
 
 /**
     * Deletes an existing Operacion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {

             $model = $this->findModel($id); 

            if ($model->estatus!=22) {
                
                Yii::$app->session->setFlash('error','No se  puede eliminar la Operación de Código: '.$model->codigo.', debido a que ya ha sido procesada');

            } else {

               $this->findModel($id)->delete();

               Yii::$app->session->setFlash('warning','Se ha eliminada la Operación de Código: '.$model->codigo);
            }
            
            

            return $this->redirect(['index']);
        }
    }
    /**
     * Finds the Operaciones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Operaciones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Operaciones::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /*Send mail notification*/

    public function Mailsend($id, $to, $subject, $templateemail){
          

           $model=$this->findModel($id);
           Yii::$app->mailer->compose(['html' => $templateemail, 'text' => $templateemail], [ 'model' => $model,])
             ->setFrom(['info@app.multitasas.com'=>'Equipo Multitasas'])
             ->setTo($to)
             ->setSubject($subject)
             ->send();
        }
}

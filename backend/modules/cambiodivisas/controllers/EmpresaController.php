<?php

namespace backend\modules\cambiodivisas\controllers;

use Yii;
use backend\modules\cambiodivisas\models\Empresa;
use backend\modules\cambiodivisas\models\EmpresaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * EmpresaController implements the CRUD actions for Empresa model.
 */
class EmpresaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Empresa models.
     * @return mixed
     */
    /*public function actionIndex()
    {
        $searchModel = new EmpresaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }*/

    /**
     * Displays a single Empresa model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    // public function actionView($id)
    // {
    //     return $this->render('view', [
    //         'model' => $this->findModel($id),
    //     ]);
    // }

    /**
     * Creates a new Empresa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {

            $idempresa=Empresa::find()->orderBy('id ASC')->one();

            if ($idempresa=="") {
               $model = new Empresa();
            } else {

                $model = $this->findModel($idempresa);
            }

            if ($model->load(Yii::$app->request->post())) {

                $image = UploadedFile::getInstance($model, 'image');
                
               if (!is_null($image)) {
                 

                     $model->image = $image->name;
                     $basenameAndExtension = explode('.', $image->name);
                     $ext = end($basenameAndExtension);
                     $model->logo = $model->image;//$image->name;
                      // generate a unique file name to prevent duplicate filenames
                     // $model->documentopath = Yii::$app->security->generateRandomString().".{$ext}";
                      // the path to save file, you can set an uploadPath
                      // in Yii::$app->params (as used in example below)                  
                      Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/img/';


                      $path = Yii::$app->params['uploadPath'];// . $estudiante->documento_identidad.'/';
                      $path2 = $path .$model->logo;
                       
                      if (FileHelper::createDirectory($path,$mode=0775,$recursive=true)){
                        $image->saveAs($path2);
                      }     
                }else{
                    $model->logo="logo.png";
                }

               
    //echo $model->logo;
                if ( $model->save() ) {

                        Yii::$app->session->setFlash('success','El Registro de la empresa '.$model->nombre.' se realizó correctamente');
                        return $this->redirect(['create']);
                    } else {

                        $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                        Yii::$app->session->setFlash('error',$errores);
                    }


             } else {
                 return $this->render('create', [
                     'model' => $model,
                 ]);
             }
        }


    }

    /**
     * Updates an existing Empresa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
   /* public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    *
     * Deletes an existing Empresa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/

    /**
     * Finds the Empresa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Empresa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Empresa::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

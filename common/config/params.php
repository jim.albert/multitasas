<?php
return [
    'adminEmail' => 'info@multitasas.com', 
    'supportEmail' => 'info@multitasas.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    'user.passwordMinLength' => 8,
    'imgUrl'=>'http://localhost//multitasas/common/uploads/img/',
];

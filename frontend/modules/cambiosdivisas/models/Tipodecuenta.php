<?php

namespace frontend\modules\cambiosdivisas\models;

use Yii;

/**
 * This is the model class for table "tipo_cuenta".
 *
 * @property int $idtipocuenta
 * @property string $codigo
 * @property string $descripcion
 * @property int $estatus
 *
 * @property CuentaBancariaSistema[] $cuentaBancariaSistemas
 * @property CuentaBancariaUsuarios[] $cuentaBancariaUsuarios
 * @property Estatus $estatus0
 */
class Tipodecuenta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo_cuenta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'descripcion', 'estatus'], 'required'],
            [['estatus'], 'integer'],
            [['codigo', 'descripcion'], 'string', 'max' => 45],
            [['estatus'], 'exist', 'skipOnError' => true, 'targetClass' => Estatus::className(), 'targetAttribute' => ['estatus' => 'idstatus']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idtipocuenta' => Yii::t('app', 'Id Tipo de Cuenta'),
            'codigo' => Yii::t('app', 'Código'),
            'descripcion' => Yii::t('app', 'Descripción'),
            'estatus' => Yii::t('app', 'Estatus'),
        ];
    }

    /**
     * Gets query for [[CuentaBancariaSistemas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuentaBancariaSistemas()
    {
        return $this->hasMany(CuentaBancariaSistema::className(), ['id_tipo_cuenta' => 'idtipocuenta']);
    }

    /**
     * Gets query for [[CuentaBancariaUsuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuentaBancariaUsuarios()
    {
        return $this->hasMany(CuentaBancariaUsuarios::className(), ['id_tipo_cuenta' => 'idtipocuenta']);
    }

    /**
     * Gets query for [[Estatus0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstatus0()
    {
        return $this->hasOne(Estatus::className(), ['idstatus' => 'estatus']);
    }
}

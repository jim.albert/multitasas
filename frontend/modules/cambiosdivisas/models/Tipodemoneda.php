<?php

namespace frontend\modules\cambiosdivisas\models;

use Yii;

/**
 * This is the model class for table "tipo_moneda".
 *
 * @property int $idbtipomoneda
 * @property string $codigo
 * @property string $descripcion
 * @property int $estatus
 *
 * @property CuentaBancariaSistema[] $cuentaBancariaSistemas
 * @property CuentaBancariaUsuarios[] $cuentaBancariaUsuarios
 * @property Operaciones[] $operaciones
 * @property Operaciones[] $operaciones0
 * @property Estatus $estatus0
 * @property Validaciones[] $validaciones 
 */
class Tipodemoneda extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo_moneda';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'descripcion', 'estatus'], 'required'],
            [['estatus'], 'integer'],
            [['codigo', 'descripcion'], 'string', 'max' => 45],
            [['estatus'], 'exist', 'skipOnError' => true, 'targetClass' => Estatus::className(), 'targetAttribute' => ['estatus' => 'idstatus']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idbtipomoneda' => Yii::t('app', 'Id Tipo de Moneda'),
            'codigo' => Yii::t('app', 'Código'),
            'descripcion' => Yii::t('app', 'Descripción'),
            'estatus' => Yii::t('app', 'Estatus'),
        ];
    }

    /**
     * Gets query for [[CuentaBancariaSistemas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuentaBancariaSistemas()
    {
        return $this->hasMany(CuentaBancariaSistema::className(), ['id_moneda' => 'idbtipomoneda']);
    }

    /**
     * Gets query for [[CuentaBancariaUsuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuentaBancariaUsuarios()
    {
        return $this->hasMany(CuentaBancariaUsuarios::className(), ['id_moneda' => 'idbtipomoneda']);
    }

    /**
     * Gets query for [[Operaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOperaciones()
    {
        return $this->hasMany(Operaciones::className(), ['id_tipo_moneda_envia' => 'idbtipomoneda']);
    }

    /**
     * Gets query for [[Operaciones0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOperaciones0()
    {
        return $this->hasMany(Operaciones::className(), ['id_tipo_moneda_recibe' => 'idbtipomoneda']);
    }

    /**
     * Gets query for [[Estatus0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstatus0()
    {
        return $this->hasOne(Estatus::className(), ['idstatus' => 'estatus']);
    }

    /**
     * Gets query for [[Validaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getValidaciones()
    {
        return $this->hasMany(Validaciones::className(), ['tipo_moneda' => 'idbtipomoneda']);
    }

    public function getMonedas()
    {
        return $this->codigo.' - '.$this->descripcion;
    }
}

<?php

namespace frontend\modules\cambiosdivisas\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\cambiosdivisas\models\Operaciones;
use frontend\modules\cambiosdivisas\models\Cuentabancariadeusuarios;
use Yii;

/**
 * OperacionesSearch represents the model behind the search form of `frontend\modules\cambiosdivisas\models\Operaciones`.
 */
class OperacionesSearch extends Operaciones
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idoperacion', 'id_banco_envia', 'id_cuenta_bancaria_usuario', 'id_tipo_moneda_envia', 'id_tipo_cambio', 'id_tipo_moneda_recibe', 'id_cuenta_bancaria_sistema', 'estatus'], 'integer'],
            [['codigo', 'cod_referencia', 'fecha_registro', 'fecha_pago_sistema', 'fecha_proceso','tipo'], 'safe'],
            [['monto_envio', 'monto_recibe', 'monto_cambio_usado'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Operaciones::find()->joinWith('cuentabancariabsuario')->where(['id_usuario' => Yii::$app->user->identity->getId()]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->defaultOrder = ['idoperacion' => SORT_DESC];
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'idoperacion' => $this->idoperacion,
            'id_banco_envia' => $this->id_banco_envia,

            'id_cuenta_bancaria_usuario' => $this->id_cuenta_bancaria_usuario,
            'id_tipo_moneda_envia' => $this->id_tipo_moneda_envia,
            'monto_envio' => $this->monto_envio,
            'id_tipo_cambio' => $this->id_tipo_cambio,
            'id_tipo_moneda_recibe' => $this->id_tipo_moneda_recibe,
            'monto_recibe' => $this->monto_recibe,
            'monto_cambio_usado' => $this->monto_cambio_usado,
            'id_cuenta_bancaria_sistema' => $this->id_cuenta_bancaria_sistema,
            'fecha_registro' => $this->fecha_registro,
            'fecha_pago_sistema' => $this->fecha_pago_sistema,
            'fecha_proceso' => $this->fecha_proceso,
            'tipo' => $this->tipo,
            'operaciones.estatus' => $this->estatus,//[22,23],
            //'cuentabancariabsuario.id_usuario'=> Yii::$app->user->identity->getId(),
        ]);

        $query->andFilterWhere(['like', 'codigo', $this->codigo])
            ->andFilterWhere(['like', 'cod_referencia', $this->cod_referencia]);
            //->andFilterWhere(['in', 'operaciones.estatus', $this->estatus]);

        return $dataProvider;
    }
}

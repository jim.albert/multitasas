<?php

namespace frontend\modules\cambiosdivisas\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\cambiodivisas\models\Paises;

/**
 * PaisesSearch represents the model behind the search form of `backend\modules\cambiodivisas\models\Paises`.
 */
class PaisesSearch extends Paises
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idpaises', 'estatus', 'remesas', 'cambios', 'criptomonedas'], 'integer'],
            [['codigo', 'descripcion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Paises::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idpaises' => $this->idpaises,
            'estatus' => $this->estatus,
            'remesas' => $this->remesas,
            'cambios' => $this->cambios,
            'criptomonedas' => $this->criptomonedas,
        ]);

        $query->andFilterWhere(['like', 'codigo', $this->codigo])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}

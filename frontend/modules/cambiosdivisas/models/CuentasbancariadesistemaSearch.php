<?php

namespace frontend\modules\cambiosdivisas\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\cambiosdivisas\models\Cuentasbancariadesistema;

/**
 * CuentasbancariadesistemaSearch represents the model behind the search form of `frontend\modules\cambiosdivisas\models\Cuentasbancariadesistema`.
 */
class CuentasbancariadesistemaSearch extends Cuentasbancariadesistema
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idcuenta_bancaria', 'id_tipo_cuenta', 'id_banco', 'id_moneda', 'estatus'], 'integer'],
            [['nro_cuenta', 'nro_interbancario', 'alias', 'fecha_registro'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cuentasbancariadesistema::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idcuenta_bancaria' => $this->idcuenta_bancaria,
            'id_tipo_cuenta' => $this->id_tipo_cuenta,
            'id_banco' => $this->id_banco,
            'id_moneda' => $this->id_moneda,
            'estatus' => $this->estatus,
            'fecha_registro' => $this->fecha_registro,
        ]);

        $query->andFilterWhere(['like', 'nro_cuenta', $this->nro_cuenta])
            ->andFilterWhere(['like', 'nro_interbancario', $this->nro_interbancario])
            ->andFilterWhere(['like', 'alias', $this->alias]);

        return $dataProvider;
    }
}

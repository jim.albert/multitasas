<?php

namespace frontend\modules\cambiosdivisas\models;

use Yii;

/**
 * This is the model class for table "tipo_cambio".
 *
 * @property int $idcambio
 * @property string $codigo
 * @property string $descripcion
 * @property float $monto
* @property float $venta
 * @property string $fecha
 * @property int $estatus
 * @property int $id_moneda_compra 
 * @property int $id_moneda_vende 
 *
 * @property Operaciones[] $operaciones
 * @property Estatus $estatus0
 * @property TipoMoneda $monedacompra 
* @property TipoMoneda $monedaVende
 */
class Tipodecambio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo_cambio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'descripcion', 'operacion', 'monto', 'fecha', 'estatus', 'venta', 'id_moneda_compra', 'id_moneda_vende'], 'required'],
            [['monto','venta'], 'number'],
            [['fecha'], 'safe'],
            [['estatus', 'id_moneda_compra', 'id_moneda_vende'], 'integer'],
            [['codigo', 'descripcion'], 'string', 'max' => 45],
            [['estatus'], 'exist', 'skipOnError' => true, 'targetClass' => Estatus::className(), 'targetAttribute' => ['estatus' => 'idstatus']],
            [['id_moneda_compra'], 'exist', 'skipOnError' => true, 'targetClass' => Tipodemoneda::className(), 'targetAttribute' => ['id_moneda_compra' => 'idbtipomoneda']], 
           [['id_moneda_vende'], 'exist', 'skipOnError' => true, 'targetClass' => Tipodemoneda::className(), 'targetAttribute' => ['id_moneda_vende' => 'idbtipomoneda']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcambio' => Yii::t('app', 'Id Tipo de Cambio'),
            'codigo' => Yii::t('app', 'Código'),
            'descripcion' => Yii::t('app', 'Descripción'),
            'monto' => Yii::t('app', 'Compra'),
            'venta' => Yii::t('app', 'Venta'),
            'fecha' => Yii::t('app', 'Fecha'),
            'estatus' => Yii::t('app', 'Estatus'),
             'id_moneda_compra' => 'Moneda Compra',
           'id_moneda_vende' => 'Moneda Vende',
        ];
    }

    /**
     * Gets query for [[Operaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOperaciones()
    {
        return $this->hasMany(Operaciones::className(), ['id_tipo_cambio' => 'idcambio']);
    }

    /**
     * Gets query for [[Estatus0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstatus0()
    {
        return $this->hasOne(Estatus::className(), ['idstatus' => 'estatus']);
    }

    /** 
    * Gets query for [[Monedacompra]]. 
    * 
    * @return \yii\db\ActiveQuery 
    */ 
   public function getMonedacompra() 
   { 
       return $this->hasOne(Tipodemoneda::className(), ['idbtipomoneda' => 'id_moneda_compra']); 
   } 
 
   /** 
    * Gets query for [[MonedaVende]]. 
    * 
    * @return \yii\db\ActiveQuery 
    */ 
   public function getMonedavende() 
   { 
       return $this->hasOne(Tipodemoneda::className(), ['idbtipomoneda' => 'id_moneda_vende']); 
   } 
}

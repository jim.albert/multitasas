<?php

namespace frontend\modules\cambiosdivisas\models;

use Yii;

/**
 * This is the model class for table "bancos".
 *
 * @property int $idbancos
 * @property string $codigo
 * @property string $descripcion
 * @property int $estatus
 * @property int $id_pais 
 *
 * @property Paises $pais 
 * @property Estatus $estatus0
 * @property CuentaBancariaSistema[] $cuentaBancariaSistemas
 * @property CuentaBancariaUsuarios[] $cuentaBancariaUsuarios
 * @property Operaciones[] $operaciones
 */
class Bancos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bancos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'descripcion', 'estatus', 'id_pais'], 'required'], 
            [['estatus', 'id_pais'], 'integer'],
            [['codigo', 'descripcion'], 'string', 'max' => 45],
            [['id_pais'], 'exist', 'skipOnError' => true, 'targetClass' => Paises::className(), 'targetAttribute' => ['id_pais' => 'idpaises']],
            [['estatus'], 'exist', 'skipOnError' => true, 'targetClass' => Estatus::className(), 'targetAttribute' => ['estatus' => 'idstatus']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idbancos' => Yii::t('app', 'Id Bancos'),
            'codigo' => Yii::t('app', 'Código'),
            'descripcion' => Yii::t('app', 'Descripción'),
            'estatus' => Yii::t('app', 'Estatus'),
            'id_pais' => Yii::t('app', 'Pais'), 
        ];
    }

    /**
     * Gets query for [[Estatus0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstatus0()
    {
        return $this->hasOne(Estatus::className(), ['idstatus' => 'estatus']);
    }

    /**
     * Gets query for [[CuentaBancariaSistemas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuentaBancariaSistemas()
    {
        return $this->hasMany(CuentaBancariaSistema::className(), ['id_banco' => 'idbancos']);
    }

    /**
     * Gets query for [[CuentaBancariaUsuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuentaBancariaUsuarios()
    {
        return $this->hasMany(CuentaBancariaUsuarios::className(), ['id_banco' => 'idbancos']);
    }

    /**
     * Gets query for [[Operaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOperaciones()
    {
        return $this->hasMany(Operaciones::className(), ['id_banco_envia' => 'idbancos']);
    }
    /** 
    * Gets query for [[Pais]]. 
    * 
    * @return \yii\db\ActiveQuery 
    */ 
   public function getPais() 
   { 
       return $this->hasOne(Paises::className(), ['idpaises' => 'id_pais']); 
   }

   public function getCodigopais() 
   { 
       return $this->pais->descripcion; 
   }
}

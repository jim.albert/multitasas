<?php

namespace frontend\modules\cambiosdivisas\models;

use Yii;

/**
 * This is the model class for table "operaciones".
 *
 * @property int $idoperacion
 * @property string|null $codigo
 * @property int|null $id_banco_envia
 * @property int|null $id_cuenta_bancaria_usuario
 * @property int|null $id_tipo_moneda_envia
 * @property float|null $monto_envio
 * @property int|null $id_tipo_cambio
 * @property int|null $id_tipo_moneda_recibe
 * @property float|null $monto_recibe
 * @property float|null $monto_cambio_usado
 * @property int|null $id_cuenta_bancaria_sistema
 * @property string|null $cod_referencia
 * @property string|null $fecha_registro
 * @property string|null $fecha_pago_sistema
 * @property string|null $fecha_proceso
 * @property int|null $estatus
 * @property string|null $tipo
 * @property int $com_agentes
 * 
 * @property Bancos $bancoEnvia
 * @property CuentaBancariaSistema $cuentaBancariaSistema
 * @property CuentaBancariaUsuarios $cuentaBancariaUsuario
 * @property Estatus $estatus0
 * @property TipoCambio $tipoCambio
 * @property TipoMoneda $tipoMonedaEnvia
 * @property TipoMoneda $tipoMonedaRecibe
 */
class Operaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_banco_envia', 'id_cuenta_bancaria_usuario', 'id_tipo_moneda_envia', 'monto_envio',
              'id_tipo_cambio', 'id_tipo_moneda_recibe', 'monto_recibe', 'monto_cambio_usado', 'fecha_registro', 'estatus','id_cuenta_bancaria_sistema','cod_referencia','tipo'], 'required'],
            
            // [['id_cuenta_bancaria_sistema','cod_referencia'] , 'required' , 'when' => function($model) {
            //         return ($model->cod_referencia!==null);
            //     } , 'whenClient' => 'function(attribute,value){return ($("#Cod_refencia").bootstrapSwitch("state")==true)}' ] ,


            [['id_banco_envia', 'id_cuenta_bancaria_usuario', 'id_tipo_moneda_envia', 'id_tipo_cambio', 'id_cuenta_bancaria_sistema', 'estatus', 'com_agentes'], 'integer'],
            [['monto_envio', 'monto_cambio_usado'], 'number',],
            [['fecha_registro', 'fecha_pago_sistema', 'fecha_proceso', 'id_tipo_moneda_recibe'], 'safe'],
            [['codigo', 'cod_referencia'], 'string', 'max' => 45],
            [['codigo'], 'unique'],
            [['id_banco_envia'], 'exist', 'skipOnError' => true, 'targetClass' => Bancos::className(), 'targetAttribute' => ['id_banco_envia' => 'idbancos']],
            [['id_cuenta_bancaria_sistema'], 'exist', 'skipOnError' => true, 'targetClass' => Cuentasbancariadesistema::className(), 'targetAttribute' => ['id_cuenta_bancaria_sistema' => 'idcuenta_bancaria']],
            [['id_cuenta_bancaria_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Cuentabancariadeusuarios::className(), 'targetAttribute' => ['id_cuenta_bancaria_usuario' => 'idcuenta_bancaria']],
            [['estatus'], 'exist', 'skipOnError' => true, 'targetClass' => Estatus::className(), 'targetAttribute' => ['estatus' => 'idstatus']],
            [['id_tipo_cambio'], 'exist', 'skipOnError' => true, 'targetClass' => Tipodecambio::className(), 'targetAttribute' => ['id_tipo_cambio' => 'idcambio']],
            [['id_tipo_moneda_envia'], 'exist', 'skipOnError' => true, 'targetClass' => Tipodemoneda::className(), 'targetAttribute' => ['id_tipo_moneda_envia' => 'idbtipomoneda']],
            [['id_tipo_moneda_recibe'], 'exist', 'skipOnError' => true, 'targetClass' => Tipodemoneda::className(), 'targetAttribute' => ['id_tipo_moneda_recibe' => 'idbtipomoneda']], 
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idoperacion' => Yii::t('app', 'Idoperacion'),
            'codigo' => Yii::t('app', 'Código'),
            'id_banco_envia' => Yii::t('app', 'Entidad Bancaria desde la que nos enviarás el dinero.'),
            'id_cuenta_bancaria_usuario' => Yii::t('app', '¿A qué cuenta deseas que depositemos el dinero?'),
            'id_tipo_moneda_envia' => Yii::t('app', 'Moneda de Envío'),
            'monto_envio' => Yii::t('app', 'Quiero comprar:'),
            'id_tipo_cambio' => Yii::t('app', 'Tipo de Cambio'),
            'id_tipo_moneda_recibe' => Yii::t('app', 'Moneda que Recibe'),
            'monto_recibe' => Yii::t('app', 'Monto a Trasferir'),
            'monto_cambio_usado' => Yii::t('app', 'Tipo de Cambio'),
            'id_cuenta_bancaria_sistema' => Yii::t('app', 'Cuenta Bancaria En la que nos Transferiste/Depositaste el Dinero'),
            'cod_referencia' => Yii::t('app', 'Código de Referencia / Número de Operación'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'fecha_pago_sistema' => Yii::t('app', 'Fecha Pago Sistema'),
            'fecha_proceso' => Yii::t('app', 'Fecha Proceso'),
            'estatus' => Yii::t('app', 'Estatus'),
            'tipo' => Yii::t('app', 'Operación'),
            'com_agentes'=> Yii::t('app', 'Com. Agente'),
        ];
    }

    /**
     * Gets query for [[BancoEnvia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBancoEnvia()
    {
        return $this->hasOne(Bancos::className(), ['idbancos' => 'id_banco_envia']);
    }

    /**
     * Gets query for [[CuentaBancariaSistema]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuentaBancariaSistema()
    {
        return $this->hasOne(Cuentasbancariadesistema::className(), ['idcuenta_bancaria' => 'id_cuenta_bancaria_sistema']);
    }

    /**
     * Gets query for [[CuentaBancariaUsuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuentabancariabsuario()
    {
        return $this->hasOne(Cuentabancariadeusuarios::className(), ['idcuenta_bancaria' => 'id_cuenta_bancaria_usuario']);
    }

    /**
     * Gets query for [[Estatus0]]. 
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstatus0()
    {
        return $this->hasOne(Estatus::className(), ['idstatus' => 'estatus']);
    }

    /**
     * Gets query for [[TipoCambio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoCambio()
    {
        return $this->hasOne(Tipodecambio::className(), ['idcambio' => 'id_tipo_cambio']);
    }

    /**
     * Gets query for [[TipoMonedaEnvia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoMonedaEnvia()
    {
        return $this->hasOne(Tipodemoneda::className(), ['idbtipomoneda' => 'id_tipo_moneda_envia']);
    }

    /**
     * Gets query for [[TipoMonedaRecibe]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoMonedaRecibe()
    {
        return $this->hasOne(Tipodemoneda::className(), ['idbtipomoneda' => 'id_tipo_moneda_recibe']);
    }

    /**
     * Gets query for [[TipoMonedaRecibe]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSoporteoperacion()
    {
        return 'Código: '.$this->codigo.' - Cuenta Bancaria:'.$this->cuentabancariabsuario->alias;
        /*tipo de oiperacion */
    }
}

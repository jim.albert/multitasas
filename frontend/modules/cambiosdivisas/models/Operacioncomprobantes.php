<?php

namespace frontend\modules\cambiosdivisas\models;

use Yii;

/**
 * This is the model class for table "operacion_comprobantes".
 *
 * @property int $idcomprobante
 * @property string $imagen
 * @property int $id_operacion
 *
 * @property Operaciones $operacion
 */
class Operacioncomprobantes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operacion_comprobantes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['imagen', 'id_operacion'], 'required'],
            [['imagen'], 'string'],
            [['id_operacion'], 'integer'],
            [['id_operacion'], 'exist', 'skipOnError' => true, 'targetClass' => Operaciones::className(), 'targetAttribute' => ['id_operacion' => 'idoperacion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcomprobante' => Yii::t('app', 'Idcomprobante'),
            'imagen' => Yii::t('app', 'Comprobante Bancario'),
            'id_operacion' => Yii::t('app', 'Id Operacion'),
        ];
    }

    /**
     * Gets query for [[Operacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOperacion()
    {
        return $this->hasOne(Operaciones::className(), ['idoperacion' => 'id_operacion']);
    }
}

<?php

namespace frontend\modules\cambiosdivisas\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\cambiosdivisas\models\Tipodecambio;

/**
 * TipodecambioSearch represents the model behind the search form of `frontend\modules\cambiosdivisas\models\Tipodecambio`.
 */
class TipodecambioSearch extends Tipodecambio
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idcambio', 'estatus'], 'integer'],
            [['codigo', 'descripcion', 'fecha'], 'safe'],
            [['monto'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tipodecambio::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idcambio' => $this->idcambio,
            'monto' => $this->monto,
            'fecha' => $this->fecha,
            'estatus' => $this->estatus,
        ]);

        $query->andFilterWhere(['like', 'codigo', $this->codigo])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}

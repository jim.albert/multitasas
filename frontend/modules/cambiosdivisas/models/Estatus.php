<?php

namespace frontend\modules\cambiosdivisas\models;

use Yii;

/**
 * This is the model class for table "estatus".
 *
 * @property int $idstatus
 * @property string|null $codigo
 * @property string|null $descripcion
 * @property string|null $tabla
 * @property string|null $color
 * @property string|null $fecha
 * @property int $estatus
 *
 * @property Bancos[] $bancos
 * @property CuentaBancariaSistema[] $cuentaBancariaSistemas
 * @property CuentaBancariaUsuarios[] $cuentaBancariaUsuarios
 * @property Operaciones[] $operaciones
 * @property TipoCambio[] $tipoCambios
 * @property TipoCuenta[] $tipoCuentas
 * @property TipoDocumento[] $tipoDocumentos
 * @property TipoMoneda[] $tipoMonedas
 * @property Usuario[] $usuarios
 * @property Validaciones[] $validaciones
 */
class Estatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estatus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'descripcion', 'tabla', 'color', 'estatus'], 'required'],
            [['idstatus'], 'integer'],
            [['estatus'], 'integer'], 
            [['fecha'], 'safe'],
            [['codigo', 'descripcion'], 'string', 'max' => 45],
            [['tabla','color'], 'string', 'max' => 30],
            [['idstatus'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idstatus' => Yii::t('app', 'Id Estatus'),
            'codigo' => Yii::t('app', 'Código'),
            'descripcion' => Yii::t('app', 'Descripción'),
            'tabla' => Yii::t('app', 'Opción'),
            'color' => Yii::t('app', 'Color'),
            'fecha' => Yii::t('app', 'Fecha Registro'),
            'estatus' => Yii::t('app', 'Estatus'),
        ];
    }

    /**
     * Gets query for [[Bancos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBancos()
    {
        return $this->hasMany(Bancos::className(), ['estatus' => 'idstatus']);
    }

    /**
     * Gets query for [[CuentaBancariaSistemas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuentaBancariaSistemas()
    {
        return $this->hasMany(CuentaBancariaSistema::className(), ['estatus' => 'idstatus']);
    }

    /**
     * Gets query for [[CuentaBancariaUsuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuentaBancariaUsuarios()
    {
        return $this->hasMany(CuentaBancariaUsuarios::className(), ['estatus' => 'idstatus']);
    }

    /**
     * Gets query for [[Operaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOperaciones()
    {
        return $this->hasMany(Operaciones::className(), ['estatus' => 'idstatus']);
    }

    /**
     * Gets query for [[TipoCambios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoCambios()
    {
        return $this->hasMany(TipoCambio::className(), ['estatus' => 'idstatus']);
    }

    /**
     * Gets query for [[TipoCuentas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoCuentas()
    {
        return $this->hasMany(TipoCuenta::className(), ['estatus' => 'idstatus']);
    }

    /**
     * Gets query for [[TipoDocumentos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoDocumentos()
    {
        return $this->hasMany(TipoDocumento::className(), ['estatus' => 'idstatus']);
    }

    /**
     * Gets query for [[TipoMonedas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoMonedas()
    {
        return $this->hasMany(TipoMoneda::className(), ['estatus' => 'idstatus']);
    }

    /**
     * Gets query for [[Usuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['estatus' => 'idstatus']);
    }

    /**
     * Gets query for [[Validaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getValidaciones()
    {
        return $this->hasMany(Validaciones::className(), ['estatus' => 'idstatus']);
    }
}

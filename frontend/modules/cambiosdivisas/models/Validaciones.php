<?php

namespace frontend\modules\cambiosdivisas\models;

use Yii;

/**
 * This is the model class for table "validaciones".
 *
 * @property int $idvalidacion
 * @property string $codigo
 * @property string $descripcion
 * @property float $desde
 * @property float $hasta
 * @property string $fecha_registro
 * @property string $tipo_transferencia
 * @property int $estatus
 *
 * @property Estatus $estatus0
 */
class Validaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'validaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            

            [['codigo', 'descripcion', 'desde', 'hasta', 'fecha_registro', 'tipo_transferencia', 'tipo_moneda', 'estatus'], 'required'],
            [['desde', 'hasta'], 'number','numberPattern' => '/^\s*[+-]?\d+[.,]\d{2}\s*$/'],
            [['fecha_registro'], 'safe'],
            [['tipo_moneda', 'estatus'], 'integer'],
            [['codigo', 'descripcion', 'tipo_transferencia'], 'string', 'max' => 45],
            [['estatus'], 'exist', 'skipOnError' => true, 'targetClass' => Estatus::className(), 'targetAttribute' => ['estatus' => 'idstatus']],
            [['tipo_moneda'], 'exist', 'skipOnError' => true, 'targetClass' => Tipodemoneda::className(), 'targetAttribute' => ['tipo_moneda' => 'idbtipomoneda']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idvalidacion' => Yii::t('app', 'Id Validación'),
            'codigo' => Yii::t('app', 'Código'),
            'descripcion' => Yii::t('app', 'Descripción'),
            'desde' => Yii::t('app', 'Desde'),
            'hasta' => Yii::t('app', 'Hasta'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'tipo_transferencia' => Yii::t('app', 'Tipo de Transferencia'),
            'tipo_moneda' => Yii::t('app', 'Tipo Moneda'),
            'estatus' => Yii::t('app', 'Estatus'),
        ];
    }

    /**
     * Gets query for [[Estatus0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstatus0()
    {
        return $this->hasOne(Estatus::className(), ['idstatus' => 'estatus']);
    }

    /**
     * Gets query for [[TipoMoneda]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoMoneda()
    {
        return $this->hasOne(Tipodemoneda::className(), ['idbtipomoneda' => 'tipo_moneda']);
    }

    

}

<?php

namespace frontend\modules\cambiosdivisas\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\cambiosdivisas\models\Validaciones;

/**
 * ValidacionesSearch represents the model behind the search form of `frontend\modules\cambiosdivisas\models\Validaciones`.
 */
class ValidacionesSearch extends Validaciones
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idvalidacion', 'estatus', 'tipo_moneda'], 'integer'],
            [['codigo', 'descripcion', 'fecha_registro', 'tipo_transferencia'], 'safe'],
            [['desde', 'hasta'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Validaciones::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idvalidacion' => $this->idvalidacion,
            'desde' => $this->desde,
            'hasta' => $this->hasta,
            'fecha_registro' => $this->fecha_registro,
            'tipo_moneda' => $this->tipo_moneda,
            'estatus' => $this->estatus,
        ]);

        $query->andFilterWhere(['like', 'codigo', $this->codigo])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'tipo_transferencia', $this->tipo_transferencia]);

        return $dataProvider;
    }
    
}

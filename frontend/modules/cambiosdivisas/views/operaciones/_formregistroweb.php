<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

use kartik\widgets\SwitchInput;

use frontend\modules\cambiosdivisas\models\Tipodemoneda;
use frontend\modules\cambiosdivisas\models\Tipodecambio;
use frontend\modules\cambiosdivisas\models\Bancos;
use frontend\modules\cambiosdivisas\models\Cuentabancariadeusuarios;
use frontend\modules\cambiosdivisas\models\Cuentasbancariadesistema;

use frontend\modules\cambiosdivisas\models\Usuario;
//$paisuser=Usuario::findOne(Yii::$app->user->identity->getId());

/* @var $this yii\web\View */
/* @var $model frontend\modules\cambiosdivisas\models\Operaciones */
/* @var $form yii\widgets\ActiveForm */


$jsc = <<< JS


        

        function montorecibe(){

            

            var valor=$('#id_tipo_moneda_envia').bootstrapSwitch('state');

            if (valor==true){

               var item=1;
                
            }else{
                
                
                var item=2;
            }

            var monto_compra =$('#operaciones-monto_compra').val();
            var monto_venta =$('#operaciones-monto_venta').val();
            var monto_envio=$('#operaciones-monto_envio').val();
            
            if (item==1){
                
                var monto_cambio_usado =monto_compra;
                var monto_operacion=parseFloat(monto_envio)*parseFloat(monto_cambio_usado);
            }else{
                
                var monto_cambio_usado =monto_venta;
                var monto_operacion=parseFloat(monto_envio)/parseFloat(monto_cambio_usado);
            }

            $('#operaciones-monto_recibe').val(monto_operacion.toLocaleString("es-PE"));

            $('#operaciones-monto_cambio_usado').val(monto_cambio_usado);

            $.post("index.php?r=/cambiosdivisas/operaciones/listmonedascontra&id=" + item, 

            function(data){
                //console.log(data);
                $("#operaciones-id_tipo_moneda_recibe").val(data);

                $('#mensaje_moneda_recibe').html(data);

                
                
            });

            $.post("index.php?r=/cambiosdivisas/operaciones/listmonedasdescription&id=" + item, 

            function(data){
                
                //var operaciondt='Estas Comprando: '+data+' con un Tipo de Cambio de: '+monto_cambio_usado;
                //$('#mensaje_moneda').html(operaciondt);

                $('#mensaje_moneda_compra').html(data);
            });
            
            
            $('#tiempoopermensage').css("display","block");
            
             
            
        }

        


JS;

$this->registerJs($jsc, $this::POS_END);
?>

<div class="operaciones-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="col-md-12 col-sm-12">
        
        <div class=" text-success" style="background-color: #ffffff70">
            <p class="text-center mb-3 mb-sm-0" style="font-weight:500">Tipo de cambio</p>
            <div class="text-center">
                <span class="mb-0">Compra: <span style="font-weight: 700;font-size: 2rem" id="buy-exchange-rate"><?= number_format($tipocambio->monto , 3, '.', ',') ?></span></span>&nbsp;&nbsp;&nbsp;
                <span class="mb-0">Venta: <span style="font-weight: 700;font-size: 2rem" id="sell-exchange-rate"><?=   number_format($tipocambio->venta , 3, '.', ',')?></span></span>
          </div>
        </div>

        <?= $form->field($model, 'id_tipo_cambio')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'monto_cambio_usado')->hiddenInput()->label(false) ?>
        <input type="hidden" id="operaciones-monto_compra" class="form-control" name="Operaciones[monto_compra]" aria-invalid="false" value=<?= $tipocambio->monto ?>>
        <input type="hidden" id="operaciones-monto_venta" class="form-control" name="Operaciones[monto_venta]" aria-invalid="false" value=<?= $tipocambio->venta ?>>
<?php //$form->field($model, 'monto_cambio_usado')->hiddenInput()->label(false) ?>

        <div class="col-md-6 col-sm-6">
            <div class="col-md-6 col-sm-6">
                <?= $form->field($model, 'monto_envio')->textInput(['onchange' => 'montorecibe()',
                        ])     ?>
            </div>

            <div class="col-md-6 col-sm-6" >
                <label class="control-label" >Tipo de Moneda</label>
                <?php 

                    echo SwitchInput::widget([
                        'id' => 'id_tipo_moneda_envia',
                        'name' => 'id_tipo_moneda_envia',
                         'value'=>true,
                        'pluginOptions' => [
                            
                            'onColor' => 'success',
                            'offColor' => 'primary',
                            'onText'=>'Dolar - $',
                            'offText'=>'Soles - S/',

                        ],
                        'options' => ['onchange' => 'montorecibe()',
                        
                        ],
                    ]);
                 ?>
            </div>

        </div>

        <div class="col-md-6 col-sm-6">

            <div class="col-md-6 col-sm-6" >
                <?= $form->field($model, 'monto_recibe')->textInput(['readonly'=>true]) ?>
            </div>

            <div class="col-md-6 col-sm-6 " style="padding-right: 0px; padding-left: 0px;padding-top: 8px;">

                <h3 class="bg bg-success text-success " id="mensaje_moneda_recibe" style="padding-left: 15px;"> </h3>
            </div>
            
            <?= $form->field($model, 'id_tipo_moneda_envia')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'id_tipo_moneda_recibe')->hiddenInput()->label(false) ?>
        </div>
        

        <!-- <div class="col-md-12 col-sm-12">
            <div class="bg-success text-success text-center"><h3 id="mensaje_moneda"> </h3></div>
            
        </div>
         -->

    </div>

    <div class="col-md-12 col-sm-12" id="tiempoopermensage" style="display:none;">
        
          <div class="form-group col-md-12 col-sm-12 text-center" style="margin-top: 1%;">
       
            <?= Html::a('Enviar Dinero', "https://app.multitasas.com/frontend/web/index.php", ['class' => 'btn btn-success', 'title' => 'Enviar Dinero', 'target'=>'multitasas']) ?>
       
        </div>
     </div>     

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\cambiodivisas\models\Operacioncomprobantes; 
/* @var $this yii\web\View */
/* @var $model frontend\modules\cambiosdivisas\models\Operaciones */

$this->title = 'Código de Operación: <strong class="text-success">'.$model->codigo.'</strong>';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Operaciones'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="operaciones-view">
     <div class="col-md-9 col-sm-9">
        <h1><?= Html::decode($this->title) ?></h1> 
       
    </div>
    <div class="col-md-3 col-sm-3">
        
        <span class="operation-details-item">Estatus:</span>
         <span class="label " style="background-color:<?=$model->estatus0->color?>; color:#fff"><?=$model->estatus0->descripcion?></span>
         <br>
         <span class="operation-details-item">Fecha de Regsitro:</span>
         <br>
         <span ><?= \Yii::$app->formatter->asDate($model->fecha_registro, 'php:d-m-Y') ?></span>
    </div>

    <div class="col-md-6 col-sm-6">
        <h3 class="bg bg-info text-info " style="padding: 5px">Tú Enviaste:</h3>
        

        <div class="table-responsive" > 
            <table class="table table-sm ">
            <tbody>
                
                <!-- <tr>
                    <td style="border: 0px"><span class="operation-details-item">Banco:</span></td>
                    <td style="border: 0px"><?= $model->bancoEnvia->descripcion ?></td>
                </tr> -->
                <tr>
                    <td style="border: 0px"><span class="operation-details-item">Monto Transferido:</span></td>
                    <td style="border: 0px"><span class="currentfix"><?= $model->tipoMonedaEnvia->monedas.' '. number_format($model->monto_envio , 2, ',', '.') ?></span></td>
                </tr>
                
            </tbody>
            </table>
        </div>

    </div>


    <div class="col-md-6 col-sm-6">
        <h3 class="bg bg-success text-success " style="padding: 5px">Tú Recibiste:</h3>
        <div class="table-responsive">
            <table class="table table-borderless">
            <tbody>
                
                <tr>
                    <td style="border: 0px"><span class="operation-details-item">Banco:</span></td>
                    <td style="border: 0px"><?= $model->cuentabancariabsuario->banco->descripcion ?></td>
                </tr>
                <tr>
                    <td style="border: 0px"><span class="operation-details-item">Cuenta:</span></td>
                    <td style="border: 0px"><span class="currentfix"><?= $model->cuentabancariabsuario->cuentanrodetalle ?></span></td>
                </tr>
                <tr>
                    <td style="border: 0px"><span class="operation-details-item">Monto:</span></td>
                    <td style="border: 0px"> <span class="currentfix"><?= $model->tipoMonedaRecibe->monedas.' '. number_format($model->monto_recibe , 2, ',', '.') ?></span></td>
                </tr>
            </tbody>
            </table>
        </div>
    </div>

    <div class="col-md-12 col-sm-12">
        <h3 class="bg bg-warning text-warning " style="padding: 5px">Detalle:</h3>
        <div class="table-responsive">
            <table class="table table-borderless">
            <tbody> 
                
                <tr>
                    <td ><span class="operation-details-item">Tipo de Cambio:</span></td>
                    <td ><?= number_format($model->monto_cambio_usado , 2, ',', '.') ?></td>
                </tr>
                <tr>
                    <td ><span class="operation-details-item table-cell">Banco:</span></td>
                    <td ><span class="currentfix"><?= $model->bancoEnvia->descripcion ?></span></td>
                </tr>
                <?php if ($model->id_cuenta_bancaria_sistema!="1") { ?>
                <tr>
                    <td ><span class="operation-details-item table-cell">Cuenta Bancaria del Sistema:</span></td>
                    <td ><span class="currentfix"><?= $model->cuentaBancariaSistema->nroctas ?></span></td>
                </tr>
                <?php } ?>
                <tr>
                    <td ><span class="operation-details-item table-cel">Monto Transferido:</span></td>
                    <td ><span class="currentfix"><?= $model->tipoMonedaEnvia->monedas.' '.number_format($model->monto_envio , 2, ',', '.') ?></span></td>
                </tr>
                <tr>
                    <td ><span class="operation-details-item table-cell">Código de Referencia / Número de Operación :</span></td>
                    <td > <span class="currentfix"><?= $model->cod_referencia ?></span></td>
                </tr>

                <?php if ($model->fecha_proceso!="") { ?>
                         <tr>
                            <td ><span class="operation-details-item">Fecha de Proceso: </span></td>
                            <td > <span class="currentfix"><?= \Yii::$app->formatter->asDate($model->fecha_proceso, 'php:d-m-Y')  ?></span></td>
                        </tr>

                <?php } ?>

                <?php if ($model->fecha_pago_sistema!="") { ?>
                         <tr>
                            <td ><span class="operation-details-item">Fecha de Pago: </span></td>
                            <td > <span class="currentfix"><?= \Yii::$app->formatter->asDate($model->fecha_pago_sistema, 'php:d-m-Y')  ?></span></td>
                        </tr>

                <?php } ?>

                
            </tbody>
            </table>
        </div>


    </div>
    <?php if ($model->tipo=="REMESA") { ?>
         <div class=" col-md-12 col-sm-12 " >

             <h4 class="bg bg-info text-info text-center" style="padding: 5px">Comprobantes de Pago </h4>
              <br style="clear:both"/>

        </div>
            <?php 

            //echo Url::to(['/common/uploads/'];

           // echo Yii::getAlias('@imageurl');

            $comprobantes=Operacioncomprobantes::find()->where(['id_operacion' => $model->idoperacion])->orderBy('orden ASC')->all();

            if ($comprobantes!=''){

                foreach ($comprobantes as $key => $comprobantes) {

                    echo '<div class=" col-md-6 col-sm-6 " >';
                    echo Html::a(Html::img(Yii::$app->params['imgUrl'].'comprobantes/'.$model->idoperacion.'/'.$comprobantes->imagen, ['alt' => 'Descargar Comprobante de Pago','title' => 'Comprobante de Pago', 'width'=>'75%' ]).'<br> Descargar Comprobante',Yii::$app->params['imgUrl'].'comprobantes/'.$model->idoperacion.'/'.$comprobantes->imagen, ['target'=>$comprobantes->imagen, 'download'=>$comprobantes->imagen]);
                    echo '</div>';

                }

             }   ?>
                        

    <?php } ?>
<br style="clear:both"/><br style="clear:both"/>
    <div class=" col-md-12 col-sm-12" >
        <center>
        
            <?= Html::a('Regresar', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Regresar']) ?>
        </center>
    </div>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\cambiosdivisas\models\OperacionesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="operaciones-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idoperacion') ?>

    <?= $form->field($model, 'codigo') ?>

    <?= $form->field($model, 'id_banco_envia') ?>

    <?= $form->field($model, 'id_cuenta_bancaria_usuario') ?>

    <?= $form->field($model, 'id_tipo_moneda_envia') ?>

    <?php // echo $form->field($model, 'monto_envio') ?>

    <?php // echo $form->field($model, 'id_tipo_cambio') ?>

    <?php // echo $form->field($model, 'id_tipo_moneda_recibe') ?>

    <?php // echo $form->field($model, 'monto_recibe') ?>

    <?php // echo $form->field($model, 'monto_cambio_usado') ?>

    <?php // echo $form->field($model, 'id_cuenta_bancaria_sistema') ?>

    <?php // echo $form->field($model, 'cod_referencia') ?>

    <?php // echo $form->field($model, 'fecha_registro') ?>

    <?php // echo $form->field($model, 'fecha_pago_sistema') ?>

    <?php // echo $form->field($model, 'fecha_proceso') ?>

    <?php // echo $form->field($model, 'estatus') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

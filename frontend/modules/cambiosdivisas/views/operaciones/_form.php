<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\cambiosdivisas\models\Operaciones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="operaciones-form">

    <?php $form = ActiveForm::begin(); ?>
<div class="bg bg-form col-md-12 col-sm-12">
    <?= $form->field($model, 'codigo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_banco_envia')->textInput() ?>

    <?= $form->field($model, 'id_cuenta_bancaria_usuario')->textInput() ?>

    <?= $form->field($model, 'id_tipo_moneda_envia')->textInput() ?>

    <?= $form->field($model, 'monto_envio')->textInput() ?>

    <?= $form->field($model, 'id_tipo_cambio')->textInput() ?>

    <?= $form->field($model, 'id_tipo_moneda_recibe')->textInput() ?>

    <?= $form->field($model, 'monto_recibe')->textInput() ?>

    <?= $form->field($model, 'monto_cambio_usado')->textInput() ?>

    <?= $form->field($model, 'id_cuenta_bancaria_sistema')->textInput() ?>

    <?= $form->field($model, 'cod_referencia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_registro')->textInput() ?>

    <?= $form->field($model, 'fecha_pago_sistema')->textInput() ?>

    <?= $form->field($model, 'fecha_proceso')->textInput() ?>

    <?= $form->field($model, 'estatus')->textInput() ?>
</div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use yii\helpers\Url;
use kartik\widgets\SwitchInput;
 
use frontend\modules\cambiosdivisas\models\Tipodemoneda;
use frontend\modules\cambiosdivisas\models\Tipodecambio;
use frontend\modules\cambiosdivisas\models\Bancos;
use frontend\modules\cambiosdivisas\models\Cuentabancariadeusuarios;
use frontend\modules\cambiosdivisas\models\Cuentasbancariadesistema;

use kartik\number\NumberControl;

use frontend\modules\cambiosdivisas\models\Usuario;
//$paisuser=Usuario::findOne(Yii::$app->user->identity->getId());

/* @var $this yii\web\View */
/* @var $model frontend\modules\cambiosdivisas\models\Operaciones */
/* @var $form yii\widgets\ActiveForm */

$jsc = <<< JS

        

        function montorecibe(){

            //var item=$('#operaciones-id_tipo_moneda_envia').val();
            //var valor=$('#id_tipo_moneda_envia').bootstrapSwitch('state');

            //if (valor==true){

            //   var item=1;
                
            //}else{
                
                
            //    var item=2;
            //}
            var item=2;
            var monedacompra=4;

            var monto_compra =$('#operaciones-monto_compra').val();
            var monto_venta =$('#operaciones-monto_venta').val();
            var monto_envio=$('#operaciones-monto_envio').val();

            
            if (item==1){
                
                var monto_cambio_usado =monto_compra;
                var monto_operacion=parseFloat(monto_envio)*parseFloat(monto_cambio_usado);
            }else{
                
                var monto_cambio_usado =monto_venta;
                var monto_operacion=parseFloat(monto_envio)*parseFloat(monto_cambio_usado);
            }

            //$('#operaciones-monto_recibe').val(monto_operacion.toLocaleString("es-ES"));
            $('#operaciones-monto_recibe').val(monto_operacion.toFixed(2));
            $('#operaciones-monto_recibe-disp').val(monto_operacion.toFixed(2));

            $('#operaciones-monto_cambio_usado').val(monto_cambio_usado);

            $.post("index.php?r=/cambiosdivisas/operaciones/listmonedasven&id=" + monedacompra, 

            function(data){
                
                $('#mensaje_moneda_recibe').html(data);

                var operaciondt='Estas Comprando: '+data+' con un Tipo de Cambio de: '+monto_cambio_usado;
                
            }); 
            
            $('#tiempoopermensage').css("display","block");

          
            
        }

       

JS;

$this->registerJs($jsc, $this::POS_END);
?>

<div class="operaciones-form">

    <?php $form = ActiveForm::begin(); ?>    

    <div class="col-md-12 col-sm-12">

            <div class=" text-success" style="background-color: #ffffff70">
              <p class="text-center mb-3 mb-sm-0" style="font-weight:500">Tipo de cambio</p>
              <div class="text-center">
                <!-- <span class="mb-0">Dolar - $: <span style="font-weight: 700;font-size: 2rem" id="buy-exchange-rate"><?= number_format($tipocambio->monto , 2, ',', '.') ?></span></span>&nbsp;&nbsp;&nbsp; -->
                <span class="mb-0">Soles - S/: <span style="font-weight: 700;font-size: 2rem" id="sell-exchange-rate"><?=   number_format($tipocambio->venta , 2, ',', '.')?></span></span>
              </div>
            </div>

        <?= $form->field($model, 'id_tipo_cambio')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'monto_cambio_usado')->hiddenInput()->label(false) ?>
        <input type="hidden" id="operaciones-monto_compra" class="form-control" name="Operaciones[monto_compra]" aria-invalid="false" value=<?= $tipocambio->monto ?>>
        <input type="hidden" id="operaciones-monto_venta" class="form-control" name="Operaciones[monto_venta]" aria-invalid="false" value=<?= $tipocambio->venta ?>>



        <div class="col-4 col-md-6 col-sm-6">
            <div class="col-4 col-md-6 col-sm-6" >
                <?= $form->field($model, 'monto_envio')->widget(NumberControl::classname(),
                [
                    'options' => ['onchange' => 'montorecibe()',],
                    'maskedInputOptions' => [
                    'groupSeparator' => '.',
                    'radixPoint' => ','
                ],

                ])->label('Quiero Enviar:')     ?>
            </div>

            <div class="col-4 col-md-6 col-sm-6" >
               
                <?= $form->field($model, 'id_tipo_moneda_envia')->widget(Select2::classname(), [
                    'data' => ['2'=>'Soles - S/'],
                    'language' => 'es',
                    'options' => ['placeholder' => 'Seleccionar una Opción ...', 
                                  'onchange' => 'montorecibe()',
                                 ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
                
        
            </div>

        </div>

        <div class="col-4 col-md-6 col-sm-6">
            <div class="col-4 col-md-6 col-sm-6" >
                <?= $form->field($model, 'monto_recibe')->widget(NumberControl::classname(),
                [
                    //'options' => ['onchange' => 'montorecibe()',],
                    'readonly'=>true,
                    'maskedInputOptions' => [
                    'groupSeparator' => '.',
                    'radixPoint' => ',',

                ],

                ])->label('Monto a Recibir:')     ?>
                
            </div>

            <div class="col-4 col-md-6 col-sm-6 " style="padding-right: 0px; padding-left: 0px;padding-top: 8px;">
                <h3 class="bg bg-success text-success " id="mensaje_moneda_recibe" style="padding-left: 15px;"> </h3>
            </div>
        
            <?= $form->field($model, 'id_tipo_moneda_recibe')->hiddenInput()->label(false) ?>
        </div>

    </div>

    <div class="col-md-12 col-sm-12" id="tiempoopermensage" style="display:none;">
         
         
         <div class="form-group col-md-12 col-sm-12 text-center" style="margin-top: 1%;">
       
            <?= Html::a('Enviar Dinero', "https://app.multitasas.com/frontend/web/index.php", ['class' => 'btn btn-success', 'title' => 'Enviar Dinero', 'target'=>'multitasas']) ?>
       
        </div>
    </div>     
    

    <?php ActiveForm::end(); ?>

</div>

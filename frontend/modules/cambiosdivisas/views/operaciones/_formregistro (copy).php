<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

use kartik\widgets\SwitchInput;

use frontend\modules\cambiosdivisas\models\Tipodemoneda;
use frontend\modules\cambiosdivisas\models\Tipodecambio;
use frontend\modules\cambiosdivisas\models\Bancos;
use frontend\modules\cambiosdivisas\models\Cuentabancariadeusuarios;
use frontend\modules\cambiosdivisas\models\Cuentasbancariadesistema;
/* @var $this yii\web\View */
/* @var $model frontend\modules\cambiosdivisas\models\Operaciones */
/* @var $form yii\widgets\ActiveForm */

// use buttflattery\formwizard\FormWizard;
// echo FormWizard::widget([
//     'steps'=>[
//         [
//             'model'=>$model,
//             'title'=>'My Shoots',
//             'description'=>'Add your shoots',
//             'formInfoText'=>'Fill all fields'
//         ],
        
//     ]
// ]);

$jsc = <<< JS


        // function valor(item){


        //     var id = $('#defunidadcurricular-regimen_id').val();
        //     //alert(id);
        //     $.get('index.php?r=mallacurricular/defregimen/get-valor', {id : id}, function(data){
        //         $('#defunidadcurricular-valor').val(data);

        //     });
        // }

        function nroreferencia(){

           // if ($("#Cod_refencia").attr("checked")){

            var valor=$('#Cod_refencia').bootstrapSwitch('state');

            if (valor==true){

                $("#codigorefcss").css("display", "block");
                
            }else{
                
                
                $("#codigorefcss").css("display", "none");
            }

            
        }

        function montorecibe(){

            var item=$('#operaciones-id_tipo_moneda_envia').val();

            var monto_compra =$('#operaciones-monto_compra').val();
            var monto_venta =$('#operaciones-monto_venta').val();
            var monto_envio=$('#operaciones-monto_envio').val();

            
            if (item==1){
                
                var monto_cambio_usado =monto_compra;
                var monto_operacion=parseFloat(monto_envio)*parseFloat(monto_cambio_usado);
            }else{
                
                var monto_cambio_usado =monto_venta;
                var monto_operacion=parseFloat(monto_envio)/parseFloat(monto_cambio_usado);
            }

            //var monto_operacion=parseFloat(monto_envio)*parseFloat(monto_cambio_usado);

            $('#operaciones-monto_recibe').val(monto_operacion.toFixed(3));

            $('#operaciones-monto_cambio_usado').val(monto_cambio_usado);

            $.post("index.php?r=/cambiosdivisas/operaciones/listmonedascontra&id=" + item, 

            function(data){
                //console.log(data);
                $("#operaciones-id_tipo_moneda_recibe").val(data);
                
            });


        }

        function tipomoneda(){
            var item=$('#operaciones-id_tipo_moneda_envia').val();


            $.post("index.php?r=/cambiosdivisas/operaciones/listmonedasdescription&id=" + item, 

            function(data){
                console.log(data);

                $('#mensaje_moneda_compra').html(data);
            });
            
        }


JS;

$this->registerJs($jsc, $this::POS_END);
?>

<div class="operaciones-form">

    <?php $form = ActiveForm::begin(); ?>

    

    <div class="col-md-6 col-sm-6">
        
        
        <?= $form->field($model, 'id_banco_envia')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Bancos::find()->orderBy('codigo ASC')->all(), 'idbancos','codigo'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...',
                          'onchange'  => '
                            $.post("index.php?r=/cambiosdivisas/cuentabancariadeusuarios/listcuentas&id=' . '" + $(this).val(), function(data){
                                $("select#operaciones-id_cuenta_bancaria_usuario").html(data);
                            })
                        ',
                    ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        
    </div>
    <div class="col-md-6 col-sm-6">
        
        
        <?= $form->field($model, 'id_cuenta_bancaria_usuario')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Cuentabancariadeusuarios::find()->where(['id_usuario' => Yii::$app->user->identity->getId()])->orderBy('alias ASC')->all(), 'idcuenta_bancaria','cuentanro'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...',
                          'onchange'  => '
                            $.post("index.php?r=/cambiosdivisas/operaciones/listmonedas_dt&id=' . '" + $(this).val(), function(data){
                                $("#operaciones-id_tipo_moneda_envia").val(data);
                            });tipomoneda();
                        ',
                    ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        
    </div>

    <div class="col-md-12 col-sm-12">

        


        <?= $form->field($model, 'id_tipo_cambio')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'monto_cambio_usado')->hiddenInput()->label(false) ?>
        <input type="hidden" id="operaciones-monto_compra" class="form-control" name="Operaciones[monto_compra]" aria-invalid="false" value=<?= $tipocambio->monto ?>>
        <input type="hidden" id="operaciones-monto_venta" class="form-control" name="Operaciones[monto_venta]" aria-invalid="false" value=<?= $tipocambio->venta ?>>
<?php //$form->field($model, 'monto_cambio_usado')->hiddenInput()->label(false) ?>


        <div class="col-md-8 col-sm-8">
            <div class="col-md-6 col-sm-6">

                <?= $form->field($model, 'monto_envio')->textInput(['onchange' => 'montorecibe()',])     ?>
            </div>

            <div class="col-md-4 col-sm-4">

                <h3 id="mensaje_moneda_compra"> </h3>
            </div>


        </div>

        <div class="col-md-6 col-sm-6">
            <?= $form->field($model, 'monto_recibe')->textInput(['readonly'=>true]) ?>
            <?php /* $form->field($model, 'id_tipo_moneda_envia')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Tipodemoneda::find()->orderBy('descripcion ASC')->all(), 'idbtipomoneda','monedas'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...',
                        'onchange' => 'montorecibe()',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); */?>
        <?= $form->field($model, 'id_tipo_moneda_envia')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'id_tipo_moneda_recibe')->hiddenInput()->label(false) ?>
        </div>
        

        <div class="col-md-12 col-sm-12">
            <div class="bg-success text-success text-center"><h3 id="mensaje_moneda"> </h3></div>
            
        </div>
        


        

    </div>

     <div class="col-md-12 col-sm-12">

    <label class="control-label" >¿Ya Posee un Código de Referencia?</label>
    <?php 

        echo SwitchInput::widget([
            'id' => 'Cod_refencia',
            'name' => 'Cod_refencia',
            'pluginOptions' => [
                
                'onColor' => 'success',
                'offColor' => 'danger',
                'onText'=>'Si',
                'offText'=>'No',

            ],
            'options' => ['onchange' => 'nroreferencia()',
            ],
        ]);
     ?>
     </div>
    <div class="col-md-12 col-sm-12">
        <div id="codigorefcss">
            <div class="col-md-6 col-sm-6">
                
                <?= $form->field($model, 'id_cuenta_bancaria_sistema')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map($cuentasbancarias=Cuentasbancariadesistema::find()->where(['estatus' => '13'])->andWhere(['!=' ,'idcuenta_bancaria', '1'])->orderBy('alias ASC')->all(), 'idcuenta_bancaria','nroctas'),
                    'language' => 'es',
                    'options' => ['placeholder' => 'Seleccionar una Opción ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>

                
            </div>
            <div class="col-md-6 col-sm-6">
                <?= $form->field($model, 'cod_referencia')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    </div>



    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

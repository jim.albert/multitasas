<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use yii\helpers\Url;
use kartik\widgets\SwitchInput;
use yii\web\JsExpression;
 
use frontend\modules\cambiosdivisas\models\Tipodemoneda;
use frontend\modules\cambiosdivisas\models\Tipodecambio;
use frontend\modules\cambiosdivisas\models\Bancos;
use frontend\modules\cambiosdivisas\models\Cuentabancariadeusuarios;
use frontend\modules\cambiosdivisas\models\Cuentasbancariadesistema;

use kartik\number\NumberControl;

use frontend\modules\cambiosdivisas\models\Usuario;
$paisuser=Usuario::findOne(Yii::$app->user->identity->getId());

/* @var $this yii\web\View */
/* @var $model frontend\modules\cambiosdivisas\models\Operaciones */
/* @var $form yii\widgets\ActiveForm */

// use buttflattery\formwizard\FormWizard;
// echo FormWizard::widget([
//     'steps'=>[
//         [
//             'model'=>$model,
//             'title'=>'My Shoots',
//             'description'=>'Add your shoots',
//             'formInfoText'=>'Fill all fields'
//         ],
        
//     ]
// ]);

$jsc = <<< JS


        

        function nroreferencia(){

           // if ($("#Cod_refencia").attr("checked")){

            var valor=$('#Cod_refencia').bootstrapSwitch('state');

            if (valor==true){

                $("#codigorefcss").css("display", "block");
                
            }else{
                
                
                $("#codigorefcss").css("display", "none");
            }

            
        }

        function montorecibe(){

            var item=$('#operaciones-id_tipo_moneda_envia').val();
            //alert (item);
            var monedacompra=4;

            
            $.post("index.php?r=/cambiosdivisas/operaciones/montocompra&id=" + item, 

            function(monto){
                console.log(monto);
                //alert ("1 monto_compra "+monto);
                
                var monto_compra =monto;
                $('#operaciones-monto_compra').val(monto);
                //$('#operaciones-monto_envio').val(monto);
               // #operaciones-monto_envio').val()

                var monto_venta =0;
            var monto_envio=$('#operaciones-monto_envio').val();

            //alert ("2 monto_venta "+monto_venta);
            //alert ("3 monto_envio  "+monto_envio);

            var monto_cambio_usado =monto_compra;
                var monto_operacion=parseFloat(monto_envio)*parseFloat(monto_cambio_usado);

            $('#operaciones-monto_venta').val(monto_venta);

            $('#operaciones-monto_recibe').val(monto_operacion.toFixed(3));
            $('#operaciones-monto_recibe-disp').val(monto_operacion.toFixed(3));
            

            $('#operaciones-monto_cambio_usado').val(monto_cambio_usado);

            $.post("index.php?r=/cambiosdivisas/operaciones/listmonedasven&id=" + monedacompra, 

            function(data){
             
                

                $('#mensaje_moneda_recibe').html(data);

                var operaciondt='Estas Comprando: '+data+' con un Tipo de Cambio de: '+monto_cambio_usado;
                $('#mensaje_moneda').html(operaciondt);
                
                
            });

            
            
            $('#tiempoopermensage').css("display","block");

            
            
            timer_count();
            
            });

            
            
            
        }

        function timer_count(){
            //===
            // VARIABLES
            //===
            const DATE_TARGET = new Date();
            DATE_TARGET.setMinutes( DATE_TARGET.getMinutes() + 10 );

            //DATE_TARGET.setSeconds( DATE_TARGET.getSeconds() + 10 );
            // DOM for render

            const SPAN_MINUTES = document.querySelector('span#minutes');
            const SPAN_SECONDS = document.querySelector('span#seconds');
            // Milliseconds for the calculations
            const MILLISECONDS_OF_A_SECOND = 1000;
            const MILLISECONDS_OF_A_MINUTE = MILLISECONDS_OF_A_SECOND * 60;
            const MILLISECONDS_OF_A_HOUR = MILLISECONDS_OF_A_MINUTE * 60;
            const MILLISECONDS_OF_A_DAY = MILLISECONDS_OF_A_HOUR * 24

            //===
            // FUNCTIONS
            //===

            /**
             * Method that updates the countdown and the sample
             */
            function updateCountdown() {
                // Calcs
                const NOW = new Date()
                const DURATION = DATE_TARGET - NOW;
                
                const REMAINING_MINUTES = Math.floor((DURATION % MILLISECONDS_OF_A_HOUR) / MILLISECONDS_OF_A_MINUTE);
                const REMAINING_SECONDS = Math.floor((DURATION % MILLISECONDS_OF_A_MINUTE) / MILLISECONDS_OF_A_SECOND);
                
                // Render
                
                SPAN_MINUTES.textContent = REMAINING_MINUTES;
                SPAN_SECONDS.textContent = REMAINING_SECONDS;

                if (REMAINING_MINUTES<=0 && REMAINING_SECONDS<=0){
                    location.reload(true);
                }
            }

            //===
            // INIT
            //===
            updateCountdown();
            // Refresh every second
            setInterval(updateCountdown, MILLISECONDS_OF_A_SECOND);

            
        }


JS;

$this->registerJs($jsc, $this::POS_END);

$url = Yii::$app->params['imgUrl'].'flags/png/';
$format = <<< SCRIPT
function format(state) {
    if (!state.id) return state.text; // optgroup
    src = '$url' +  state.id.toLowerCase() + '.png'
    return '<img class="flag" src="' + src + '" width="20px"/> ' + state.text;
}
SCRIPT;
$this->registerJs($format, $this::POS_HEAD);
$escape = new JsExpression("function(m) { return m; }");
?>

<div class="operaciones-form">

    <?php $form = ActiveForm::begin(); ?>
    
<div class="bg bg-form col-md-12 col-sm-12">
    <div class="col-md-6 col-sm-6">
        
        
        <?= $form->field($model, 'id_banco_envia')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Bancos::find()->where(['id_pais'=>$paisuser->id_pais])->orderBy('codigo ASC')->all(), 'idbancos','codigo'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...',
                          // 'onchange'  => '
                          //   $.post("index.php?r=/cambiosdivisas/cuentabancariadeusuarios/listcuentas&id=' . '" + $(this).val(), function(data){
                          //       $("select#operaciones-id_cuenta_bancaria_usuario").html(data);
                          //   })',
                    ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        
    </div>
    <div class="col-md-6 col-sm-6">
        

        
        <?= $form->field($model, 'id_cuenta_bancaria_usuario')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Cuentabancariadeusuarios::find()->where(['id_usuario' => Yii::$app->user->identity->getId()])->orderBy('nombre_beneficiario ASC')->all(), 'idcuenta_bancaria','cuentanro','tipo'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...',
                          // 'onchange'  => '
                          //   $.post("index.php?r=/cambiosdivisas/operaciones/listmonedas_dt&id=' . '" + $(this).val(), function(data){
                          //       $("#operaciones-id_tipo_moneda_envia").val(data);
                          //   }); ',
                    ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        
    </div>

    <div class="col-md-12 col-sm-12">

        


        <?= $form->field($model, 'id_tipo_cambio')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'monto_cambio_usado')->hiddenInput()->label(false) ?>
        <input type="hidden" id="operaciones-monto_compra" class="form-control" name="Operaciones[monto_compra]" aria-invalid="false" value=<?= $tipocambio->monto ?>>
        <input type="hidden" id="operaciones-monto_venta" class="form-control" name="Operaciones[monto_venta]" aria-invalid="false" value=<?= $tipocambio->venta ?>>
<?php //$form->field($model, 'monto_cambio_usado')->hiddenInput()->label(false) ?>


        <div class="col-md-6 col-sm-6">
            <div class="col-md-6 col-sm-6" style="padding-right: 0px; padding-left: 0px;">

                <?= $form->field($model, 'monto_envio')->widget(NumberControl::classname(),
                [
                    'options' => ['onchange' => 'montorecibe()',],
                    'maskedInputOptions' => [
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'digits' => 4,
                ],

                ])->label('Quiero Enviar:')     ?>

            </div>

            <div class="col-md-6 col-sm-6" style="padding-right: 0px; padding-left: 0px;">

                
               
                <?php /*= $form->field($model, 'id_tipo_moneda_envia')->widget(Select2::classname(), [
                    'data' => ['2'=>'Soles - S/'],
                    'language' => 'es',
                    'options' => ['placeholder' => 'Seleccionar una Opción ...', 
                                  'onchange' => 'montorecibe()',
                                 ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);*/ 

                $data=ArrayHelper::map(Tipodemoneda::find()->where(['estatus' => '5'])->andwhere(['remesas' => '1'])->andwhere(['!=','idbtipomoneda', '4'])->all(), 'idbtipomoneda','descripcion');
                ?>

                <?= $form->field($model, 'id_tipo_moneda_envia', [
                            'template' => '{label} <div class="field">{input}{error}{hint}</div>',
                     ])->widget(Select2::classname(),[
                    'data' => $data,
                    'options' => ['placeholder' => 'Seleccionar una Opción ...',
                                    'onchange' => 'montorecibe()'
                                 ],
                    'pluginOptions' => [
                        'templateResult' => new JsExpression('format'),
                        'templateSelection' => new JsExpression('format'),
                        'escapeMarkup' => $escape,
                        'allowClear' => true
                    ],
                ]);  ?>


        
            </div>


        </div>

        <div class="col-md-6 col-sm-6">
            


            <div class="col-md-6 col-sm-6" style="padding-right: 0px; padding-left: 0px;">

                <!-- <?= $form->field($model, 'monto_recibe')->textInput(['readonly'=>true])->label('Monto a Recibir:') ?> -->

                <?= $form->field($model, 'monto_recibe')->widget(NumberControl::classname(),
                [
                    //'options' => ['onchange' => 'montorecibe()',],
                    'readonly'=>true,
                    'maskedInputOptions' => [
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'digits' => 4,

                ],
                
                ])->label('Monto a Recibir:')     ?>
            </div>

            <div class="col-md-6 col-sm-6 " style="padding-right: 0px; padding-left: 0px;padding-top: 5px;">

                <h3 class="bg bg-success text-success " id="mensaje_moneda_recibe" style="padding-left: 15px;"> </h3>
            </div>
            <?php /* $form->field($model, 'id_tipo_moneda_envia')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Tipodemoneda::find()->orderBy('descripcion ASC')->all(), 'idbtipomoneda','monedas'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...',
                        'onchange' => 'montorecibe()',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); */?>
        
        <?= $form->field($model, 'id_tipo_moneda_recibe')->hiddenInput()->label(false) ?>
        </div>
        

        <div class="col-md-12 col-sm-12">
            <div class="bg-success text-success text-center"><h3 id="mensaje_moneda"> </h3></div>
            
        </div>
        


        

    </div>

    <div class="col-md-12 col-sm-12" id="tiempoopermensage" style="display:none;background-color: #f5f5f5;">
         <div class="col-md-6 col-sm-6  ">

            <center>
                <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
                        <lottie-player src="./img/lf30_editor_kfx1bQ.json" background="transparent"  speed="1"  style="width: 200px; height: 200px;" loop  autoplay></lottie-player>

            </center>
            
            
         </div>


         <div class="col-md-6 col-sm-6  ">
            
    

        <h3  class="text-center" style="margin-top: 15%;">¡Tienes <span class="text-danger" style="font-weight: 900;"><span id="minutes"></span> Minutos con <span id="seconds"></span> segundos </span>  para realizar tu operación!</h3>
            
         </div>

         <div class="col-md-12 col-sm-12">
            <center>

            <label class="control-label" >¿Ya Tranferiste/Depositaste?</label>
                <?php 

                    echo SwitchInput::widget([
                        'id' => 'Cod_refencia',
                        'name' => 'Cod_refencia',
                        'pluginOptions' => [
                            
                            'onColor' => 'success',
                            'offColor' => 'danger',
                            'onText'=>'Si',
                            'offText'=>'No',

                        ],
                        'options' => ['onchange' => 'nroreferencia()',
                        ],
                    ]);
                 ?>
            </center> 
         </div>
     </div>     
    <div class="col-md-12 col-sm-12" style="background-color: #f5f5f5;">
            <div id="codigorefcss">
            
             <div class="col-md-6 col-sm-6">
                
               <?= $form->field($model, 'cod_referencia')->textInput(['maxlength' => true]) ?> 
            </div>

            <div class="col-md-6 col-sm-6">
        
                
                  
                        <?= $form->field($modelcomprobantes, 'imagen')->widget(FileInput::classname(), [
                      'options' => ['accept' => 'image/*'],
                       'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png', 'jpeg'],'showPreview' => false,
                        'showCaption' => true,
                        'showRemove' => true,
                        'showUpload' => false,
                        'maxFileSize'=>1024,
                        'msgSizeTooLarge' => 'File "{name}" (<b>{size} KB</b>) exceeds maximum allowed upload size of <b>{maxSize} KB</b>.',
                    ],
                  ]);   ?>
                   <span class="bg bg-warning text-warning">Las imagenes a procesar deben pesar menos de 1024 Kb (1 Mb).</span>
            </div>
            <?= $form->field($model, 'id_cuenta_bancaria_sistema')->hiddenInput()->label(false) ?> 
        </div>
    </div>

  </div>  

    <div class="form-group col-md-12 col-sm-12" style="margin-top: 2%;">
        <?= Html::submitButton(Yii::t('app', 'Procesar'), ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancelar', ['/site/index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

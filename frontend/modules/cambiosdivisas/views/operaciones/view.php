<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\cambiosdivisas\models\Operaciones */

$this->title = $model->idoperacion;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Operaciones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="operaciones-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->idoperacion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idoperacion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idoperacion',
            'codigo',
            'id_banco_envia',
            'id_cuenta_bancaria_usuario',
            'id_tipo_moneda_envia',
            'monto_envio',
            'id_tipo_cambio',
            'id_tipo_moneda_recibe',
            'monto_recibe',
            'monto_cambio_usado',
            'id_cuenta_bancaria_sistema',
            'cod_referencia',
            'fecha_registro',
            'fecha_pago_sistema',
            'fecha_proceso',
            'estatus',
        ],
    ]) ?>

</div>

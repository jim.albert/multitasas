<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\cambiosdivisas\models\Operaciones */

$this->title = Yii::t('app', 'Envío de Dinero');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Operaciones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operaciones-create"> 

     <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formregistro_remesa', [
        'model' => $model,
        'tipocambio' => $tipocambio,
        'modelcomprobantes'=> $modelcomprobantes,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\cambiosdivisas\models\Operaciones */

$this->title = Yii::t('app', 'Registrar Operación');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Operaciones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operaciones-create"> 

    <?= $this->render('_formregistroweb', [
        'model' => $model,
        'tipocambio' => $tipocambio,
    ]) ?>

</div>

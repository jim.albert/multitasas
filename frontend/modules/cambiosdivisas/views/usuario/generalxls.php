<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Operacionempresa */

$this->title = Yii::t('app', 'Operaciones Desde:'.$desde.' Hasta:'.$hasta);
header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename='.$this->title.'.xls');
?>
<div class="operacionempresa-create">

    

    <?= $this->render('_generalpdf', [
        'desde' => $desde, 'hasta' => $hasta,
         
    ]) ?>

</div> 
 
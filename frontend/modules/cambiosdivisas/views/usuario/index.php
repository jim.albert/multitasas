<?php

//use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\helpers\Html;
use kartik\select2\Select2;
use backend\modules\cambiodivisas\models\Estatus;
use backend\modules\cambiodivisas\models\Tipodedocumento;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\cambiodivisas\models\EstatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Usuarios');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="validaciones-index">

    

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php 
    

    $gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],

    
            //'id',
            'nombres',
            'apellidos',
            //'id_tipo_documento',
            'nro_documento',
            //'fecha_nacimiento',
            'email:email',
            //'password',
            //'fecha_registro',
            //'pep',
            //'nro_operacion',
            //'monto_operacion',
            //'estatus'
            //'tipo_transferencia',
            // [
            //     //'class' => 'kartik\grid\BooleanColumn',
            //     'attribute' => 'tipo_transferencia', 
            //     //'label'=>'Estatus',
            //     //'vAlign' => 'middle',
                
            //     'filterType' => GridView::FILTER_SELECT2,
            //     'filter' => ['Mismo Banco'=>'Mismo Banco', 'Interbancario'=>'Interbancario', 'Internacional'=>'Internacional'],
            //     'filterWidgetOptions' => [
            //         'pluginOptions' => ['allowClear' => true],
            //     ],
            //     'filterInputOptions' => ['placeholder' => 'Opciones'],
            //     'format' => 'raw'
                
            // ],
            // //'tipo_moneda',
            // [
            //     //'class' => 'kartik\grid\BooleanColumn',
            //     'class' => 'kartik\grid\DataColumn',
            //     'attribute' => 'tipo_moneda',
            //     'value' => 'tipoMoneda.monedas', 
            //     'label'=>'Tipo de Moneda',
            //     //'vAlign' => 'middle',
                
            //     'filterType' => GridView::FILTER_SELECT2,
            //     'filter' => ArrayHelper::map(Tipodemoneda::find()->orderBy('descripcion ASC')->all(), 'idbtipomoneda','monedas'),
            //     'filterWidgetOptions' => [
            //         'pluginOptions' => ['allowClear' => true],
            //     ],
            //     'filterInputOptions' => ['placeholder' => 'Opciones'],
            //     'format' => 'raw'
            //     'filter'    => Select2::widget([
            //                   'model' => $searchModel,
            //                   'attribute' => 'estatus',
            //                   'data' => ['1'=>'Activo', '0'=>'Inactivo'],
            //                   'options' => [

            //                       //'multiple' => true,
            //                       'placeholder' => 'Seleccionar una Opción ...'
            //                   ],

            //                   'pluginOptions' => [
            //                       'allowClear' => true
            //                   ],
            //               ]),
            // ], 

            // //'desde',
            // //'hasta',
            // [
            //     'attribute' => 'desde',    
            //     'hAlign' => 'right',
            //     'vAlign' => 'middle',
            // ],
            // [
            //     'attribute' => 'hasta',    
            //     'hAlign' => 'right',
            //     'vAlign' => 'middle',
            // ],
            // //'fecha_registro',
            // //'estatus',
            // [
            //     //'class' => 'kartik\grid\EditableColumn',
            //     'attribute' => 'fecha_registro',    
            //     'hAlign' => 'center',
            //     'vAlign' => 'middle',
            //     //'width' => '9%',
            //     'format' => 'date',
            //     'xlFormat' => "dd\\-mmm\\, \\-yyyy",
            //     'headerOptions' => ['class' => 'kv-sticky-column'],
            //     'contentOptions' => ['class' => 'kv-sticky-column'],
                
            // ],
            
            [
                //'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'estatus', 
                'value' => 'estatus0.descripcion', 
                //'label'=>'Estatus',
                'vAlign' => 'middle',
                
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Estatus::find()->where(['tabla'=>'Usuarios'])->orderBy('descripcion ASC')->all(), 'idstatus','descripcion'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Opciones'],
                'format' => 'raw'
                
            ], 
    ['class' => 'kartik\grid\ActionColumn', ]
            //['class' => 'kartik\grid\CheckboxColumn']
    ];


    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,

        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        
        'resizableColumns'=>true,
        
        'toolbar' =>  [
            ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success', 'title' => 'Registrar Validación']) . ' '.
                    
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Refrescar Consulta'])
            ],
            '{export}',
            '{toggleData}'
        ],

        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => false,
        
        'panel' => [
            'heading'=>'<h1 class="h1-head"><i class="glyphicon glyphicon-cog"> </i> '.Html::encode($this->title).'</h1>',
            'type' => GridView::TYPE_PRIMARY,
            'footer'=>false
        ],
    
    ]);

    ?>

    
</div>

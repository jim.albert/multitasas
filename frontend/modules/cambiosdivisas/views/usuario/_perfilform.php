<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use backend\modules\cambiodivisas\models\Tipodedocumento;
use backend\modules\cambiodivisas\models\Estatus;
use backend\modules\cambiodivisas\models\Paises;
/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuario-form">

    <?php $form = ActiveForm::begin(); ?>
<div class="bg bg-form col-md-12 col-sm-12">
    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'nombres')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'id_pais')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Paises::find()->orderBy('descripcion ASC')->all(), 'idpaises','descripcion'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>

    <div class="col-md-4 col-sm-4">
        
        <?= $form->field($model, 'id_tipo_documento')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Tipodedocumento::find()->orderBy('descripcion ASC')->all(), 'idbtipodocumento','codigo'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'nro_documento')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4 col-sm-4">
        
        <?= $form->field($model, 'fecha_nacimiento')->widget(DatePicker::classname(),[
            'name' => 'fecha', 
            'value' => date('Y-M-D'),
            'options' => ['placeholder' => 'Seleccionar Fecha ...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
            ]
        ]); ?>
    </div>

    <div class="col-md-6 col-sm-6">
        <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
    </div>
    <div class="col-md-6 col-sm-6">
        
        <?= $form->field($model, 'pep')->widget(Select2::classname(), [
            'data' => ['1'=>'SI','0'=>'NO'],
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        <?= $form->field($model, 'estatus')->hiddenInput()->label(false) ?>
    </div>    
</div>
    <div class="form-group col-md-12 col-sm-12">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancelar', ['/'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


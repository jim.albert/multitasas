<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Usuario */

$this->title = Yii::t('app', 'Perfil de Usuario');

?>
<div class="usuario-create">

     <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <span class=" text-center bg bg-info text text-info">Completa los datos de tu perfil. Se le informa que estos datos no podrán ser modificados, en caso de requerir algún cambio contactar a nueestro equipo de soporte.</span>

    <?= $this->render('_perfilform', [
        'model' => $model,
    ]) ?>


</div>

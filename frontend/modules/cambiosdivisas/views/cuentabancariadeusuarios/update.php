<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Cuentabancariadeusuarios */

$this->title = 'Cuenta Bancaria: ' .$model->alias.' - '.$model->moneda->monedas;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cuenta Bancaria'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->alias, 'url' => ['view', 'id' => $model->idcuenta_bancaria]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="cuentabancariadeusuarios-update">

     <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1> 


    <?php 

    if ($model->tipo=="Propia") {
    	echo $this->render('_form', [ 'model' => $model, ]);
    } else {
    	echo $this->render('_formbeneficiario', [ 'model' => $model, ]);
    }
    

     ?>



</div>

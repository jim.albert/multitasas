<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use frontend\modules\cambiosdivisas\models\Estatus;
use frontend\modules\cambiosdivisas\models\Tipodemoneda;
use frontend\modules\cambiosdivisas\models\Bancos;
use frontend\modules\cambiosdivisas\models\Tipodecuenta;

use frontend\modules\cambiosdivisas\models\Usuario;
$paisuser=Usuario::findOne(Yii::$app->user->identity->getId());

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Cuentabancariadeusuarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cuentabancariadeusuarios-form">

    <?php $form = ActiveForm::begin(); ?>
<div class="bg bg-form col-md-12 col-sm-12">
    <div class="col-md-4 col-sm-4">
        
        <?= $form->field($model, 'id_tipo_cuenta')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Tipodecuenta::find()->orderBy('codigo ASC')->all(), 'idtipocuenta','codigo'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-md-4 col-sm-4">
       
        <?= $form->field($model, 'id_banco')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Bancos::find()->joinWith('pais')->where(['paises.remesas'=>1])->orderBy('codigo ASC')->all(), 'idbancos','codigo','codigopais'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-md-4 col-sm-4">
        

        <?= $form->field($model, 'id_moneda')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Tipodemoneda::find()->orderBy('descripcion ASC')->all(), 'idbtipomoneda','monedas'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-md-6 col-sm-6">
        <?= $form->field($model, 'nro_cuenta')->textInput(['maxlength' => 17]) ?>
    </div>
    <div class="col-md-6 col-sm-6">
        <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'nro_interbancario')->hiddenInput()->label(false) ?>
    </div>

    

    <div class="form-group col-md-12 col-sm-12">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancelar', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-primary', 'title' => 'Cancelar']) ?>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\CuentabancariadeusuariosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cuentabancariadeusuarios-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idcuenta_bancaria') ?>

    <?= $form->field($model, 'id_tipo_cuenta') ?>

    <?= $form->field($model, 'id_banco') ?>

    <?= $form->field($model, 'id_moneda') ?>

    <?= $form->field($model, 'nro_cuenta') ?>

    <?php // echo $form->field($model, 'nro_interbancario') ?>

    <?php // echo $form->field($model, 'alias') ?>

    <?php // echo $form->field($model, 'id_usuario') ?>

    <?php // echo $form->field($model, 'estatus') ?>

    <?php // echo $form->field($model, 'fecha_registro') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

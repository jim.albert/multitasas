<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\widgets\SwitchInput;
use frontend\modules\cambiosdivisas\models\Estatus;
use frontend\modules\cambiosdivisas\models\Tipodemoneda;
use frontend\modules\cambiosdivisas\models\Bancos;
use frontend\modules\cambiosdivisas\models\Tipodecuenta;

use frontend\modules\cambiosdivisas\models\Usuario;
$paisuser=Usuario::findOne(Yii::$app->user->identity->getId());

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Cuentabancariadeusuarios */
/* @var $form yii\widgets\ActiveForm */ 

$jsc = <<< JS


        

        function activa_pagomovil(){

           // if ($("#Cod_refencia").attr("checked")){

            var valor=$('#cuentabancariadeusuarios-pago_movil').bootstrapSwitch('state');
            //alert(valor);

            if (valor==true){

                $("#cuentabancariadeusuarios-nro_telefono").prop("readonly", false);
                
            }else{
                
                
                $("#cuentabancariadeusuarios-nro_telefono").prop("readonly", true);
            }

            
        }

JS;

$this->registerJs($jsc, $this::POS_END);
?>

<div class="cuentabancariadeusuarios-form">

    <?php $form = ActiveForm::begin(); ?>
<div class="bg bg-form col-md-12 col-sm-12">
    <div class="col-md-4 col-sm-4">
        
        <?= $form->field($model, 'tipo_documento')->widget(Select2::classname(), [
            'data' => ['Cédula de Identidad'=>'Cédula de Identidad', 'Extrangero'=>'Extrangero', 'Pasaporte'=>'Pasaporte','RIF'=>'RIF'],
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'doc_beneficiario')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'nombre_beneficiario')->textInput(['maxlength' => true]) ?>
    </div>

    

    <div class="col-md-4 col-sm-4">
        
        <?= $form->field($model, 'id_tipo_cuenta')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Tipodecuenta::find()->orderBy('codigo ASC')->all(), 'idtipocuenta','codigo'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-md-4 col-sm-4">
       
        <?= $form->field($model, 'id_banco')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Bancos::find()->joinWith('pais')->where(['paises.remesas'=>1])->orderBy('codigo ASC')->all(), 'idbancos','codigo','codigopais'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-md-4 col-sm-4">
        

        <?= $form->field($model, 'id_moneda')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Tipodemoneda::find()->orderBy('descripcion ASC')->all(), 'idbtipomoneda','monedas'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'nro_cuenta')->textInput(['maxlength' => 20]) ?>
    </div>
    
    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
    </div>


    <div class="col-md-2 col-sm-2"style="padding-right: 2px">
        <?= $form->field($model, 'pago_movil')->widget(SwitchInput::classname(),[
                        'id' => 'pago_movil',
                        'name' => 'pago_movil',
                        'pluginOptions' => [
                            
                            'onColor' => 'success',
                            'offColor' => 'danger',
                            'onText'=>'Si',
                            'offText'=>'No',

                        ],
                        'options' => ['onchange' => 'activa_pagomovil()'], ]) ?>
        
    </div>    
    <div class="col-md-2 col-sm-2" style="padding-left: 2px">
        
        <?= $form->field($model, 'nro_telefono')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
    </div> 
</div>


    <div class="form-group col-md-12 col-sm-12">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancelar', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-primary', 'title' => 'Cancelar']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

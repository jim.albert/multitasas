<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Cuentabancariadeusuarios */

$this->title = 'Cuenta Bancaria: ' .$model->alias.' - '.$model->moneda->monedas;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cuenta Bancaria'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cuentabancariadeusuarios-view">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->idcuenta_bancaria], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idcuenta_bancaria], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Cancelar', ['index'], ['class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idcuenta_bancaria',
            [
                'attribute' => 'tipoCuenta.codigo',
                //'label'=>'Estatus',
                'label' => ' Tipo de Cuenta ',                

            ],
            
            [
                'attribute' => 'banco.codigo',
                //'label'=>'Estatus',
                'label' => ' Tipo de Moneda ',                

            ],
            
            [
                'attribute' => 'moneda.monedas',
                //'label'=>'Estatus',
                'label' => ' Tipo de Moneda ',                

            ],
            'nro_cuenta',
            'nro_interbancario',
            'alias',
            //'id_usuario',
            [
                'attribute' => 'estatus0.descripcion',
                //'label'=>'Estatus',
                'label' => ' Estatus ',                

            ],
            'fecha_registro',
        ],
    ]) ?>

</div>

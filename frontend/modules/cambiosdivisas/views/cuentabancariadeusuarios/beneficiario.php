<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cambiodivisas\models\Cuentabancariadeusuarios */

$this->title = Yii::t('app', 'Registrar Cuenta Bancaria de Beneficiario');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Registrar Cuenta Bancaria'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cuentabancariadeusuarios-create">

    <h1 class=" text-center bg bg-primary"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formbeneficiario', [
        'model' => $model,
    ]) ?>

</div>

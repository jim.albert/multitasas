<?php

//use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use frontend\modules\cambiosdivisas\models\Estatus;
use frontend\modules\cambiosdivisas\models\Tipodecuenta;
use frontend\modules\cambiosdivisas\models\Bancos;
use frontend\modules\cambiosdivisas\models\Tipodemoneda;
use backend\modules\cambiodivisas\models\Paises;

use frontend\modules\cambiosdivisas\models\Usuario;

$paisuser=Usuario::findOne(Yii::$app->user->identity->getId());

$modules_operation_pais=Paises::findOne($paisuser->id_pais);
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\cambiodivisas\models\EstatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Cuentas Bancarias');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cuentasbancariadesistemas-index">

    

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php 
    

    $gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],

    
            //'id_tipo_cuenta',
            // 'idcuenta_bancaria',
            // 'id_tipo_cuenta',
            // 'id_banco',
            // 'id_moneda',
            // 'nro_cuenta',
            //'nro_interbancario',
            //'alias',
            //'id_usuario',
            //'estatus',
            //'fecha_registro',
            
            //'id_banco',
            [
                //'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'id_banco', 
                'value' => 'banco.codigo', 
                //'label'=>'Estatus',
                'vAlign' => 'middle', 
                
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Bancos::find()->where(['id_pais'=>$paisuser->id_pais])->orderBy('codigo ASC')->all(), 'idbancos','codigo'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Opciones'],
                'format' => 'raw'
                
            ],
            //'id_moneda',
            [
                'attribute' => 'nro_cuenta',
                'vAlign' => 'middle',
            ],
            [
                //'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'id_moneda', 
                'value' => 'moneda.monedas', 
                //'label'=>'Estatus',
                'vAlign' => 'middle',
                
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Tipodemoneda::find()->orderBy('descripcion ASC')->all(), 'idbtipomoneda','monedas'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Opciones'],
                'format' => 'raw'
                
            ],
            
            
             
             [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'alias',
                'vAlign' => 'middle',
            ],
            
            [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'tipo',
                'vAlign' => 'middle',
            ],
            [
                //'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'estatus', 
                'value' => 'estatus0.descripcion', 
                //'label'=>'Estatus',
                'vAlign' => 'middle',
                
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Estatus::find()->where(['tabla'=>'Cuentas Bancarias de Usuarios'])->orderBy('descripcion ASC')->all(), 'idstatus','descripcion'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Opciones'],
                'format' => 'raw'
                
            ],
            
    ['class' => 'kartik\grid\ActionColumn', ]
            //['class' => 'kartik\grid\CheckboxColumn']
    ];

    $boton1="";
    $boton2="";

    if ($modules_operation_pais->cambios==1){ 
            $boton1= Html::a('<i class="glyphicon glyphicon-plus"></i> Cambio de Divisas', ['create'], ['class' => 'btn btn-success', 'title' => 'Registrar Cuenta Bancaria']);
        }

        if ($modules_operation_pais->remesas==1){ 
             $boton2= Html::a('<i class="glyphicon glyphicon-plus"></i> Envío de dinero', ['beneficiario'], ['class' => 'btn btn-primary', 'title' => 'Registrar Cuenta Bancaria']);
        }


    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,

        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        
        'resizableColumns'=>true,
        
        'toolbar' =>  [
            ['content'=>
               $boton1 . ' '. $boton2 . ' '.
                    
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Refrescar Consulta'])
            ],
            //'{export}',
            '{toggleData}'
        ],

        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => false, 
        
        'panel' => [
            'heading'=>'<h1 class="h1-head"><i class="glyphicon glyphicon-cog"> </i> '.Html::encode($this->title).'</h1>',
            'type' => GridView::TYPE_PRIMARY,
            'footer'=>true
        ],
    
    ]);

    ?>

    
</div>


<?php

namespace frontend\modules\cambiosdivisas\controllers;

use Yii;
use frontend\modules\cambiosdivisas\models\Cuentabancariadeusuarios;
use frontend\modules\cambiosdivisas\models\CuentabancariadeusuariosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CuentabancariadeusuariosController implements the CRUD actions for Cuentabancariadeusuarios model.
 */
class CuentabancariadeusuariosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cuentabancariadeusuarios models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {

            $this->layout = '@frontend/views/layouts/main';
            $searchModel = new CuentabancariadeusuariosSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->setPagination(['pageSize' => 10]);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,

        ]);


             
        
        }
    }

    /**
     * Displays a single Cuentabancariadeusuarios model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {

            $this->layout = '@frontend/views/layouts/informativo';
            return $this->render('view', [
            'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Cuentabancariadeusuarios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {

            $this->layout = '@frontend/views/layouts/informativo';
            $model = new Cuentabancariadeusuarios();

            $model->id_usuario=Yii::$app->user->identity->getId();
            $model->estatus='19';
            $model->fecha_registro=date('Y-m-d');
            $model->tipo='Propia';
            $model->tipo_documento='-';
            $model->doc_beneficiario='0';
            $model->pago_movil='0';
            $model->nro_telefono='0';
            $model->nombre_beneficiario='-';
            $model->nro_interbancario='00000000000000000000';

            if ($model->load(Yii::$app->request->post())) {
                $model->id_usuario=Yii::$app->user->identity->getId();
            $model->estatus='19';
            $model->fecha_registro=date('Y-m-d');
                
                if ( $model->save() ) {
                        Yii::$app->session->setFlash('success','Cuenta bancaria '.$model->alias.' registrada correctamente');
                        $this->layout = '@frontend/views/layouts/operacion';
                        return $this->redirect(['index']);
                    } else {

                        $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                        Yii::$app->session->setFlash('error',$errores);
                        return $this->render('create', [
                    'model' => $model,
                ]);
                    }

                
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            } 
        }
    }

    
    /**
     * Creates a new Cuentabancariadeusuarios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionBeneficiario()
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {

            $this->layout = '@frontend/views/layouts/informativo';
            $model = new Cuentabancariadeusuarios();

            $model->id_usuario=Yii::$app->user->identity->getId();
            $model->estatus='19';
            $model->fecha_registro=date('Y-m-d');
            $model->tipo='Beneficiario';
            $model->nro_interbancario='00000000000000000000';
            $model->nro_telefono='0';

            if ($model->load(Yii::$app->request->post())) {
                $model->id_usuario=Yii::$app->user->identity->getId();
            $model->estatus='19';
            $model->fecha_registro=date('Y-m-d');
                
                if ( $model->save() ) {
                        Yii::$app->session->setFlash('success','Cuenta bancaria '.$model->alias.' registrada correctamente');
                        $this->layout = '@frontend/views/layouts/operacion';
                        return $this->redirect(['index']);
                    } else {

                        $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                        Yii::$app->session->setFlash('error',$errores);
                        return $this->render('beneficiario', [
                        'model' => $model,
                    ]);
                    }

                
            } else {
                return $this->render('beneficiario', [
                    'model' => $model,
                ]);
            } 
        }
    }

    /**
     * Updates an existing Cuentabancariadeusuarios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {

            $this->layout = '@frontend/views/layouts/informativo';
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post())) {
                
                if ( $model->save() ) {
                        Yii::$app->session->setFlash('success','Cuenta bancaria '.$model->alias.' Actualizada correctamente');
                        return $this->redirect(['index']);
                    } else {

                        $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                        Yii::$app->session->setFlash('error',$errores);
                        return $this->render('update', [
                    'model' => $model,
                ]);
                    }

                
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            } 
        }
    }

    /**
     * Deletes an existing Cuentabancariadeusuarios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {

            $this->layout = '@frontend/views/layouts/informativo';
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Cuentabancariadeusuarios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cuentabancariadeusuarios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cuentabancariadeusuarios::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionListcuentas($id)
        {
            $total = Cuentabancariadeusuarios::find()->where(['id_banco' => $id, 'id_usuario' => Yii::$app->user->identity->getId() ])->count();
            
            $municipios = Cuentabancariadeusuarios::find()->where(['id_banco' => $id, 'id_usuario' => Yii::$app->user->identity->getId()
                    ])->all();
            

            if ($total > 0) {
               // echo 'prompt'    => 'Selecione una opción ...';
                echo "<option value=''>Selecione una opción ...</option>";
                foreach($municipios as $key => $value) {
                    echo "<option value='". $value->idcuenta_bancaria ."'>$value->cuentanro</option>";
                }
            } else {
                echo "<option></option>";
            }
        }
}

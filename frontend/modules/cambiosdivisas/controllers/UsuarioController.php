<?php

namespace frontend\modules\cambiosdivisas\controllers;

use Yii;
use frontend\modules\cambiosdivisas\models\Usuario;
use frontend\modules\cambiosdivisas\models\UsuarioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\modules\cambiosdivisas\models\Estatus;
use backend\modules\cambiodivisas\models\OperacionempresaSearch;
/**
 * UsuarioController implements the CRUD actions for Usuario model.
 */
class UsuarioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Usuario models.
     * @return mixed
     */
    /*public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {

            $this->layout = '@frontend/views/layouts/informativo';

            $searchModel = new UsuarioSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }*/

    /**
     * Displays a single Usuario model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
/*    public function actionView($id)
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {

            $this->layout = '@frontend/views/layouts/informativo';
            return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
        }
    }
*/
    /**
     * Creates a new Usuario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
 /*   public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {

            $this->layout = '@frontend/views/layouts/informativo';

            $model = new Usuario();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('create', [
                'model' => $model,
        ]);
        }
    }
*/
    /**
     * Creates a new Usuario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionPerfil()
    {

        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {

            $this->layout = '@frontend/views/layouts/informativo';

            $modeluser = Usuario::findOne(Yii::$app->user->identity->getId());
            //print_r($modeluser);

            if ($modeluser!== null) {

                // if ($modeluser->load(Yii::$app->request->post()) && $modeluser->save()) {
                //     return $this->redirect(['view', 'id' => $modeluser->id]);
                // }

                //     return $this->render('update', [
                //         'model' => $modeluser,
                // ]);

                if ($modeluser->load(Yii::$app->request->post())) {
                    
                    if ( $modeluser->save() ) {
                            Yii::$app->session->setFlash('success','Actualización del Perfil '.$modeluser->nombres.' - '.$modeluser->apellidos.' se realizó correctamente');
                            return $this->redirect(['perfil']);
                        } else {

                            $errores = "";

                            foreach ( $modeluser->getErrors() as $key => $value ) {
                                foreach ( $value as $row => $field ) {
                                    $errores .= $field . "<br>";
                                }
                            }

                            Yii::$app->session->setFlash('error',$errores);
                            return $this->render('perfil', [
                            'model' => $modeluser,
                        ]);
                        }

                    
                } else {
                    return $this->render('perfil', [
                        'model' => $modeluser,
                    ]);
                } 

            }else{

                $model = new Usuario();

                $model->email=Yii::$app->user->identity->email;
                $model->id=Yii::$app->user->identity->getId();
                //$modelstatus=Estatus::findOne('18');
                //$model->estatus=$modelstatus->descripcion;
                $model->estatus='16';

                if ($model->load(Yii::$app->request->post())) {
                    $model->estatus='16';

                    if ( $model->save() ) {
                            Yii::$app->session->setFlash('success','El Registro del Perfil '.$model->nombres.' - '.$model->apellidos.' se realizó correctamente');
                            return $this->redirect(['perfil']);
                        } else {

                            $errores = "";

                            foreach ( $model->getErrors() as $key => $value ) {
                                foreach ( $value as $row => $field ) {
                                    $errores .= $field . "<br>";
                                }
                            }

                            Yii::$app->session->setFlash('error',$errores);
                            return $this->render('perfil', [
                        'model' => $model,
                    ]);
                        }

                    
                } else {
                    return $this->render('perfil', [
                        'model' => $model,
                    ]);
                } 

                /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }

                return $this->render('perfil', [
                    'model' => $model,
                ]);*/
            }
        }
    }
public function actionPerfilnuevo()
    {

        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {

            $this->layout = '@frontend/views/layouts/register';

            //$modeluser = Usuario::findOne(Yii::$app->user->identity->getId());
            //print_r($modeluser);

                $model = new Usuario();

                $model->email=Yii::$app->user->identity->email;
                $model->id=Yii::$app->user->identity->getId();
                //$modelstatus=Estatus::findOne('18');
                //$model->estatus=$modelstatus->descripcion;
                $model->estatus='16';

                if ($model->load(Yii::$app->request->post())) {
                    $model->estatus='16';

                    if ( $model->save() ) {
                            Yii::$app->session->setFlash('success','El Registro del Perfil '.$model->nombres.' - '.$model->apellidos.' se realizó correctamente');
                            return $this->redirect(['/']);
                        } else {

                            $errores = "";

                            foreach ( $model->getErrors() as $key => $value ) {
                                foreach ( $value as $row => $field ) {
                                    $errores .= $field . "<br>";
                                }
                            }

                            Yii::$app->session->setFlash('error',$errores);
                            return $this->render('perfilnew', [
                                'model' => $model,
                            ]);
                        }

                    
                } else {
                    return $this->render('perfilnew', [
                        'model' => $model,
                    ]);
                } 

                /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }

                return $this->render('perfil', [
                    'model' => $model,
                ]);*/
            
        }
    }
    
    /**
     * Deletes an existing Usuario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed 
     * @throws NotFoundHttpException if the model cannot be found
     */
/*    public function actionDelete($id)
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {

            $this->layout = '@frontend/views/layouts/informativo';
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }
    }
*/

    /**
     * If search is successful, the browser will be redirected to the 'csv' file.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPrintreportgeneral()
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            //$model = $this->findModel($id);
            $this->layout = '@frontend/views/layouts/informativo';

            $model = new OperacionempresaSearch();
                
            //$filename=$model->fecha_apertura.'.csv';
            //$session = Yii::$app->session;
                
                return $this->render('reporteGeneral', [
                    'model' => $model,
                    
                ]);
         
        }
    }

    /** 
     * If search is successful, the browser will be redirected to the 'csv' file.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPrintreportgeneralpdf($desde, $hasta)
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {

           // $this->layout = '@frontend/views/layouts/informativo';
            //$model = $this->findModel($id);
            //$model = new OperacionempresaSearch();
                
            //$filename=$model->fecha_apertura.'.csv'; 
            //$session = Yii::$app->session;
               
                return $this->render('generalpdf', [
                    'desde' => $desde, 'hasta' => $hasta,
                    
                ]);
        
        }
    }
    /**
     * If search is successful, the browser will be redirected to the 'csv' file.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPrintreportgeneralxls($desde, $hasta)
    {
        if (Yii::$app->user->isGuest) {            
            return $this->redirect(['/user/security/login']);
        }else {
            //$model = $this->findModel($id);
            //$model = new OperacionempresaSearch();
                
            //$filename=$model->fecha_apertura.'.csv';
            //$session = Yii::$app->session;
            $this->layout = '@backend/views/layouts/xlsfile';
               
                return $this->render('generalxls', [
                    'desde' => $desde, 'hasta' => $hasta,
                    
                ]);
        
        }
    }
    /**
     * Finds the Usuario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usuario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Usuario::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

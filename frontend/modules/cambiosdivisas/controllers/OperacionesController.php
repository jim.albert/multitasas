<?php

namespace frontend\modules\cambiosdivisas\controllers;

use Yii;
use frontend\modules\cambiosdivisas\models\Operaciones;
use frontend\modules\cambiosdivisas\models\OperacionesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\modules\cambiosdivisas\models\Tipodecambio;
use frontend\modules\cambiosdivisas\models\Tipodemoneda;
use frontend\modules\cambiosdivisas\models\Cuentabancariadeusuarios;
use frontend\modules\cambiosdivisas\models\Operacioncomprobantes;
use backend\modules\cambiodivisas\models\Usuarioperfil;
use backend\modules\cambiodivisas\models\Operacionesusuarios;

use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * OperacionesController implements the CRUD actions for Operaciones model.
 */
class OperacionesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Operaciones models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {

            $searchModel = new OperacionesSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->setPagination(['pageSize' => 10]);
            $this->layout = '@frontend/views/layouts/informativo';
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Operaciones model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*public function actionView($id)
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {
            $this->layout = '@frontend/views/layouts/informativo';
            return $this->render('view', [
            'model' => $this->findModel($id),
            ]);
        }
    }*/

    /**
     * Displays a single Operaciones model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDetalle($id)
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {
            $this->layout = '@frontend/views/layouts/informativo';
            return $this->render('detalle', [
            'model' => $this->findModel($id),
            ]);
        }
    }
    

    /**
     * Creates a new Operaciones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
   /* public function actionCreate()
    {
        

        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {

            $model = new Operaciones();

            $tipocambio=Tipodecambio::find()->where(['estatus' => '9'])->orderBy('codigo ASC')->one();
            $model->id_tipo_cambio=$tipocambio->idcambio;

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->idoperacion]);
            }

            return $this->render('create', [
                'model' => $model,
                'tipocambio' => $tipocambio,
            ]);
        }
    }*/

    /**
     * Creates a new Operaciones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRegistro()
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {
            $model = new Operaciones();
            $this->layout = '@frontend/views/layouts/operacion';
            $tipocambio=Tipodecambio::find()->where(['estatus' => '9','operacion'=>'CAMBIO'])->orderBy('codigo ASC')->one();
            $model->id_tipo_cambio=$tipocambio->idcambio;
            $model->tipo='CAMBIO';


            if ($model->load(Yii::$app->request->post())) {
                
                
                $model->estatus='22';
                $model->fecha_registro=date('Y-m-d h:m:s');
                $model->id_tipo_moneda_recibe=$this->actionListmonedascontraid($model->id_tipo_moneda_envia);
                
                $model->codigo=Yii::$app->security->generateRandomString(12);

                if ($model->id_cuenta_bancaria_sistema=="") {

                    $model->id_cuenta_bancaria_sistema="1";
                }
                if ($model->cod_referencia=="") {

                   $model->cod_referencia="-";
                }
                

                
                if ( $model->save() ) {
                        Yii::$app->session->setFlash('success','Su Operación se ha registrado con el Código <strong>'.$model->codigo.'</strong> ');
                        $this->layout = '@frontend/views/layouts/operacion';
                        return $this->redirect(['index']);
                    } else {

                        $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                        Yii::$app->session->setFlash('error',$errores);
                        return $this->render('registro', [
                            'model' => $model,
                            'tipocambio' => $tipocambio,
                        ]);
                    }

                
            } else {
                return $this->render('registro', [
                'model' => $model,
                'tipocambio' => $tipocambio,
            ]);
            }


            // if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //     return $this->redirect(['view', 'id' => $model->idoperacion, ]);
            // }

            
        }
    }

    /**
     * Creates a new Operaciones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRemesas()
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {
            $model = new Operaciones();

            $modelcomprobantes = new Operacioncomprobantes();
            $Usuarioperfil=Usuarioperfil::find()->where(['id_usuario'=>Yii::$app->user->identity->getId()])->one();


            $this->layout = '@frontend/views/layouts/remesas';
            $tipocambio=Tipodecambio::find()->where(['estatus' => '9', 'operacion'=>'REMESA'])->orderBy('codigo ASC')->one();
            $model->id_tipo_cambio=$tipocambio->idcambio;
            $model->id_tipo_moneda_recibe=4;
            $model->tipo='REMESA';
            $model->id_tipo_moneda_envia=2;
            $model->id_cuenta_bancaria_sistema=1;

            if ($model->load(Yii::$app->request->post()) && $modelcomprobantes->load(Yii::$app->request->post())) {
                
                
                $model->estatus='22';
                $model->fecha_registro=date('Y-m-d h:m:s');
                //$model->id_tipo_moneda_recibe=4;
                $model->codigo=Yii::$app->security->generateRandomString(12);

                

                if ($model->id_cuenta_bancaria_sistema=="") {
                    $model->id_cuenta_bancaria_sistema="1";
                }
                if ($model->cod_referencia=="") {
                   $model->cod_referencia="-";
                }
                
                /**usuario agente*/
                if ($Usuarioperfil) {
                  # code...
                
                  if ($Usuarioperfil->perfil->agentes==1) {
                      
                      $model->com_agentes=1; 
                  } else {
                      $model->com_agentes=0;
                  }
                }else{
                  $model->com_agentes=0;
                }

                if ( $model->save() ) {

                   if ($Usuarioperfil) {
                      $model->codigo='ED_'.date('dmy').'_'.$model->idoperacion;
                      $model->save();

                      if ($Usuarioperfil->perfil->agentes==1) {
                          $Operacionesusuarios = new Operacionesusuarios();
                          $Operacionesusuarios->tipo_comision='Agente';
                          $Operacionesusuarios->id_operacion=$model->idoperacion;
                          $Operacionesusuarios->id_usuario=Yii::$app->user->identity->getId();
                          $Operacionesusuarios->save();
                          
                      } 
                    }


                        $modelcomprobantes->id_operacion = $model->idoperacion;
                        $modelcomprobantes->orden=1;

                        $image = UploadedFile::getInstance($modelcomprobantes, 'imagen');
                
                       if (!is_null($image)) {
                         
                                
                             $modelcomprobantes->imagen = $image->name;
                             $basenameAndExtension = explode('.', $image->name);
                             $ext = end($basenameAndExtension);
                             //$model->logo = $model->image;//$image->name;
                              // generate a unique file name to prevent duplicate filenames
                             // $model->documentopath = Yii::$app->security->generateRandomString().".{$ext}";
                              // the path to save file, you can set an uploadPath
                              // in Yii::$app->params (as used in example below)                  
                             // Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/img/';

                              Yii::$app->params['uploadPath'] = Yii::getAlias('@common') . '/uploads/img/';


                              $path = Yii::$app->params['uploadPath'].'comprobantes/'.$model->idoperacion.'/';// . $estudiante->documento_identidad.'/';
                              $path2 = $path .$modelcomprobantes->imagen;

                              //$modelcomprobantes->imagen = Yii::getAlias('@frontend'). '/web/img/comprobantes/'.$model->idoperacion.'/'.$modelcomprobantes->imagen;
                               
                              if (FileHelper::createDirectory($path,$mode=0775,$recursive=true)){
                                $image->saveAs($path2);
                              }     
                              $modelcomprobantes->save();
                        }else{
                            $modelcomprobantes->imagen="logo.png";
                        }


                        $this->mailsend($model->idoperacion, Yii::$app->user->identity->email, 'Nueva Operación de Envío de Dinero - Código: '.$model->codigo);

                        $Usuarionotificaciones=Usuarioperfil::find()->where(['email_notification'=>1])->all();

                        foreach ($Usuarionotificaciones as $key => $Usuarionotificacion) {
                            if ($Usuarionotificacion->perfil->administrador==1) {
                              $this->mailsend($model->idoperacion, $Usuarionotificacion->usuario->email, 'Registro de Operación de Envío de Dinero - Código: '.$model->codigo );
                              
                            }
                        }


                        Yii::$app->session->setFlash('success','Su Operación se ha registrado con el Código <strong>'.$model->codigo.'</strong> ');
                        $this->layout = '@frontend/views/layouts/main';
                        return $this->redirect(['index']);
                    } else {

                        $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                        Yii::$app->session->setFlash('error',$errores);
                        return $this->render('registroremesas', [
                            'model' => $model,
                            'tipocambio' => $tipocambio,
                            'modelcomprobantes'=> $modelcomprobantes,
                        ]);
                    }

                
            } else {
                return $this->render('registroremesas', [
                'model' => $model,
                'tipocambio' => $tipocambio,
                'modelcomprobantes'=> $modelcomprobantes,
            ]);
            }


            // if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //     return $this->redirect(['view', 'id' => $model->idoperacion, ]);
            // }

            
        }
    }
    /**
     * Updates an existing Operaciones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {

            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->idoperacion]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }*/

    /**
     * Creates a new Operaciones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRemesaswebservices()
    {
        
            $model = new Operaciones();

            //$modelcomprobantes = new Operacioncomprobantes();

            $this->layout = '@frontend/views/layouts/mainweb';
            $tipocambio=Tipodecambio::find()->where(['estatus' => '9', 'operacion'=>'REMESA'])->orderBy('codigo ASC')->one();
            $model->id_tipo_cambio=$tipocambio->idcambio;
            $model->id_tipo_moneda_recibe=4;
            $model->id_tipo_moneda_envia=2;
            $model->tipo='REMESA';
            $model->monto_envio=50;

            if ($model->load(Yii::$app->request->post())) {
                
                
                echo "<script>top.window.location = 'https://app.multitasas.com/frontend/web/index.php'</script>";
                die;

            }
                return $this->render('registroremesasweb', [
                'model' => $model,
                'tipocambio' => $tipocambio, 
                //'modelcomprobantes'=> $modelcomprobantes,
            ]);
     
        
    }


    /**
     * Creates a new Operaciones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCambiowebservices()
    {
        
            $model = new Operaciones();

            //$modelcomprobantes = new Operacioncomprobantes();

            $this->layout = '@frontend/views/layouts/mainweb';
            $tipocambio=Tipodecambio::find()->where(['estatus' => '9','operacion'=>'CAMBIO'])->orderBy('codigo ASC')->one();
            $model->id_tipo_cambio=$tipocambio->idcambio;
            $model->tipo='CAMBIO';
            $model->monto_envio=50;

            if ($model->load(Yii::$app->request->post())) {
                
                
                echo "<script>top.window.location = 'https://app.multitasas.com/frontend/web/index.php'</script>";
                die;

            }
                return $this->render('registrocambioweb', [
                'model' => $model,
                'tipocambio' => $tipocambio,
            ]);
        
    }
    /**
     * Deletes an existing Operaciones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
   /* public function actionDelete($id)
    {
        if (Yii::$app->user->isGuest) {

            
            return $this->redirect(['/user/security/login']);
        }else {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }
    }*/

    /**
     * Finds the Operaciones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Operaciones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Operaciones::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }


    public function actionListmonedas($id)
        {
            $Cuentabancariadeusuarios = Cuentabancariadeusuarios::find()->where(['idcuenta_bancaria' => $id])->one();

            $total = Tipodemoneda::find()->where(['idbtipomoneda'=> $Cuentabancariadeusuarios->id_moneda])->count();
            
            $municipios = Tipodemoneda::find()->where(['idbtipomoneda'=> $Cuentabancariadeusuarios->id_moneda])->all();

            /*$total = Tipodemoneda::find()->andWhere(['!=' ,'idbtipomoneda' , $Cuentabancariadeusuarios->id_moneda])->count();
            
            $municipios = Tipodemoneda::find()->andWhere(['!=', 'idbtipomoneda' , $Cuentabancariadeusuarios->id_moneda])->all();*/
            

            if ($total > 0) {
               // echo 'prompt'    => 'Selecione una opción ...';
                echo "<option value=''>Selecione una opción ...</option>";
                foreach($municipios as $key => $value) {
                    echo "<option value='". $value->idbtipomoneda ."'>$value->monedas</option>";
                }
            } else {
                echo "<option></option>";
            }
        }

    public function actionListmonedascontra($id)
        {
            //$Cuentabancariadeusuarios = Cuentabancariadeusuarios::find()->where(['idcuenta_bancaria' => $id])->one();

            $total = Tipodemoneda::find()->Where(['!=' ,'idbtipomoneda', $id])->count();
            
            $municipios = Tipodemoneda::find()->Where(['!=' , 'idbtipomoneda', $id])->one();

            

            if ($total > 0) {
               // echo 'prompt'    => 'Selecione una opción ...';
                return $municipios->monedas;
            }
        }
    public function actionListmonedascontraid($id)
        {
            //$Cuentabancariadeusuarios = Cuentabancariadeusuarios::find()->where(['idcuenta_bancaria' => $id])->one();

            $total = Tipodemoneda::find()->Where(['!=' ,'idbtipomoneda', $id])->count();
            
            $municipios = Tipodemoneda::find()->Where(['!=' , 'idbtipomoneda', $id])->one();

            

            if ($total > 0) {
               // echo 'prompt'    => 'Selecione una opción ...';
                return $municipios->idbtipomoneda;
            }
        }

    public function actionListmonedas_dt($id)
        {
            $Cuentabancariadeusuarios = Cuentabancariadeusuarios::find()->where(['idcuenta_bancaria' => $id])->one();

            $total = Tipodemoneda::find()->where(['idbtipomoneda'=> $Cuentabancariadeusuarios->id_moneda])->count();
            
            $municipios = Tipodemoneda::find()->where(['idbtipomoneda'=> $Cuentabancariadeusuarios->id_moneda])->one();

            /*$total = Tipodemoneda::find()->andWhere(['!=' ,'idbtipomoneda' , $Cuentabancariadeusuarios->id_moneda])->count();
            
            $municipios = Tipodemoneda::find()->andWhere(['!=', 'idbtipomoneda' , $Cuentabancariadeusuarios->id_moneda])->all();*/
            

            /*if ($total > 0) {
               // echo 'prompt'    => 'Selecione una opción ...';
                echo "<option value=''>Selecione una opción ...</option>";
                foreach($municipios as $key => $value) {
                    echo "<option value='". $value->idbtipomoneda ."'>$value->monedas</option>";
                }
            } else {
                echo "<option></option>";
            }*/
            if ($total > 0) {
               // echo 'prompt'    => 'Selecione una opción ...';
                return $municipios->idbtipomoneda;
            }
        }
        public function actionListmonedasdescription($id)
        {
            //$Cuentabancariadeusuarios = Cuentabancariadeusuarios::find()->where(['idcuenta_bancaria' => $id])->one();

            $total = Tipodemoneda::find()->Where(['idbtipomoneda'=> $id])->count();
            
            $municipios = Tipodemoneda::find()->Where(['idbtipomoneda'=> $id])->one();

            

            if ($total > 0) {
               // echo 'prompt'    => 'Selecione una opción ...';
                return $municipios->monedas;
            }
        }

        public function actionListmonedasven($id)
        {
            //$Cuentabancariadeusuarios = Cuentabancariadeusuarios::find()->where(['idcuenta_bancaria' => $id])->one();

            $totalTipodemoneda = Tipodemoneda::find()->Where(['=' ,'idbtipomoneda', $id])->count();
            
            $Tipodemoneda = Tipodemoneda::find()->Where(['=' , 'idbtipomoneda', $id])->one();

            

            if ($totalTipodemoneda > 0) {
               // echo 'prompt'    => 'Selecione una opción ...';
                return $Tipodemoneda->monedas;
              //return $Tipodemoneda;
            }
        }

        public function actionMontocompra($id)
        {
                        
            //$Tipodemoneda = Tipodemoneda::find()->Where(['=' , 'idbtipomoneda', $id])->one();

            $tipocambio=Tipodecambio::find()
            ->joinWith('monedacompra')
            ->where(['tipo_cambio.estatus' => '9', 'operacion'=>'REMESA'])
            ->andwhere(['tipo_moneda.remesas' => '1'])
            ->andwhere(['tipo_moneda.idbtipomoneda' => $id])
            ->one();

            

            if ($tipocambio) {
               // echo 'prompt'    => 'Selecione una opción ...';
                return $tipocambio->venta;
              //return $Tipodemoneda;
            }
        }


        public function Mailsend($id, $to, $subject){
          /*Yii::$app->mailer->compose()
           ->setFrom('jim.alberto@gmail.com')
           ->setTo('support@optimal-websites.com')
           ->setSubject('Email enviado desde Yii2-Swiftmailer')
           ->send();*/
          // $id=80;

           $model=$this->findModel($id);
           Yii::$app->mailer->compose(['html' => '@common/mail/operaciondetalle', 'text' => '@common/mail/operaciondetalle'], [ 'model' => $model,])
             ->setFrom(['info@app.multitasas.com'=>'Equipo Multitasas'])
             ->setTo($to)
             ->setSubject($subject)
             ->send();
        }

}

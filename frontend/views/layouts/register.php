<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;


AppAsset::register($this); 
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <?php
    NavBar::begin([
        'brandLabel' => Html::a(Html::img('@web/img/logo.png', ['alt' => 'MultiTasas.com','title' => 'MultiTasas.com', 'width'=>'70px' ]),['/site/index']),
        'brandUrl' => Yii::$app->homeUrl, 
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    // $menuItems[] = ['label' => '<span class="glyphicon glyphicon-user"> </span> Registrarse', 'url' => ['/user/registration/register']];
    //    $menuItems[] = ['label' => '<span class="glyphicon glyphicon-lock"> </span> Iniciar Sesión', 'url' => ['/user/security/login']];
        $menuItems[] = '<li>'
                        . Html::beginForm(['/user/security/logout'], 'post')
                        . Html::submitButton(
                            '<span class="glyphicon glyphicon-lock"> </span> Cerrar Sesión (' . Yii::$app->user->identity->username . ')', 
                            ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm()
                        . '</li>';
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'encodeLabels' => false,
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>


<?php if (!Yii::$app->user->isGuest) { ?>

    <div class="container">
        <br style="clear:both"/>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>

<?php }else{

    Yii::$app->urlManager->createUrl(['/user/security/login']);

    ?>

        <br style="clear:both"/><br style="clear:both"/>
        <div class="container">
            
            
            <?= $content ?>
        </div>
    
<?php } ?>

</div>
<br style="clear:both"/>

<?php  echo $this->render('_footer', [ ]);  ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
 
use kartik\sidenav\SideNav;
use frontend\modules\cambiosdivisas\models\Tipodecambio;
use frontend\modules\cambiosdivisas\models\Cuentasbancariadesistema;
use frontend\modules\cambiosdivisas\models\Usuario;
use backend\modules\cambiodivisas\models\Paises;
use backend\modules\cambiodivisas\models\Empresa;
$empresa=Empresa::find()->orderBy('id ASC')->one();

$tipocambio=Tipodecambio::find()->where(['estatus' => '9', 'operacion'=>'CAMBIO'])->orderBy('codigo ASC')->one();
$cuentasbancarias=Cuentasbancariadesistema::find()->where(['estatus' => '13'])->andWhere(['!=' ,'idcuenta_bancaria', '1'])->andwhere(['visible' => '1'])->orderBy('alias ASC')->all();

$modeluser = Usuario::findOne(Yii::$app->user->identity->getId());

$modules_operation_pais=Paises::findOne($modeluser->id_pais);

use yii\helpers\Url;
AppAsset::register($this); 
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class=" col-md-12 col-sm-12 " >
    <?php
    
    ?>
    <?= $this->render('_menu', [
        'modeluser' => $modeluser,'modules_operation_pais'=>$modules_operation_pais
    ]) ?>
    </div>
    <br style="clear:both"/><br style="clear:both"/><br style="clear:both"/><br style="clear:both"/><br style="clear:both"/>

    

<?php if (!Yii::$app->user->isGuest) { ?>

    
<div class="left side-menu col-md-3 col-sm-3" > 

    <div class="container">


        <div class="bg bg-information text-center " style="border-radius: .2rem; padding-bottom: 5px; padding-top: 5px;">
            
            <h3 style="font-weight: 900">Horario de atención</h3>
            <p ><?= Html::decode($empresa->horario) ?></p>
            
        </div>
    


        <h3 class="bg bg-information text-center" style="border-radius: .2rem; margin-bottom: 0px; padding-bottom: 5px; padding-top: 5px; font-weight: 900">Tipo de Cambio</h3>
        
        <div class=" text-center "> 
            <div class=" col-md-6 col-sm-6 bg bg-info text-info text-center ">
                <h4>Compra</h4>
                <p><?= number_format($tipocambio->monto , 2, ',', '.'); ?></p>
            </div>
            
        
            <div class="col-md-6 col-sm-6 bg bg-success text-success text-center ">
                <h4>Venta</h4>
                <p><?= number_format($tipocambio->venta , 2, ',', '.'); ?></p>
            </div>
            
        </div>
        <br style="clear:both"/>
        <h3 class="bg bg-information text-center" style="border-radius: .2rem; margin-bottom: 0px; padding-bottom: 5px; padding-top: 5px; font-weight: 900">Cuentas Bancarias</h3>

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              
                <?php  foreach($cuentasbancarias as $key => $value) {              ?>

                    

                    <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id=<?= "heading".$value->idcuenta_bancaria ?>>
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href=<?= "#collapse".$value->idcuenta_bancaria ?> aria-expanded="false" aria-controls=<?= "collapse".$value->idcuenta_bancaria ?>>
                          <i class="glyphicon glyphicon-eye-open"></i> <?= $value->banco->codigo.' - '. $value->tipoCuenta->codigo.' - '.$value->moneda->monedas ?> 
                        </a>
                      </h4>
                    </div>
                    <div id=<?= "collapse".$value->idcuenta_bancaria ?> class="panel-collapse collapse" role="tabpanel" aria-labelledby=<?= "heading".$value->idcuenta_bancaria ?>>
                      <div class="panel-body">
                        Nro. <a style="cursor: pointer;" title="Copiar" onclick=<?= 'copyToClipboard("'.$value->nro_cuenta.'")' ?>><?= $value->nro_cuenta ?></a>
                        
                        <br>
                        CCI: <a style="cursor: pointer;" title="Copiar" onclick=<?= 'copyToClipboard("'.$value->nro_interbancario.'")' ?>><?= $value->nro_interbancario ?></a>
                      </div>
                    </div>
                  </div>

                  <?php
                    
                  }
                  ?>    
                  
              
              
            </div>


    </div>

</div>


    <div class="col-md-9 col-sm-9" style="padding-left: 0px; padding-right: 0px;" >

        <div class="container">

            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>

<?php }else{

    Yii::$app->urlManager->createUrl(['/user/security/login']);

    ?>

        <div class="wrap col-md-12 col-sm-12"  >
         <br style="clear:both"/><br style="clear:both"/>
        <div class="container">
            
            
            <?= $content ?>
        </div>
    </div>
    
<?php } ?>

</div>

<?php  echo $this->render('_footer', [ ]);  ?>


<?php 

$jsc = <<< JS


    function copyToClipboard(e) {
        var tempItem = document.createElement('input');

        tempItem.setAttribute('type','text');
        tempItem.setAttribute('display','none');
        
        
        let content = e;
        if (e instanceof HTMLElement) {
                content = e.innerHTML;
        }
        

        tempItem.setAttribute('value',content);
        document.body.appendChild(tempItem);
        
        tempItem.select();
        document.execCommand('Copy');

        tempItem.parentElement.removeChild(tempItem);
    }

JS;

$this->registerJs($jsc, $this::POS_END);

 ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

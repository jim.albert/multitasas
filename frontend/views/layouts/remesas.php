<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
 
use kartik\sidenav\SideNav;
use frontend\modules\cambiosdivisas\models\Tipodecambio;
use frontend\modules\cambiosdivisas\models\Cuentasbancariadesistema;
use frontend\modules\cambiosdivisas\models\Usuario;
use backend\modules\cambiodivisas\models\Paises;

use backend\modules\cambiodivisas\models\Empresa;
$empresa=Empresa::find()->orderBy('id ASC')->one();

$tipocambio=Tipodecambio::find()->joinWith('monedacompra')->where(['tipo_cambio.estatus' => '9', 'operacion'=>'REMESA'])->andwhere(['tipo_moneda.remesas' => '1'])->all();

$cuentasbancarias=Cuentasbancariadesistema::find()
                ->where(['estatus' => '13'])
                ->andWhere(['!=' ,'idcuenta_bancaria', '1'])
                ->andwhere(['visible' => '1'])
                ->orderBy('alias ASC')->all();

/*$cuentasbancarias=Cuentasbancariadesistema::find()->joinWith('banco')
                ->where(['cuenta_bancaria_sistema.estatus' => '13'])
                ->andWhere(['!=' ,'idcuenta_bancaria', '1'])
                ->andWhere(['=' ,'id_pais', $modeluser->id_pais])
                ->orderBy('alias ASC')->all();
*/

$modeluser = Usuario::findOne(Yii::$app->user->identity->getId());

$modules_operation_pais=Paises::findOne($modeluser->id_pais);

use yii\helpers\Url;
AppAsset::register($this); 
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class=" col-md-12 col-sm-12 " >
    
    <?= $this->render('_menu', [
        'modeluser' => $modeluser,'modules_operation_pais'=>$modules_operation_pais
    ]) ?>
    </div>
    <br style="clear:both"/><br style="clear:both"/><br style="clear:both"/><br style="clear:both"/><br style="clear:both"/>


<?php if (!Yii::$app->user->isGuest) { ?>

    
<div class="left side-menu col-md-3 col-sm-3" > 

    <div class="container">


        <!-- <div class="bg bg-information text-center " style="border-radius: .2rem; padding-bottom: 5px; padding-top: 5px;">
            
            <h3 style="font-weight: 900">Horario de atención</h3>
             <p ><?php //= Html::decode($empresa->horario) ?></p>
            
        </div> -->

        <h3 class="bg bg-primary text-center" style="border-radius: .2rem; margin-bottom: 0px; margin-top: 0px; padding-bottom: 5px; padding-top: 5px; font-weight: 900">Tasa del Día</h3>
        
        
            <div class=" col-md-12 col-sm-12 bg bg-info text-info  ">
                <!-- <h4><?php //= Html::img(Yii::$app->params['imgUrl'].'flags/png/peru.png', ['alt' => 'Soles','title' => 'Soles', 'width'=>'20px' ])  ?> Soles - S/ -->
                <?php if ($tipocambio) {  
                    

                        foreach ($tipocambio as $key => $value) {
                            echo '<h5> '.Html::img(Yii::$app->params['imgUrl'].'flags/png/'.$value->monedacompra->idbtipomoneda.'.png', ['alt' => $value->monedacompra->descripcion ,'title' => $value->monedacompra->descripcion, 'width'=>'20px' ]).' '.$value->monedacompra->descripcion.' - '.$value->monedacompra->codigo . ' :  <b>'. number_format($value->venta , 0, ',', '.').'</b></h5>'; 
                        }
                    }
                ?><!-- </h4> -->
            </div>
            
        <!-- 
            <div class="col-md-6 col-sm-6 bg bg-success text-success text-center ">
                <h4>Dólares - $</h4>
                <p><?php //= $tipocambio->venta; ?></p>
            </div> -->
            
       
        <br style="clear:both"/>
        <h4 class="list-group-item success text-center" style="border-radius: .2rem; margin-bottom: 0px; padding-bottom: 5px; padding-top: 5px; font-weight: 900">Nuestras Cuentas Bancarias</h4>

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              
                <?php  foreach($cuentasbancarias as $key => $value) {              ?>

                    

                    <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id=<?= "heading".$value->idcuenta_bancaria ?>>
                      <h5 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href=<?= "#collapse".$value->idcuenta_bancaria ?> aria-expanded="false" aria-controls=<?= "collapse".$value->idcuenta_bancaria ?>>
                           <?= $value->banco->codigo.' - '. $value->tipoCuenta->codigo.' - '.$value->moneda->monedas ?> <i class="glyphicon glyphicon-triangle-bottom pull-right text-success"></i>
                        </a>
                      </h5>
                    </div>
                    <div id=<?= "collapse".$value->idcuenta_bancaria ?> class="panel-collapse collapse" role="tabpanel" aria-labelledby=<?= "heading".$value->idcuenta_bancaria ?>>
                      <div class="panel-body">
                        Nro. <a style="cursor: pointer;" title="Copiar" onclick=<?= 'copyToClipboard("'.$value->nro_cuenta.'")' ?>><?= $value->nro_cuenta ?> <i class="glyphicon glyphicon-copy pull-right text-success" ></i></a>
                        
                        <br>
                        CCI: <a style="cursor: pointer;" title="Copiar" onclick=<?= 'copyToClipboard("'.$value->nro_interbancario.'")' ?>><?= $value->nro_interbancario ?><i class="glyphicon glyphicon-copy pull-right text-success" ></i></a>
                      </div>
                    </div>
                  </div>

                  <?php
                    
                  }
                  ?>    
                  
              
              
            </div>


    </div>

</div>


    <div class="col-md-9 col-sm-9" style="padding-left: 0px; padding-right: 0px;" >

        <div class="container">

            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>

<?php }else{

    Yii::$app->urlManager->createUrl(['/user/security/login']);

    ?>

        <div class="wrap col-md-12 col-sm-12"  >
         <br style="clear:both"/><br style="clear:both"/>
        <div class="container">
            
            
            <?= $content ?>
        </div>
    </div>
    
<?php } ?>

</div>

<?php  echo $this->render('_footer', [ ]);  ?>


<?php 

$jsc = <<< JS


    function copyToClipboard(e) {
        var tempItem = document.createElement('input');

        tempItem.setAttribute('type','text');
        tempItem.setAttribute('display','none');
        
        
        let content = e;
        if (e instanceof HTMLElement) {
                content = e.innerHTML;
        }
        

        tempItem.setAttribute('value',content);
        document.body.appendChild(tempItem);
        
        tempItem.select();
        document.execCommand('Copy');

        tempItem.parentElement.removeChild(tempItem);
    }

JS;

$this->registerJs($jsc, $this::POS_END);

 ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

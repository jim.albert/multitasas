<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
 
use kartik\sidenav\SideNav;
use frontend\modules\cambiosdivisas\models\Tipodecambio;
use frontend\modules\cambiosdivisas\models\Cuentasbancariadesistema;
use frontend\modules\cambiosdivisas\models\Usuario;
use backend\modules\cambiodivisas\models\Paises;
use backend\modules\cambiodivisas\models\Empresa;
$empresa=Empresa::find()->orderBy('id ASC')->one();

//$tipocambio=Tipodecambio::find()->where(['estatus' => '9', 'operacion'=>'CAMBIO'])->orderBy('codigo ASC')->one();
//$cuentasbancarias=Cuentasbancariadesistema::find()->where(['estatus' => '13'])->andWhere(['!=' ,'idcuenta_bancaria', '1'])->orderBy('alias ASC')->all();

$modeluser = Usuario::findOne(Yii::$app->user->identity->getId());

$modules_operation_pais=Paises::findOne($modeluser->id_pais);

use yii\helpers\Url;
AppAsset::register($this); 
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class=" col-md-12 col-sm-12 " >
   
    <?= $this->render('_menu', [
        'modeluser' => $modeluser,'modules_operation_pais'=>$modules_operation_pais
    ]) ?>
    </div>
    <br style="clear:both"/><br style="clear:both"/><br style="clear:both"/><br style="clear:both"/><br style="clear:both"/>

    

<?php if (!Yii::$app->user->isGuest) { ?>

<!--     
<div class="left side-menu col-md-3 col-sm-3" > 

    <div class="container">


        <div class="bg bg-information text-center " style="border-radius: .2rem; padding-bottom: 5px; padding-top: 5px;">
            
            <h3 style="font-weight: 900">Horario de atención</h3>
            <p ><?php // Html::decode($empresa->horario) ?></p>
            

            
        </div>
    


        
        <br style="clear:both"/>
        


    </div>

</div> -->


    <div class="col-md-12 col-sm-12" style="padding-left: 0px; padding-right: 0px;" >

        <div class="container">

            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>

<?php }else{

    Yii::$app->urlManager->createUrl(['/user/security/login']);

    ?>

        <div class="wrap col-md-12 col-sm-12"  >
         <br style="clear:both"/><br style="clear:both"/>
        <div class="container">
            
            
            <?= $content ?>
        </div>
    </div>
    
<?php } ?>

</div>

<?php  echo $this->render('_footer', [ ]);  ?>

<?php 

$jsc = <<< JS


    function copyToClipboard(e) {
        var tempItem = document.createElement('input');

        tempItem.setAttribute('type','text');
        tempItem.setAttribute('display','none');
        
        
        let content = e;
        if (e instanceof HTMLElement) {
                content = e.innerHTML;
        }
        

        tempItem.setAttribute('value',content);
        document.body.appendChild(tempItem);
        
        tempItem.select();
        document.execCommand('Copy');

        tempItem.parentElement.removeChild(tempItem);
    }

JS;

$this->registerJs($jsc, $this::POS_END);

 ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

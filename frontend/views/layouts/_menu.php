<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use frontend\modules\cambiosdivisas\models\Usuario;
use backend\modules\cambiodivisas\models\Paises;
use backend\modules\cambiodivisas\models\Empresa;
use backend\modules\cambiodivisas\models\Usuarioperfil;
$empresa=Empresa::find()->orderBy('id ASC')->one();
$session = Yii::$app->session;

if (!Yii::$app->user->isGuest) {

    $modeluser = Usuario::findOne(Yii::$app->user->identity->getId());
    $Usuarioperfil=Usuarioperfil::find()->where(['id_usuario'=>Yii::$app->user->identity->getId()])->one();
}
 
NavBar::begin([
        'brandLabel' => Html::a(Html::img('@web/img/'.$empresa->logo, ['alt' => 'MultiTasas.com','title' => 'MultiTasas.com', 'width'=>'70px' ]),['/site/index']),
        'brandUrl' => Yii::$app->homeUrl, 
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    

if ($modeluser!== null) {
    $modules_operation_pais=Paises::findOne($modeluser->id_pais);

    
    if (Yii::$app->user->isGuest) {


        $menuItems[] = ['label' => '<span class="glyphicon glyphicon-user"> </span> Registrarse', 'url' => ['/user/registration/register']];
        $menuItems[] = ['label' => '<span class="glyphicon glyphicon-lock"> </span> Iniciar Sesión', 'url' => ['/user/security/login']];
    } else {

        // $menuItems = [['label' => '<span class="glyphicon glyphicon-cog"> </span> Modules', 'url' => ['#'],
        //     'items' => Yii::$app->getModule('tickets')->dashboardNavItems()
        // ]];
        $itemscom="";

        if ($session['apertura']) {
            if ($modules_operation_pais->cambios==1 ){ 
                $items[]=['label' => 'Cambio de Divisas', 'url' => ['/cambiosdivisas/operaciones/registro']];
            }

            if ($modules_operation_pais->remesas==1 ){ 
                 $items[]=['label' => 'Envíos de Dinero', 'url' => ['/cambiosdivisas/operaciones/remesas']];
            }
        } else {
             $items[]=['label' => Html::decode($empresa->mensaje_apertura), 'url' => '#'];
        }
        
        if ($Usuarioperfil) {
            # code...
            
            if ($Usuarioperfil->perfil->agentes!=0) {
                $itemscom=['label' => 'Reportes de Comisiones', 'url' => ['/cambiosdivisas/usuario/printreportgeneral']];
            }else{
                $itemscom="";
            }
        }

        

        $menuItems = [

        


        ['label' => '<span class="glyphicon glyphicon-cog"> </span> Inicio de operación', 'url' => ['#'],
            'items' => $items
        ],

        ['label' => '<span class="glyphicon glyphicon-usd"> </span> Cuentas Bancarias', 'url' => ['/cambiosdivisas/cuentabancariadeusuarios']],
        ['label' => '<span class="glyphicon glyphicon-stats"> </span> Historial de Operaciones', 'url' => ['/cambiosdivisas/operaciones/index']],

        // ['label' => '<span class="glyphicon glyphicon-cog"> </span> Modules', 'url' => ['#'],
        //     'items' => Yii::$app->getModule('tickets')->dashboardNavItems()
        // ],

        ['label' => '<span class="glyphicon glyphicon-tasks"> </span> Soporte', 'url' => ['/support/ticket/index']],

        ['label' => '<span class="glyphicon glyphicon-user"> </span> Usuario', 'url' => ['#'],
                        'items' => [
                            //
                            ['label' => 'Información Personal', 'url' => ['/cambiosdivisas/usuario/perfil']],
                            ['label' => 'Cuenta', 'url' => ['/user/settings/accountfrontend']],
                            $itemscom,
                            ['label'=> '<li>'
                        . Html::beginForm(['/user/security/logout'], 'post')
                        . Html::submitButton(
                            '<span class="glyphicon glyphicon-lock"> </span> Cerrar Sesión (' . Yii::$app->user->identity->username . ')',
                            ['class' => 'btn btn-link text text-warning logout']
                        )
                        . Html::endForm()
                        . '</li>',]

        ]],
    ];

      
    }

 }else{
    $menuItems[] = ['label' => '<span class="glyphicon glyphicon-user"> </span> Registrarse', 'url' => ['/user/registration/register']];
        $menuItems[] = ['label' => '<span class="glyphicon glyphicon-lock"> </span> Iniciar Sesión', 'url' => ['/user/security/login']];
}
 /*$menuItems[] = '<li>'
                        . Html::beginForm(['/user/security/logout'], 'post')
                        . Html::submitButton(
                            '<span class="glyphicon glyphicon-lock"> </span> Cerrar Sesión (' . Yii::$app->user->identity->username . ')',
                            ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm()
                        . '</li>';*/
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'encodeLabels' => false,
        'items' => $menuItems,
    ]);
    NavBar::end();


   

    ?>
    
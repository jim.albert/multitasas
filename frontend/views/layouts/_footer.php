<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;


    ?>
    <?php if (!Yii::$app->user->isGuest) { ?>
    <div class="soporte">

    	<?= Html::a('<svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" fill="currentColor" class="bi bi-people-fill" viewBox="0 0 16 16">
			  	 <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
			 	 <path fill-rule="evenodd" d="M5.216 14A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216z"/>
			 	 <path d="M4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5z"/>
			</svg>
			<br>Soporte', ['/support/ticket/index'], [ 'title' => 'Soporte de Usuarios']) ?>

    	
	</div>
<?php } ?>
    <footer class="footer">
    <div class="container">
        <p class="pull-left">Copyright &copy; <?= date('Y') ?>. All Rights Reserved.  </p>

        <p class="pull-right">Powered by <?= Html::a(Html::decode(Yii::$app->name, ['alt' => 'MultiTasas.com','title' => 'MultiTasas.com', 'width'=>'50%' ]),'https://multitasas.com/') ?></p>
    </div>
</footer>
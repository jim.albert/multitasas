<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\cambiodivisas\models\Empresa;
$empresa=Empresa::find()->orderBy('id ASC')->one();
$session = Yii::$app->session;
$this->title = $empresa->nombre; 
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>

        
        <p class="lead" style="padding-right: 10%;padding-left: 10%;"><?= Html::decode($empresa->mensaje_front) ?></p>
        
    </div>
    <br>
    <?php if ($modeluser== null) { ?>
	        
	            <div class="col-sm-6">
		          <div class="list-group">
		            <a href="index.php?r=cambiosdivisas/usuario/perfil" class="list-group-item active">
		              <h3 class="list-group-item-heading">Perfil de Usuarios</h3>
		              <p class="list-group-item-text">Para poder comenzar las operaciones de cambiarias debes terminar de registrar tu datos personales.</p>
		              <br>
		              <button type="button" class="btn btn-default">Registrar</button>
		            </a>
		            
		          </div>
		        </div>
			<?php } ?>

			<?php if ($modelbanco== null) { ?>
	        <div class="col-sm-6">
		          <div class="list-group">
		            <a href="index.php?r=cambiosdivisas/cuentabancariadeusuarios/index" class="list-group-item success">
		              <h3 class="list-group-item-heading txt-wite">Cuentas Bancarias</h3>
		              <p class="list-group-item-text">Para poder comenzar las operaciones de cambiarias debes terminar de registrar al menos una cuenta bancaria.</p>
		              <br>
		              <button type="button" class="btn btn-default">Registrar</button>
		            </a>
		            
		          </div>
		        </div>
			<?php } ?>

<?php if (!$session['apertura']){ ?>
            
                <div class="col-sm-12 bg bg-warning text-center">
                 
                    <!-- <a href="../cambiodivisas/operacionempresa/create" class="list-group-item active"> -->
                      <h3 style="font-weight: 900">Horario de atención</h3>
            			<p ><?= Html::decode($empresa->horario) ?></p>
            			<h3 class="text-warning"><?= Html::decode($empresa->mensaje_apertura) ?></h3>
            			
                      <br>
                      <!-- <button type="button" class="btn btn-default">Apertura de Operaciones</button> -->
                    <!-- </a> -->
                    
                  
                </div>
  <?php } else{  ?>
	    
	    	



			<?php if ($modelbanco!= null and $modeluser!= null ) { ?>


			
	        <div class="row"><br style="clear:both"/>

	        	<div class="col-sm-2"> </div>
	        	
	        	<?php if ($modules_operation_pais->cambios==1){  ?> 
		        	<div class="col-sm-4">
		        		<center>
			          <div class="list-group">
			            <a href="index.php?r=cambiosdivisas/operaciones/registro" class="list-group-item success">

			            	<svg xmlns="http://www.w3.org/2000/svg" id="Layer_5" height="50" viewBox="0 0 64 64" width="50" fill= "#FFFFFF"><g><path d="m33 43h-2v2c-1.654 0-3 1.346-3 3s1.346 3 3 3h2c.551 0 1 .449 1 1s-.449 1-1 1h-3v-1h-2v3h3v2h2v-2c1.654 0 3-1.346 3-3s-1.346-3-3-3h-2c-.551 0-1-.449-1-1s.449-1 1-1h3v1h2v-3h-3z"></path><path d="m14 27h2v-2c1.654 0 3-1.346 3-3s-1.346-3-3-3h-2c-.551 0-1-.449-1-1s.449-1 1-1h3v1h2v-3h-3v-2h-2v2c-1.654 0-3 1.346-3 3s1.346 3 3 3h2c.551 0 1 .449 1 1s-.449 1-1 1h-3v-1h-2v3h3z"></path><path d="m15 31c5.67 0 10.35-4.313 10.936-9.831 1.74 1.154 3.824 1.831 6.064 1.831s4.324-.677 6.064-1.831c.586 5.518 5.266 9.831 10.936 9.831 6.065 0 11-4.935 11-11s-4.935-11-11-11c-2.188 0-4.281.637-6.065 1.822-.591-5.513-5.268-9.822-10.935-9.822s-10.344 4.309-10.935 9.822c-1.784-1.185-3.877-1.822-6.065-1.822-6.065 0-11 4.935-11 11s4.935 11 11 11zm43-11c0 4.962-4.038 9-9 9s-9-4.038-9-9c0-.169.015-.335.026-.502 1.538-1.645 2.576-3.757 2.875-6.102 1.668-1.542 3.817-2.396 6.099-2.396 4.962 0 9 4.038 9 9zm-26-17c4.962 0 9 4.038 9 9s-4.038 9-9 9-9-4.038-9-9 4.038-9 9-9zm-17 8c2.282 0 4.431.854 6.099 2.397.299 2.345 1.337 4.456 2.875 6.102.011.166.026.332.026.501 0 4.962-4.038 9-9 9s-9-4.038-9-9 4.038-9 9-9z"></path><path d="m32 19c2.206 0 4-1.794 4-4h-2c0 1.103-.897 2-2 2s-2-.897-2-2h2v-2h-2v-2h2v-2h-2c0-1.103.897-2 2-2s2 .897 2 2h2c0-2.206-1.794-4-4-4s-4 1.794-4 4h-2v2h2v2h-2v2h2c0 2.206 1.794 4 4 4z"></path><path d="m47.72 19h-1.72v2h2v2h-2v2h2v2h2v-2h2v-2h-2v-2h2v-2h-1.72l3.501-4.375-1.562-1.25-3.219 4.024-3.219-4.024-1.562 1.25z"></path><path d="m63 46c0-1.654-1.346-3-3-3h-16c-.993 0-1.868.49-2.414 1.236l-4.478-6.397 3.182-9.547-5.826-1.457-1.588 3.176-1.402-2.104-7.836 1.306 3.242 8.645-4.464 6.378c-.06-.082-.126-.16-.195-.236.48-.532.78-1.229.78-2v-2c0-1.654-1.346-3-3-3h-16.001c-1.654 0-3 1.346-3 3v2c0 .771.301 1.468.78 2-.479.532-.78 1.229-.78 2v2c0 .771.301 1.468.78 2-.479.532-.78 1.229-.78 2v2c0 .771.301 1.468.78 2-.479.532-.78 1.229-.78 2v2c0 1.654 1.346 3 3 3h16c1.654 0 3-1.346 3-3v-.87c2.133 2.369 5.213 3.87 8.645 3.87h.711c3.432 0 6.512-1.501 8.645-3.87v.87c0 1.654 1.346 3 3 3h16c1.654 0 3-1.346 3-3v-2c0-.771-.301-1.468-.78-2 .48-.532.78-1.229.78-2v-2c0-.771-.301-1.468-.78-2 .48-.532.78-1.229.78-2v-2zm-19-1h1v2h2v-2h2v2h2v-2h2v2h2v-2h2v2h2v-2h1c.551 0 1 .449 1 1v2c0 .551-.449 1-1 1h-16c-.081 0-.162.011-.243.017-.166-.815-.42-1.606-.757-2.367v-.65c0-.551.449-1 1-1zm-13.474-14.907 2.598 3.896 2.412-4.824 2.174.543-2.431 7.292h-6.586l-2.33-6.213zm-26.526 18.907c-.551 0-1-.449-1-1v-2c0-.551.449-1 1-1h1v2h2v-2h2v2h2v-2h2v2h2v-2h2v2h2v-2h1c.551 0 1 .449 1 1v.651c-.335.755-.587 1.541-.754 2.349h-.246zm-1-9c0-.551.449-1 1-1h1v2h2v-2h2v2h2v-2h2v2h2v-2h2v2h2v-2h1c.551 0 1 .449 1 1v2c0 .551-.449 1-1 1h-16c-.551 0-1-.449-1-1zm18 20c0 .551-.449 1-1 1h-16c-.551 0-1-.449-1-1v-2c0-.551.449-1 1-1h1v2h2v-2h2v2h2v-2h2v2h2v-2h2v2h2v-2h1c.551 0 1 .449 1 1zm-1-5h-16c-.551 0-1-.449-1-1v-2c0-.551.449-1 1-1h1v2h2v-2h2v2h2v-2h2v2h2v-2h2v2h2v-2h1 .013c-.003.119-.013.236-.013.355 0 1.298.223 2.543.616 3.709-.199-.041-.405-.064-.616-.064zm12.355 6h-.711c-5.317 0-9.644-4.327-9.644-9.645 0-1.989.603-3.901 1.744-5.531l4.777-6.824h6.959l4.777 6.824c1.14 1.63 1.743 3.543 1.743 5.531 0 5.318-4.327 9.645-9.645 9.645zm11.645-9.645c0-.118-.01-.236-.013-.354.004.001.008-.001.013-.001h1v2h2v-2h2v2h2v-2h2v2h2v-2h2v2h2v-2h1c.551 0 1 .449 1 1v2c0 .551-.449 1-1 1h-16c-.211 0-.417.023-.616.065.393-1.167.616-2.412.616-3.71zm17 8.645c0 .551-.449 1-1 1h-16c-.551 0-1-.449-1-1v-2c0-.551.449-1 1-1h1v2h2v-2h2v2h2v-2h2v2h2v-2h2v2h2v-2h1c.551 0 1 .449 1 1z"></path></g></svg>
			            <br>
			              <h3 class="list-group-item-heading txt-wite">Cambio de Divisas</h3>
			              <p class="list-group-item-text">Compra y/o Vender Dólares a través de nuestra plataforma, es Súper FÁCIL.</p></br>
			              <br>
			              <button type="button" class="btn btn-default">Iniciar Operación</button>
			            </a>
			            
			          </div>
			          </center>
			        </div>
			    <?php } ?>

		    
		    	<?php if ($modules_operation_pais->remesas==1){  ?>
			    	<div class="col-sm-4">
			    	<center>
			          <div class="list-group">
			            <a href="index.php?r=cambiosdivisas/operaciones/remesas" class="list-group-item success">
			              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" height="50" width="50" fill= "#FFFFFF">
							<g>
								<g>
									<path d="M85.072,454.931c-1.859-1.861-4.439-2.93-7.069-2.93s-5.21,1.069-7.07,2.93c-1.86,1.861-2.93,4.44-2.93,7.07    s1.069,5.21,2.93,7.069c1.86,1.86,4.44,2.931,7.07,2.931s5.21-1.07,7.069-2.931c1.86-1.859,2.931-4.439,2.931-7.069    S86.933,456.791,85.072,454.931z"></path>
								</g>
							</g>
							<g>
								<g>
									<path d="M469.524,182.938c-1.86-1.861-4.43-2.93-7.07-2.93c-2.63,0-5.21,1.069-7.07,2.93c-1.859,1.86-2.93,4.44-2.93,7.07    s1.07,5.21,2.93,7.069c1.86,1.86,4.44,2.931,7.07,2.931c2.64,0,5.21-1.07,7.07-2.931c1.869-1.859,2.939-4.439,2.939-7.069    S471.393,184.798,469.524,182.938z"></path>
								</g>
							</g>
							<g>
								<g>
									<path d="M509.065,2.929C507.189,1.054,504.645,0,501.992,0L255.998,0.013c-5.522,0-9.999,4.478-9.999,10V38.61l-94.789,25.399    c-5.335,1.43-8.501,6.913-7.071,12.247l49.127,183.342l-42.499,42.499c-5.409-7.898-14.491-13.092-24.764-13.092H30.006    c-16.542,0-29.999,13.458-29.999,29.999v162.996C0.007,498.542,13.464,512,30.006,512h95.998c14.053,0,25.875-9.716,29.115-22.78    l11.89,10.369c9.179,8.004,20.939,12.412,33.118,12.412h301.867c5.522,0,10-4.478,10-10V10    C511.992,7.348,510.94,4.804,509.065,2.929z M136.002,482.001c0,5.513-4.486,10-10,10H30.005c-5.514,0-10-4.486-10-10V319.005    c0-5.514,4.486-10,10-10h37.999V424.2c0,5.522,4.478,10,10,10s10-4.478,10-10V309.005h37.999c5.514,0,10,4.486,10,10V482.001z     M166.045,80.739l79.954-21.424V96.37l-6.702,1.796c-2.563,0.687-4.746,2.362-6.072,4.659s-1.686,5.026-0.999,7.588    c3.843,14.341-4.698,29.134-19.039,32.977c-2.565,0.688-4.752,2.366-6.077,4.668c-1.325,2.301-1.682,5.035-0.989,7.599    l38.979,144.338h-20.07l-10.343-40.464c-0.329-1.288-0.905-2.475-1.676-3.507L166.045,80.739z M245.999,142.229v84.381    l-18.239-67.535C235.379,155.141,241.614,149.255,245.999,142.229z M389.663,492H200.125V492c-7.345,0-14.438-2.658-19.974-7.485    l-24.149-21.061V325.147l43.658-43.658l7.918,30.98c1.132,4.427,5.119,7.523,9.688,7.523l196.604,0.012c7.72,0,14,6.28,14,14    c0,7.72-6.28,14-14,14H313.13c-5.522,0-10,4.478-10,10c0,5.522,4.478,10,10,10h132.04c7.72,0,14,6.28,14,14c0,7.72-6.28,14-14,14    H313.13c-5.522,0-10,4.478-10,10c0,5.522,4.478,10,10,10h110.643c7.72,0,14,6.28,14,14c0,7.72-6.28,14-14,14H313.13    c-5.522,0-10,4.478-10,10c0,5.522,4.478,10,10,10h76.533c7.72,0,14,6.28,14,14C403.662,485.72,397.382,492,389.663,492z     M491.994,492h-0.001h-71.359c1.939-4.273,3.028-9.01,3.028-14s-1.089-9.727-3.028-14h3.139c18.747,0,33.999-15.252,33.999-33.999    c0-5.468-1.305-10.635-3.609-15.217c14.396-3.954,25.005-17.149,25.005-32.782c0-7.584-2.498-14.595-6.711-20.255V235.007    c0-5.522-4.478-10-10-10c-5.522,0-10,4.478-10,10v113.792c-2.35-0.515-4.787-0.795-7.289-0.795h-0.328    c1.939-4.273,3.028-9.01,3.028-14c0-18.748-15.252-33.999-33.999-33.999h-16.075c17.069-7.32,29.057-24.286,29.057-44.005    c0-26.389-21.468-47.858-47.857-47.858c-26.388,0-47.857,21.469-47.857,47.858c0,19.719,11.989,36.685,29.057,44.005h-54.663    V109.863c17.864-3.893,31.96-17.988,35.852-35.853h75.221c3.892,17.865,17.988,31.96,35.852,35.853v31.09c0,5.522,4.478,10,10,10    s10-4.478,10-10v-40.018c0-5.522-4.478-10-10-10c-14.847,0-26.924-12.079-26.924-26.925c0-5.522-4.478-10-10-10h-93.076    c-5.522,0-10,4.478-10,10c0,14.847-12.078,26.925-26.924,26.925c-5.522,0-10,4.478-10,10v199.069H266V20.011L491.994,20V492z     M378.996,283.858c-15.361,0-27.857-12.497-27.857-27.857s12.497-27.858,27.857-27.858S406.853,240.64,406.853,256    S394.357,283.858,378.996,283.858z"></path>
			</g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			</svg>
			              <h3 class="list-group-item-heading txt-wite">Envíos de Dinero</h3>
			              <p class="list-group-item-text">Es muy Fácil, además contamos con el respaldo de nuestro equipo de personas que atienden las operaciones desde el inicio de la solicitud hasta que el dinero llega a tus familiares.</p>
			              <br>
			              <button type="button" class="btn btn-default">Iniciar Operación</button>
			            </a>
			            
			          </div>
			          </center>
			        </div>
		        <?php } ?>


		    	<?php if ($modules_operation_pais->criptomonedas==1){  ?>
			    	<div class="col-sm-4">
			          <div class="list-group">
			            <a href="#" class="list-group-item success">
			              <h3 class="list-group-item-heading txt-wite">Intercambio de Criptomonedas</h3>
			              <p class="list-group-item-text">Para poder comenzar las operaciones de cambiarias debes terminar de registrar almenos una cuenta bancaria.</p>
			              <br>
			              <button type="button" class="btn btn-default">Iniciar Operación</button>
			            </a>
			            
			          </div>
			        </div>
			     </div>
				<?php } ?>
			<?php } ?>
	    <div class="col-sm-2"> </div>

 		<?php }   ?>



</div>

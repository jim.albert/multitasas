<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'language'=>'es',
    //'timeZone' => 'America/Lima',
    'sourceLanguage'=>'en',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        /* 'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ], */
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'i18n' => [

            'translations' => [

                'app' => [

                    'class' => 'yii\i18n\PhpMessageSource',

                    'basePath' => '@common/messages',

                    'sourceLanguage' => 'en-US',

                ],
                 'yii' => [

                    'class' => 'yii\i18n\PhpMessageSource',

                    'basePath' => '@common/messages',

                    'sourceLanguage' => 'en-US',

                ],                

             ],

        ],
        
        // 'urlManager' => [
        //     'enablePrettyUrl' => true,
        //     'showScriptName' => false,
        //     'rules' => [
        //     ],
        // ],
        
    ],
    'modules' => [
        'user' => [
            // following line will restrict access to admin controller from frontend application
            'as frontend' => 'dektrium\user\filters\FrontendFilter',
        ],
        'cambiosdivisas' => [
            'class' => 'frontend\modules\cambiosdivisas\Module',
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module',
            // enter optional module parameters below - only if you need to  
            // use your own export download action or custom translation 
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => [
                    // 'class' => 'yii\i18n\PhpMessageSource',
                    // 'basePath' => '@kvgrid/messages',
                    // 'forceTranslation' => true]
        ],
        'support' => [
        'class' => 'akiraz2\support\Module',
        'userModel' => 'common\models\User',
        'yii2basictemplate' => false,
        'adminMatchCallback' => false,//false - for frontend, true - for backend
        'hashGenerator' => null,//user function for generation unique id for ticket or null for standart generator (The ticket id will be something like this: lkLHOoIho)
    ],
    ],
    'params' => $params,
];

[![Yii2](https://img.shields.io/badge/required-Yii2_v2.0.35-blue.svg)](https://packagist.org/packages/yiisoft/yii2)
[![Downloads](https://img.shields.io/packagist/dt/wdmg/yii2-helpers.svg)](https://packagist.org/packages/wdmg/yii2-helpers)
[![Packagist Version](https://img.shields.io/packagist/v/wdmg/yii2-helpers.svg)](https://packagist.org/packages/wdmg/yii2-helpers)
![Progress](https://img.shields.io/badge/progress-ready_to_use-green.svg)
[![GitHub license](https://img.shields.io/github/license/wdmg/yii2-helpers.svg)](https://github.com/wdmg/yii2-helpers/blob/master/LICENSE)

<img src="./docs/images/yii2-helpers.png" width="100%" alt="Custom helpers for Yii2" />

# Yii2 Helpers
Custom helpers for Yii2

# Requirements 
* PHP 5.6 or higher
* Yii2 v.2.0.35 and newest

# Installation
To install the helpers, run the following command in the console:

`$ composer require "wdmg/yii2-helpers"`

# Status and version [ready to use]
* v.1.4.1 - Added string detection methods to `StringHelper`
* v.1.4.0 - Added `KeyboardLayoutHelper` and `DbSchemaTrait` for migrations
* v.1.3.6 - Fixed CIDR methods. Added IPv6 methods in `IpAddressHelper`
* v.1.3.5 - IpAddressHelper fixed
* v.1.3.4 - Added IpAddressHelper
* v.1.3.3 - Added buildTree() method to ArrayHelper
* v.1.3.2 - Up to date dependencies
* v.1.3.1 - Added crossMerging() method to ArrayHelper
* v.1.3.0 - Added TextAnalyzer helper
* v.1.2.2 - Added exportCSV, importCSV methods to ArrayHelper
* v.1.2.1 - Added stripTags() method for StringHelper